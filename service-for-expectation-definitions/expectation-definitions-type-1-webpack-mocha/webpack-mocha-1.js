
var nodeExternals = require('webpack-node-externals')

// const VueLoaderPlugin = require('vue-loader/lib/plugin')

const commonSharedFile1WebpackSharedFile1 = require('../../service-for-simple-algebra/simple-algebra-type-1-webpack-service/service-for-common-shared-files/common-shared-file-1-webpack-shared-file-1')

// const webpack = require("webpack");
// const path = require("path");
// const fs = require("fs");

var config = {
  /*
    mocha-webpack will set entry/output options at runtime so we don't need to set them here
  entry:
  output: */
  ...commonSharedFile1WebpackSharedFile1,
  mode: 'development',
  target: 'node',
  externals: [nodeExternals()]
}

module.exports = config

