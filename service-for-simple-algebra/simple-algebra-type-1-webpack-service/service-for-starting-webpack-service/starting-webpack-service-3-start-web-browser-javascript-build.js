

const webpack = require('webpack')
const commonSharedFile2OnWebpackCompleteRun = require('../service-for-common-shared-files/common-shared-file-2-on-webpack-complete-run')

const webpackServiceType3BuildForWebBrowserJavaScript = require('../webpack-service-type-3-build-for-web-browser-javascript')

// Start Web Browser JavaScript Build

createWebpackDistributionFile().catch((errorMessage) => {
  console.log(errorMessage)
})



// Algorithms

async function createWebpackDistributionFile () {
  let webpackConfigurationFileObject = {}

  // Select config
  switch(process.argv[2]) {
    case 'production': 
      // webpackConfigurationFileObject = webpackNodeProductionConfig
      break
    case 'development':
      webpackConfigurationFileObject = webpackServiceType3BuildForWebBrowserJavaScript.webpackWebBrowserConfiguration
      break
    default:
      webpackConfigurationFileObject = webpackServiceType3BuildForWebBrowserJavaScript.webpackWebBrowserConfiguration
  }

  const compiler = webpack(webpackConfigurationFileObject)
  
  compiler.run(commonSharedFile2OnWebpackCompleteRun.onWebpackCompilerCompleteRun)
}


module.exports = {
  createWebpackDistributionFile
}