

const webpack = require('webpack')
const commonSharedFile2OnWebpackCompleteRun = require('../service-for-common-shared-files/common-shared-file-2-on-webpack-complete-run')

const webpackServiceType2BuildForNodejsJavaScript = require('../webpack-service-type-2-build-for-nodejs-javascript')

// Start Nodejs JavaScript Build

createWebpackDistributionFile().catch((errorMessage) => {
  console.log(errorMessage)
})



// Algorithms

async function createWebpackDistributionFile () {
  let webpackConfigurationFileObject = {}

  // Select config
  switch(process.argv[2]) {
    case 'production': 
      // webpackConfigurationFileObject = webpackNodeProductionConfig
      break
    case 'development':
      webpackConfigurationFileObject = webpackServiceType2BuildForNodejsJavaScript.webpackNodejsConfiguration
      break
    default:
      webpackConfigurationFileObject = webpackServiceType2BuildForNodejsJavaScript.webpackNodejsConfiguration
  }

  const compiler = webpack(webpackConfigurationFileObject)
  
  compiler.run(commonSharedFile2OnWebpackCompleteRun.onWebpackCompilerCompleteRun)
}


module.exports = {
  createWebpackDistributionFile
}



