

const webpack = require('webpack')
const commonSharedFile2OnWebpackCompleteRun = require('../../../service-for-common-shared-files/common-shared-file-2-on-webpack-complete-run')

const webpackConfiguration = require('../../../service-for-daemon/service-for-daemon-1/daemon-1-1-build-starting-initialize-daemon-1').webpackConfiguration

// Start JavaScript Build

createWebpackDistributionFile().catch((errorMessage) => {
  console.log(errorMessage)
})


// Algorithms

async function createWebpackDistributionFile () {
  let webpackConfigurationFileObject = {}

  // Select config
  switch(process.argv[2]) {
    case 'production': 
      // webpackConfigurationFileObject = webpackNodeProductionConfig
      break
    case 'development':
      webpackConfigurationFileObject = webpackConfiguration
      break
    default:
      webpackConfigurationFileObject = webpackConfiguration
  }

  const compiler = webpack(webpackConfigurationFileObject)
  
  compiler.run(commonSharedFile2OnWebpackCompleteRun.onWebpackCompilerCompleteRun)
}


module.exports = {
  createWebpackDistributionFile
}


