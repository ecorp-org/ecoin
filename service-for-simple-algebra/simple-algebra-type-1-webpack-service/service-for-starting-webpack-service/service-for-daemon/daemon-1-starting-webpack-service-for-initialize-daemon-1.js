

const webpack = require('webpack')
const commonSharedFile2OnWebpackCompleteRun = require('../../service-for-common-shared-files/common-shared-file-2-on-webpack-complete-run')

const daemon1BuildInitializeDaemon1WebpackConfiguration = require('../../service-for-daemon/daemon-1-build-initialize-daemon-1')

// Start JavaScript Build

createWebpackDistributionFile().catch((errorMessage) => {
  console.log(errorMessage)
})


// Algorithms

async function createWebpackDistributionFile () {
  let webpackConfigurationFileObject = {}

  // Select config
  switch(process.argv[2]) {
    case 'production': 
      // webpackConfigurationFileObject = webpackNodeProductionConfig
      break
    case 'development':
      webpackConfigurationFileObject = daemon1BuildInitializeDaemon1WebpackConfiguration.webpackConfiguration
      break
    default:
      webpackConfigurationFileObject = daemon1BuildInitializeDaemon1WebpackConfiguration.webpackConfiguration
  }

  const compiler = webpack(webpackConfigurationFileObject)
  
  compiler.run(commonSharedFile2OnWebpackCompleteRun.onWebpackCompilerCompleteRun)
}


module.exports = {
  createWebpackDistributionFile
}

