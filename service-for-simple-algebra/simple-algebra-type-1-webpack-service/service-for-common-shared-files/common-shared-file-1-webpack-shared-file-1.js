
// const path = require('path')

// const nodeExternals = require('webpack-node-externals')

const VueLoaderPlugin = require('vue-loader/lib/plugin')



const webpackCommonConfig = {
  mode: 'development',
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'ts-loader',
      exclude: /node_modules/,
      options: {
        appendTsSuffixTo: [/\.vue$/]
      }
    }, {
      test: /\.html$/,
      loader: 'html-loader?exportAsEs6Default',
      exclude: /node_modules/
    }, {
      test: /\.vue$/,
      loader: 'vue-loader',
      exclude: /node_modules/,
      options: {
        esModule: true
      }
    }, {
      test: /\.css$/,
      // loader: 'css-loader'
      use: [
        'vue-style-loader',
        'css-loader'
      ]
    }, {
      test   : /\.(ttf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/,
      // loader : 'file-loader'
      use: [{
        loader: 'url-loader',
      } /*{
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }*/]
    }, {
      // test: /\.json$/,
      // loader: 'json-loader'
      // }, {
      test: /\.(jp(e?)g|png|svg|gif)$/,
      use: [{
        loader: 'url-loader',
        options: {
          // The `mimetype` and `encoding` arguments will be obtained from your options
          // The `resourcePath` argument is path to file.
          generator: (content, mimetype, encoding, resourcePath) => {
            if (/\.html$/i.test(resourcePath)) {
              return `data:${mimetype},${content.toString()}`;
            }

            const something = `data:${mimetype}${
              encoding ? `;${encoding}` : ''
            },${content.toString(encoding)}`

            return something
          },
        }
      }]
    }, {
      test: /\.scss$/,
      // loader: 'sass-loader',
      use: [
        'vue-style-loader',
        'css-loader',
        'sass-loader'
      ]
      // use: [
      //   'vue-style-loader',
      //   'css-loader',
      //   'scss-loader'
      // ]
    }, {
      test: /\.node$/,
      use: [{
        loader: 'node-loader'
      }]
    }]
  },
  externals: [
    // nodeExternals(),
    'firebase-admin'
  ],
  resolve: {
    // mainFields: ["main", "module", "browser"],
    extensions: [ '.tsx', '.ts', '.js', '.json', '.vue', '.css', '.jpg', '.png', '.node'],
    alias: {
      // 'vue$': path.resolve('precompiled/consumer/web-browser/data-structures/project-service-website/node_modules/vue/dist/vue.esm.js'),
      // 'vue$': 'vue/dist/vue.esm.js', // 'vue/dist/vue.common.js' for webpack 1
      // 'precompiled': path.resolve(__dirname, '../precompiled'),
      // '@': path.resolve(__dirname, '../../../precompiled/consumer/web-browser/data-structures/project-service-website/src') // ,
      // ['firebase/app']: path.resolve(__dirname, 'node_modules/firebase/app/dist/index.cjs.js'),
      // ['firebase']: path.resolve(__dirname, 'node_modules/firebase/dist/index.node.cjs.js') // ,
      // ['firebase-admin']: path.resolve(__dirname, 'node_modules/firebase-admin/lib/index.js')
    }
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  devtool: false
}

module.exports = webpackCommonConfig



