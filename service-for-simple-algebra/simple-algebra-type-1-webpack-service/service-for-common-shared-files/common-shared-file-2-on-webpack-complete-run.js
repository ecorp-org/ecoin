

function onWebpackCompilerCompleteRun (errorMessage, stats) {
  if (errorMessage || stats.hasErrors()) {
    console.error(errorMessage)
  }
  console.log(stats.toString({
    chunks: false,  // Makes the build much quieter
    colors: true    // Shows colors in the console
  }))
}

module.exports = {
  onWebpackCompilerCompleteRun
}



