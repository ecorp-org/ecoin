

const path = require('path')

const webpackConfiguration = require('../daemon-1-build-initialize-daemon-1').webpackConfiguration




const entryFilePath = `./service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-1-algorithms/algorithms-type-5-initialize-daemon/service-for-starting-initialize-daemon-1/starting-initialize-daemon-1-1.ts`

const distributionFileType1DirectoryFilePath = `./service-for-game-manifold/game-manifold-type-1-distribution-file/distribution-file-type-1-javascript-based-files/service-for-daemon/daemon-1`
const startDaemon1FileName = `start-daemon-1.js`

webpackConfiguration.entry.javascript = path.resolve(entryFilePath)

webpackConfiguration.output.filename = startDaemon1FileName
webpackConfiguration.output.path = path.resolve(distributionFileType1DirectoryFilePath)


module.exports = {
  webpackConfiguration
}

