

const path = require('path')

const commonSharedFile1WebpackSharedFile1 = require('../service-for-common-shared-files/common-shared-file-1-webpack-shared-file-1.js')


const initializeDaemon1 = `./service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-1-algorithms/algorithms-type-5-initialize-daemon/initialize-daemon-1.ts`

const distributionFileType1DirectoryFilePath = `./service-for-game-manifold/game-manifold-type-1-distribution-file/distribution-file-type-1-javascript-based-files/service-for-daemon/daemon-1`
const daemon1FileName = `daemon-1.js`


const webpackConfiguration = {
  ...commonSharedFile1WebpackSharedFile1,
  target: 'node',
  mode: 'development',
  node: {
    __dirname: false,
  },
  entry: {
    javascript: path.resolve(initializeDaemon1),
  },
  output: {
    filename: daemon1FileName,
    libraryTarget: 'umd',
    path: path.resolve(distributionFileType1DirectoryFilePath)
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'ts-loader',
      exclude: /node_modules/,
      options: {
        compilerOptions: {
          noEmit: false // ,
          // outDir: path.resolve(outputDirectoryPath)
        },
        appendTsSuffixTo: [/\.vue$/]
      }
    }, ...commonSharedFile1WebpackSharedFile1.module.rules.slice(1)]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json', '.vue', '.css', '.jpg', '.png', '.node']
  },
  externals: ['firebase-admin', 'firebase']
}


module.exports = {
  webpackConfiguration
}

