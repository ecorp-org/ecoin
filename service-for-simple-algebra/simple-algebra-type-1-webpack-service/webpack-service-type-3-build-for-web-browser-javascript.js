



const path = require('path')

const commonSharedFile1WebpackSharedFile1 = require('./service-for-common-shared-files/common-shared-file-1-webpack-shared-file-1.js')

// const initialWorkingProgramType1 = `./service-for-game-program/game-program-type-1-initial-working-program/initial-working-program-type-1-import-program-successfully/import-program-successfully-type-1-webpack-import.ts`
const initialWorkingProgramType2 = `./service-for-game-program/game-program-type-1-initial-working-program/initial-working-program-type-1-import-program-successfully/import-program-successfully-type-2-original-codebase-import.ts`


const distributionFileType1DirectoryFilePath = `./service-for-game-manifold/game-manifold-type-1-distribution-file/distribution-file-type-1-javascript-based-files`
const javaScriptBasedFile3WebBrowserJavaScript = `javascript-based-file-3-web-browser-javascript.js`


const webpackWebBrowserConfiguration = {
  ...commonSharedFile1WebpackSharedFile1,
  target: 'web',
  mode: 'development',
  node: {
    __dirname: false,
    global: true,
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  entry: {
    javascript: path.resolve(initialWorkingProgramType2),
  },
  output: {
    filename: javaScriptBasedFile3WebBrowserJavaScript,
    libraryTarget: 'umd',
    path: path.resolve(distributionFileType1DirectoryFilePath)
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'ts-loader',
      exclude: /node_modules/,
      options: {
        compilerOptions: {
          noEmit: false // ,
          // outDir: path.resolve(outputDirectoryPath)
        },
        appendTsSuffixTo: [/\.vue$/]
      }
    }, ...commonSharedFile1WebpackSharedFile1.module.rules.slice(1)]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json', '.vue', '.css', '.jpg', '.png', '.node']
  },
  externals: ['firebase-admin', 'firebase', 'express']
}


module.exports = {
  webpackWebBrowserConfiguration
}



