
echo 'Publishing Customer Website (Development Deploy)'

# Copy Development Environment Variables
# To The Customer Website

# .firebaserc
cp ./service-for-environment-variables/environment-variables-type-2-development-variables/development-variables-type-1-customer-website/customer-website-type-1-firebase-variables/.firebaserc ./service-for-game-introduction/game-introduction-type-1-web-browser-introduction/web-browser-introduction-type-1-customer-website

# firebase.json
cp ./service-for-environment-variables/environment-variables-type-2-development-variables/development-variables-type-1-customer-website/customer-website-type-1-firebase-variables/firebase.json ./service-for-game-introduction/game-introduction-type-1-web-browser-introduction/web-browser-introduction-type-1-customer-website


# Navigate to the 
# Customer Website Directory

cd ./service-for-game-introduction/game-introduction-type-1-web-browser-introduction/web-browser-introduction-type-1-customer-website

# Build The Project

npm run build

# Copy The Enviroment Variables
# To The Distribution Directory

cp .firebaserc ./dist

cp firebase.json ./dist

rm -rf .firebaserc

rm -rf firebase.json

# Deploy To Firebase

cd ./dist

firebase deploy --token "$FIREBASE_TOKEN"

