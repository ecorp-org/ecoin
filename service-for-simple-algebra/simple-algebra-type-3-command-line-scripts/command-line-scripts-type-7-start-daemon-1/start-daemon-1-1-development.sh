


# Set Environment Variables
# For Development Environment

if [ -n "$FIREBASE_ADMIN_PRIVATE_KEY" ]; then
  echo ""
else
  FIREBASE_ADMIN_PRIVATE_KEY=$(cat ./service-for-secret-variables/secret-variables-type-1-firebase/ecoin369-firebase-adminsdk-bwhbg-834695107d.json)
fi

if [ -n "$GENERAL_SECRET_VARIABLES" ]; then
  echo ""
else
  GENERAL_SECRET_VARIABLES=$(cat ./service-for-secret-variables/secret-variables-type-3-general/general-1.json)
fi


export GENERAL_SECRET_VARIABLES=$GENERAL_SECRET_VARIABLES

export FIREBASE_ADMIN_PRIVATE_KEY=$FIREBASE_ADMIN_PRIVATE_KEY
export FILE_DISTRIBUTION_TYPE='development'
export FIREBASE_AUTH_EMULATOR_HOST='localhost:9099'



# Start Daemon

node ./service-for-game-manifold/game-manifold-type-1-distribution-file/distribution-file-type-1-javascript-based-files/service-for-daemon/daemon-1/start-daemon-1.js



