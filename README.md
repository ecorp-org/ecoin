<h1>
  <a href="https://gitlab.com/ecorp-org/ecoin"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/15242461/android-chrome-512x512.png?width=64" alt="Ecoin" width="35"></a>
  Ecoin
</h1>

[Table of Contents](#table-of-contents-a-to-e)

## Community Development Team
[Table of Contents](#table-of-contents-a-to-e) | 1 Company Listed

| **Company Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> |
-- | -- |
| **Company Name** | E Corp ([@ecorp-org](https://gitlab.com/ecorp-org)) |

## Minimum Description
[Table of Contents](#table-of-contents-a-to-e) | 1 Item of Interest

A `data structure for` **creating** `digital currency`, **sending** digital currency and **receiving** digital currency.


## Median Description
[Table of Contents](#table-of-contents-a-to-e) | 1 to 5 Items of Interest

A `data structure for` **creating** `digital currency`, **sending** digital currency and **receiving** digital currency from digital currency accounts.

A `product resource center` is available to buy and sell resources such as houses, furniture, food and apartment spaces.

A `network recycling center` is available for adding additional support for 3rd party network protocols such as Ethereum, Bitcoin or other digital currency or non-digital currency network protocol solutions.

A `job employment center` is available for creating jobs, applying for jobs and being accepted or rejected for a particular job.

A `digital currency exchange` is available for exchanging the native digital currency such as Ecoin for a platform agnostic digital currency such as US dollars, Russian Rubles, Chinese Yuan and other Native-Earth-based currencies of interest that are supported by the Stripe or PayPal exchange services.

## Maximum Description
[Table of Contents](#table-of-contents-a-to-e) | 1 to 10 Items of Interest

<details>
<summary>(1) A Note About Digital Currencies</summary>

### (1) A Note About Digital Currencies

**Part I:**

What do you believe a digital currency is? Do you believe a gun is a digital currency? Do you believe a weapon like a baton is a digital currency? What do you think Chinese people wear to school every day? Do you believe that Chinese people wear clothes?

What do you think about people who wear hats?

What do you think about people who wear clothes to school without a jacket on?

What do you think a necktie is?

A necktie is a digital currency with a certain social reward credit like 'you are smart' associated to it for a common English person.

If you are in disagreement of this statement, you are possibly also very smart.

If you are in agreement that statements written on the internet are also like digital currencies then you are also correct. If you believe you're correct. If you believe. If. . . .

Thank you for believing because I'm trying to make a simple point clearer and clearer with more and more interesting thoughts to be juggled by skeptical readers.

Skeptical readers need to understand that this program of reading a line of text is like a computer program with 'warnings' and 'watch out' and 'impact points' like 'wow, that was impactful' and all of these things can be interpretted or processed as a money system or a reward system for various other purposes like 'maybe I should read on' or 'maybe I should stop reading since this part of the text is really starting to offend me terribly so'. If you buy a pair of socks that smell weird, then your digital currency or your native fiate currency is also at risk of being interpretted as a 'bad smelly coin' or a 'equivalent smelly bad use of the coin' or someone might say that you are a 'bad spender' or that 'you don't spend your money appropriately'.

You are an interesting person so all of these things are not necessarily the case. These are personal observations from Jon Ide the author of this note to realize that a lot of information evne on the internet is enabling the possible ability of 'digital currency' without needing to necessarily associate numbers or number signs or dollars or dollar signs or currency or currency symbols to the text or image or video or interactive content of interest.

If you enjoy reading this note on what a digital currency is, then I thank you very much.

I hope you have a good day.

Digital currencies are things that '`you believe in`'

Do you believe this product statement was useful? Then keep sending it to your friends and family to see if they also believe in using that currency as a common medium of exchange for goods and services like 'kindness' and 'hospitality' and other enjoyable social things like a minimum wage.

**Part II:**

To be clear, a `database` that is built using a traditional so-called 'digital computer' is what is meant by the term 'digital currency'

A digital computer is a device that stores information in terms of 0's and 1's which can be used to represent other numbers like 101 which is 5 in binary-to-decimal form.

If you have a digital computer then you can store numbers that can be used to represent 'currency' or 'the amount of something that you have'

A currency can be representative of '`how much of that thing of interest do you have?`'

'how many curves on the area chart?'

'how many bars on the bar graph?'

'how many syllables in the word 'appalachian'?'

'how many coins of gold are in your pocket?'

'how many coins of silver that weigh a certain amount such as 5 grams or 10 grams or 16 grams do you have in your pocket?'

'do you hike during the winter?'

. . 

A lot of answers can be represented by numbers such as 1, 2, 3, 4, 5, etc. . But if you don't know how to represent an answer is a number, you can create a counting scheme like ASCII (American Standard Code for Information Exchange)

[ASCII](https://en.wikipedia.org/wiki/ASCII) allows you to represent information that would possibly otherwise be difficult to characterize or to interpret as a reasonable response.

'Yes'

'Yes' in ASCII can be represented as

Y: 89 <br>
e: 101 <br>
s: 115 <br>

89 101 115

Which is a ASCII to Decimal Representation.

. . 

If you need a tutorial on digital currencies, I would recommend using Google and YouTube and Wikipedia and other online internet resources to learn more :)

</details>

<details>
<summary>(2) A Note About Digital Currency Exchanges</summary>

### (2) A Note About Digital Currency Exchanges

A digital currency exchange allows users to trade resources with one another in exchange for the digital currency of their interest.

If you have 10 Ecoin, you can trade that to someone for 10 Bitcoin or for 10 US Dollars or for 10 Ethereum or for 10 Interesting Ideas.

If your digital currency exchange does not allow trading of resources where 1 individual receives a result of interest and the second receiving individual doesn't receive the introduced currency of interest, then the currency exchange is called 'mononuclear'

`Mononuclear` means there is a nucleus of interest such as (1) receiving a currency from an individual of interest such as a third party trade sender or third party trade creator or the individual that introduced the trade for one currency for another. (2) Only one nucleus successfully applying the action of interest would then suppose that a second nucleus (`binuclear` or `bi-nuclear`) may also need to be involved to bi-couple the nuclear membrane of a successful transaction or a successful trade that leaves both parties with the intended original trade agreement proposal kept in tact. If there are more parties to consider, a `tri-nuclear` or `quad-nuclear` or `N-Ary-Nuclear` membrane development needs to be considered.

`Mononuclearity` means there is only one individual that receives their item of interest and the second individual is not necessarily receiving their item of interest.

A trade is hypocritical if one person is receiving an item of interest and the other person or individual or company or factory or factoid or carrying capacity item of interest is still being fooled around with.

⚠️ If you **catch a misbehaving party** trading mononuclearity or mononuclearly in the Ecoin project presented here at this particular instantiation of the project, [please don't be afraid to reach out to the staff of the project](https://gitlab.com/ecorp-org/ecoin/-/issues) and determine if your resource can be included in a bi-nuclear crash correction scheme that involves further additive things like 'exchanging your item for a similar to representatively related item from the product resource center or another resource center of your choice'

</details>

<details>
<summary>(3) A Note About Product Resource Centers</summary>

### (3) A Note About Product Resource Centers

A product resource center is a big religious emblem of a social structure that relates to interesting ideas of interest.

Big religions like 'I really enjoy sunlight' and 'I really enjoy life' are really interesting religions to be involved in. On the one hand, these things are beautiful, big, bold and super stupendous in their scale and reach. There are so many beautiful and joyful people that enjoy walking outside and experiencing the rays of the sun blistering hot on their skin as they walk by any apartment complex. If the apartment complex is reasonable in size, then the person is going to wonder to themselves 'Geeze, I really love apartment complexes.' 'Geeze, I really love, love' 'Geeze, I really love, love, love, love' 'Geeze, I really love, x-ary-available love boxes'

If you are interested in a religion, you may find yourself out of luck to find that it is hot ready and sold for the very first buyer who says 'I would love that item' 'I want to pay the world for that item' 'no takesie-backsies' 'no take backs' 'no returns' 'no rewards' 'no concerns' 'no rewinds' 'no re-realizations' And then. Aliens. Aliens re-discover the original slide show for that sweet juicy peace of item that was listed on the price because aliens are always searching for new ways to re-create things >.<

Well. If you find this item as an interesting religious emblem, you can also say, 'I want that' 'Was I the original one to create that in my own version of reality?' 'what was the reality like before I owned that for myself?' 'creepy eyes like mine are always interested in creepy things like that' 'I like that'

'I like that' a religion for all of us.

A product resource center lets you see what you want and then you can say 'I like that'

If you read books, you can say 'I like that' and so then the 'book' is a product resource center. The book is a product resource center for 'ahh, that was a beautiful or that was an amazing or that was a likewise re-accessible or like-wise I-like-that-accessible thought that I would have liked because I remember reading those thoughts'

If you are from Earth planet '123' and your shopping mall is called 'XYZ' then you are a person that possibly shops for things that you like at 'XYZ'

Do you like '`Amazon`'? <br>
Do you like '`Alibaba`'? <br>
Do you like '`Shopify`'? <br>
Do you like '`Etsy`'? <br>
Do you like '`Ebay`'? <br>

Do you like websites? <br>
Do you like sports? <br>
Do you like religion? <br>

A product resource center is a general term meant to mean '`you can get what you like from that place of interest`' or '`you can get what you like`'

</details>

<details>
<summary>(4) A Note About Network Recycling Centers</summary>

### (4) A Note About Network Recycling Centers

A network recycling center is a general term meant to mean 'do you really care where this item is going or where it's from?'

Maybe you don't care but it's like 'if you do care, then that's a big deal not to share it with anyone'

Sharing your contact information is very interesting. In theory if you don't need a phone number, and you have a house address, someone can find you and reach out to your for promotional information like interesting items of interest like a car or a bicycle or a road or a train track or an underground idea.

If you are selling underground ideas to people, you may not care if those ideas are sold in a public arena or over the fence or over the fence in a public arena, you may only care that the person receives the message and that if encryption is involved, you can say 'yea, the message is scrambled so only the recipient can read it with a private key or something like that'

If a private key is involved in message scrambling or message encryption . . all you need is a third party to help you recover that secret private key if you ever lose it. What better to have a friend that's not even on the network that you're on to provide you the secret private key that you lost? If your network recycling center codebase is written properly, you won't have to re-create the network protocol or the network protocol codebase for the respective codebase that you would like to integrate with.

</details>

<details>
<summary>(5) Digital Currencies as A Common Medium of Exchange</summary>

### (5) Digital Currencies as A Common Medium of Exchange

Digital currencies are a nice label to give yourself if you are a human being that enjoys being used for interesting ideas.

Digital currencies are interesting to think about. If you have a job in society, you can acknowledge that you are a digital currency for certain things being achieved. If those things are being achieved through you then that is very interesting to say that only your particular currency type can satisfy that particular idea. If a particular idea is very tastey and very yummy like why isn't there a song about that particular topic in that particular way? Well that would mean that everyone in society should be a digital currency. If you keep yourself alive in everybody's wallets here on Planet Earth or elsewhere that you visit, why then would there be trouble to see that your coin is not O_O recycled in the cosmos.

A `non-fungible` token is a related 'unique' asset that is not interchangeable for another similar related token or asset.

A block of gold is interchangeable for a block silver but their values of elementary chemical composition is perhaps unique and so a block of gold is perhaps more fairly exchanged with another item that is also like a block of gold but is perhaps shaped differently or weighs differently or something like that.

If you have a common medium of exchange, then you can say 'universal basic income' for 'physical and personal goods of interest'

All the items can be added to the list of resources that can be purchased if your common medium of exchange gives you that authority. 'Quit putting a godda** dollar sign on every fu**ing thing on this planet' was a quote provided for by '[Bill Hicks](https://www.theguardian.com/stage/2015/nov/26/bill-hicks-dead-funny)' which was meant to mean 'you can always add more and more things to the list of things that you can buy with a 'common medium of exchange' like a US Dollar or a Russian Ruble or Gold or Silver or any other common medium of exchange' and so the idea is that 'a common medium of exchange' is also 'a universal basic income' because it will provide you arbitrarily more resources as you need them because for example you can say 'I would like to buy the sun' or 'I would like to buy the Universe' or 'I would like to buy the answer to 'why?'' and so if you are a person that enforces patents or laws or restrictions on the things that you are wanting to exchange with people that use or apply your common medium of exchange with the help and support of lovely military doctors who whiplash you with a nice gun or whiplash you wish a nice lawsuit because you have broken international trading laws or because you have broken or not obliged with local laws for a particular area of interest such as a small town from ABC-ville and so everyone has to say that ABC-ville entrepreneurs are also manditorily recommended to be superior in selling the answer to 'why?' and so there is blah-blah-blah-blah consequences involved in blah-blah-blah.

Blah-blah-blah.

</details>

<details>
<summary>(6) Product Resource Centers As A Gift Shopping Service For Energies Of Interest</summary>

### (6) Product Resource Centers As A Gift Shopping Service For Energies Of Interest

Product resource centers are interesting to think about. You can name a product resource center anything you would like. I am not sure if the energy of the product resource center of your interest is available. I only think that possibly it is easy to communicate about it since you are someone who is interested in archiving information with neighborhood search reliance tickets like '`where are you from?`' and '`how do I find you again?`'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

'How do I find you again?'

`I remember, it was you again.`

This is an old addage machine about life on Earth and doesn't necessarily apply to computer systems outside of Earth. You are me and I am you is only an old addage machine computer language. We are free.

I don't know

</details>

<details>
<summary>(7) Network Recycling Centers For Interesting Ideas Being Recycled On The Same Network For Effective Dual-Integration</summary>

### (7) Network Recycling Centers For Interesting Ideas Being Recycled On The Same Network For Effective Dual-Integration

**Part I:**

I don't know. A network recycling center is interesting. '`Network Protocol Connection`' '`Network Protocol Member`' '`Network Protocol Message`' These are the items of interest in a network protocol that I have been subscribed to for a few months now during my developmental research effort. If you find more items of interest, please let me know.

A Network Protocol Connection is interesting to say 'when, where, how, who, with what, how many, what for?' A lot of questions are asked, you don't even know what it's about. A connection can be about anything. If it is a simple webrtc connection, you may need a single-use signal instruction set like a string value like

```javascript
let webRTCDataSignal = 'v=0\n o=- 8729769264156733154 3 IN IP4 127.0.0.1\n s=-\n t=0 0\n a=extmap-allow-mixed\n a=msid-semantic: WMS\n'
```

And if your connection is an http web server then maybe your string is as simple as

```javascript
let httpWebAddress = 'https://website.com'
```

And if you have a house, you can say your connection can be as simple as

```javascript
let physicalHouseAddress = '123 Apple Street, California United States of America, Planet Earth, Milky Way Galaxy'
```
And if you have a name, you can have your friends connect with you as simple as calling you

```javascript
let personalPrecompiledName = 'Yes, That Is Interesting'
let personalCompiledName = 'Compiled Name Here'
```

And if you have more ways for people to contact you such as 'just raise your left hand and clap 3 times and I will be there' or 'just make a wish, and I will be there' or 'just place a tooth under your bedroom pillow and I will give you money to represent how much that tooth specification is worth' and so many other jolly toys and tricks can be compiled to be really good connection names or really good connection symbols.

`A connection is really amazing`. You can connect one member with another very quickly. Members are also interesting. Member. Member. Member. Member. Member. Member. Member. Member. Member. Member. Member. If you have 11 members, you can say that each of them can form a connection with one another such as 'we are colleagues at work, I know his badge number' or 'we are office partners on the job, I know what they look like when I seem them at work, they're always smiling, woohoo informal information is amazing'

'Informal information is amazing' is a joke it is a lie, it is not informal it is quite formal to see what your friend looks like without typing that in your forehead as a logged item. Just smelling them and acknowledging the particular scent is a memory that forms a strong connection like 'I can't place my finger on that but it's something like . . if I had to guess . . move more to the right . . move more to the left . . yea . . that smell . . maybe that smell . . right . . '

'Informal information is amazing' that is a hypothetical statement. Member. Member. Member. Member. Member. Member. Member. A clock can be a member of a conversation if people point at it and ask what it thinks about the time . . 'Hey, geeze their Mr. Clock. What do you think the time is?' . . 'Ahh geeze fellas, I hate to come at you so quick with all the dollar sign minutes I got stored in my battery compartment tick-tock rings, but geeze what do you think my time says? Can you read me doctor?'

`A Member is interesting`. You can have members be things that you care about that need to form a connection with one another. An HTML page forms a connection with a JavaScript page and so each of these two pages or each of these two file types are Members of the list of Members that can form a modern day website. A modern day website doesn't necessarily need these two members, for example you can have a TXT file or Text File that is an https-accessible website available for people to consume at a web address . . and so that is a very basic idea of what that could possibly mean for a website to be a website . . being . . http- or https- accessible . . and so that is a possible way to talk about what a website is . . and more or less connections may be available that people create in their mind to associate with the topic of websites . . 

`A Message is interesting`. If you are a member, you can communicate with another member by 'sending them a message' . . Communication is a nuanced term . . it is like 'illusion' . . 'illusion' looks like 'light' and 'not light' . . 'light' but . . 'not light' . . and so it's like . . 'yea' . . and . . 'not yea' . . 'I see that . . ' and 'I don't see that' . . and so it's like the illusion of a duck and a rabbit in a photograph . . do you see the duck . . or do you see the rabbit . . can you orient yourself to see either one? . . and so communication is also like that because you still need to say 'I haven't received your message' 'I haven't received your message' 2 Months later from now you can come back and start to say 'oh, now I see a hippopotamus in the photograph, is that what you meant?' 'oh, now I see a llama in the photograph, are you sure you sent me one message in that photograph or did you send me like 200,000 subpartitionable messages?'

'How am I supposed to interpret this?'

An HTML Editor is a nice place to send a 'HTML' file message . . and so if you send a 'MP4' file message instead, maybe you will find it difficult to read the file format . . 

An ant on an ant hill is an interesting compiled programming language and if you can read the precompiled code you can possibly start to control the behavior of the ant but the messages of 'hey, can you move down off the anthill for a little bit?' 'hey, can you tell me if you have any milk in your anthill frigerator system?'

'If I had any milk, I will have antennas that are beaming with sweet aspects like calcium suspects that you can subpartition, right?'

. . 

Partitioning messages is a big scientific endeavor. What does it mean to interpret a message properly?

Why is this message difficult to read?

Why?

When?

How?

What?

Who?

How much?

How many?

Do you care? Are you interested?

That is what a connection is for. A connection allows you to create a new meaning for the message and say that you are creating a connection between particular members of interest. One ant might say there is no milk but another ant may say there is milk.

You can message the ants. You can send them things to think about like books and magazines of milky dialogues. If there is no real connection formed like 'I like this milk' then . . 'please, doctor scientist who keeps experimenting with our milky aspect, why don't you do us a real milky favor and create sweet milk that makes more sense. right? what do you want from us? we are simple ants'

'Connection' 'Member' 'Message'

'Network Protocol Connection' 'Network Protocol Member' 'Network Protocol Message' . . Wow. Well. That takes us a very long way for all the things that we want to think about right? Well. If you enjoyed that Message . . please let me know in the comments description below.

We are ants.

. . 

**Part II:**

`Dual Integration` means we can continue to subpartition a set of interesting ideas with the list of 'Network Protocol Connection' 'Network Protocol Member' and 'Network Protocol Message' and that is a complete set of things we can use to say 'I don't know' and 'I will never know' and 'I will possibly know a lot because wow, you don't need to tell me to stop subpartitioning right?'

'Wow'

</details>

<details>
<summary>(8) Do You Like Free Money? A Universal Basic Income Is Available For You</summary>

### (8) Do You Like Free Money? A Universal Basic Income Is Available For You

**Part I:**

Money is an interesting paradigm. It is like a language. If you can speak a language you can write a story.

Stories are amazing. Money is an amazing story. It says you can write a great deal of stories like who has sex with who and who speaks with whom and why do they speak with someone and why would they think about someone?

If you have all the money on the planet, you need to have other money military lords that have other money types that say you are not allowed to say your money is god.

If your money is god. Then that's a big deal. We want to have money that is interesting like 'you can say you are safe from harming yourself and harming others' 'if rocks with sharp metal pointy ends were traded for money that could be a big deal to throw signs at the possibility that people have cut hands and bruished fingers all the time because it is so dangerous to wield katana blade money systems'

'Katana Blade Money Systems' are like aliens negotiating alien flying craft technology but they always have to give you headaches along the way and so it's a problem. Katana Blade Money Systems are weird if people don't agree to place sheaths on all the blades during a safety precautioned transaction, right? Well that is like you need to keep interesting energies under wraps before they are revealed for later use when you're training with throwing your money around in the backyard at logs and wooden posts for fun because you're so wealthy.

Well. A good story is to say you can have skilled blade that says 'wow, look at all those skilled blades' Look at all those skilled blades . . They are so interesting and so skilled. And all the skill is so cool to witness on its own for free in your private yacth of a reality right \-\_\- right ------------

\-\_\- Alright well eventually you need to say \-\_\-

\-\_\-

Alright

**Part II:**

A Universal Basic Income is a way to encourage more of the money resource to be created during the lifecycle runtime lifeplan of the money system's negotiated existence. When people are brain washed in spirals of 'I agree' then you can have a real money system with trust and loyalty and 'yes, that is very good'

A Universal Basic Income can give people 'houses' 'food' 'children' and a great deal of other things of interest.

If the money is always so available, why shouldn't you be able to buy so many millions of millions of houses?

If the money is always so available, why shouldn't you be able to buy so many hungries of hungries of jewcy gummy fruit snacks?

If the money is always so available, should children be \-\_\-

If there are orders of magnitudes of money available, why shouldn't you throw a party in cyberspace and say that virtual reality technology is amazing and allows you to say 'virtual reality technology is amazing' even while you are inside of the virtual reality technology. Do you believe that aliens have virtual landscape in this mind body complex we are living in? Do you believe they can 3D print planet Earth with a wand?

Do you believe they can ask for permission to purchase more virtual real estate in minds? Do you believe minds make good music machines? Can you memorize so many different stories of what happened to dot dot dot . . that person's name . . dot dot dot and their . . so called ancestors . . dot dot dot . . 

Why . . why . . why . . why are you telling me these things exist?

I don't know . .

**Part III:**

?\-\_\-?

</details>

<details>
<summary>(9) Notes On A Universal Basic Income</summary>

### (9) Notes On A Universal Basic Income

A person is an interesting topic. If a person asks you 'what is a Universal Basic Income?' Will you know what to say in response?

That was rude. That was so rude of you. I am sorry. You are not allowed to continue shopping here. You cannot access the resources available here. I am sorry. You are not allowed to shop here anymore, sir. I am sorry, you are not allowed to shop here any more, ma'am.

You are an interesting person if you are starting to pick up on my message here, it means to say, Universal Basic Income 'just keep coming'

'Just Keep Coming'

'Just Keep Coming'

</details>

<details>
<summary>(10) Notes On A New Social Economy</summary>

### (10) Notes On A New Social Economy

A new social economy. It is very interesting what to say about the development of this project. It has been a demonology project from many dimensions. You would be confused to follow the project from its initial first video of a person who wanted to contribute to the world by developing a simple one-time usable digital currency token that supports a universal basic income.

Research and research and research has been poured into the development of this project. If you are watching the directory structure and noticing that it reminds you of '[the-light-architecture](https://gitlab.com/ecorp-org/the-light-architecture)' then you are very observant and correct that this project '[Ecoin](https://gitlab.com/ecorp-org/ecoin)' is based on various research ideas that had helped to establish thoughts that are now originating in '[the-light-architecture](https://gitlab.com/ecorp-org/the-light-architecture)' and so those thoughts are now applied here and there when needed for useful ideas like `network protocol` and `event listener` and `database` and `permission rule` and `scale matrix amana` and `anana anana anana` and many other ideas like how to name folders and what function names should look like to keep a consistent naming schema.

All of these things are interesting and you can learn more at '[the-light-architecture](https://gitlab.com/ecorp-org/the-light-architecture)' which is a project sponsored and developed at '[E Corp](https://gitlab.com/ecorp-org)' and so don't be shy to forget that it is an interesting company. E Corp was a brand name showcased in the television show '[Mr. Robot](https://en.wikipedia.org/wiki/Mr._Robot)' which means to say 'in a hypothetical probable world, weird things happen like corporations are evil and the evil company is really big and they deal a lot of interesting reports for people to consider such as the reports that are considered by 'Elliot' the computer programmer of the story'

If you like the show '[Mr. Robot](https://en.wikipedia.org/wiki/Mr._Robot)' then you will also think about ideas like that was an interesting thing to do to name your company after a movie company . . a company based in a popular informational graphic . . it's like movies and television shows are like aspects of real life . . look how goofy life could be if you just flip the switch to say . . 'act like this is the world that we live in'

'Act like this is the world that we live in'

'Act like this is the world that we live in'

</details>

## Ideas That Are Recommended For You
[Table of Contents](#table-of-contents-a-to-e) | 1 to 3 Areas of Interest Listed

| **Idea Index Number** | 1 | 2 | 3 |
| -- | -- | -- | -- |
| **Idea Category of Interest** | Publish a Product Resource | Publish a Project Public Resource | Publish a Project Public Resource #2 |
| **Idea Minimum Description** | If you are interested, you can learn more about what it means to publish your own Ecoin Version including all of the public product resources that the Ecoin project provides. | If you are product resource owner, you may be interested in publishing your own product resource environment. | If you are a product resource owner, you may be interested in publishing a different type of product resource environment but ensure that it has a limited capacity of ideas to re-loop over or else the product resource environment might consume more than all the species energy capacity without trailing any left for other entities or individuals to use |

## Projects That Consume This Resource
[Table of Contents](#table-of-contents-a-to-e) | 0 Consumers Listed (and 0 Discovered Since April 23, 2021; <mark>[Let Us Know](https://gitlab.com/ecorp-org/ecoin/-/issues/12) If You Use The <a href="">JavaScript Library</a> To Be Included In This Consumer Listing Resource</mark>)

## Projects That Provide A Related Usecase
[Table of Contents](#table-of-contents-a-to-e) | 17 Providers Listed (and More Tangentially Available With a Search on The Internet)

| **Provider Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> | <a href= "https://github.com/bitcoin" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/528860?s=200&v=4" ></a> | <a href= "https://github.com/ethereum/" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/6250754?s=200&v=4" ></a> | <a href= "https://github.com/EOSIO" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/26932212?s=200&v=4" ></a> | <a href= "https://github.com/input-output-hk" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/12909177?s=200&v=4" ></a> | <a href= "https://github.com/hashgraph/" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/31002956?s=200&v=4" ></a> | <a href= "https://github.com/paypal" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/476675?s=200&v=4" ></a> | <a href= "https://github.com/stripe/" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/856813?s=200&v=4" ></a> | <a href= "https://github.com/CirclesUBI" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/30386623?s=200&v=4" ></a> | <a href= "https://mannabase.com" target="_blank"><img height="36" src="https://mannabase.com/img/logo.c0b699a7.png" ></a> | <a href= "https://github.com/GoodDollar" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/41892903?s=200&v=4" ></a> | <a href= "https://github.com/swiftdemand" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/35290885?s=200&v=4" ></a> | <a href= "https://github.com/amzn" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/8594673?s=200&v=4" ></a> | <a href= "https://github.com/alibaba" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/1961952?s=200&v=4" ></a> | <a href= "https://github.com/Shopify" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/8085?s=200&v=4" ></a> | <a href= "https://github.com/coinbase" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/1885080?s=200&v=4" ></a> | <a href= "https://github.com/gemini" target="_blank"><img height="36" src="https://avatars.githubusercontent.com/u/8368139?s=200&v=4" ></a> |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| **Provider Name** | E Corp ([@ecorp-org](https://gitlab.com/ecorp-org)) | Bitcoin ([@bitcoin](https://github.com/bitcoin)) | Ethereum ([@ethereum](https://github.com/ethereum/)) | EOSIO ([@eosio](https://github.com/EOSIO)) | Input Output HK ([@input-output-hk](https://github.com/input-output-hk)) | Hedera Hashgraph ([@hashgraph](https://github.com/hashgraph/)) | PayPal ([@paypal](https://github.com/paypal)) | Stripe ([@stripe](https://github.com/stripe/)) | Circles UBI ([@CirclesUBI](https://github.com/CirclesUBI)) | Mannabase ([https://mannabase.com](https://mannabase.com)) | GoodDollar ([@GoodDollar](https://github.com/GoodDollar)) | SwiftDemand ([@swiftdemand](https://github.com/swiftdemand)) | Amazon ([@amzn](https://github.com/amzn)) | Alibaba ([@alibaba](https://github.com/alibaba)) | Shopify ([@shopify](https://github.com/Shopify)) | Coinbase ([@coinbase](https://github.com/coinbase)) | Gemini ([@gemini](https://github.com/gemini)) |
| **Provider Product Listing** | (1) [Ecoin](https://gitlab.com/ecorp-org/ecoin) (same usecase) | (1) [Bitcoin](https://en.wikipedia.org/wiki/Bitcoin) (`digital currency`) | (1) [Ethereum](https://en.wikipedia.org/wiki/Ethereum) (`digital currency`) | (1) [EOS](https://en.wikipedia.org/wiki/EOS.IO) (`digital currency`) | (1) [Ada](https://coinmarketcap.com/currencies/cardano) (`digital currency`) | (1) [Hedera Hashgraph (HBar)](https://hedera.com/hbar) (`digital currency`) | (1) [PayPal](https://en.wikipedia.org/wiki/PayPal) (`digital currency`, including physical resource to digital resource tridimensional coupling) | (1) [Stripe](https://en.wikipedia.org/wiki/Stripe_(company)) (`digital currency`, including physical resource to digital resource tridimensional coupling) | (1) [Circles]() (`digital currency`, including Universal Basic Income) | (1) [Mannabase]() (`digital currency`, including Universal Basic Income) | (1) [GoodDollar]() (`digital currency`, including Universal Basic Income) | (1) [SwiftDemand]() (`digital currency`, including Universal Basic Income) | (1) [Amazon]() (`product resource center`) | (1) [Alibaba]() (`product resource center`) | (1) [Shopify]() (`product resource center`) | (1) [Coinbase]() (`product resource center`, including digital currency trading) | (1) [Gemini]() (`product resource center`, including digital currency trading) |

<!--
## What To Expect From This Community Document
[Table of Contents](#table-of-contents-a-to-e)

### Table of Contents (A to E)

- (A) Project Inspiration (1 Area of Interest)
- (B) Project References (5 Areas of Interest)
- (C) Project Guidelines (4 Areas of Interest)
- (D) Project Intended Usecase (4 Areas of Interest)
- (E) Project Accidental Usecase (5 Areas of Interest)

## (A) Project Inspiration
[Table of Contents](#table-of-contents-a-to-e) | 1 to 1 Item of Interest

<details>
<summary>(A.1) The Venus Project</summary>

### (A.1) The Venus Project

`The Venus Project` encouraged the development of the 'product resource center' which is the idea that people could get all the resources they need from a 'resource center' just by ordering those items and not having to pay for them with utilitarian tokens like 'do you have enough money?' . . Instead the proposed alternative would possibly be another type of utilitarian token like . . 'do you agree that you don't need that item in another aspect of society?' 'do you agree that the resources available will not be very instringent on the whole of society or would you like to do another measure of what that means to have enough resources on the planet?'

A Resource Based Economy measures the available resources of a planetary ecosystem and subdivides those resources for people to use according to the measurement carrying quotient which is '`human trust`' and '`human morality`' which are interesting to say 'aliens would help you build the social structure you want if they see that you trust one another and that you are moral or equimoral'

If the population builds computer algorithms or robots to manufacture and produce the resources of interest, then that is a great way to help ensure that there are resources for everyone regardless of the cost.

Artificial intelligence algorithms are coming. They are in the form of Pleiadian spaceships that gift the society with more and more information as they need it.

`You are free.`

</details>

## (B) Project References
[Table of Contents](#table-of-contents-a-to-e) | 1 to 5 Items of Interest

<details>
<summary>(B.1) Project Local Community Recommendation List</summary>

### (B.1) Project Local Community Recommendation List

1 to 3 Items of Interest

### (B.1.1) Recommendation List If Your Project Needs Energies

If You are looking for a good time to really showcase something fantastic that no one has ever thought of before, you are recommended to use this list

0 Items Listed

### (B.1.2) Recommendation List If Your Project Needs Restrictive Energies

If you would like to showcase something related to how you've pondered a specific thought and thought it would be real to show where that thought is being referenced, have an item for that here

1 Item Listed

<details>
<summary>I think a digital currency with a universal basic income could help support the livelihoods of poor underprivileged people across the world who would otherwise need to spend a lot of time thinking about how ponzi schemes work</summary>

**I think a digital currency with a universal basic income could help support the livelihoods of poor underprivileged people across the world who would otherwise need to spend a lot of time thinking about how ponzi schemes work**

I was thinking about doing something interesting and then all of a sudden I realized that I could do that interesting thing on another program application file. I thought to call that program application file `Ecoin` after the `Ecoin` money label or money title in the popular HBO television show `Mr. Robot`.

Ecoin was a chance to also teach people how to program computers by live streaming the development series.

If it wasn't for the teaching, maybe I wouldn't have come as far as I have because I have received a lot of help from energies that I didn't know about.

The project is very interesting and the codebase could also be interesting. I hope it is interesting to say that all of the people on the planet no longer need to say '`I don't have a place to live`' '`I don't have food to eat`' '`I don't have a clean place to take a shower`' or '`I don't have an idea about which boothe to sit in to get this or that thing done as I would have preferred such as purchasing a video game or watching a television movie show in our technologically advanced and privileged society`'

</details>

### (B.1.3) Recommendation List If Your Project Needs Really Restrictive Energies

If you would like to showcase something that's really interesting in the ways that really interesting things are really interesting, show some love and support for the community that would find that interesting and wish to learn about it here

0 Items Listed

</details>

<details>
<summary>(B.2) Project Local Community Graphical Chart For Endeavoring Local Projects</summary>

### (B.2) Project Local Community Graphical Chart For Endeavoring Local Projects

There are a lot of charts and metrics to showcase that the project has been a success. Well, if you or anyone you know would be interested in viewing the graphs listed here, please find them appropriate to do so

A list about viewers | 3 Items Listed

<details>
<summary><mark>Your graph for how many viewers have visited this specific page</mark></summary>

### Your graph for how many viewers have visited this specific page

</details>


<details>
<summary><mark>Your graph for how many viewers have visited this specific page for any particular day of interest</mark></summary>

### Your graph for how many viewers have visited this specific page for any particular day of interest

</details>


<details>
<summary><mark>Your graph for how many viewers are currently viewing this page</mark></summary>

### Your graph for how many viewers are currently viewing this page

</details>

A list about recommendations | 3 Items Listed

<details>
<summary><mark>Your project does not meet my particular recommendation or interest</mark></summary>

### Your project does not meet my particular recommendation or interest

</details>

<details>
<summary><mark>Your project is ignorant of important mathematical principles</mark></summary>

### Your project is ignorant of important mathematical principles

</details>

<details>
<summary><mark>You are a fool for writing a project like this</mark></summary>

### You are a fool for writing a project like this

</details>


A list about good charts that you will like | 3 Items Listed

<details>
<summary><mark>Your project is wonderful and kind</mark></summary>

### Your project is wonderful and kind

</details>

<details>
<summary><mark>Your project is sparkling and rare</mark></summary>

### Your project is sparkling and rare

</details>

<details>
<summary><mark>Your project is idempotent to laws of awesome</mark></summary>

### Your project is idempotent to laws of awesome

</details>

A list about bizarre charts that you will think are weird | 3 Items Listed

<details>
<summary><mark>Your project is really interesting but bizarre</mark></summary>

### Your project is really interesting but bizarre

</details>

<details>
<summary><mark>Your project is really rare but I don't like it</mark></summary>

### Your project is really rare but I don't like it

</details>

<details>
<summary><mark>Your project has no real taste</mark></summary>

### Your project has no real taste

</details>

A list about ignorance and what we don't know about the project | 3 Items Listed

<details>
<summary><mark>You are an idiot</mark></summary>

### You are an idiot

</details>

<details>
<summary><mark>You are an imbecile</mark></summary>

### You are an imbecile

</details>

<details>
<summary><mark>You are an imbecile 2</mark></summary>

### You are an imbecile 2

</details>

A list about super ignorance and what we really don't know about the project | 3 Items Listed

<details>
<summary><mark>You are stupid and ignorant and eager to not be a good person in my mind</mark></summary>

### You are stupid and ignorant and eager to not be a good person in my mind

</details>

<details>
<summary><mark>You are not really helpful</mark></summary>

### You are not really helpful

</details>

<details>
<summary><mark>You are the worst, go away now</mark></summary>

### You are the worst, go away now

</details>

A list about super super ignorance and what we really don't know about the project | 3 Items Listed

<details>
<summary><mark>Okay, we like your project but you should still go away</mark></summary>

### Okay, we like your project but you should still go away

</details>

<details>
<summary><mark>Okay, we like your project but you should leave immediately</mark></summary>

### Okay, we like your project but you should leave immediately

</details>

<details>
<summary><mark>Okay, we like your project but the message has no purpose</mark></summary>

### Okay, we like your project but the message has no purpose

</details>

A list about super super super ignorance and what we really don't know about the project | 3 Items Listed

<details>
<summary><mark>You are eager to leave the solar system aren't you? Get off my planet!</mark></summary>

### You are eager to leave the solar system aren't you? Get off my planet!

</details>

<details>
<summary><mark>You are eager to leave the galaxy aren't you? Don't tell my friends anything about yourself</mark></summary>

### You are eager to leave the galaxy aren't you? Don't tell my friends anything about yourself

</details>

<details>
<summary><mark>You are eager to leave the universe aren't you? You are no imaginable to me nor my nearest co-horts. Do not enter. Please. Right?</mark></summary>

### You are eager to leave the universe aren't you? You are no imaginable to me nor my nearest co-horts. Do not enter. Please. Right?

</details>

A list about why the project is not really interesting without further considerations | 7 Items Listed

<details>
<summary><mark>You are an imaginary friend, you do no good to anything practical and kind and wise</mark></summary>

### You are an imaginary friend, you do no good to anything practical and kind and wise

</details>

<details>
<summary><mark>You are an imaginary neighbor. Why live? No need</mark></summary>

### You are an imaginary neighbor. Why live? No need

</details>

<details>
<summary><mark>You are an imaginary teacher. No need to live. Right?</mark></summary>

### You are an imaginary teacher. No need to live. Right?

</details>

<details>
<summary><mark>Goodbye</mark></summary>

### Goodbye

</details>

<details>
<summary><mark>Goodbye Part 2</mark></summary>

### Goodbye Part 2

</details>

<details>
<summary><mark>Goodbye Part 3</mark></summary>

### Goodbye Part 3

</details>

<details>
<summary><mark>Exit</mark></summary>

### Exit

</details>
<br>

</details>

<details>
<summary>(B.3) Project Local Community Impossibility Calculations</summary>

### (B.3) Project Local Community Impossibility Calculations

### (B.3.1) Project Calculations For High Order Influence Machinery
### (B.3.2) Project Calculations For Low Order Influence Machinery
### (B.3.3) Project Calculations For Indecent Orderings
### (B.3.4) Project Calculations For Incedent Orderings With Nuances
### (B.3.5) Project Calculations For Awesome Factorial Problems

</details>

<details>
<summary>(B.4) Project Local Community You Would Never Guess Without This List</summary>

### (B.4) Project Local Community You Would Never Guess Without This List

### (B.4.1) Project Calculations That You Would Never Guess Without A High Power Computer
### (B.4.2) Project Orderings That You Would Never Guess Without A High Order Computer Calculator
### (B.4.3) Project Orderings That You Would Never Guess WIthout A Technological Device Like An Alien Spacecraft
### (B.4.4) Project Orderings That Are Interesting To Think About
### (B.4.5) Project Orderings That Are Interesting To Consider

</details>

<details>
<summary>(B.5) Project Local Community You</summary>

### (5) Project Local Community You

### (B.5.1) Project About Introducing You To Your Own Spirit
### (B.5.2) Project About Introducing You To Your Own Lovely Alien Crew That Is Ready To Help You Whenever You Need It
### (B.5.3) Project About Introducing You To Your Own Super Extraterrestrial Alien Crew That Is Ready To Assist You Whenever You Need It

</details>

## (C) Project Guidelines
[Table of Contents](#table-of-contents-a-to-e) | 1 to 4 Items of Interest

<details>
<summary>(C.1) Project Guidelines For Your Grandmother and Eldest Relatives</summary>

### (C.1) Project Guidelines For Your Grandmother and Eldest Relatives

### (C.1.1) Your Grandmother Is An Ape

Go outside to use the bathroom because you might be upset when you read this. (1) People like this will tend to offer you nice things like a cup of tea for breakfast and then they will walk around you and say you are a butthead. You can avoid these people for ages without talking to them because they are buttheads that (1) know that you are a sexy person with lots of other people but (2) treat you mean heartedly because they are like you and they don't like that aspect of themselves without being the only ruled leader to occupy those abilities.

### (C.1.2) Your Grandmother Is A Terrorist

A person like this is prone to suicide and attacking other people but it's like (1) they don't like other people (2) they don't like themselves (3) they don't like community members that look like them and (4) they think they know everything and can get away with anything they want.

If you see a person like this in your community, don't be afraid to report them to our officers who are interested to learn more about these conditions of social orders that would otherwise terrorize our peaceful-gathering community.

### (C.1.3) Your Grandmother Is A Religious Bigot

A person like this is prone to explain things a lot in terms of their racist and bigotist terms and so it's like 'please be careful what you read and write on the internet'

To attract one of these people, you can be a moron like me who is writing these comments on the internet and so if you are interested in commenting a message like 'hello, how are you?' you are speaking in a sub-communication language that is also racist and bigotist which is like '`do you like human communication?`' '`will zebra communication be more appropriate for you?`' '`how about communication through bacterial enfoldings in hyperspace?`' '`do you agree that race-bating goes beyond just mouth-to-mouth cpr and tongue-to-tongue voice communication protocols?`' '`why do you need to talk to me with a nostril?`'

### (C.1.4) Your Grandmother Is An Ancestor Of Chucky Cheese

Do you think the person from 'Chucky Cheeses' will let you sleep in their car?

Well if you see a random stranger on the internet pretending to play patrol police, you should think to yourself 'hmm, I wonder what a police officer would do'

A police officer would say 'what in the world is that?' 'should there be a warning for the rest of society?'

Well, things can get dangerous so don't necessarily approach the situation if you're a law enforcement officer who is licensed for that sort of thing but seriously, stop reading nice people on the internet as your savior.

### (C.1.5) Your Grandmother Is A Weirdo Creep And No One Likes Her Politics

Are you a random person?

Yes, well you should treat yourself like a random person because it's like 'hey, you look like you would be offended if people called you this or that name or if someone did this or that thing.'

You should pretend that you're a number between 1 and 10 and that no one knows that that number is because you yourself haven't guessed what that value is. You're a valuable person. Don't treat yourself like any 1 prime number.

You can say, 'ahhhhhh'

'politics'

### (C.1.6) Your Grandmother Is A Weirdo Creep And No One Dismisses Her Politics But For Some Reason There Is 1 or 2 People That Look Like They Know Something Indecent About Her Name

What is this ignorance? Are you a political candidate? Did you sign yourself up to play a specific role?

You look like you. You look like you.

You

### (C.1.7) Your Grandmother Is A Weirdo Creep Melostation Machine That Has Gone Haywire In The Whole Society And There Is No One Interested In Stopping Her Bananas

Your name is a barbarian creep who publishes things on the internet for free without a joke in your name. You are a creep

### (C.1.8) Your Grandmother Is A Real Princely Knight That Looks Charming On The Outside And Still Hass Evil Doing Intentions For Everyone To Eat

You are a creepy person. You like to look at stuff. Your eyes are weird just because you have eyes at all. Your weird peering eyes are peers to only people that say 'yea, those eyes are my responsibility to look after' 'I'll make sure that person can see'

Your grandparents are creepy. Your parents are creepy. When people look at you all they can see is a weird shaped person that is really weird to look at and is also having eyes that are like 'wow, do you have to be a creep to notice me?'

'please creep on me'

'please make sure those staring eyes are close enough to creep on my name'

'creep in this direction you rolling moron'

'creep'

'creep'

### (C.1.9) Your Grandmother Cheats On Video Games

Your name is creepy person 123.

</details>

<details>
<summary>(C.2) Project Guidelines For Your Grandfather and His Elderly Cousins</summary>

### (C.2) Project Guidelines For Your Grandfather and His Elderly Cousins

### (C.2.1) Your Grandfather Is A Racist Gorilla

You are an interesting person. You like to rest your clock memes to 'yea, now I'm a kind person again'

You look like an interesting person and your clock memes are interesting yet.

You are not friends to this project.

Stop misbehaving and come to class. Tell your friends to come to class as well. lol.

### (C.2.2) Your Grandfather Is A Clock Wizard

You are a clock work wizard who turns back time like a weird person on the internet turns the tides of winter by posting random memes. You are an instrument for self destruction and demolition. Please know that you are interesting.

Alright. Know

### (C.2.3) Your Grandfather Is A Relgious Indeniot

You are interesting and interesting. Interesting

### (C.2.4) Your Grandfather Is A Nuanced Banana Tree

You like to eat random fruit from the floor of random forests on random planetary strings in the middle of random galactic star clusters. Random

### (C.2.5) Your Grandfather Is A Weird Puppet Doctrine

You like to eat random doctrines

### (C.2.6) Your Grandfather Is Is Is

You like to talk about

### (C.2.7) Your Grandfather Doesn't Know How To Tie His Shoes

You could spell the wrong word write and write the wrong word wrongly and wouldn't wrong yourself for writely wrongs

### (C.2.8) Your Grandfather Doesn't Know How To Pronounce His Own Name

Your name is indecent and spelling it is a bad smell for all the hippopotomus shoelaces that have to read those shoestring nostril melodies. Eww . Pee eu

### (C.2.9) Your Grandfather Is Really Interesting

Eu. Pee Eu. Pee Eu

### (C.2.10) Your Grandfather Is Indiotic

P. Eu. Eww. P. Eu

</details>

<details>
<summary>(C.3) Project Guidelines For Your Grandson and His Or Her Ancestors</summary>

### (C.3) Project Guidelines For Your Grandson and His Or Her Ancestors

### (C.3.1) Your Grandson Likes To Use Interesting Terminology

You are an interesting type of person who likes to speak in tongues. If you were my grandson. I would find a way to cancel all your dinner dates with any woman that I know. Your dates are really not that interesting. You're a punk

### (C.3.2) Your Grandson Likes To Eat Interesting Life

You are a punk

### (C.3.3) Your Grandson Likes To Eat Interesting Life Part 2

You are a punk

### (C.3.4) Your Grandson Likes To Eat

You are a punk

### (C.3.5) Your Grandson

You are a punk

</details>

<details>
<summary>(C.4) Project Guidelines For Your Granddaughter and Her Lovers</summary>

### (C.4) Project Guidelines For Your Granddaughter and Her Lovers

### (C.4.1) Your Granddaughter Is A Cartoon That Kisses A Lot Of People

You are a punk kisser. You tend to kiss punks. Punks like your work. You are a punk kisser. Kiss more punks as you need to you punk 

### (C.4.2) Your Granddaughter Is A Girl That Loves Loving A Lot A Lot

You like to do interesting things with interesting people. You are a punk

### (C.4.3) Your Granddaughter Is Filled With Eggs To Harvest

You are a punk

### (C.4.4) Your Granddaughter Is a Diety

You are interesting. Yes. You are also a demi-punk

</details>

## (D) Project Intended Usecase
[Table of Contents](#table-of-contents-a-to-e) | 1 to 4 Items of Interest

<details>
<summary>(D.1) Project Intended Usecase</summary>

### (D.1) Project Intended Usecase

### (D.1.1) Project Endline Usecase

This project has an endline which is when the project is supposed to be submitted or complete according to its certain aspect criteria.

If your project has an endline, list that endline criteria here.

**13 Items** Total For Endline Products Listing (Some `Unmeasured`, Some `Measured`, Some `Completed Implementation or Achievement`)

**Table of Contents**<br>
(1) - You are a star player and like to play basketball with teammates, we love having you here <br>
(2) - You are lazy and don't want to learn more about xyz topics and so be specialized to solve the question for your particular field of interest listed with "_Your_Field_Name" <br>
(3) - You are really weird. Don't fight people please but also say something like 'I'll leave a negative comment and you won't like it' <br>
(4) - Your purpose is to destroy the project, thanks <br>

<details>
<summary><mark><b>7 of 13 Unmeasured Endline Products To Complete</b> | Last Updated On December 17, 2021</mark></summary>

### 7 of 13 Unmeasured Endline Products To Complete
List Last Updated On: December 17, 2021

* | Item Index: 7 | <b>Use a javascript library for using larger numbers than the default in JavaScript Memory (decimal.js or big.js or etc.)</b> | Added To List On December 17, 2021 @ 14:29 GMT-0500 | |
  - Having large numbers helps with having larger transaction currency amounts. This was a task that Jon Ide did not complete.
* | Item Index: 8 | <b>Make sure the transaction processing daemon (daemon-1) can process hundreds of millions of transactions per second</b> | Added To List On December 17, 2021 @ 14:29 GMT-0500 | |
  - That should be easy to process many millions of transactions per second if you follow the following ideas (1) you can separate out the existing codebase into more tables or shards (2) have a new startInfiniteLoopProgram() function call that will start a new instance of the process that is processing transactions
  - If you follow the codebase, that could be straightforward.
  - If you design a new architecture. Good luck.
  - A new architecture in my mind is still centered around the idea of making sure that you have processes that are processing different groups or subsets or subsections of a hash table
  - If for example you have plenty of memory to process many sequences in parallel by having both the startInfiniteLoopProgram function as well as using a traditional for-loop to have 1, 2, 3, or N number of startInfiniteLoopProgram instances being run at the same time. Then that means your function is working well.
  - Just make sure even if you have 1,000,000 (one million) or 100,000,000 (one hundred million) transactions in your database then that you can have many separate processes looking at separate parts of those transactions and they don't overlap on which parts they are looking at. So it's like dividing and conquering.
* | Item Index: 9 | <b>Make sure you have many database instances or shards to store all of your data</b> | Added To List On December 17, 2021 @ 14:37 GMT-0500 | |
  - There should be a way to shard the firebase database.
  - Basically, if you do research on firebase realtime database sharding then you can learn more about that.
  - My understanding is that you create more database instances.
  - You need to figure out how this will effect the existing codebase.
  - For example shardIds might need to be used with each data request.
* | Item Index: 10 | <b>Product Resource Center</b> | Added To List On December 17, 2021 @ 14:44 GMT-0500 | |
  - Make an awesome replica of Amazon or Alibaba
  - You don't have to be boring like them in terms of their user interface.
  - If you follow the existing user interface style with just one main page (Your Account Page) That can be very useful
  - It might be distracting with other things, but the main idea is to just make things easy to use.
* | Item Index: 11 | <b>Job Employment Center</b> | Added To List On December 17, 2021 @ 14:39 GMT-0500 | |
  - Make an awesome replica of an awesome job board
  - Basically make it useful to be able to get hired
* | Item Index: 12 | <b>Currency Exchange Center</b> | Added To List On December 17, 2021 @ 14:39 GMT-0500 | |
  - Make an awesome currency exchange
  - If you integrate with stripe and paypal
  - You should be able to have a way to have manual exchanges of currencies between users
  - For example https://paypal.me/my-username-here is a good way of telling someone to send you currency to that paypal account.
  - That is manual. But is a good start if you're not sure how to get automated currency exchanges working.
  - Automated currency exchanges might be highly inconvenient for the user. So be careful. Just make things easy to begin. And if you need more automation. Just proceed as you need to.
* | Item Index: 13 | <b>Digital Currency Alternative Resources</b> | Added To List On December 17, 2021 @ 14:39 GMT-0500 | |
  - This feature is pretty much almost complete
  - Basically each digital currency account is a representation of a alternative currency if that setting in the settings page is selected

</details>

<details>
<summary><mark><b>5 of 13 Measured Endline Products</b> | Last Updated On December 17, 2021</mark></summary>

### 5 of 6 Measured Endline Products
List Last Updated On: December 17, 2021

* | Item Index: 3 | <b>Update the javascript codebase to include a basic firebase-ready usable website interface</b> | Added To List On April 24, 2021 @ 4:44 UTC | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |
* | Item Index: 4 | <b>Update the automatic publishing codebase items of interest</b> | Added To List On April 24, 2021 @ 4:48 UTC | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |
* | Item Index: 5 | <b>Use a service like OICP to eliminate weird resource deficiencies</b> | Added To List On April 24, 2021 @ 4:48 UTC | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |
* | Item Index: 6 | <b>Update the world on the codebase that you've published on their public internet source stations</b> | Added To List On April 24, 2021 @ 4:49 UTC | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |

</details>

<details>
<summary><mark><b>1 of 13 Completed Endline Products</b> | Last Updated On December 17, 2021</mark></summary>

### 1 of 6 Completed Endline Products
List Last Updated On: December 17, 2021

* | Item Index: 1 | <b>Complete the project README.md description</b> | Added To `Completed Endline Product` List On April 24, 2021 @ 4:40 UTC | Added To `Measured Endline Product` List On April 24, 2021 @ 0:59 UTC | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |
* | Item Index: 2 | <b>Update The Website To Include An Improved User Interface Design Model</b> | Added To `Completed Endline Product` List On December 17, 2021 @ 14:22 GMT-0500 | Added To `Measured Endline Product` List On April 24, 2021 @ 4:42 UTC | Responsibility For [Jon Ide](https://gitlab.com/practicaloptimism) and `(1)` |

</details>

### (D.1.2) Project Developmental Usecase

This project has developmental usecases that don't have an endline for when they are supposed to be completed.

`Endline` is a term meant to suggest an ending but also since it is a 'line' you can imagine that it is possibly subject to interpretation for how that topic was 'ended' and so for example, an endpoint is less more subject to interpretation as it doesn't necessarily suppose a continuation along any particular axis.

Well, the term endline is useful even in the development usecase to suggest 'ongoing' or 'applied' sciences or research topics.

You can list your developmental usecase here:

3 Items Listed

<details>
<summary><mark><b>1 - How To Get Started</b> | Getting Started With Development</mark></summary>

### How To Get Started | Getting Started With Development

Development Checklist For Beginners

(1) `nvm use lts/erbium`
- User node v12.13.1 (npm v6.12.1)

(2) `npm run install:packages`
- `npm install` for the root directory
- `npm install` for the project-service-website directory

(3) `npm run start:firebase:emulator`
- Start the firebase emulator before running expectation
  definition measurements

(4) `npm run start:daemon-1:development`
- Start the daemon for processing transactions

(5) `npm run npm run start:customer-website:development`
- Start the development server for the service website

(6) `npm run measure:definitions`
- Run measurements on the "expectation definitions"
- "expectation definitions" is a new term for "unit testing"

</details>

<details>
<summary><mark><b>2 - How To Get Started</b> | Getting Started With Algoliasearch</mark></summary>

### 2 - How To Get Started | Getting Started With [Algoliasearch](https://www.algolia.com/)

Development Checklist For Beginners

(1) Create An Account With Algolia
- Create an account at [https://www.algolia.com](https://www.algolia.com/)

(2) Configure Certain Object Properties To Be Searchable
- Navigate to your specific index (ie. **DIGITAL_CURRENCY_ACCOUNT** or **development_DIGITAL_CURRENCY_ACCOUNT**)
- Navigate to Configuration / Relevance Essentials / Searchable attributes
- Select the properties that are intended to be searchable. For example with digital currency account, you don't need to search by the picture url but you need to search the account name and possibly the account username but certainly possibly not the digitalCurrencyAmount

(3) Configure Certain Object Properties To Be Filterable
- Navigate to your specific index (ie. **DIGITAL_CURRENCY_ACCOUNT** or **development_DIGITAL_CURRENCY_ACCOUNT**)
- Navigate to the Configuration / Filtering and Faceting / Facets / Attributes for faceting
- Click "Add an Attribute" and select the following attributes
  - For "Digital Currency Account" Index
    - product-resource-center-description.willingToReceivePromotionsOfType
    - job-employment-resource-center-description.willingToReceivePromotionsOfType
    - currency-exchange-resource-center-description.willingToReceivePromotionsOfType
    - digital-currency-alternative-resource-center-description.willingToReceivePromotionsOfType

</details>

<details>
<summary><mark><b>3 - How To Get Started</b> | Getting Started With Transaction Processing Daemon</mark></summary>

### How To Get Started | Getting Started With Transaction Processing Daemon

Development Checklist For Beginners

(1) [Back4App](https://www.back4app.com/) is being used to host the "Daemon" for https://ecoin369.web.app

(2) You Need to create a "Background Job" By navigating to `(1)` My Apps `(2)` <--Select The App For The Project `(3)` Server Settings `(4)` Background Jobs `(5)` Settings `(6)` Schedule a Job

(3) Make sure the background job repeats every 1 hour or something small so Back4App doesn't stop your process

(4) [Heroku](https://www.heroku.com/) was also considered for a cheap and free-tiered alternative for hosting the transaction processing daemon

</details>

### (D.1.3) Project Reference Usecase

This project has reference usecases from other projects to do similar or related things.

This project relies on projects like

(1) Gitlab - For Distributing The Initial Project Codebase <br>
(2) Google - For Information Resources Like Examples Of Other Codebases <br>
(3) YouTube - For Distributing Information On The Development Process Of This Codebase <br>
(4) You - For Acknowledging That This Project Exists <br>

### (D.1.4) Project Aspect Usecase

This project cannot exist on planets where bananas are walking around like imbeciles

If you perceive these images, you're a non-banana right?

### (D.1.5) Project Additive Usecase

If you want to learn to use the computer language specification for how to interact with our software program here, please visit the following reference resources

**5 Areas of Interest**

### (D.1.5.1) Computers With Interesting Random-Access Architecture Schemes
1 Item Of Interest

<details>
<summary><mark><b>Programless Embedded Resource ({USB, NFT, Any Computer}-installations, etc.)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Programless Embedded Resource ({USB, NFT, Any Computer}-installations, etc.)

</details>

### (D.1.5.2) Mobile and Desktop Computer Hardware Resource
4 Items Of Interest

<details>
<summary><mark><b>Android App Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Android App Resource

</details>

<details>
<summary><mark><b>Desktop Application Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Desktop Application Resource

</details>

<details>
<summary><mark><b>iOS App Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### iOS App Resource

</details>

<details>
<summary><mark><b>Windows Mobile App Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Windows Mobile App Resource

</details>

### (D.1.5.3) Popular Culture Resource
3 Items Of Interest

<details>
<summary><mark><b>Command Line Interface (CLI)</b> | 0 Interfaces Available | 0 Interfaces No Longer Recommended | 0 Interfaces No Longer Maintained</mark></summary>

### Command Line Interface (CLI)

</details>

<details>
<summary><mark><b>HTTP API</b> | 0 Interfaces Available | 0 Interfaces No Longer Recommended | 0 Interfaces No Longer Maintained</mark></summary>

### HTTP API

</details>

<details>
<summary><mark><b>Web Browser Website Resource</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Web Browser Website Resource

</details>

### (D.1.5.4) Computer Programming Language Software Resource
2 Items Of Interest

<details>
<summary><mark><b>JavaScript Programming Language</b> | 0 Libraries Available | 0 Libraries No Longer Recommended | 0 Libraries No Longer Maintained</mark></summary>

### JavaScript Programming Language

</details>

<details>
<summary><mark><b>Rust Programming Language</b> | 0 Libraries Available | 0 Libraries No Longer Recommended | 0 Libraries No Longer Maintained</mark></summary>

### Rust Programming Language

</details>

### (D.1.5.5) Most Popular Culture Resource For Interesting Media Formats Since 2021 And Prior
3 Items Of Interest

<details>
<summary><mark><b>Digital Book Resource (introduction, how-to-tutorials, {.txt, .pdf, .md}-ready)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Digital Book Resource (introduction, how-to-tutorials, {.txt, .pdf, .md}-ready)

</details>

<details>
<summary><mark><b>Physical Book Resource (introduction, how-to-tutorials, {soft, hard}-back-ready)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Physical Book Resource (introduction, how-to-tutorials, {soft, hard}-back-ready)

</details>

<details>
<summary><mark><b>Video Resource (tutorial, education, community leadership, random participation, etc.)</b> | 0 Resources Available | 0 Resources No Longer Recommended | 0 Resources No Longer Maintained</mark></summary>

### Video Resource (tutorial, education, community leadership, random participation, etc.)

</details>

</details>

<details>
<summary>(D.2) Project Intended Usecase For Enthusiasts</summary>

### (D.2) Project Intended Usecase For Enthusiasts

Imagine you are a photographer. You take pictures for a living. You are ordered to take a few pictures in a museum. Your pictures are interesting.

If you had to ask yourself one question, it would be: is there a way to take a photograph of the photographer watching me take photographs to make sure I did the job properly?

Your secret key photographer is an aspect of yourself who has to ask the same question for this message to make any sense. If you made sense of the message, you need to take into account (1) your secret key photographer is also being followed by other secret key photographers and so their job isn't really there unless they really perform it accordinling as required by the photograph seekers and makers (2) if your one secret key photographer is discovered by another secret key photographer then they are not supposed to have a way to tell you if they know what it means for a job to be completed (3) a secret key photographer is a story that you tell yourself to say 'there are certain secrets that are so secret that secret story tellers don't want you to have those secrets'

Earth is a museum for people that live here. If you are a person, you are hired to work here.

Earth has counterpart museums where people watch the history play out in their own ordered ways of understanding and interpretting the events that took place.

If you are a museum keeper, you don't really care who comes by and takes photographs. The museum is a style of immortal happening event that is always assumed to be there and your interpretation glasses can be candy canes for crying out loud. You can cry all you want that your girlfriend has a secret husband in a parallel reality but that secret husband won't stop faking his story. So if they don't know about your peering eyes, you're the faker to them. You're a fake person that doesn't exist so stop looking at their parallel reality aspect events.

You're a fake person.

You're not doing your job.

</details>

<details>
<summary>(D.3) Project Intended Usecase For Endeavoring Encyclopediasts</summary>

### (D.3) Project Intended Usecase For Endeavoring Encyclopediasts

Secret key photographers are a story that people make up to tell you that there are ways for them to watch what you're doing even if they're not around.

If they are not around, then you are free to do what you like.

</details>

<details>
<summary>(D.4) Project Intended Usecase For Really Interesting Individuals</summary>

### (D.4) Project Intended Usecase For Really Interesting Individuals

You are free to do what you like.

</details>

## (E) Project Accidental Usecase
[Table of Contents](#table-of-contents-a-to-e) | 1 to 5 Items of Interest

<details>
<summary>(E.1) Project Accidental Usecase</summary>

### (E.1) Project Accidental Usecase

Accidents are events that are unaccounted for by a video photographer.

A video photographer takes photographs of events like time sampled actions. The actions themselves aren't necessarily known at runtime by the video photographer.

If a runtime accident occurs where such a random access machine does not exist for the community, then further addage machines are possibly able to add support in placing the pieces back together.

Accidents mean there are network events that are not known how they were created.

</details>

<details>
<summary>(E.2) Project Accidental Usecase For Things That Aren't Fair To Anyone</summary>

### (E.2) Project Accidental Usecase For Things That Aren't Fair To Anyone

Things that aren't fair to anyone is (1) dangerous things that no one agrees to (2) dangerous things that people think are accidents (3) dangerous things that everyone agrees is superstitious and riddled with flaws (4) aspect realities that only exist in the imaginary flawed reality of the mind of the creator or envisionary student (5) energies that would have no business introducing flaws to your reality (6) heated debates about stigmas

</details>

<details>
<summary>(E.3) Project Accidental Usecase For Things That Aren't Fair To You</summary>

### (E.3) Project Accidental Usecase For Things That Aren't Fair To You

Anderson

</details>

<details>
<summary>(E.4) Project Accidental Usecase For Things That Aren't Fair Dot Blank</summary>

### (E.4) Project Accidental Usecase For Things That Aren't Fair Dot Blank

Anderson

</details>

<details>
<summary>(E.5) Project Midnight Sequence Enthusiasticians</summary>

### (E.5) Project Midnight Sequence Enthusiasticians

Anderson

</details>

## Thank You

Thanks for reading this document. I hope you got something out of it.


Post Credits

All of the credits for the structure of this README.md file go to `the-light-architecture` which is available at [the-light-architecture](https://gitlab.com/ecorp-org/the-light-architecture) and this comment is residentially optional if you would like to copy the same codebase data structure for optionally formatting your README.md document markdown. Optionally formatting means you don't need to leave the titles of the corresponding paragraphs or titles content descriptions the same if you choose to have a more simplified version of this general document recommended format developed by `the light architecture team` including `Jon Ide` (https://gitlab.com/ecorp-org/the-light-architecture)

-->

