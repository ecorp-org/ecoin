declare class ServiceInitializationInformation {
    distributionType?: string;
    useEmulator?: boolean;
}
export { ServiceInitializationInformation };
