declare class DeleteItemAlgorithmResponse {
    isDeleted: boolean;
    errorMessage: any;
    additionalInformaiton: {
        [additionalInformationId: string]: any;
    };
}
declare function deleteItem2FirebaseAdmin(storeId: string, itemId: string, isFileDocument?: boolean): Promise<DeleteItemAlgorithmResponse>;
export { deleteItem2FirebaseAdmin, DeleteItemAlgorithmResponse };
