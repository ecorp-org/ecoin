declare class DeleteItemAlgorithmResponse {
    isDeleted: boolean;
    errorMessage: any;
    additionalInformaiton: {
        [additionalInformationId: string]: any;
    };
}
declare function deleteItem1(storeId: string, itemId: string, isFileDocument?: boolean): Promise<DeleteItemAlgorithmResponse>;
export { deleteItem1, DeleteItemAlgorithmResponse };
