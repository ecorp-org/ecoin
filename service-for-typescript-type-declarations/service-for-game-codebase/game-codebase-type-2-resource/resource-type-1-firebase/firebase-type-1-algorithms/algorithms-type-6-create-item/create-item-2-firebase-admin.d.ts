declare function createItem2FirebaseAdmin(storeId: string, itemId: string, item: any, isFileDocument?: boolean, triggerChildChangedEvent?: boolean, isSingleWrite?: boolean): Promise<any>;
export { createItem2FirebaseAdmin };
