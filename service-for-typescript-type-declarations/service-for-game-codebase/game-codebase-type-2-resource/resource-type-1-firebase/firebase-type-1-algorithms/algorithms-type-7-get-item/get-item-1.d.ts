declare class GetItem1Options {
    orderByValue?: boolean;
    limitToFirst?: number;
    limitToLast?: number;
    startAt?: number;
}
declare function getItem1(storeId: string, itemId: string, options?: GetItem1Options): Promise<any>;
export { getItem1, GetItem1Options };
