import { OnEventCallback } from '../../firebase-type-3-constants/constants-1-firebase-events';
declare function addItemEventListener2FirebaseAdmin(storeId: string, itemId: string, eventId: string, onEventCallback: typeof OnEventCallback): Promise<string>;
export { addItemEventListener2FirebaseAdmin, OnEventCallback };
