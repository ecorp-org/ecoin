declare function updateItem1(storeId: string, itemId: string, item: any, isFileDocument?: boolean, triggerChildChangedEvent?: boolean): Promise<any>;
export { updateItem1 };
