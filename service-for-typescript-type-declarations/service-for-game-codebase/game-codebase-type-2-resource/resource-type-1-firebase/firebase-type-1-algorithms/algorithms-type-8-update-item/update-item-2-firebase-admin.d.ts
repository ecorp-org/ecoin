declare function updateItem2FirebaseAdmin(storeId: string, itemId: string, item: any, isFileDocument?: boolean, triggerChildChangedEvent?: boolean): Promise<any>;
export { updateItem2FirebaseAdmin };
