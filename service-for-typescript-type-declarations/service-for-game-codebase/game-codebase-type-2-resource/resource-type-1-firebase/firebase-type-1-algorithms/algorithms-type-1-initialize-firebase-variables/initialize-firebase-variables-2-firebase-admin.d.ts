import { ServiceInitializationInformation } from '../../firebase-type-2-data-structures/data-structures-1-service';
declare function initializeFirebaseVariables2FirebaseAdmin(serviceInformation: ServiceInitializationInformation): Promise<void>;
export { initializeFirebaseVariables2FirebaseAdmin };
