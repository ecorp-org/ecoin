import firebase from 'firebase/compat/app';
declare const EVENT_ID_GET = "get";
declare const EVENT_ID_CREATE = "create";
declare const EVENT_ID_UPDATE = "update";
declare const EVENT_ID_DELETE = "delete";
declare type EventType = 'create' | 'update' | 'get' | 'delete';
declare const OnEventCallback: (data: any, _options: any) => any;
declare const OnItemStoreEventCallback: (itemIdList: string[], itemList?: any[] | undefined) => any;
export { firebase, EVENT_ID_GET, EVENT_ID_CREATE, EVENT_ID_UPDATE, EVENT_ID_DELETE, EventType, OnEventCallback, OnItemStoreEventCallback };
