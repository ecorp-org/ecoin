import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/database';
import 'firebase/compat/storage';
import 'firebase/compat/analytics';
declare const firebaseVariableTree: {
    firebase?: typeof firebase;
    firebaseApp?: firebase.app.App;
    firebaseAdminApp?: firebase.app.App;
    auth?: firebase.auth.Auth;
    realtimeDatabase?: firebase.database.Database;
    storage?: firebase.storage.Storage;
    analytics?: firebase.analytics.Analytics;
    itemEventListenerTable: {
        [itemEventListenerId: string]: any;
    };
};
declare const firebaseRealtimeDatabaseVariableTree: {
    [firebaseServiceAccountId: string]: firebase.database.Database;
};
export { firebaseVariableTree, firebaseRealtimeDatabaseVariableTree };
