import * as firebaseAdmin from 'firebase-admin';
declare const firebaseAdminVariableTree: {
    firebaseAdmin?: typeof firebaseAdmin;
    firebaseApp?: firebaseAdmin.app.App;
    auth?: firebaseAdmin.auth.Auth;
    realtimeDatabase?: firebaseAdmin.database.Database;
    storage?: firebaseAdmin.storage.Storage;
    itemEventListenerVariableTree: {
        [itemEventListenerId: string]: any;
    };
};
export { firebaseAdminVariableTree };
