import * as algoliasearch from 'algoliasearch';
declare const algoliasearchVariableTree: {
    itemSearchIndexVariableTree: {
        [itemSearchIndexId: string]: algoliasearch.SearchIndex;
    };
    itemSearchIndexNameToIndexIdMap: {
        [indexName: string]: string;
    };
};
export { algoliasearchVariableTree };
