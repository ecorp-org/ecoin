import { InitializeSearchIndexOptions } from '../../algoliasearch-type-2-data-structures/data-structures-2-initialize-search-index-options';
declare function initializeAlgoliasearchVariables1(itemSearchIndexNameToIndexIdMap?: {
    [indexName: string]: string;
}, itemSearchIndexNameToOptionsMap?: {
    [indexName: string]: InitializeSearchIndexOptions;
}): void;
export { initializeAlgoliasearchVariables1 };
