declare class DigitalCurrencyTransaction {
    transactionId: string;
    transactionOrderIndex: number;
    transactionName?: string;
    accountSenderId: string;
    accountRecipientIdVariableTable: {
        [accountId: string]: {
            dateAccountDeleted?: number;
            transactionCurrencyAmount: number;
        };
    };
    onBehalfOfAccountId: string;
    transactionCurrencyAmount: number;
    transactionType: string;
    textMemorandum?: string;
    specialTransactionType?: string;
    dateTransactionCreated?: string;
    dateTransactionProcessed?: string;
    dateTransactionScheduledForProcessing?: string;
    dateTransactionReScheduledForProcessing?: string;
    dateTransactionScheduledForCancellingRecurrence?: string;
    isProcessed: boolean;
    isBeingProcessed: boolean;
    dateLastBeingProcessed?: string;
    isRecurringTransaction: boolean;
    millisecondRecurringDelay: number;
    numberOfTimesRecurred: number;
    numberOfTimesRecurredSinceRestart: number;
    dateRecurrenceCancelled?: string;
    dateRecurrenceRestarted?: string;
    originalTimeNumberUntilNextRecurrence?: number;
    originalTimeScaleUntilNextRecurrence?: string;
    numberOfTimesProcessingFailed?: number;
    latestReasonForProcessingFailed?: string;
    constructor(defaultOption?: DigitalCurrencyTransaction);
}
declare const SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME = "SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME";
export { DigitalCurrencyTransaction, SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME };
