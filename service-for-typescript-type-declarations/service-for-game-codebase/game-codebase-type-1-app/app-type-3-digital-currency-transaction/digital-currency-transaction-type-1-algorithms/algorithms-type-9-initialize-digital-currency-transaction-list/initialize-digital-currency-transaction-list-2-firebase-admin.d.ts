import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare let variableTable: {
    activeAccountIdVariableTable: {
        [accountId: string]: number;
    };
    newActiveAccountIdVariableTable: {
        [accountId: string]: number;
    };
    oldActiveAccountIdVariableTable: {
        [accountId: string]: number;
    };
    addNewActiveAccountEventListenerId: any;
    removeOldActiveAccountEventListenerId: any;
    updateNewActiveAccountIdVariableTableIntervalId: any;
    updateOldActiveAccountIdVariableTableIntervalId: any;
    NUMBER_OF_MILLISECONDS_TRANSACTION_PROCESSING_BUFFER: number;
    NUMBER_OF_MILLISECONDS_TO_DELETE_UNUSED_ACCOUNT_DATA: number;
    NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE: number;
    TRANSACTION_TYPE_ID_ONE_TIME_PAYMENT: string;
    TRANSACTION_TYPE_ID_RECURRING_TRANSACTION: string;
    NUMBER_OF_TIMES_FAILED_TRANSACTION_PROCESSING_IS_ALLOWED: number;
    NUMBER_OF_MILLISECONDS_TO_TRY_TRANSACTION_PROCESSING_AGAIN: number;
};
declare function initializeDigitalCurrencyTransactionList(untilMillisecondTimeAgo?: number): Promise<void>;
declare function initializeDigitalCurrencyRecurringTransactionList(untilMillisecondTimeAgo?: number): Promise<void>;
declare function getDigitalCurrencyTransactionListByTransactionTypeId(transactionTypeId: string, untilMillisecondTimeAgo?: number): Promise<DigitalCurrencyTransaction[]>;
declare function getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts(digitalCurrencyTransaction: DigitalCurrencyTransaction, digitalCurrencyAmountForEachAcount?: number, initializeFromPreviousReference?: boolean): DigitalCurrencyTransaction;
declare function initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts(digitalCurrencyTransaction: DigitalCurrencyTransaction, digitalCurrencyAmountForEachAcount?: number, initializeFromPreviousReference?: boolean): Promise<DigitalCurrencyTransaction>;
declare function initializeAllAccountsForAccountIdVariableTable(): Promise<void>;
declare function uninitializeAllAccountsForAccountIdVariableTableEventListener(): void;
declare function updateDigitalCurrencyRecurringTransactionList(untilMillisecondTimeAgo?: number): Promise<void>;
declare function initializeDigitalCurrencyRecurringTransactionUpdate(_digitalCurrencyTransaction: DigitalCurrencyTransaction): Promise<void>;
declare function initializeDigitalCurrencyTransaction(digitalCurrencyTransaction: DigitalCurrencyTransaction): Promise<DigitalCurrencyTransaction | undefined>;
declare function initializeDigitalCurrencyAccountTransactionUpdate(digitalCurrencySenderAccountId: string, digitalCurrencyRecipientAccountId: string, digitalCurrencyTransactionId: string, currencyAmount: number, isRecurringTransaction?: boolean, isCreatingCurrencyTransaction?: boolean, isRefundPayment?: boolean): Promise<void>;
declare function initializeDigitalCurrencyAccountTransactionDataQueueProcessing(): Promise<void>;
declare function initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessing(): Promise<void>;
declare function initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessing(): Promise<void>;
declare const initializeDigitalCurrencyTransactionListAlgorithm: {
    function: typeof initializeDigitalCurrencyTransactionList;
};
declare const initializeDigitalCurrencyRecurringTransactionListAlgorithm: {
    function: typeof initializeDigitalCurrencyRecurringTransactionList;
};
declare const getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm: {
    function: typeof getDigitalCurrencyTransactionListByTransactionTypeId;
};
declare const getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm: {
    function: typeof getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts;
};
declare const initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm: {
    function: typeof initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts;
};
declare const initializeAllAccountsForAccountIdVariableTableAlgorithm: {
    function: typeof initializeAllAccountsForAccountIdVariableTable;
};
declare const uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm: {
    function: typeof uninitializeAllAccountsForAccountIdVariableTableEventListener;
};
declare const updateDigitalCurrencyRecurringTransactionListAlgorithm: {
    function: typeof updateDigitalCurrencyRecurringTransactionList;
};
declare const initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm: {
    function: typeof initializeDigitalCurrencyRecurringTransactionUpdate;
};
declare const initializeDigitalCurrencyTransactionAlgorithm: {
    function: typeof initializeDigitalCurrencyTransaction;
};
declare const initializeDigitalCurrencyAccountTransactionUpdateAlgorithm: {
    function: typeof initializeDigitalCurrencyAccountTransactionUpdate;
};
declare const initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm: {
    function: typeof initializeDigitalCurrencyAccountTransactionDataQueueProcessing;
};
declare const initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessingAlgorithm: {
    function: typeof initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessing;
};
declare const initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessingAlgorithm: {
    function: typeof initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessing;
};
export { variableTable, initializeDigitalCurrencyTransactionListAlgorithm, initializeDigitalCurrencyRecurringTransactionListAlgorithm, getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm, getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm, initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm, uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm, initializeAllAccountsForAccountIdVariableTableAlgorithm, updateDigitalCurrencyRecurringTransactionListAlgorithm, initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm, initializeDigitalCurrencyTransactionAlgorithm, initializeDigitalCurrencyAccountTransactionUpdateAlgorithm, initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm, initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessingAlgorithm, initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessingAlgorithm, };
