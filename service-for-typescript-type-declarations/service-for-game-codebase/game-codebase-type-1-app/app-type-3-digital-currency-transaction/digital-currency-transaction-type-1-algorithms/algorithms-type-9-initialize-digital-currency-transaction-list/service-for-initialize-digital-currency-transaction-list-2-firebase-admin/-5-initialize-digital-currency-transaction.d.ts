import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare let variableTable: {
    NUMBER_OF_TIMES_FAILED_TRANSACTION_PROCESSING_IS_ALLOWED: number;
    NUMBER_OF_MILLISECONDS_TO_TRY_TRANSACTION_PROCESSING_AGAIN: number;
};
declare function initializeDigitalCurrencyTransaction(digitalCurrencyTransaction: DigitalCurrencyTransaction, withNumberOfRecurringIterations?: number): Promise<DigitalCurrencyTransaction>;
declare const initializeDigitalCurrencyTransactionAlgorithm: {
    function: typeof initializeDigitalCurrencyTransaction;
};
export { variableTable, initializeDigitalCurrencyTransactionAlgorithm };
