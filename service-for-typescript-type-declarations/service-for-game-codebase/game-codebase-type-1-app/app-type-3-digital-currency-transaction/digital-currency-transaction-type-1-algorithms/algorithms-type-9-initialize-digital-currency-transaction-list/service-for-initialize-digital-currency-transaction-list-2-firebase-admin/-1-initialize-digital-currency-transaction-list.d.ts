import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare let variableTable: {
    NUMBER_OF_MILLISECONDS_TRANSACTION_PROCESSING_BUFFER: number;
    TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION: string;
    TRANSACTION_TYPE_ID_RECURRING_TRANSACTION: string;
};
declare function initializeDigitalCurrencyTransactionList(untilMillisecondTimeAgo?: number, isRecurringTransaction?: boolean, withDataQueueProcessing?: boolean): Promise<void>;
declare function initializeDigitalCurrencyRecurringTransactionList(untilMillisecondTimeAgo?: number, withDataQueueProcessing?: boolean): Promise<void>;
declare function getDigitalCurrencyTransactionListByTransactionTypeId(transactionTypeId: string, untilMillisecondTimeAgo?: number, isRecurringTransaction?: boolean): Promise<DigitalCurrencyTransaction[]>;
declare const initializeDigitalCurrencyTransactionListAlgorithm: {
    function: typeof initializeDigitalCurrencyTransactionList;
};
declare const initializeDigitalCurrencyRecurringTransactionListAlgorithm: {
    function: typeof initializeDigitalCurrencyRecurringTransactionList;
};
declare const getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm: {
    function: typeof getDigitalCurrencyTransactionListByTransactionTypeId;
};
export { variableTable, initializeDigitalCurrencyTransactionListAlgorithm, initializeDigitalCurrencyRecurringTransactionListAlgorithm, getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm };
