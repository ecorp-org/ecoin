import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function updateDigitalCurrencyRecurringTransactionList(untilMillisecondTimeAgo?: number, withDataQueueProcessing?: boolean): Promise<void>;
declare function initializeDigitalCurrencyRecurringTransactionUpdate(digitalCurrencyTransaction: DigitalCurrencyTransaction): Promise<DigitalCurrencyTransaction>;
declare const updateDigitalCurrencyRecurringTransactionListAlgorithm: {
    function: typeof updateDigitalCurrencyRecurringTransactionList;
};
declare const initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm: {
    function: typeof initializeDigitalCurrencyRecurringTransactionUpdate;
};
export { updateDigitalCurrencyRecurringTransactionListAlgorithm, initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm };
