declare let variableTable: {
    NUMBER_OF_MILLISECONDS_TO_DELETE_UNUSED_ACCOUNT_DATA: number;
};
declare function initializeDigitalCurrencyAccountTransactionDataQueueProcessing(): Promise<void>;
declare function initializeDigitalCurrencyAccountTransactionUpdate(digitalCurrencySenderAccountId: string, digitalCurrencyRecipientAccountId: string, digitalCurrencyTransactionId: string, currencyAmount: number, isRecurringTransaction?: boolean, isCreatingCurrencyTransaction?: boolean, isRefundPayment?: boolean): Promise<void>;
declare const initializeDigitalCurrencyAccountTransactionUpdateAlgorithm: {
    function: typeof initializeDigitalCurrencyAccountTransactionUpdate;
};
declare const initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm: {
    function: typeof initializeDigitalCurrencyAccountTransactionDataQueueProcessing;
};
export { variableTable, initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm, initializeDigitalCurrencyAccountTransactionUpdateAlgorithm };
