declare let variableTable: {
    activeAccountIdVariableTable: {
        [accountId: string]: {
            accountId: string;
            dateAccountCreated: string;
            dateAddedToList: string;
        };
    };
    newActiveAccountIdVariableTable: {
        [accountId: string]: {
            accountId: string;
            dateAccountCreated: string;
            dateAddedToList: string;
        };
    };
    oldActiveAccountIdVariableTable: {
        [accountId: string]: {
            accountId: string;
            dateAccountDeleted: string;
            dateAddedToList: string;
        };
    };
    addNewActiveAccountEventListenerId: any;
    removeOldActiveAccountEventListenerId: any;
    updateNewActiveAccountIdVariableTableIntervalId: any;
    updateOldActiveAccountIdVariableTableIntervalId: any;
    NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE: number;
};
declare function initializeAllAccountsForAccountIdVariableTable(): Promise<void>;
declare function uninitializeAllAccountsForAccountIdVariableTableEventListener(): void;
declare function getDigitalCurrencyAccountListByDateCreated({ startDateAccountCreated, endDateAccountCreated }: {
    startDateAccountCreated?: string;
    endDateAccountCreated?: string;
}): {
    accountId: string;
    dateAccountCreated: string;
}[];
declare const initializeAllAccountsForAccountIdVariableTableAlgorithm: {
    function: typeof initializeAllAccountsForAccountIdVariableTable;
};
declare const uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm: {
    function: typeof uninitializeAllAccountsForAccountIdVariableTableEventListener;
};
declare const getDigitalCurrencyAccountListByDateCreatedAlgorithm: {
    function: typeof getDigitalCurrencyAccountListByDateCreated;
};
export { variableTable, uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm, initializeAllAccountsForAccountIdVariableTableAlgorithm, getDigitalCurrencyAccountListByDateCreatedAlgorithm };
