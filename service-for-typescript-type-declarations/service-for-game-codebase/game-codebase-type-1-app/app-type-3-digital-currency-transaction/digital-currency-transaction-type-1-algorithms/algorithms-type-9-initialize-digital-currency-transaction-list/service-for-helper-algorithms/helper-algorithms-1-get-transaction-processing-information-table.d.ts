import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function getTransactionProcessingInformationTable(digitalCurrencyTransaction: DigitalCurrencyTransaction): {
    dateTransactionRecurred: Date;
    dateTransactionScheduledToBeProcessed: Date;
    dateTransactionRecurrenceRestarted: Date;
    dateTransactionRecurredTimePartition: {
        millisecondDay: number;
        millisecondHour: number;
        millisecondMinute: number;
        millisecondSecond: number;
    };
    dateTransactionScheduledToBeProcessedTimePartition: {
        millisecondDay: number;
        millisecondHour: number;
        millisecondMinute: number;
        millisecondSecond: number;
    };
    numberOfRecurrencesSinceRestarting: number;
};
declare const getTransactionProcessingInformationTableAlgorithm: {
    function: typeof getTransactionProcessingInformationTable;
};
export { getTransactionProcessingInformationTableAlgorithm };
