import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function getDigitalCurrencyTransactionById2FirebaseAdmin(digitalCurrencyTransactionId: string): Promise<DigitalCurrencyTransaction | null>;
declare const getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm: {
    function: typeof getDigitalCurrencyTransactionById2FirebaseAdmin;
};
export { getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm };
