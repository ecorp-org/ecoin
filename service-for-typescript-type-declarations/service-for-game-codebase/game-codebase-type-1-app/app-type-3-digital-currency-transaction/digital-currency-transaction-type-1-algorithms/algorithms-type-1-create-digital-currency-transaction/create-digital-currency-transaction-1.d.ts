import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function createDigitalCurrencyTransaction1(digitalCurrencyTransactionInformation: DigitalCurrencyTransaction): Promise<DigitalCurrencyTransaction>;
declare function updateNumberOfActiveTransactionsTimeTableForNetworkStatistics(date?: Date, isRecurringTransaction?: boolean): Promise<void>;
declare function updateNumberOfActiveTransactionsTimeTableForAccountStatistics(date?: Date, digitalCurrencyAccountId?: string, isRecurringTransaction?: boolean): Promise<void>;
export { createDigitalCurrencyTransaction1, updateNumberOfActiveTransactionsTimeTableForNetworkStatistics, updateNumberOfActiveTransactionsTimeTableForAccountStatistics };
