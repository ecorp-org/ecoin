import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function updateDigitalCurrencyAccount2FirebaseAdmin(digitalCurrencyTransactionId: string, propertyId: string, propertyValue: string, isSpecialTransaction?: boolean): Promise<DigitalCurrencyTransaction | null>;
export { updateDigitalCurrencyAccount2FirebaseAdmin };
