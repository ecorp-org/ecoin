declare class DigitalCurrencyAccount {
    accountId: string;
    firebaseAuthUid?: string;
    accountName: string;
    accountProfilePictureUrl: string;
    accountUsername: string;
    accountCurrencyAmount: number;
    dateAccountCreated: string;
    dateLastActive: string;
    constructor(defaultOption?: DigitalCurrencyAccount);
}
export { DigitalCurrencyAccount };
