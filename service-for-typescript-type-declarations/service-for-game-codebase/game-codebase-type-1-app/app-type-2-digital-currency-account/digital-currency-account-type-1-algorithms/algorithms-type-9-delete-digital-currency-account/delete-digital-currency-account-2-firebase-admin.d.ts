import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1';
declare function deleteDigitalCurrencyAccount2FirebaseAdmin(digitalCurrencyAccountId: string, forceDelete?: boolean): Promise<ServiceAccount | undefined>;
export { deleteDigitalCurrencyAccount2FirebaseAdmin };
