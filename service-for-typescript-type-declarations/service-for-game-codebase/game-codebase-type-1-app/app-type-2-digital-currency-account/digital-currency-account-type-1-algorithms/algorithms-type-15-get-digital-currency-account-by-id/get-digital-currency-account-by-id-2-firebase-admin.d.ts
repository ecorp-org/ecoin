import { DigitalCurrencyAccount } from '../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1';
declare function getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyAccountId: string): Promise<DigitalCurrencyAccount>;
export { getDigitalCurrencyAccountById2FirebaseAdmin };
