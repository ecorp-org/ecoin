declare function updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId: string, descriptionType: string, propertyId: string, propertyValue: any): Promise<any>;
declare function getPromotionTypeForDescriptionType(descriptionType: string): string;
export { updateDigitalCurrencyAccountDescription2FirebaseAdmin, getPromotionTypeForDescriptionType };
