declare function updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId: string, descriptionType: string, propertyId: string, propertyValue: any): Promise<any>;
declare function getPromotionTypeForDescriptionType(descriptionType: string): string;
export { updateDigitalCurrencyAccountDescription1, getPromotionTypeForDescriptionType };
