declare class InfiniteLoopProgram {
    infiniteLoopId: string;
    isInfiniteLoopStarted: boolean;
    dateStarted?: Date;
    iterationIndex: number;
    millisecondRegularDelayTime: number;
    millisecondDelayToRestartSetInterval: number;
    specifiedStartDate: Date;
    millisecondTimeLeftToStart: number;
    timeoutId: any;
    intervalId: any;
    restartSetIntervalIntervalId: any;
    initialAlgorithmFunctionArgumentList: any[];
    functionInvocationPromiseResultList: Promise<any>[];
    algorithmFunction: Function;
    constructor(millisecondRegularDelayTime?: number);
}
export { InfiniteLoopProgram };
