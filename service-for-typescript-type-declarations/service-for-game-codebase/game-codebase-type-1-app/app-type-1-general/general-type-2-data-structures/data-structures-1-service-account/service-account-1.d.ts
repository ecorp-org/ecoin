declare class ServiceAccount {
    serviceAccountId: string;
    activeDigitalCurrencyAccountId: string;
    digitalCurrencyAccountIdTable: {
        [accountId: string]: boolean;
    };
    numberOfDigitalCurrencyAccounts: number;
    isAnonymousAccount: boolean;
    isEmailVerified: boolean;
    emailAddress: string;
}
export { ServiceAccount };
