import { InitializeSearchIndexOptions } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-2-data-structures/data-structures-2-initialize-search-index-options';
declare class ResourceInformation {
    distributionType?: string;
    firebaseUseEmulator?: boolean;
    algoliasearchItemSearchIndexNameToIndexIdMap?: {
        [indexName: string]: string;
    };
    algoliasearchItemSearchIndexNameToOptionsMap?: {
        [indexName: string]: InitializeSearchIndexOptions;
    };
}
export { ResourceInformation };
