import { InfiniteLoopProgram } from '../../general-type-2-data-structures/data-structures-3-infinite-loop/infinite-loop-1';
declare function startInfiniteLoopProgram(infiniteLoopAlgorithmFunction: Function, startDate: Date, millisecondRegularDelayTime: number, initialAlgorithmFunctionArgumentList?: any[], sequentializeAsyncFunction?: boolean, millisecondDelayToRestartSetInterval?: number): InfiniteLoopProgram;
export { startInfiniteLoopProgram };
