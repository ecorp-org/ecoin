declare function getMillisecondTimePartitionList1(millisecondDateTime?: number): {
    millisecondDay: number;
    millisecondHour: number;
    millisecondMinute: number;
    millisecondSecond: number;
};
declare function getMillisecondTimePartitionList2(isoDateString?: string): {
    millisecondDay: number;
    millisecondHour: number;
    millisecondMinute: number;
    millisecondSecond: number;
};
export { getMillisecondTimePartitionList1, getMillisecondTimePartitionList2 };
