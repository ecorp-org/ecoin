import { DigitalCurrencyTransaction } from '../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare const numberOfDecimalPlacesAllowed = 10;
declare const universalBasicIncomeStartDate: Date;
declare const millisecondDelayBetweenUniversalBasicIncomeDistribution: number;
declare const universalBasicIncomeAmountDistributed = 1;
declare const universalBasicIncomeTextMemorandum: string;
declare const universalBasicIncomeTransaction: DigitalCurrencyTransaction;
declare const specialTransactionVariableTable: {
    [digitalCurrencyTransactionId: string]: DigitalCurrencyTransaction;
};
declare const numberOfSpecialTransactions: number;
export { numberOfDecimalPlacesAllowed, universalBasicIncomeStartDate, millisecondDelayBetweenUniversalBasicIncomeDistribution, universalBasicIncomeAmountDistributed, universalBasicIncomeTextMemorandum, universalBasicIncomeTransaction, specialTransactionVariableTable, numberOfSpecialTransactions };
