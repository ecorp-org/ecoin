declare const projectVariables: {
    projectId: string;
    projectName: string;
    projectOrganizationId: string;
    projectOrganizationName: string;
    projectNameIdentifierGroup: {};
    projectWebsiteIdentifierGroup: {};
    projectDirectoryStaticFileHostBaseUrl: string;
    projectImageIconUrl: string;
    projectImageIconUrl2: string;
    projectJavaScriptLibraryId: string;
    textDescriptionGroup: {};
    projectWebsiteUrlTable: {
        userServiceWebsiteList: string[];
        donateUrl: string;
    };
    projectVideoIntroductionUrl: string;
    companyPoweredByCompanyImageIconUrl: string;
    digitalCurrencyId: string;
    digitalCurrencyName: string;
    digitalCurrencySymbolImageIconUrl: string;
    digitalCurrencyAccountDefaultThumbnailImageIconUrl: string;
    digitalCurrencyAccountDefaultBackgroundImageForColorfulEffectImageIconUrl: string;
    digitalCurrencyAcceptedHereImageUrl: string;
};
declare const socialMediaVariables: {
    gitlab: {
        imageIconUrl: string;
        resourceUrl: string;
    };
    youtube: {
        imageIconUrl: string;
        resourceUrl: string;
    };
    twitter: {
        imageIconUrl: string;
        resourceUrl: string;
    };
};
declare const customMessages: {
    thankYouForJoiningUniversalBasicIncome: string;
};
export { projectVariables, socialMediaVariables, customMessages };
