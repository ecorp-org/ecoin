declare const firebaseAdminDevelopmentConfig: {
    serviceAccountId: string;
    apiKey: string;
    authDomain: string;
    databaseURL: string;
    projectId: string;
    storageBucket: string;
};
export { firebaseAdminDevelopmentConfig };
