/// <reference types="node" />
import express from 'express';
import http from 'http';
import * as WebSocket from 'websocket';
import SimplePeer from 'simple-peer';
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import 'firebase/storage';
import * as firebaseAdmin from 'firebase-admin';
declare class DatabaseConstructorItem {
    activate: typeof databaseConstructorItemActivateAlgorithm;
}
declare class EventListenerConstructorItem {
    activate: typeof databaseConstructorItemActivateAlgorithm;
}
declare class PermissionRuleConstructorItem {
    activate: typeof databaseConstructorItemActivateAlgorithm;
}
declare class NetworkProtocolConstructorItem {
    activate: typeof databaseConstructorItemActivateAlgorithm;
}
declare class ScaleMatrixAmanaConstructorItem {
    activate: typeof databaseConstructorItemActivateAlgorithm;
}
declare class InteractiveGraphConstructorItem {
    activate: typeof databaseConstructorItemActivateAlgorithm;
}
declare class ComputerProgramManagerConstructorItem {
    activate: typeof databaseConstructorItemActivateAlgorithm;
}
declare class DatabaseConstructorItemActivateFunctionItemDataStructure<OriginalItemDataStructure> {
    originalItem: OriginalItemDataStructure;
    copyableItem: OriginalItemDataStructure;
    runableItem: Function;
    secondRunableItem: Function;
    emergencyCatchUpItem: Function;
    carryForthTheOperationAndPerformThisListOfTasksAsWell: Function;
    constructor(originalItem: OriginalItemDataStructure, copyableItem: OriginalItemDataStructure, runableItem: Function, secondRunableItem: Function, emergencyCatchUpItem: Function, carryForthTheOperationAndPerformThisListOfTasksAsWell: Function);
}
declare const itemOfInterestList: (typeof DatabaseConstructorItem)[];
declare function createItem(itemId: string, item: DatabaseConstructorItemActivateFunctionItemDataStructure<any>, itemOfInterestCount?: number): Promise<any>;
declare function updateItem(_itemId: string, _itemRecoveryId?: string, _item?: DatabaseConstructorItemActivateFunctionItemDataStructure<any>): Promise<any>;
declare function getItem(_itemId: string, _itemRecoveryId?: string): Promise<any>;
declare function deleteItem(_itemId: string, _itemRecoveryId?: string): Promise<any>;
declare const DATABASE_INTERCEPTING_MESSENGER_SERVICE_ID_CONSTANT_ID = "interceptingMessengerServiceId";
declare const DATABASE_DIRECT_INDIVIDUAL_MESSENGER_SERVICE_ID_CONSTANT_ID = "directIndividualMessengerServiceId";
declare const DATABASE_INITIAL_TEMPORARY_PASSWORD = "password";
declare const DATABASE_EXPECTING_MANY_UPDATES_CONSTANT_ID = "expectingManyUpdates";
declare const PLACE_ID_TYPE_FIREBASE = "firebase";
declare const FIREBASE_VERSION_7_API_KEY_CONSTANT_ID = "firebaseApiKey";
declare const FIREBASE_VERSION_7_AUTH_DOMAIN_CONSTANT_ID = "firebaseAuthDomain";
declare const FIREBASE_VERSION_7_DATABASE_URL_CONSTANT_ID = "firebaseDatabaseURL";
declare const FIREBASE_VERSION_7_PROJECT_ID_CONSTANT_ID = "firebaseProjectId";
declare const FIREBASE_VERSION_7_CLOUD_STORAGE_BUCKET_CONSTANT_ID = "firebaseStorageBucket";
declare const FIREBASE_VERSION_7_MESSAGING_SENDER_ID_CONSTANT_ID = "firebaseMessagingSenderId";
declare const FIREBASE_VERSION_7_APP_ID_CONSTANT_ID = "firebaseAppId";
declare const FIREBASE_VERSION_7_MEASUREMENT_ID_CONSTANT_ID = "firebaseMeasurementId";
declare const FIREBASE_VERSION_7_REALTIME_DATABASE_SELECTED_CONSTANT_ID = "selectRealtimeDatabase";
declare const FIREBASE_VERSION_7_CLOUD_STORAGE_SELECTED_CONSTANT_ID = "selectStorage";
declare const FIREBASE_VERSION_7_CLOUD_FIRESTORE_SELECTED_CONSTANT_ID = "selectCloudFirestore";
declare const PLACE_ID_TYPE_FIREBASE_ADMIN = "firebase-admin";
declare const FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CERTIFICATE_CONSTANT_ID = "firebaseAdminCredentialCertificate";
declare const FIREBASE_ADMIN_VERSION_8_CREDENTIAL_REFRESH_TOKEN_CONSTANT_ID = "firebaseAdminCredentialRefreshToken";
declare const FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CONSTANT_ID = "firebaseAdminCredential";
declare const FIREBASE_ADMIN_VERSION_8_SERVICE_ACCOUNT_ID_CONSTANT_ID = "firebaseAdminServiceAccountId";
declare const FIREBASE_ADMIN_VERSION_8_API_KEY_CONSTANT_ID = "firebaseAdminAPIKey";
declare const FIREBASE_ADMIN_VERSION_8_AUTH_DOMAIN_CONSTANT_ID = "firebaseAdminAuthDomain";
declare const FIREBASE_ADMIN_VERSION_8_DATABASE_URL_CONSTANT_ID = "firebaseAdminDatabaseURL";
declare const FIREBASE_ADMIN_VERSION_8_PROJECT_ID_CONSTANT_ID = "firebaseAdminProjectId";
declare const FIREBASE_ADMIN_VERSION_8_STORAGE_BUCKET_CONSTANT_ID = "firebaseAdminStorageBucket";
declare const FIREBASE_ADMIN_VERSION_8_MESSAGING_SENDER_ID_CONSTANT_ID = "firebaseAdminMessagingSenderId";
declare const FIREBASE_ADMIN_VERSION_8_APP_ID_CONSTANT_ID = "firebaseAdminAppId";
declare const FIREBASE_ADMIN_VERSION_8_MEASUREMENT_ID_CONSTANT_ID = "firebaseAdminMeasurementId";
declare const FIREBASE_ADMIN_VERSION_8_REALTIME_DATABASE_SELECTED_CONSTANT_ID = "selectRealtimeDatabase";
declare const FIREBASE_ADMIN_VERSION_8_STORAGE_SELECTED_CONSTANT_ID = "selectStorage";
declare const PLACE_ID_TYPE_FIREBASE_TESTING = "firebase-testing";
declare const FIREBASE_TESTING_VERSION_0_DATABASE_NAME_CONSTANT_ID = "firebaseTestingDatabaseName";
declare const FIREBASE_TESTING_VERSION_0_AUTH_UID_CONSTANT_ID = "firebaseTestingAuthUID";
declare const FIREBASE_TESTING_VERSION_0_PROJECT_ID_CONSTANT_ID = "firebaseTestingProjectId";
declare const FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_RULES_CONSTANT_ID = "firebaseTestingRealtimeDatabaseRules";
declare const FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_SELECTED_CONSTANT_ID = "selectFirebaseRealtimeDatabase";
declare const FIREBASE_TESTING_VERSION_0_FIREBASE_ADMIN_REALTIME_DATABASE_SELECTED_CONSTANT_ID = "selectFirebaseAdminRealtimeDatabase";
declare const PLACE_ID_TYPE_LOCAL_FORAGE = "localforage";
declare const LOCALFORAGE_VERSION_1_DATABASE_NAME_CONSTANT_ID = "localForageDatabaseName";
declare const LOCALFORAGE_VERSION_1_DATABASE_STORE_NAME_LIST_CONSTANT_ID = "localForageDatabaseStoreNameList";
declare const LOCALFORAGE_VERSION_1_DATABASE_PASSWORD_CONSTANT_ID = "localForageDatabasePassword";
declare const localMemoryDatabase: {
    [localMemoryAddressId: string]: any;
};
declare function databaseConstructorItemActivateAlgorithm(itemId: string, item: DatabaseConstructorItemActivateFunctionItemDataStructure<any>): Promise<void>;
declare function createAnonymousFunction(httpWebAddressPortNumber?: number): {
    webServerResponse: {
        webServerResponse: {
            httpServer: import("express-serve-static-core").Express;
            httpServerByHttp: http.Server;
        };
    };
};
declare function startSelfCodebaseDeveloper(): void;
declare function isTheCurrentComputerDoingPoorly(): void;
declare function startWebServer(_information?: any): {
    webServerResponse: {
        httpServer: import("express-serve-static-core").Express;
        httpServerByHttp: http.Server;
    };
};
declare function createWebServer(): {
    httpServer: import("express-serve-static-core").Express;
    httpServerByHttp: http.Server;
};
declare function createWebServerWebSocketAddon(websocketServerConfig?: WebSocket.IServerConfig): {
    websocketServer: WebSocket.server;
};
declare function createWebServerWebSocketWebRTCRelayComputerProgram(_httpServer: express.Express, _websocketServer: WebSocket.server): {
    webRTCClientRequestTable: {};
    webRTCClientResponseTable: {};
    webRTCClientRequestFailedTable: {};
    webRTCClientResponseFailedTable: {};
    administratorInformation: {};
};
declare const WEB_ADDRESS_CONNECTION_TYPE_WEBRTC = "WEB_RTC_CONNECTION";
declare const WEB_ADDRESS_CONNECTION_TYPE_WEBSOCKET = "WEBSOCKET_CONNECTION";
declare const WEB_ADDRESS_CONNECTION_TYPE_HTTP = "HTTP_CONNECTION";
declare function registerForCreatingAWebAddress(isAllowedConnectionTypeTable: {
    [connectionTypeId: string]: boolean;
}, isConnectionInitiator?: boolean): {
    webAddressTable: {
        [webAddressIdentity: string]: any;
    };
    webAddressTypeToIdentityMap: {
        [webAddressType: string]: any;
    };
};
declare function registerForCreatingAWebRTCWebAddress(isRequestCreatorOrInitiator?: boolean, onConnectMessageCreated?: Function, onDataMessageCreated?: Function, onStreamMessageCreated?: Function, onTrackMessageCreated?: Function, onCloseMessageCreated?: Function, onErrorMessageCreated?: Function): {
    simplePeer: SimplePeer.Instance;
    webRTCSignalMessageAsWebAddress: any;
};
declare function createWebSocketWebRTCConnection(_targetWebAddressIdentity: string, _connectionType?: string): void;
declare function createNAryDeviceRelayComputerProgram(): void;
declare function createWebpackReadyComputerProgram(): void;
declare function createWebServerReadyComputerProgram(): void;
declare function startWebServerDeveloper(_information: any): void;
declare function startVistaAttackResiliance(_information: any): void;
declare function performIncerceptingMessagingProtocol(interceptingMessengerServiceId: string, directIndividualMessengerServiceId: string, expectingManyUpdates: boolean): Promise<unknown>;
declare function startNetworkConnection(_information: any): void;
declare function getFirebaseInformation(generalProtocolAddress: string): any;
declare function firebaseRealtimeDatabasePublish(firebaseRealtimeDatabase: any, firebaseItemAddress: string, firebaseItem: any): Promise<any>;
declare function firebaseStoragePublish(firebaseCloudStorage: any, firebaseItemAddress: string, firebaseItem: any): Promise<any>;
declare function getFirebaseAdminInformation(generalProtocolAddress: string): {
    firebaseAdminApp: firebaseAdmin.app.App;
    firebaseAdminProtocolPlace: any;
    firebaseAdminProtocolAddress: any;
    firebaseAdminProtocolPathList: any;
    firebaseAdminProtocolQueryParameterObjectDataStructure: any;
};
declare function firebaseAdminRealtimeDatabasePublish(firebaseAdminRealtimeDatabase: any, firebaseAdminItemAddress: string, firebaseAdminItem: any): Promise<any>;
declare function firebaseAdminStoragePublish(firebaseAdminStorage: any, firebaseAdminItemAddress: string, firebaseAdminItem: any): Promise<any>;
declare function getFirebaseTestingInformation(generalProtocolAddress: string): Promise<any>;
declare function getLocalForageInformation(generalProtocolAddress: string): Promise<{
    localForageDatabaseName: any;
    localForageDatabaseStoreNameList: any;
    localForageDatabasePassword: any;
    referenceTable: LocalForage | null;
    informationTable: {
        [informationTableId: string]: LocalForage;
    };
    localForageProtocolPlace: any;
    localForageProtocolAddress: any;
    localForageProtocolPathList: any;
    localForageProtocolQueryParameterObjectDataStructure: any;
}>;
declare function localForageDatabasePublish(localForageDatabaseStore: any, localForageItemAddress: string, localForageItem: any): Promise<any>;
declare function getComputerIdentity(additiveAmount?: number): string;
export { DatabaseConstructorItem, EventListenerConstructorItem, PermissionRuleConstructorItem, NetworkProtocolConstructorItem, ScaleMatrixAmanaConstructorItem, InteractiveGraphConstructorItem, ComputerProgramManagerConstructorItem, DatabaseConstructorItemActivateFunctionItemDataStructure, itemOfInterestList, createItem, updateItem, getItem, deleteItem, DATABASE_INTERCEPTING_MESSENGER_SERVICE_ID_CONSTANT_ID, DATABASE_DIRECT_INDIVIDUAL_MESSENGER_SERVICE_ID_CONSTANT_ID, DATABASE_INITIAL_TEMPORARY_PASSWORD, DATABASE_EXPECTING_MANY_UPDATES_CONSTANT_ID, PLACE_ID_TYPE_FIREBASE, FIREBASE_VERSION_7_API_KEY_CONSTANT_ID, FIREBASE_VERSION_7_AUTH_DOMAIN_CONSTANT_ID, FIREBASE_VERSION_7_DATABASE_URL_CONSTANT_ID, FIREBASE_VERSION_7_PROJECT_ID_CONSTANT_ID, FIREBASE_VERSION_7_CLOUD_STORAGE_BUCKET_CONSTANT_ID, FIREBASE_VERSION_7_MESSAGING_SENDER_ID_CONSTANT_ID, FIREBASE_VERSION_7_APP_ID_CONSTANT_ID, FIREBASE_VERSION_7_MEASUREMENT_ID_CONSTANT_ID, FIREBASE_VERSION_7_REALTIME_DATABASE_SELECTED_CONSTANT_ID, FIREBASE_VERSION_7_CLOUD_STORAGE_SELECTED_CONSTANT_ID, FIREBASE_VERSION_7_CLOUD_FIRESTORE_SELECTED_CONSTANT_ID, PLACE_ID_TYPE_FIREBASE_ADMIN, FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CERTIFICATE_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_CREDENTIAL_REFRESH_TOKEN_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_SERVICE_ACCOUNT_ID_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_API_KEY_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_AUTH_DOMAIN_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_DATABASE_URL_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_PROJECT_ID_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_STORAGE_BUCKET_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_MESSAGING_SENDER_ID_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_APP_ID_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_MEASUREMENT_ID_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_REALTIME_DATABASE_SELECTED_CONSTANT_ID, FIREBASE_ADMIN_VERSION_8_STORAGE_SELECTED_CONSTANT_ID, PLACE_ID_TYPE_FIREBASE_TESTING, FIREBASE_TESTING_VERSION_0_DATABASE_NAME_CONSTANT_ID, FIREBASE_TESTING_VERSION_0_AUTH_UID_CONSTANT_ID, FIREBASE_TESTING_VERSION_0_PROJECT_ID_CONSTANT_ID, FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_RULES_CONSTANT_ID, FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_SELECTED_CONSTANT_ID, FIREBASE_TESTING_VERSION_0_FIREBASE_ADMIN_REALTIME_DATABASE_SELECTED_CONSTANT_ID, PLACE_ID_TYPE_LOCAL_FORAGE, LOCALFORAGE_VERSION_1_DATABASE_NAME_CONSTANT_ID, LOCALFORAGE_VERSION_1_DATABASE_STORE_NAME_LIST_CONSTANT_ID, LOCALFORAGE_VERSION_1_DATABASE_PASSWORD_CONSTANT_ID, localMemoryDatabase, createAnonymousFunction, startSelfCodebaseDeveloper, isTheCurrentComputerDoingPoorly, startWebServer, createWebServer, createWebServerWebSocketAddon, createWebServerWebSocketWebRTCRelayComputerProgram, WEB_ADDRESS_CONNECTION_TYPE_WEBRTC, WEB_ADDRESS_CONNECTION_TYPE_WEBSOCKET, WEB_ADDRESS_CONNECTION_TYPE_HTTP, registerForCreatingAWebAddress, registerForCreatingAWebRTCWebAddress, createWebSocketWebRTCConnection, createNAryDeviceRelayComputerProgram, createWebpackReadyComputerProgram, createWebServerReadyComputerProgram, startWebServerDeveloper, startVistaAttackResiliance, performIncerceptingMessagingProtocol, startNetworkConnection, databaseConstructorItemActivateAlgorithm, getFirebaseInformation, firebaseRealtimeDatabasePublish, firebaseStoragePublish, getFirebaseAdminInformation, firebaseAdminRealtimeDatabasePublish, firebaseAdminStoragePublish, getFirebaseTestingInformation, getLocalForageInformation, localForageDatabasePublish, getComputerIdentity, firebase };
