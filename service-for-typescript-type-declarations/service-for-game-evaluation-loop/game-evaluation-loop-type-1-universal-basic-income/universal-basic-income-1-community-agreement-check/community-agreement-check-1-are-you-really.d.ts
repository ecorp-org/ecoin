declare class PortableCryptographyAuthenticationKey {
    cryptographicPublicKey: string;
    cryptographicPrivateKey: string;
    providerServiceId: string;
    providerAuthenticationId: string;
    providerAuthenticationInformationTable: any;
    cryptographyPublicKeyAlgorithmId: string;
    cryptographyPublicKeyCryptographyInformationTable: any;
    individualId: string;
    individualName: string;
    individualInformationTable: any;
    individualRecoverableInformationDirectoryTable: any;
    serviceResourceOriginalLocationUrl: string;
}
declare function createAuthenticatorKey(individualId: string, individualName: string, individualInformationTable: any, isCryptographicAuthenticatorKey: boolean, nameHere: boolean, providerServiceId: string, keepTrackOfThisKey: boolean, authenticatorKeyPermissionRuleTable: any): Promise<PortableCryptographyAuthenticationKey>;
export { PortableCryptographyAuthenticationKey, createAuthenticatorKey };
