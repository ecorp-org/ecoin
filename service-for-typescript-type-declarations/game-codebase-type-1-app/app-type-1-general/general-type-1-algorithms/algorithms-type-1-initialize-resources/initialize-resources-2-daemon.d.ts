import { ResourceInformation } from '../../general-type-2-data-structures/data-structures-2-resource-information/resource-information-1';
declare function initializeResources2Daemon(resourceInformation: ResourceInformation): Promise<void>;
export { initializeResources2Daemon };
