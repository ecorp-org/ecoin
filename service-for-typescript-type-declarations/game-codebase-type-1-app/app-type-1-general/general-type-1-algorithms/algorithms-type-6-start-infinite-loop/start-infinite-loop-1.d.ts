import { InfiniteLoopProgram } from '../../general-type-2-data-structures/data-structures-3-infinite-loop/infinite-loop-1';
declare function startInfiniteLoopProgram(infiniteLoopAlgorithmFunction: Function, startDate: Date, millisecondRegularDelayTime: number, initialAlgorithmFunctionArgumentList?: any[], isAsyncSequence?: boolean): InfiniteLoopProgram;
export { startInfiniteLoopProgram };
