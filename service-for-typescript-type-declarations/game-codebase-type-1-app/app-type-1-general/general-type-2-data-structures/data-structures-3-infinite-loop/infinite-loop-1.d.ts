declare class InfiniteLoopProgram {
    infiniteLoopId: string;
    isInfiniteLoopStarted: boolean;
    dateStarted?: Date;
    millisecondRegularDelayTime: number;
    specifiedStartDate?: Date;
    millisecondTimeLeftToStart: number;
    timeoutId: any;
    intervalId: any;
    algorithmFunction: Function;
    constructor(millisecondRegularDelayTime?: number);
}
export { InfiniteLoopProgram };
