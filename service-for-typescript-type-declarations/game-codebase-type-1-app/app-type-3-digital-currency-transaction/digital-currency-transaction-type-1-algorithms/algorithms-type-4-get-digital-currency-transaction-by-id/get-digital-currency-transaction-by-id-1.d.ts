import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function getDigitalCurrencyTransactionById1(digitalCurrencyTransactionId: string): Promise<DigitalCurrencyTransaction>;
export { getDigitalCurrencyTransactionById1 };
