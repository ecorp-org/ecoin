import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function initializeDigitalCurrencyTransactionList(_untilMillisecondTimeAgo?: number): Promise<void>;
declare function initializeDigitalCurrencyTransactionForAllAccounts(_digitalCurrencyTransaction: DigitalCurrencyTransaction): Promise<void>;
declare function initializeAllAccountsForAccountIdVariableTable(): Promise<void>;
declare function initializeDigitalCurrencyTransaction(): void;
declare function initializeDigitalCurrencyAccountTransactionUpdate(): void;
export { initializeDigitalCurrencyTransactionList, initializeDigitalCurrencyTransactionForAllAccounts, initializeAllAccountsForAccountIdVariableTable, initializeDigitalCurrencyTransaction, initializeDigitalCurrencyAccountTransactionUpdate };
