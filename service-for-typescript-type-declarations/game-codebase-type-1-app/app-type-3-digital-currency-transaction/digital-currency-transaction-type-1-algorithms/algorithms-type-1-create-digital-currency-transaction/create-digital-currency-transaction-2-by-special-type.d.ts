import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1';
declare function createDigitalCurrencyTransaction2BySpecialType(digitalCurrencyTransactionInformation: DigitalCurrencyTransaction): Promise<DigitalCurrencyTransaction>;
export { createDigitalCurrencyTransaction2BySpecialType };
