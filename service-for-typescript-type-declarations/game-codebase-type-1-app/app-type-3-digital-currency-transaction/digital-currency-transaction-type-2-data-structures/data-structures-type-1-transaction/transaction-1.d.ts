declare class DigitalCurrencyTransaction {
    transactionId: string;
    transactionOrderIndex: number;
    accountSenderId: string;
    accountRecipientIdVariableTable: {
        [accountId: string]: {
            transactionCurrencyAmount: number;
        };
    };
    onBehalfOfAccountId: string;
    transactionCurrencyAmount: number;
    transactionType: string;
    textMemorandum?: string;
    specialTransactionType?: string;
    dateTransactionCreated?: number;
    dateTransactionProcessed?: number;
    dateTransactionScheduledForProcessing?: number;
    isScheduledForLaterProcessing: boolean;
    isProcessed: boolean;
    isRecurringTransaction: boolean;
    millisecondRecurringDelay: number;
    numberOfTimesRecurred: number;
    dateRecurrenceCancelled?: number;
    originalTimeNumberUntilNextRecurrence?: number;
    originalTimeScaleUntilNextRecurrence?: string;
    numberOfTimesProcessingFailed?: number;
    latestReasonForProcessingFailed?: string;
    constructor(defaultOption?: DigitalCurrencyTransaction);
}
declare const SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME = "SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME";
export { DigitalCurrencyTransaction, SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME };
