
import { projectVariables } from './project-variables-1-introduction'
// import { getTimePeriodScaleFromMillisecondDuration } from '../../../service-for-game-introduction/game-introduction-type-1-web-browser-introduction/web-browser-introduction-type-1-customer-website/src/service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-1-basic/basic-type-2-digital-currency/service-for-helper-algorithms/helper-algorithms-3-get-time-period-scale-from-millisecond-duration'

import {
  DigitalCurrencyTransaction,
  SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME
} from '../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

// import {
//   NUMBER_OF_MILLISECONDS_IN_A_MINUTE
// } from '../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-3-constants/constants-3-time'

import {
 administratorVariables
} from '../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-3-constants/constants-4-administrator'

// import { getUTCDate1 } from '../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'


// Digital Currency Variables

const numberOfDecimalPlacesAllowed = 10




// Universal basic income
// const universalBasicIncomeStartDate = new Date('Sat Jul 04 2020 00:00:00 GMT-0500 (Central Daylight Time)')
const universalBasicIncomeStartDate = new Date('2021-11-09T10:00:00Z')
// const millisecondDelayBetweenUniversalBasicIncomeDistribution = 1 * 60 * 1000 // 1 minutes
const millisecondDelayBetweenUniversalBasicIncomeDistribution = 24 * 60 * 60 * 1000 // 24 hours
const universalBasicIncomeAmountDistributed = 1 // 1 digital currency amount
// const universalBasicIncomeDistributionTimeScale = getTimePeriodScaleFromMillisecondDuration(millisecondDelayBetweenUniversalBasicIncomeDistribution)
const universalBasicIncomeTextMemorandum = `
A Universal Basic Income transaction for the ${projectVariables.projectName} network.
`

const universalBasicIncomeTransaction = new DigitalCurrencyTransaction()
universalBasicIncomeTransaction.transactionId = `universal-basic-income`
universalBasicIncomeTransaction.transactionName = 'Universal Basic Income'
universalBasicIncomeTransaction.accountSenderId = administratorVariables.ADMINISTRATOR_ACCOUNT_ID
universalBasicIncomeTransaction.specialTransactionType = SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME
universalBasicIncomeTransaction.transactionCurrencyAmount = universalBasicIncomeAmountDistributed
universalBasicIncomeTransaction.isRecurringTransaction = true
universalBasicIncomeTransaction.dateTransactionCreated = universalBasicIncomeStartDate.toISOString()
universalBasicIncomeTransaction.transactionType = 'Universal Basic Income'
universalBasicIncomeTransaction.textMemorandum = universalBasicIncomeTextMemorandum
universalBasicIncomeTransaction.millisecondRecurringDelay = millisecondDelayBetweenUniversalBasicIncomeDistribution
universalBasicIncomeTransaction.originalTimeNumberUntilNextRecurrence = 1
universalBasicIncomeTransaction.originalTimeScaleUntilNextRecurrence = 'Minute'


// Special Transactions

const specialTransactionVariableTable: { [digitalCurrencyTransactionId: string]: DigitalCurrencyTransaction } = {
  [universalBasicIncomeTransaction.transactionId]: universalBasicIncomeTransaction
}

const numberOfSpecialTransactions = Object.keys(specialTransactionVariableTable || {}).length


export {
  numberOfDecimalPlacesAllowed,

  universalBasicIncomeStartDate,
  millisecondDelayBetweenUniversalBasicIncomeDistribution,
  universalBasicIncomeAmountDistributed,
  universalBasicIncomeTextMemorandum,

  universalBasicIncomeTransaction,
  specialTransactionVariableTable,
  numberOfSpecialTransactions
}




