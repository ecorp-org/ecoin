

// (1) Your Account Page Questions

// Question (1.1)

const whyDoesEcoinExist = {
  question: `Why does {{appName}} exist?`,
  answer: `
  <b>Short Answer</b>
  <br><br>
  {{appName}} exists to support creators like
  <a style="text-decoration: none" href="https://www.youtube.com/watch?v=lBIdk-fgCeQ" target="_blank"><b>Jacque Fresco</b></a>
  and
  <a style="text-decoration: none" href="https://www.youtube.com/watch?v=d6Rgbw2ZzPY" target="_blank"><b>Roxanne Meadows</b></a>
  in achieving their wildest dreams. <b>(*)</b>
  <br><br>
  <b>Long Answer</b>
  <br><br>
  A great <b>majority</b> of people today believe that money is the way of life --
  that money has to be the way of living.
  <br><br>
  <a style="text-decoration: none" href="https://www.youtube.com/watch?v=gBY_zADXjak" target="_blank">
    <b>The Venus Project</b>
  </a>
  offers an alternative way of living without the use of money.
  A vast <b>minority</b> of people today believe that an alternative exists.
  <br><br>
  <a style="text-decoration: none" href="https://www.youtube.com/watch?v=lBIdk-fgCeQ" target="_blank">
    <b>The Venus Project</b>
  </a>
  is based on the idea that <b>everyone</b> can achieve abundance in living.
  <br><br>
  The money-based system is based on the idea that trade can benefit
  <b>EVERYONE</b> in a transaction.
  <br><br>
  {{appName}} is a money-based system that <b>strives to achieve</b> abundance
  as an practical ideal for everyone.
  <br><br>
  <b>(*)</b> A short answer like this is meant to inspire you to learn more about
  the life and work of these people.😃
  `
}

// Question (1.2)
const howToReceiveDigitalCurrencyPayments = {
  // question: `How can I add the "Pay with {{appName}} button" to my personal or business website?`,
  question: `How can I accept {{appName}} payments on my personal or business website?`,
  answer: `
  You can start receiving payments with Ecoin from your website by integrating with the
  <a style="text-decoration: none" href="https://www.npmjs.com/package/@ecorp-org/ecoin" target="_blank">
    <b>{{appName}} javascript library</b>
  </a>
  <br><br>
  You can use the JavaScript Library to:
  <br><br>
  (1) Add the "Pay with {{appName}}" button to your website (using the <b>createHtmlComponent</b> algorithm)
  <br><br>
  (2) Call the <b>createTransaction</b> algorithm on behalf of signed in users to your website (signed in users require <b>initializeServiceAccount</b> to be called)
  `
}
/*
answer: `
You add the "<b>Pay with {{appName}} button</b>" to your person or business website by integrating
with the
<a style="text-decoration: none" href="https://www.npmjs.com/package/@ecorp-org/ecoin" target="_blank">
  <b>{{appName}} javascript library</b>
</a>
`
*/


// Question (1.3)
const whatAreTheBenefitsOfUsingThisDigitalCurrency = {
  question: `What are the Benefits of using this Digital Currency?`,
  answer: `
  You can use {{appName}} to <b>send and receive a common currency of exchange</b>.
  <br><br>
  You can send and receive {{appName}} payments from:
  <br><br>
  <ul>
    <li>customers</li>
    <li>clients</li>
    <li>donators</li>
    <li>sponsors</li>
    <li>subscribers</li>
    <li>investors</li>
    <li>employers</li>
  </ul>
  <br>
  and all sorts of individuals from various social and financial
  backgrounds.
  `
}

const yourAccountFrequentlyAskedQuestionList = [
  whyDoesEcoinExist,
  howToReceiveDigitalCurrencyPayments,
  whatAreTheBenefitsOfUsingThisDigitalCurrency
]



export {
  yourAccountFrequentlyAskedQuestionList
}





