
import { projectVariables } from './project-variables-1-introduction'

import { yourAccountFrequentlyAskedQuestionList } from './project-variables-3-frequently-asked-questions'



// Your Account Information Page
const YOUR_ACCOUNT_PAGE_INFORMATION = {
  uid: 'your-account-page',
  name: 'Your Account',
  icon: 'person',
  routePath: '/',
  description: 'View your account information',
  pageComponent: undefined,
  faqList: yourAccountFrequentlyAskedQuestionList
}

// Network Status Information Page

const NETWORK_STATUS_PAGE_INFORMATION = {
  uid: 'network-status-page',
  name: 'Network Status',
  icon: 'stars', // star, star_border, stars, trending_up
  routePath: '/network-status',
  optionList: [{
    name: 'Currency Status',
    routePath: '/status'
  }, {
    name: 'Network Status',
    routePath: '/status/network'
  }],
  shortDescription: `Resources about the ${projectVariables.projectName}
currency and network related statistics`
}

// Settings Page Information

// layout icon ideas: view_carousel, view_column, view_array, view_[something]
const SETTINGS_PAGE_INFORMATION = {
  uid: 'settings-page',
  name: 'Settings',
  icon: 'settings',
  routePath: '/settings',
  showBottomBorder: true,
  description: 'Update your information',
  optionList: [],
  shortDescription: `Resources about how to update your account`
}

// About Page Information

const ABOUT_PAGE_INFORMATION = {
  uid: 'about-page',
  name: 'About',
  icon: 'info_outline', // not_listed_location
  routePath: '/about',
  description: 'Learn more about this project',
  optionList: []
}



// Web Page Information List
const WEB_PAGE_INFORMATION_LIST = [
  YOUR_ACCOUNT_PAGE_INFORMATION,
  NETWORK_STATUS_PAGE_INFORMATION,
  SETTINGS_PAGE_INFORMATION,
  ABOUT_PAGE_INFORMATION
]

export {
  WEB_PAGE_INFORMATION_LIST,
  YOUR_ACCOUNT_PAGE_INFORMATION,
  NETWORK_STATUS_PAGE_INFORMATION,
  SETTINGS_PAGE_INFORMATION,
  ABOUT_PAGE_INFORMATION
}


