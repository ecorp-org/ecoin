
const projectVariables = {
  projectId: 'ecoin',
  projectName: 'Ecoin',
  projectOrganizationId: 'ecorp',
  projectOrganizationName: 'Ecorp',
  projectNameIdentifierGroup: {},
  projectWebsiteIdentifierGroup: {},

  projectDirectoryStaticFileHostBaseUrl: 'https://ecoin-static-file-host.web.app',
  projectImageIconUrl: `https://ecoin-static-file-host.web.app/service-for-game-introduction/game-introduction-type-1-web-browser-introduction/web-browser-introduction-type-1-customer-website/public/img/icons/android-chrome-512x512.png`,
  projectImageIconUrl2: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-1-digital-currency/digital-currency-4-digital-currency-logo.png`,

  projectJavaScriptLibraryId: 'ecoin',
  textDescriptionGroup: {},

  projectWebsiteUrlTable: {
    userServiceWebsiteList: [
      'https://ecoin369.web.app'
    ],
    donateUrl: 'https://paypal.me/ecoin369'
  },

  projectVideoIntroductionUrl: '',

  // Powered By Company Image Icon
  companyPoweredByCompanyImageIconUrl: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-2-company-information/company-information-1-powered-by-company-image-icon.png`,

  // Digital Currency Related Symbols

  digitalCurrencyId: 'ecoin',
  digitalCurrencyName: 'Ecoin',
  digitalCurrencySymbolImageIconUrl: `https://ecoin-static-file-host.web.app/service-for-game-introduction/game-introduction-type-1-web-browser-introduction/web-browser-introduction-type-1-customer-website/public/img/icons/android-chrome-512x512.png`,

  // Digital Currency Account Related Things
  digitalCurrencyAccountDefaultThumbnailImageIconUrl: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-1-digital-currency/digital-currency-2-digital-currency-account-default-thumbnail-image-icon.jpg`,
  digitalCurrencyAccountDefaultBackgroundImageForColorfulEffectImageIconUrl: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-1-digital-currency/digital-currency-1-digital-currency-account-default-background-image-for-colorful-effect-image-icon.png`,

  // Digital Currency Accepted Here
  digitalCurrencyAcceptedHereImageUrl: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-1-digital-currency/digital-currency-3-digital-currency-accepted-here-image.png`
}


const socialMediaVariables = {
  gitlab: {
    imageIconUrl: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-3-social-media/social-media-1-gitlab-icon-60x60.png`,
    resourceUrl: `https://gitlab.com/ecorp-org`
  },
  youtube: {
    imageIconUrl: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-3-social-media/social-media-2-youtube-icon-60x60.png`,
    resourceUrl: `https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g/featured`
  },
  twitter: {
    imageIconUrl: `https://ecoin-static-file-host.web.app/service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/service-for-images/images-type-3-social-media/social-media-3-twitter-icon-60x60.png`,
    resourceUrl: ``
  }
}

const customMessages = {
  thankYouForJoiningUniversalBasicIncome: `Thank you for particpating in our Universal Basic Income Program. We’re continuing to work on new ways to make {{projectName}} better for you. Stay tuned.`
}



export {
  projectVariables,
  socialMediaVariables,
  customMessages
}



