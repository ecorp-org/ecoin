


import { clientCompress } from '../../client-compress-type-3-constants/constants-1-client-compress'

async function createImageCompression1 (imageFile: any): Promise<any> {
  return new Promise((resolve) => {
    clientCompress.compress([imageFile]).then((conversions: any) => {
      const { photo } = conversions[0]
      resolve(photo.data)
    })
  })
}

export {
  createImageCompression1
}

