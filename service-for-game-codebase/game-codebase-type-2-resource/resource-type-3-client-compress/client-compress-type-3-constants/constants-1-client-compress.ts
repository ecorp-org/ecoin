

// https://www.npmjs.com/package/client-compress

// import Compress from 'client-compress'
const Compress = require('client-compress')

const options = {
  targetSize: 0.2,
  quality: 0.75 // ,
  // maxWidth: 1200,
  // maxHeight: 1200
}

const clientCompress = new Compress(options)


export {
  clientCompress
}

