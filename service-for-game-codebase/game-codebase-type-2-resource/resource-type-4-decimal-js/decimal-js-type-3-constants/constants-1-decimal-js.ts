

import Decimal from 'decimal.js'

Decimal.set({
  precision: 1e+9
})

export {
  Decimal
}


