

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

async function getItemIdList1 (storeId: string, maximumNumberOfIds?: number): Promise<any[]> {

  const realtimeDatabase = firebaseVariableTree.realtimeDatabase!

  if (!storeId) { return [] }

  let resultVariableTree

  if (maximumNumberOfIds) {
    resultVariableTree = (await realtimeDatabase.ref(`${storeId}`).limitToFirst(maximumNumberOfIds).once('value')).val()
  } else {
    resultVariableTree = (await realtimeDatabase.ref(`${storeId}`).once('value')).val()
  }

  if (!resultVariableTree) { return [] }

  // Convert result variable tree to an array
  return Object.keys(resultVariableTree)
}

export {
  getItemIdList1
}

