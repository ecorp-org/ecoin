

import { firebaseAdminVariableTree } from '../../firebase-type-4-variables/variables-2-firebase-admin'

import { getItem2FirebaseAdmin } from '../algorithms-type-7-get-item/get-item-2-firebase-admin'

async function updateItem2FirebaseAdmin (storeId: string, itemId: string, item: any, isFileDocument?: boolean, triggerChildChangedEvent?: boolean): Promise<any> {

  const realtimeDatabase = firebaseAdminVariableTree.realtimeDatabase!
  const storage = firebaseAdminVariableTree.storage!

  if (triggerChildChangedEvent && !isFileDocument) {
    const dataResult = await getItem2FirebaseAdmin(storeId, itemId)
    if (typeof item === 'object') {
      for (let itemPropertyId in item) {
        if (!item.hasOwnProperty(itemPropertyId)) {
          continue
        }
        if (typeof dataResult !== 'object') {
          await realtimeDatabase.ref(`${storeId}/${itemId}`).update({ [itemPropertyId]: 'initial' })
          continue
        }

        if (dataResult && (dataResult[itemPropertyId] !== null || dataResult[itemPropertyId] !== undefined)) {
          continue
        }

        await realtimeDatabase.ref(`${storeId}/${itemId}`).update({ [itemPropertyId]: 'initial' })
      }
    } else {
      if (dataResult === null || dataResult === undefined) {
        await realtimeDatabase.ref(`${storeId}/${itemId}`).update({ ['value']: 'initial' })
      }
    }
  }

  let updateItemResult

  // Update item in file storage if it's a file document
  if (isFileDocument) {

    // Return the file location url
    updateItemResult = await storage.bucket(`${storeId}/${itemId}`).upload(item)

  // Update 'item' property key-value pair one at a time if 'item' is an object
  // Firebase security rules are applied atomically so we may receive
  // permission denied warnings if we update properties where
  // access to write is more restrictive than what we have available presently .
  } else if (typeof item === 'object') {
    for (let propertyId in item) {
      if (!item.hasOwnProperty(propertyId)) { continue }
      await realtimeDatabase.ref(`${storeId}/${itemId}`).update({ [propertyId]: item[propertyId] })
      // try {
      //   console.log('hello: ', item)
      //   const hello = await realtimeDatabase.ref(`${storeId}/${itemId}`).update({ [propertyId]: item[propertyId] })
      // } catch (errorMessage) {
      //   console.error(errorMessage)
      //   throw Error(errorMessage)
      //   // TODO: Something to do with error messages from firebase partial read/write rules .
      // }
    }

  // Update non-object item with single write operation .
  } else {
    await realtimeDatabase.ref(`${storeId}/${itemId}`).update(item)
  }

  // get the created item for non-file document types .
  if (!updateItemResult) {
    updateItemResult = await getItem2FirebaseAdmin(storeId, itemId)
  }

  return updateItemResult
}

export {
  updateItem2FirebaseAdmin
}


