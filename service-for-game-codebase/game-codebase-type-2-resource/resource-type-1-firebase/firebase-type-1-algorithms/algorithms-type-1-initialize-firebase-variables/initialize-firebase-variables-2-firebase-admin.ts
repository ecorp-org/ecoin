

import firebaseAdmin from 'firebase-admin'

import { firebaseAdminVariableTree } from '../../firebase-type-4-variables/variables-2-firebase-admin'

import { ServiceInitializationInformation } from '../../firebase-type-2-data-structures/data-structures-1-service'

import { firebaseAdminDevelopmentConfig } from '../../../../../service-for-environment-variables/environment-variables-type-2-development-variables/development-variables-type-2-daemon/daemon-type-1-firebase-variables/firebase-variables-1-firebase-admin'
import { firebaseAdminProductionConfig } from '../../../../../service-for-environment-variables/environment-variables-type-3-production-variables/production-variables-type-2-daemon/daemon-type-1-firebase-variables/firebase-variables-1-firebase-admin'


async function initializeFirebaseVariables2FirebaseAdmin (serviceInformation: ServiceInitializationInformation) {

  let firebaseAdminConfig: any = {}

  if (serviceInformation.distributionType === 'development') {
    firebaseAdminConfig = firebaseAdminDevelopmentConfig
  } else if (serviceInformation.distributionType === 'production') {
    firebaseAdminConfig = firebaseAdminProductionConfig
  }

  const firebaseAdminServiceAccount = JSON.parse((process.env.FIREBASE_ADMIN_PRIVATE_KEY || '').replace(new RegExp('\\\\n', 'g'), '     ').replace(new RegExp('\n', 'g'), '     ') || '{}')
  firebaseAdminServiceAccount.private_key = (firebaseAdminServiceAccount.private_key || '').replace(new RegExp('     ', 'ig'), '\n')
  if (Object.keys(firebaseAdminServiceAccount).length > 0) {
    firebaseAdminConfig.credential = firebaseAdmin.credential.cert(firebaseAdminServiceAccount)
  }

  const firebaseAdminApp = firebaseAdmin.apps.length > 0 ? firebaseAdmin.app() : firebaseAdmin.initializeApp(firebaseAdminConfig)

  const auth = firebaseAdminApp.auth()
  const realtimeDatabase = firebaseAdminApp.database()
  let storage = firebaseAdminApp.storage()

  if (serviceInformation.useEmulator) {
    realtimeDatabase.useEmulator('localhost', 9000)
    // storage = { bucket: () => { return { delete: () => 1, upload: () => 1 } } } as any
  }

  firebaseAdminVariableTree.firebaseAdmin = firebaseAdmin
  firebaseAdminVariableTree.firebaseApp = firebaseAdminApp
  firebaseAdminVariableTree.auth = auth
  firebaseAdminVariableTree.realtimeDatabase = realtimeDatabase
  firebaseAdminVariableTree.storage = storage
}

export {
  initializeFirebaseVariables2FirebaseAdmin
}


