


import * as firebaseTesting from '@firebase/rules-unit-testing'


import { firebaseVariableTree } from '../../../../firebase-type-4-variables/variables-1-firebase'
import { firebaseAdminVariableTree } from '../../../../firebase-type-4-variables/variables-2-firebase-admin'

// import { firebaseAdminDevelopmentConfig } from '../../../../../../../service-for-environment-variables/environment-variables-type-2-development-variables/development-variables-type-2-daemon/daemon-type-1-firebase-variables/firebase-variables-1-firebase-admin'
// import { firebaseAdminProductionConfig } from '../../../../../../../service-for-environment-variables/environment-variables-type-3-production-variables/production-variables-type-2-daemon/daemon-type-1-firebase-variables/firebase-variables-1-firebase-admin'

import { createId1 } from '../../../../../../game-codebase-type-1-app/app-type-1-general/general-type-1-algorithms/algorithms-type-2-create-id/create-id-1'

const DEFAULT_DATABASE_NAME = 'test-database'
const DEFAULT_AUTH = {
  currentUser: {
    uid: 'default-auth-uid',
    isAnonymous: true
  },
  createUserWithEmailAndPassword (emailAddress: string, _password: string) {
    return { user: { uid: `${createId1()}`, email: emailAddress, isAnonymous: false } }
  }
}


class InitializeFirebaseVariables2FirebaseAdminOptions {
  databaseName?: string = ''
  realtimeDatabaseAnalyticsCallbackFunction: (...argumentList: any) => any = () => 1
}


async function initializeFirebaseVariables2FirebaseAdmin (options?: InitializeFirebaseVariables2FirebaseAdminOptions) {

  if (!options) {
    options = new InitializeFirebaseVariables2FirebaseAdminOptions()
  }

  const firebaseTestingTestEnvironment = await firebaseTesting.initializeTestEnvironment({
    projectId: options.databaseName || DEFAULT_DATABASE_NAME,
    database: {
      host: `localhost`,
      port: 9000,
      rules: JSON.stringify({
        'rules': {
          '.read': true,
          '.write': true
        }
      })
    },
    storage: {
      host: 'localhost',
      port: 9090
    }
  })

  // const firebaseAdminApp = firebaseTesting.initializeAdminApp({
  //   databaseName: DEFAULT_DATABASE_ID,
  //   projectId: firebaseAdminDevelopmentConfig.projectId
  // })

  // await firebaseTesting.loadDatabaseRules({
  //   databaseName: DEFAULT_DATABASE_ID,
  //   rules: JSON.stringify({
  //     '.read': true,
  //     '.write': true
  //   })
  // })

  const unauthenticatedUser = firebaseTestingTestEnvironment.unauthenticatedContext()

  // const auth = unauthenticatedUser.auth()
  const realtimeDatabase = unauthenticatedUser.database()
  // const storage = unauthenticatedUser.storage()
  const storage = {
    ref: () => { return { delete: () => { return '' } } }
  }

  const originalRealtimeDatabaseRefFunction = realtimeDatabase.ref.bind(realtimeDatabase)
  realtimeDatabase.ref = (...argumentList: any) => {
    options!.realtimeDatabaseAnalyticsCallbackFunction({
      functionName: 'ref',
      argumentList
    })
    const databaseRef = originalRealtimeDatabaseRefFunction(...argumentList)

    for (let databaseRefFunctionId in databaseRef) {
      if (typeof (databaseRef as any)[databaseRefFunctionId] !== 'function') {
        continue
      }
      const originalDatabaseFunction = (databaseRef as any)[databaseRefFunctionId].bind(databaseRef)
      ;(databaseRef as any)[databaseRefFunctionId] = (...argumentList2: any) => {
        const functionResult = originalDatabaseFunction(...argumentList2)
        options!.realtimeDatabaseAnalyticsCallbackFunction({
          functionName: databaseRefFunctionId,
          argumentList: argumentList2
        })
        return functionResult
      }
    }

    return databaseRef
  }

  ;(firebaseAdminVariableTree.firebaseAdmin as any) = firebaseTesting
  ;(firebaseAdminVariableTree.firebaseApp as any) = firebaseTestingTestEnvironment
  ;(firebaseAdminVariableTree.auth as any) = DEFAULT_AUTH
  ;(firebaseAdminVariableTree.realtimeDatabase as any) = realtimeDatabase
  ;(firebaseAdminVariableTree.storage as any) = storage

  ;(firebaseVariableTree.firebaseApp as any) = firebaseTestingTestEnvironment
  ;(firebaseVariableTree.auth as any) = DEFAULT_AUTH
  ;(firebaseVariableTree.realtimeDatabase as any) = realtimeDatabase
  ;(firebaseVariableTree.storage as any) = storage
}

export {
  initializeFirebaseVariables2FirebaseAdmin,
  DEFAULT_DATABASE_NAME,
  InitializeFirebaseVariables2FirebaseAdminOptions
}


