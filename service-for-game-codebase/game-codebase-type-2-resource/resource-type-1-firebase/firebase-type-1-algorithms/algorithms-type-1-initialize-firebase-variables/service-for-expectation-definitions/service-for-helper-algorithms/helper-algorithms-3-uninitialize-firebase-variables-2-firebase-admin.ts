

// import * as firebaseTesting from '@firebase/rules-unit-testing'

// import { firebaseVariableTree } from '../../../../firebase-type-4-variables/variables-1-firebase'
import { firebaseAdminVariableTree } from '../../../../firebase-type-4-variables/variables-2-firebase-admin'

// import { firebaseAdminDevelopmentConfig } from '../../../../../../../service-for-environment-variables/environment-variables-type-2-development-variables/development-variables-type-2-daemon/daemon-type-1-firebase-variables/firebase-variables-1-firebase-admin'
// import { firebaseAdminProductionConfig } from '../../../../../../../service-for-environment-variables/environment-variables-type-3-production-variables/production-variables-type-2-daemon/daemon-type-1-firebase-variables/firebase-variables-1-firebase-admin'



async function uninitializeFirebaseVariables2FirebaseAdmin () {

  // (firebaseAdminVariableTree.firebaseApp as any).cleanup()
  await (firebaseAdminVariableTree.firebaseApp as any).clearDatabase()
  // await (firebaseAdminVariableTree.firebaseApp as any).clearDatabase()

  // ;(firebaseAdminVariableTree.firebaseApp as any).clearStorage()
  // ;(firebaseAdminVariableTree.firebaseApp as any).clearFirestore()


  // ;(firebaseVariableTree.firebaseApp as any).cleanup()
  // ;(firebaseVariableTree.firebaseApp as any).clearDatabase()
  // ;(firebaseVariableTree.firebaseApp as any).clearStorage()

}

export {
  uninitializeFirebaseVariables2FirebaseAdmin
}

