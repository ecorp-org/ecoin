
import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import 'firebase/compat/database'
import 'firebase/compat/storage'
import 'firebase/compat/analytics'

// import * as IPFS from 'ipfs'
// import * as OrbitDB from 'orbit-db'

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'
import { firebaseDevelopmentConfig } from '../../../../../service-for-environment-variables/environment-variables-type-2-development-variables/development-variables-type-1-customer-website/customer-website-type-1-firebase-variables/firebase-variables-1-firebase'
import { firebaseProductionConfig } from '../../../../../service-for-environment-variables/environment-variables-type-3-production-variables/production-variables-type-1-customer-website/customer-website-type-1-firebase-variables/firebase-variables-1-firebase'

import { ServiceInitializationInformation } from '../../firebase-type-2-data-structures/data-structures-1-service'

async function initializeFirebaseVariables1 (serviceInformation: ServiceInitializationInformation) {

  let firebaseConfig = {}

  if (serviceInformation.distributionType === 'production') {
    firebaseConfig = firebaseProductionConfig
  } else if (serviceInformation.distributionType === 'development') {
    firebaseConfig = firebaseDevelopmentConfig
  } else {
    firebaseConfig = firebaseDevelopmentConfig
  }

  const firebaseApp = firebase.apps.length > 0 ? firebase.app() : firebase.initializeApp(firebaseConfig)
  const analytics = firebase.analytics(firebaseApp)

  const auth = firebaseApp.auth()
  const realtimeDatabase = firebaseApp.database()
  const storage = firebaseApp.storage()

  if (serviceInformation.useEmulator) {
    realtimeDatabase.useEmulator('localhost', 9000)
    auth.useEmulator('http://localhost:9099/')
  }

  firebaseVariableTree.firebase = firebase
  firebaseVariableTree.firebaseApp = firebaseApp
  firebaseVariableTree.auth = auth
  firebaseVariableTree.realtimeDatabase = realtimeDatabase
  firebaseVariableTree.storage = storage
  firebaseVariableTree.analytics = analytics

  // const ipfsOptions = {
  //   EXPERIMENTAL: { pubsub: true }
  // }

  // const ipfs = await IPFS.create(ipfsOptions)
  // const orbitDB = await OrbitDB.createInstance(ipfs)
  // const db = await orbitDB.keyvalue('test-db')

  // console.log('database: ', db)
}

export {
  initializeFirebaseVariables1
}

