
import firebaseAdmin from 'firebase-admin'

import { firebaseAdminVariableTree } from '../../firebase-type-4-variables/variables-2-firebase-admin'

import { EVENT_ID_UPDATE, EVENT_ID_CREATE, EVENT_ID_DELETE, OnEventCallback } from '../../firebase-type-3-constants/constants-1-firebase-events'


async function addItemEventListener2FirebaseAdmin (storeId: string, itemId: string, eventId: string, onEventCallback: typeof OnEventCallback): Promise<string> {

  const realtimeDatabase = firebaseAdminVariableTree.realtimeDatabase!
  const itemEventListenerTable = firebaseAdminVariableTree.itemEventListenerVariableTree

  let eventType: firebaseAdmin.database.EventType = 'value'
  switch (eventId) {
    case EVENT_ID_UPDATE:
      eventType = 'value'
      break
    case EVENT_ID_CREATE:
      eventType = 'child_changed'
      break
    case EVENT_ID_DELETE:
      eventType = 'child_removed'
      break
    default:
      eventType = 'value'
      break
  }

  const itemEventListenerId: string = Math.random().toString(16).split('.')[1]
  const itemEventListener = (dataSnapshot: firebaseAdmin.database.DataSnapshot, options: any) => {
    onEventCallback(dataSnapshot.val(), {
      key: dataSnapshot.key,
      options
    })
  }

  realtimeDatabase.ref(`${storeId}/${itemId}`).on(eventType, itemEventListener)

  itemEventListenerTable[itemEventListenerId] = itemEventListener

  return Promise.resolve(itemEventListenerId)
}


export {
  addItemEventListener2FirebaseAdmin,
  OnEventCallback
}
