

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

import { ServiceAccountRecoveryConfirmationInformation } from '../../firebase-type-2-data-structures/data-structures-2-service-account'

async function createServiceAccountRecoveryConfirmation1 (recoveryConfirmationInformation: ServiceAccountRecoveryConfirmationInformation) {
  const auth = firebaseVariableTree.auth!

  await auth.confirmPasswordReset(recoveryConfirmationInformation.passwordResetCode || '', recoveryConfirmationInformation.newPassword || '')
}

export {
  createServiceAccountRecoveryConfirmation1
}

