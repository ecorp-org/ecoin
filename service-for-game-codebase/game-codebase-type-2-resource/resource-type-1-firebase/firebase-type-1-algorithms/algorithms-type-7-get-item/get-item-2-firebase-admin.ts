

import { firebaseAdminVariableTree } from '../../firebase-type-4-variables/variables-2-firebase-admin'

async function getItem2FirebaseAdmin (storeId: string, itemId: string): Promise<any> {
  const realtimeDatabase = firebaseAdminVariableTree.realtimeDatabase!

  if (!storeId || !itemId) { return null }
  return (await realtimeDatabase.ref(`${storeId}/${itemId}`).once('value')).val()
}


export {
  getItem2FirebaseAdmin
}


