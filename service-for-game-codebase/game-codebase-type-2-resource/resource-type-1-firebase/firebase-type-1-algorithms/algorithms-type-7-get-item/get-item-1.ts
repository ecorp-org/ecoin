
import firebase from 'firebase/compat/app'
import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

class GetItem1Options {
  orderByValue?: boolean
  limitToFirst?: number
  limitToLast?: number
  startAt?: { value?: any, key?: string }
  startAfter?: { value?: any, key?: string }
  endAt?: { value?: any, key?: string }
  endBefore?: { value?: any, key?: string }
}

async function getItem1 (storeId: string, itemId: string, options?: GetItem1Options): Promise<any> {
  const realtimeDatabase = firebaseVariableTree.realtimeDatabase!

  let result = null
  let databaseReference: firebase.database.Reference | firebase.database.Query = realtimeDatabase.ref(`${storeId}/${itemId}`)

  if (!storeId || !itemId) { return result }

  if (options) {
    if (options.orderByValue) {
      databaseReference = databaseReference.orderByValue()
    }

    if (options.startAt) {
      databaseReference = databaseReference.startAt(options.startAt.value, options.startAt.key)
    }

    if (options.startAfter) {
      databaseReference = databaseReference.startAfter(options.startAfter.value, options.startAfter.key)
    }

    if (options.endAt) {
      databaseReference = databaseReference.endAt(options.endAt.value, options.endAt.key)
    }

    if (options.endBefore) {
      databaseReference = databaseReference.endAt(options.endBefore.value, options.endBefore.key)
    }

    if (options.limitToFirst) {
      databaseReference = databaseReference.limitToFirst(options.limitToFirst)
    }

    if (options.limitToLast) {
      databaseReference = databaseReference.limitToLast(options.limitToLast)
    }
  }

  result = (await databaseReference.once('value')).val()

  return result
}


export {
  getItem1,
  GetItem1Options
}



