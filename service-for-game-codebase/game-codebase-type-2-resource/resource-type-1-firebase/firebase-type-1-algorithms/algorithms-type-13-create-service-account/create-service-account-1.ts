

import firebase from 'firebase/compat/app'

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

import { ServiceAccountCreateInformation } from '../../firebase-type-2-data-structures/data-structures-2-service-account'

async function createServiceAccount1 (accountInformation: ServiceAccountCreateInformation): Promise<firebase.auth.UserCredential> {
  const { auth } = firebaseVariableTree

  let userInformation = null

  if (accountInformation.emailAddress && accountInformation.password) {
    if (accountInformation.linkWithExistingAccount) {
      const credential = firebase.auth.EmailAuthProvider.credential(accountInformation.emailAddress || '', accountInformation.password || '')
      userInformation = await auth!.currentUser!.linkWithCredential(credential)
    } else {
      userInformation = await auth!.createUserWithEmailAndPassword(accountInformation.emailAddress || '', accountInformation.password || '')
    }
  } else if (accountInformation.isAnonymous) {
    userInformation = await auth?.signInAnonymously()
  } else {
    userInformation = { user: auth?.currentUser } as any
  }

  return userInformation
}

export {
  createServiceAccount1
}

