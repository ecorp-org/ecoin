
import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

import { getItem1 } from '../algorithms-type-7-get-item/get-item-1'

async function createItem1 (storeId: string, itemId: string, item: any, isFileDocument?: boolean, triggerChildChangedEvent?: boolean): Promise<any> {

  const realtimeDatabase = firebaseVariableTree.realtimeDatabase!
  const storage = firebaseVariableTree.storage!

  if (!storeId || !itemId) { return null }

  if (triggerChildChangedEvent && !isFileDocument) {
    const dataResult = await getItem1(storeId, itemId)
    if (typeof item === 'object') {
      for (let itemPropertyId in item) {
        if (!item.hasOwnProperty(itemPropertyId)) {
          continue
        }
        if (typeof dataResult !== 'object') {
          await realtimeDatabase.ref(`${storeId}/${itemId}`).set({ [itemPropertyId]: 'initial' })
          continue
        }

        if (dataResult && (dataResult[itemPropertyId] !== null || dataResult[itemPropertyId] !== undefined)) {
          continue
        }

        await realtimeDatabase.ref(`${storeId}/${itemId}`).set({ [itemPropertyId]: 'initial' })
      }
    } else {
      if (dataResult === null || dataResult === undefined) {
        await realtimeDatabase.ref(`${storeId}/${itemId}`).set('initial')
      }
    }
  }

  let createItemResult

  if (isFileDocument) {
    storage.ref(`${storeId}/${itemId}`).put(item)
    createItemResult = storage.ref(`${storeId}/${itemId}`).getDownloadURL()

  // create item of object one property at a time in case the firebase rules
  // are restrictive in writing particular properties .
  } else if (typeof item === 'object') {
    for (let propertyId in item) {
      if (!item.hasOwnProperty(propertyId)) { continue }
      try {
        await realtimeDatabase.ref(`${storeId}/${itemId}/${propertyId}`).set(item[propertyId])
      } catch (errorMessage) {
        console.error(errorMessage)
        // TODO: Something to do with error messages from firebase partial read/write rules .
      }
    }
  // set non-object item .
  } else {
    await realtimeDatabase.ref(`${storeId}/${itemId}`).set(item)
  }

  // get the created item for non-file document types .
  if (!createItemResult) {
    createItemResult = await getItem1(storeId, itemId)
  }

  return createItemResult
}


export {
  createItem1
}








