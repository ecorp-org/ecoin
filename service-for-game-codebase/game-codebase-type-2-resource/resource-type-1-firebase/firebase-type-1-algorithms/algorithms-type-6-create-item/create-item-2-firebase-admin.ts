

import { firebaseAdminVariableTree } from '../../firebase-type-4-variables/variables-2-firebase-admin'

import { getItem2FirebaseAdmin } from '../algorithms-type-7-get-item/get-item-2-firebase-admin'

async function createItem2FirebaseAdmin (storeId: string, itemId: string, item: any, isFileDocument?: boolean, triggerChildChangedEvent?: boolean, isSingleWrite?: boolean): Promise<any> {

  const realtimeDatabase = firebaseAdminVariableTree.realtimeDatabase!
  const storage = firebaseAdminVariableTree.storage!

  if (!storeId || !itemId) { return null }

  if (triggerChildChangedEvent && !isFileDocument) {
    const dataResult = await getItem2FirebaseAdmin(storeId, itemId)
    if (typeof item === 'object') {
      for (let itemPropertyId in item) {
        if (!item.hasOwnProperty(itemPropertyId)) {
          continue
        }
        if (typeof dataResult !== 'object') {
          await realtimeDatabase.ref(`${storeId}/${itemId}`).set({ [itemPropertyId]: 'initial' })
          continue
        }

        if (dataResult && (dataResult[itemPropertyId] !== null || dataResult[itemPropertyId] !== undefined)) {
          continue
        }

        await realtimeDatabase.ref(`${storeId}/${itemId}`).set({ [itemPropertyId]: 'initial' })
      }
    } else {
      if (dataResult === null || dataResult === undefined) {
        await realtimeDatabase.ref(`${storeId}/${itemId}`).set('initial')
      }
    }
  }

  let createItemResult

  if (isFileDocument) {
    // storage.ref(`${storeId}/${itemId}`).put(item)
    await storage.bucket(`${storeId}/${itemId}`).upload(item)
    // createItemResult = storage.ref(`${storeId}/${itemId}`).getDownloadURL()
    // createItemResult = storage.bucket(`${storeId}/${itemId}`).get

  // create item of object one property at a time in case the firebase rules
  // are restrictive in writing particular properties .
  } else if (typeof item === 'object' && !isSingleWrite) {
    for (let propertyId in item) {
      if (!item.hasOwnProperty(propertyId)) { continue }
      try {
        await realtimeDatabase.ref(`${storeId}/${itemId}/${propertyId}`).set(item[propertyId])
      } catch (errorMessage) {
        console.error(errorMessage)
        // TODO: Something to do with error messages from firebase partial read/write rules .
      }
    }
  // set non-object item .
  } else {
    await realtimeDatabase.ref(`${storeId}/${itemId}`).set(item)
  }

  // get the created item for non-file document types .
  if (!createItemResult) {
    createItemResult = await getItem2FirebaseAdmin(storeId, itemId)
  }

  return createItemResult
}


export {
  createItem2FirebaseAdmin
}


