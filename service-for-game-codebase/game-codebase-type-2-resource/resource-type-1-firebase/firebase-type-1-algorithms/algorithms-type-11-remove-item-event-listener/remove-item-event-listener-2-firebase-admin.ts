


import firebaseAdmin from 'firebase-admin'

import { firebaseAdminVariableTree } from '../../firebase-type-4-variables/variables-2-firebase-admin'
import { EVENT_ID_UPDATE, EVENT_ID_CREATE, EVENT_ID_DELETE } from '../../firebase-type-3-constants/constants-1-firebase-events'


function removeItemEventListener2FirebaseAdmin (storeId: string, itemId: string, eventId: string, itemEventListenerId: string) {

  const realtimeDatabase = firebaseAdminVariableTree.realtimeDatabase!
  const itemEventListenerTable = firebaseAdminVariableTree.itemEventListenerVariableTree

  let eventType: firebaseAdmin.database.EventType = 'value'
  switch (eventId) {
    case EVENT_ID_UPDATE:
      eventType = 'value'
      break
    case EVENT_ID_CREATE:
      eventType = 'child_changed'
      break
    case EVENT_ID_DELETE:
      eventType = 'child_removed'
      break
    default:
      eventType = 'value'
      break
  }

  const itemEventListener = itemEventListenerTable[itemEventListenerId]

  realtimeDatabase.ref(`${storeId}/${itemId}`).off(eventType, itemEventListener)
}

export {
  removeItemEventListener2FirebaseAdmin
}


