
import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

async function isInitializedServiceAccount (): Promise<boolean> {
  const { auth } = firebaseVariableTree

  // Check if user is signed in
  const promise = new Promise<boolean>((resolve, _reject) => {
    auth?.onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in
        resolve(true)
      } else {
        // No user is signed in.
        resolve(false)
      }
    })
  })

  return promise
}

export {
  isInitializedServiceAccount
}


