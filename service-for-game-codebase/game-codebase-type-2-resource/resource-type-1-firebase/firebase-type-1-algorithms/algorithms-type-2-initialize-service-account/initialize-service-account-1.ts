
import firebase from 'firebase/compat/app'

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

import { ServiceAccountInitializationInformation } from '../../firebase-type-2-data-structures/data-structures-2-service-account'

import { googleAuthenticationProvider } from '../../firebase-type-3-constants/constants-2-authentication'


async function initializeServiceAccount1 (accountInformation: ServiceAccountInitializationInformation): Promise<firebase.auth.UserCredential> {
  const { auth } = firebaseVariableTree

  let userInformation = null

  if (accountInformation.isEmailAndPasswordSignIn) {
    userInformation = await auth?.signInWithEmailAndPassword(accountInformation.emailAddress! || '', accountInformation.password! || '')
  } else if (accountInformation.isAnonymousSignIn) {
    userInformation = await auth?.signInAnonymously()
  } else if (accountInformation.isGoogleSignIn) {
    userInformation = await auth?.signInWithPopup(googleAuthenticationProvider)
  } else {
    userInformation = { user: auth?.currentUser } as any
  }

  return userInformation
}

export {
  initializeServiceAccount1
}

