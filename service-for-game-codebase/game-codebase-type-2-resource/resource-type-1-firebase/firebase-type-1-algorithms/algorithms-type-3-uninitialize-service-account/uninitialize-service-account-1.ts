
import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'


async function uninitializeServiceAccount1 () {
  const { auth } = firebaseVariableTree

  await auth?.signOut()
}

export {
  uninitializeServiceAccount1
}
