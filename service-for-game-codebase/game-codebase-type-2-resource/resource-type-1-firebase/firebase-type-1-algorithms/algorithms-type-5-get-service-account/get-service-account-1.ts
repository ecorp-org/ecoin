
import firebase from 'firebase/compat/app'

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'


function getServiceAccount1 (): firebase.User | null | undefined {
  const { auth } = firebaseVariableTree
  return auth?.currentUser
}


export {
  getServiceAccount1
}

