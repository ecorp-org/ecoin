

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

class DeleteItemAlgorithmResponse {
  isDeleted: boolean = false
  errorMessage: any
  additionalInformaiton: { [additionalInformationId: string]: any } = {}
}

async function deleteItem1 (storeId: string, itemId: string, isFileDocument?: boolean): Promise<DeleteItemAlgorithmResponse> {

  const realtimeDatabase = firebaseVariableTree.realtimeDatabase!
  const storage = firebaseVariableTree.storage!

  let deleteItemResult

  if (isFileDocument) {
    await storage.ref(`${storeId}/${itemId}`).delete()
  } else {
    deleteItemResult = await realtimeDatabase.ref(`${storeId}/${itemId}`).remove()
  }

  const deleteResultResponse = new DeleteItemAlgorithmResponse()
  deleteResultResponse.isDeleted = !deleteItemResult

  deleteResultResponse.additionalInformaiton = deleteItemResult

  return deleteResultResponse
}

export {
  deleteItem1,
  DeleteItemAlgorithmResponse
}




