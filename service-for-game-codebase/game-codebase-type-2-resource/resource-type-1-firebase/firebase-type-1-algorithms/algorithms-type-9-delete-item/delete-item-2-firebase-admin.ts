
import { firebaseAdminVariableTree } from '../../firebase-type-4-variables/variables-2-firebase-admin'

class DeleteItemAlgorithmResponse {
  isDeleted: boolean = false
  errorMessage: any
  additionalInformaiton: { [additionalInformationId: string]: any } = {}
}

async function deleteItem2FirebaseAdmin (storeId: string, itemId: string, isFileDocument?: boolean): Promise<DeleteItemAlgorithmResponse> {

  const realtimeDatabase = firebaseAdminVariableTree.realtimeDatabase!
  const storage = firebaseAdminVariableTree.storage!

  let deleteItemResult

  if (isFileDocument) {
    // await storage.ref(`${storeId}/${itemId}`).delete()
    await storage.bucket(`${storeId}/${itemId}`).delete()
  } else {
    deleteItemResult = await realtimeDatabase.ref(`${storeId}/${itemId}`).remove()
  }

  const deleteResultResponse = new DeleteItemAlgorithmResponse()
  deleteResultResponse.isDeleted = !deleteItemResult

  deleteResultResponse.additionalInformaiton = deleteItemResult

  return deleteResultResponse
}

export {
  deleteItem2FirebaseAdmin,
  DeleteItemAlgorithmResponse
}

