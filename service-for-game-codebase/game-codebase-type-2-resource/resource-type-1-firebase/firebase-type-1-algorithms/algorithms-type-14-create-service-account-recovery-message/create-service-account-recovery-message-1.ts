

import { firebaseVariableTree } from '../../firebase-type-4-variables/variables-1-firebase'

import { ServiceAccountRecoveryInformation } from '../../firebase-type-2-data-structures/data-structures-2-service-account'

async function createServiceAccountRecoveryMessage1 (recoveryInformation: ServiceAccountRecoveryInformation) {
  const auth = firebaseVariableTree.auth!

  await auth.sendPasswordResetEmail(recoveryInformation.emailAddress || '')
}

export {
  createServiceAccountRecoveryMessage1
}


