

import { GoogleAuthProvider, EmailAuthProvider } from 'firebase/auth'


const googleAuthenticationProvider = new GoogleAuthProvider()
const emailAuthenticationProvider = new EmailAuthProvider()

export {
  googleAuthenticationProvider,
  emailAuthenticationProvider
}

