

import firebase from 'firebase/compat/app'

const EVENT_ID_GET = 'get'
const EVENT_ID_CREATE = 'create'
const EVENT_ID_UPDATE = 'update'
const EVENT_ID_DELETE = 'delete'


declare type EventType = 'create' | 'update' | 'get' | 'delete'

const OnEventCallback = (data: any, _options: any): any => { return { data } }
const OnItemStoreEventCallback = (itemIdList: string[], itemList?: any[]): any => { return { itemIdList, itemList } }


export {
  firebase,
  EVENT_ID_GET,
  EVENT_ID_CREATE,
  EVENT_ID_UPDATE,
  EVENT_ID_DELETE,
  EventType,
  OnEventCallback,
  OnItemStoreEventCallback
}
