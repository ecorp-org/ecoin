


class ServiceInitializationInformation {
  distributionType?: string = ''
  useEmulator?: boolean = false
}


export {
  ServiceInitializationInformation
}

