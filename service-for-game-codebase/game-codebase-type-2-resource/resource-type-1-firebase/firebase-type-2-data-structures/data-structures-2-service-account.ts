


class ServiceAccountInitializationInformation {
  emailAddress?: string = ''
  password?: string = ''

  isEmailAndPasswordSignIn?: boolean = false
  isAnonymousSignIn?: boolean = false
  isGoogleSignIn?: boolean = false
}

class ServiceAccountCreateInformation {
  isAnonymous?: boolean = false
  emailAddress?: string = ''
  password?: string = ''
  linkWithExistingAccount?: boolean = false
}

class ServiceAccountRecoveryInformation {
  emailAddress?: string = ''
}

class ServiceAccountRecoveryConfirmationInformation {
  passwordResetCode?: string = ''
  newPassword?: string = ''
}

export {
  ServiceAccountInitializationInformation,
  ServiceAccountCreateInformation,
  ServiceAccountRecoveryInformation,
  ServiceAccountRecoveryConfirmationInformation
}


