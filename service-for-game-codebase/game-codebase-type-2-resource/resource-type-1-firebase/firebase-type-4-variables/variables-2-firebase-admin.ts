
import * as firebaseAdmin from 'firebase-admin'


const firebaseAdminVariableTree: {
  firebaseAdmin?: typeof firebaseAdmin,
  firebaseApp?: firebaseAdmin.app.App,
  auth?: firebaseAdmin.auth.Auth,
  realtimeDatabase?: firebaseAdmin.database.Database,
  storage?: firebaseAdmin.storage.Storage,
  itemEventListenerVariableTree: { [itemEventListenerId: string]: any }
} = {
  itemEventListenerVariableTree: {}
}

export {
  firebaseAdminVariableTree
}

