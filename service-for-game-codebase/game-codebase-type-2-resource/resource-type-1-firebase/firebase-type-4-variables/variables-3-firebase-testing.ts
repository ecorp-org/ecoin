
import * as firebase from '@firebase/testing'
import * as firebaseDatabaseRules from '../../../../service-for-environment-variables/environment-variables-type-2-development-variables/development-variables-type-1-customer-website/customer-website-type-1-firebase-variables/database.rules.json'


// initialize test app
const firebaseApp = firebase.initializeTestApp({
  databaseName: 'example-database',
  auth: { uid: 'example-user-uid' }
})

// load firebase database rules
firebase.loadDatabaseRules({
  databaseName: 'example-database',
  rules: JSON.stringify(firebaseDatabaseRules)
}).catch((errorMessage) => console.error(errorMessage))


const realtimeDatabase = firebaseApp.database()

const firebaseTestingVariableTable = {
  realtimeDatabase
}

export {
  firebaseTestingVariableTable
}
