

import * as algoliasearch from 'algoliasearch'

const algoliasearchVariableTree: {
  itemSearchIndexVariableTree: { [itemSearchIndexId: string]: algoliasearch.SearchIndex }
  itemSearchIndexNameToIndexIdMap: { [indexName: string]: string }
} = {
  itemSearchIndexVariableTree: {},
  itemSearchIndexNameToIndexIdMap: {}
}

export {
  algoliasearchVariableTree
}

