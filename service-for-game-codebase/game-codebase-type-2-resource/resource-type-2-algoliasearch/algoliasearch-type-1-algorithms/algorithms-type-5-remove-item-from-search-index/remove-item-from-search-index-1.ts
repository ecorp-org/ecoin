
import { algoliasearchVariableTree } from '../../algoliasearch-type-4-variables/variables-1-algoliasearch'


async function removeItemFromSearchIndex1 (itemSearchIndexId: string, itemId: string): Promise<any> {

  const { itemSearchIndexVariableTree } = algoliasearchVariableTree

  if (!itemSearchIndexVariableTree[itemSearchIndexId]) {
    throw Error(`Index has not been initialized yet: ${itemSearchIndexId}`)
  }

  const itemSearchIndex = itemSearchIndexVariableTree[itemSearchIndexId]

  const objectID = itemId
  const indexResult = await (itemSearchIndex.deleteObject(objectID) as any)
  return indexResult
}

export {
  removeItemFromSearchIndex1
}

