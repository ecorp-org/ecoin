

import { algoliasearchVariableTree } from '../../algoliasearch-type-4-variables/variables-1-algoliasearch'

import { GetItemListOptions } from '../../algoliasearch-type-2-data-structures/data-structures-1-get-item-list-options'


async function getItemListBySearchText1 (itemSearchIndexId: string, searchQueryStringText: string, options?: GetItemListOptions): Promise<any[]> {

  const { itemSearchIndexVariableTree } = algoliasearchVariableTree

  if (!itemSearchIndexVariableTree[itemSearchIndexId]) {
    throw Error(`Index has not been initialized yet: ${itemSearchIndexId}`)
  }

  const itemSearchIndex = itemSearchIndexVariableTree[itemSearchIndexId]

  const searchResult = (await (itemSearchIndex.search(searchQueryStringText, {
    filters: options ? options.filters : ''
  }) as any))

  return searchResult.hits
}

export {
  getItemListBySearchText1
}

