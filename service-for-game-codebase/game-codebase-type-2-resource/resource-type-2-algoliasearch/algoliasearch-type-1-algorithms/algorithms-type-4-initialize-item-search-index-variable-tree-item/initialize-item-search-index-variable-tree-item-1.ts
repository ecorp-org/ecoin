

import { algoliasearchClient } from '../../algoliasearch-type-3-constants/constants-1-algoliasearch'

import { algoliasearchVariableTree } from '../../algoliasearch-type-4-variables/variables-1-algoliasearch'

function initializeItemSearchIndexVariableTreeItem1 (itemSearchIndexId: string): void {

  const { itemSearchIndexVariableTree } = algoliasearchVariableTree

  itemSearchIndexVariableTree[itemSearchIndexId] = algoliasearchClient.initIndex(itemSearchIndexId)
}

export {
  initializeItemSearchIndexVariableTreeItem1
}

