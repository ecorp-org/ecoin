

import { algoliasearchClient } from '../../algoliasearch-type-3-constants/constants-1-algoliasearch'

function createSearchIndex1 (indexName: string) {
  algoliasearchClient.initIndex(indexName)
}

export {
  createSearchIndex1
}

