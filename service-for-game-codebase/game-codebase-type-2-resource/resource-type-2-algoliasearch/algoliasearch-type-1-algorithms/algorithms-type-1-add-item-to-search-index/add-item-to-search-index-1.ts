
import { algoliasearchVariableTree } from '../../algoliasearch-type-4-variables/variables-1-algoliasearch'

async function addItemToSearchIndex1 (itemSearchIndexId: string, itemId: string, item: any): Promise<any> {
  const { itemSearchIndexVariableTree } = algoliasearchVariableTree

  if (!itemSearchIndexVariableTree[itemSearchIndexId]) {
    throw Error(`Index has not been initialized yet: ${itemSearchIndexId}`)
  }

  const itemSearchIndex = itemSearchIndexVariableTree[itemSearchIndexId]

  const objectID = itemId
  const indexResult = await (itemSearchIndex.saveObject({
    objectID,
    ...item
  }) as any)
  return indexResult
}

export {
  addItemToSearchIndex1
}

