

import { algoliasearchVariableTree } from '../../algoliasearch-type-4-variables/variables-1-algoliasearch'

async function updateItemSearchIndex1 (itemSearchIndexId: string, itemId: string, updateItem: any): Promise<any> {

  const { itemSearchIndexVariableTree } = algoliasearchVariableTree

  if (!itemSearchIndexVariableTree[itemSearchIndexId]) {
    throw Error(`Index has not been initialized yet: ${itemSearchIndexId}`)
  }

  const itemSearchIndex = itemSearchIndexVariableTree[itemSearchIndexId]

  const objectID = itemId
  const indexResult = await (itemSearchIndex.partialUpdateObject({
    objectID,
    ...updateItem
  }) as any)
  return indexResult
}

export {
  updateItemSearchIndex1
}

