
import { algoliasearchVariableTree } from '../../../../algoliasearch-type-4-variables/variables-1-algoliasearch'

import { InitializeSearchIndexOptions } from '../../../../algoliasearch-type-2-data-structures/data-structures-2-initialize-search-index-options'


const mockAlgoliaSearchIndexObject = {
  search: (_argumentList: any) => { return {} },
  searchForFacetValues: (_argumentList: any) => { return {} },
  findAnswers: (_argumentList: any) => { return {} },
  batch: (_argumentList: any) => { return {} },
  getObject: (_argumentList: any) => { return {} },
  getObjects: (_argumentList: any) => { return {} },
  saveObject: (_argumentList: any) => { return {} },
  saveObjects: (_argumentList: any) => { return {} },
  waitTask: (_argumentList: any) => { return {} },
  setSettings: (_argumentList: any) => { return {} },
  getSettings: (_argumentList: any) => { return {} },
  partialUpdateObject: (_argumentList: any) => { return {} },
  partialUpdateObjects: (_argumentList: any) => { return {} },
  deleteObject: (_argumentList: any) => { return {} },
  deleteObjects: (_argumentList: any) => { return {} },
  deleteBy: (_argumentList: any) => { return {} },
  clearObjects: (_argumentList: any) => { return {} },
  browseObjects: (_argumentList: any) => { return {} },
  getObjectPosition: (_argumentList: any) => { return {} },
  findObject: (_argumentList: any) => { return {} },
  exists: (_argumentList: any) => { return {} },
  saveSynonym: (_argumentList: any) => { return {} },
  saveSynonyms: (_argumentList: any) => { return {} },
  getSynonym: (_argumentList: any) => { return {} },
  searchSynonyms: (_argumentList: any) => { return {} },
  browseSynonyms: (_argumentList: any) => { return {} },
  deleteSynonym: (_argumentList: any) => { return {} },
  clearSynonyms: (_argumentList: any) => { return {} },
  replaceAllObjects: (_argumentList: any) => { return {} },
  replaceAllSynonyms: (_argumentList: any) => { return {} },
  searchRules: (_argumentList: any) => { return {} },
  getRule: (_argumentList: any) => { return {} },
  deleteRule: (_argumentList: any) => { return {} },
  saveRule: (_argumentList: any) => { return {} },
  saveRules: (_argumentList: any) => { return {} },
  replaceAllRules: (_argumentList: any) => { return {} },
  browseRules: (_argumentList: any) => { return {} },
  clear: (_argumentList: any) => { return {} }
}


function initializeAlgoliasearchVariables1 (itemSearchIndexNameToIndexIdMap?: { [indexName: string]: string }, _itemSearchIndexNameToOptionsMap?: { [indexName: string]: InitializeSearchIndexOptions }) {
  algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap = itemSearchIndexNameToIndexIdMap || {}
  const { itemSearchIndexVariableTree } = algoliasearchVariableTree

  for (let searchIndexName in algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap) {
    if (!algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap.hasOwnProperty(searchIndexName)) {
      continue
    }

    const searchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[searchIndexName]
    // const searchIndexOptions = itemSearchIndexNameToOptionsMap ? itemSearchIndexNameToOptionsMap[searchIndexName] : {}

    ;(itemSearchIndexVariableTree[searchIndexId] as any) = mockAlgoliaSearchIndexObject
  }
}

export {
  initializeAlgoliasearchVariables1
}

