
import { algoliasearchClient } from '../../algoliasearch-type-3-constants/constants-1-algoliasearch'

import { algoliasearchVariableTree } from '../../algoliasearch-type-4-variables/variables-1-algoliasearch'

import { InitializeSearchIndexOptions } from '../../algoliasearch-type-2-data-structures/data-structures-2-initialize-search-index-options'

function initializeAlgoliasearchVariables1 (itemSearchIndexNameToIndexIdMap?: { [indexName: string]: string }, itemSearchIndexNameToOptionsMap?: { [indexName: string]: InitializeSearchIndexOptions }) {
  algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap = itemSearchIndexNameToIndexIdMap || {}
  const { itemSearchIndexVariableTree } = algoliasearchVariableTree

  for (let searchIndexName in algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap) {
    if (!algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap.hasOwnProperty(searchIndexName)) {
      continue
    }

    const searchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[searchIndexName]
    const searchIndexOptions = itemSearchIndexNameToOptionsMap ? itemSearchIndexNameToOptionsMap[searchIndexName] : {}

    itemSearchIndexVariableTree[searchIndexId] = algoliasearchClient.initIndex(searchIndexId)

    itemSearchIndexVariableTree[searchIndexId].setSettings(searchIndexOptions.settings || {})
  }
}


export {
  initializeAlgoliasearchVariables1
}



