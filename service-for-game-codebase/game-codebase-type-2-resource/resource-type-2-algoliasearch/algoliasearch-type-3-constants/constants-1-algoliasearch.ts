


import algoliasearch from 'algoliasearch'

import {
  algoliaApplicationId,
  algoliaAdminApiKey // ,
  // algoliaSearchOnlyApiKey
} from '../../../../service-for-secret-variables/secret-variables-type-2-algoliasearch/algoliasearch-1-development'

const ALGOLIA_APP_ID = process.env.ALGOLIA_APPLICATION_ID || algoliaApplicationId
// const ALGOLIA_SEACH_ONLY_API_KEY = process.env.ALGOLIA_SEACH_ONLY_API_KEY || algoliaSearchOnlyApiKey
const ALGOLIA_ADMIN_API_KEY = process.env.ALGOLIA_ADMIN_API_KEY || algoliaAdminApiKey

const algoliasearchClient = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_ADMIN_API_KEY)


export {
  algoliasearchClient
}



