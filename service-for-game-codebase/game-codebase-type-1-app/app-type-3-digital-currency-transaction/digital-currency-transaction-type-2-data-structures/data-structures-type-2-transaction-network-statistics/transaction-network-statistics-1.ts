


class DigitalCurrencyTransactionNetworkStatistics {

  amountOfDigitalCurrencySent?: number
  amountOfDigitalCurrencyCreated?: number
  amountOfDigitalCurrencyDeleted?: number
  amountOfDigitalCurrencyActive?: number

  numberOfTransactionsCreated?: number
  numberOfTransactionsDeleted?: number
  numberOfActiveTransactions?: number

  numberOfRecurringTransactionsCreated?: number
  numberOfRecurringTransactionsDeleted?: number
  numberOfRecurringTransactionsActive?: number

  amountOfDigitalCurrencyTable?: {
    [dateId: number]: {
      amountOfDigitalCurrencySent: number
      amountOfDigitalCurrencyCreated: number
      amountOfDigitalCurrencyDeleted: number
      amountOfDigitalCurrencyActive: number
    }
  }

  // delete all of transactionIdTable after 10 days
  // delete all time table keys besides within the last 3 months
  createdTransactionVariableTable?: TimeTableWithTransactionId
  createdRecurringTransactionVariableTable?: TimeTableWithTransactionId

  deletedTransactionVariableTable?: TimeTableWithTransactionId
  deletedRecurringTransactionVariableTable?: TimeTableWithTransactionId

  activeTransactionVariableTable?: TimeTableWithTransactionId

}

class TimeTableWithTransactionId {
  transactionIdTable: { [transactionId: string]: boolean } = {}
  timeTable: {
    transactionIdTimeTable: {
      [dateId: number]: {
        isBeingProcessed: boolean
        isProcessingCompleted: boolean
        numberOfCreatedTransactions: number
        hourTimeTable: {
          [hourId: number]: {
            isBeingProcessed: boolean
            isProcessingCompleted: boolean
            minuteTimeTable: {
              [minuteId: number]: {
                isBeingProcessed: boolean
                isProcessingCompleted: boolean
                numberOfProcessedTransactions: number
                transactionIdVariableTable: {
                  [itemId: string]: boolean
                }
              }
            }
          }
        }
      }
    }
    numberOfProcessedTransactionsTimeTable: {
      [dateId: number]: {
        isBeingProcessed: boolean
        isProcessingCompleted: boolean
        numberOfCreatedTransactions: number
        hourTimeTable: {
          [hourId: number]: {
            isBeingProcessed: boolean
            isProcessingCompleted: boolean
            minuteTimeTable: {
              [minuteId: number]: {
                isBeingProcessed: boolean
                isProcessingCompleted: boolean
                numberOfProcessedTransactions: number
                transactionIdVariableTable: {
                  [itemId: string]: boolean
                }
              }
            }
          }
        }
      }
    }
  }
}


export {
  DigitalCurrencyTransactionNetworkStatistics
}



