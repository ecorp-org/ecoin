
import { GetItem1Options } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'


class GetAccountTransactionStatisticsOptions {
  digitalCurrencyAccountId?: string = ''
  startDate?: Date
  endDate?: Date

  getItem1Options?: GetItem1Options
}

export {
  GetAccountTransactionStatisticsOptions
}

