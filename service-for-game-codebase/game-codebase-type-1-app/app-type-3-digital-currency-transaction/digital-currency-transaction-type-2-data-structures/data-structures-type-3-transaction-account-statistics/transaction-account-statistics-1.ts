




class DigitalCurrencyTransactionAccountStatistics {
  amountOfDigitalCurrencySent?: number
  amountOfDigitalCurrencyReceived?: number
  totalDigitalCurrencyBalance?: number
  numberOfTransactionsCreated?: number
  numberOfTransactionsReceived?: number
  numberOfTransactionsDeleted?: number
  numberOfActiveTransactions?: number
  numberOfRecurringTransactionsCreated?: number
  numberOfRecurringTransactionsReceived?: number
  numberOfRecurringTransactionsDeleted?: number
  numberOfActiveRecurringTransactions?: number
  totalRecurringTransactionIncome?: number
  totalRecurringTransactionExpenditure?: number
  numberOfAccountsSentATransactionTo?: number
  numberOfAccountsReceivedATransactionFrom?: number


  createdTransactionVariableTable?: TimeTableWithTransactionId
  receivedTransactionVariableTable?: TimeTableWithTransactionId
  deletedTransactionVariableTable?: TimeTableWithTransactionId
  activeTransactionVariableTable?: TimeTableWithTransactionId

  createdRecurringTransactionVariableTable?: TimeTableWithTransactionId
  receivedRecurringTransactionVariableTable?: TimeTableWithTransactionId
  deletedRecurringTransactionVariableTable?: TimeTableWithTransactionId
  activeRecurringTransactionVariableTable?: TimeTableWithTransactionId

  amountOfDigitalCurrencySentVariableTable?: TimeTableWithTransactionId
  amountOfDigitalCurrencyReceivedVariableTable?: TimeTableWithTransactionId
  totalDigitalCurrencyBalanceVariableTable?: TimeTableWithTransactionId

  totalRecurringTransactionIncomeVariableTable?: TimeTableWithTransactionId
  totalRecurringTransactionExpenditureVariableTable?: TimeTableWithTransactionId

  accountsSentATransactionToVariableTable?: TimeTableWithTransactionId
  accountsReceivedATransactionFromVariableTable?: TimeTableWithTransactionId

}


class TimeTableWithTransactionId {
  accountIdVariableTable: { [accountId: string]: boolean } = {}
  transactionIdVariableTable: { [transactionId: string]: number } = {}
  timeTable: {
    accountIdTimeTable: {
      [dateId: number]: {
        [hourId: number]: {
          [minuteId: number]: {
            accountIdVariableTable: { [accountId: string]: number }
          }
        }
      }
    }
    transactionIdTimeTable: {
      [dateId: number]: {
        [hourId: number]: {
          [minuteId: number]: {
            transactionIdVariableTable: { [transactionId: string]: number }
          }
        }
      }
    }
    digitalCurrencyAmountTimeTable: {
      [dateId: number]: {
        [hourId: number]: {
          [minuteId: number]: {
            digitalCurrencyAmount: number
          }
        }
      }
    }
    numberOfProcessedAccountsTimeTable: {
      [dateId: number]: {
        [hourId: number]: {
          [minuteId: number]: {
            numberOfProcessedAccounts: number
          }
        }
      }
    }
    numberOfProcessedTransactionsTimeTable: {
      [dateId: number]: {
        [hourId: number]: {
          [minuteId: number]: {
            numberOfProcessedTransactions: number
          }
        }
      }
    }
  }
}

export {
  DigitalCurrencyTransactionAccountStatistics
}



