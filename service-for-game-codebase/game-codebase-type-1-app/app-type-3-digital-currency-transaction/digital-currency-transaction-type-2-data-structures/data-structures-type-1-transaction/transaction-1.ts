
// import { NUMBER_OF_MILLISECONDS_IN_A_MINUTE } from '../../../app-type-1-general/general-type-3-constants/constants-3-time'

// const earliestDate = '1970-01-01T00:00:00Z'

class DigitalCurrencyTransaction {
  transactionId: string = ''
  transactionOrderIndex: number = 0
  transactionName?: string = ''

  accountSenderId: string = ''

  accountRecipientIdVariableTable: { [accountId: string]: {
    dateAccountDeleted?: number
    transactionCurrencyAmount: number
  } } = {}
  onBehalfOfAccountId: string = ''

  transactionCurrencyAmount: number = 0

  transactionType: string = ''
  textMemorandum?: string = ''

  specialTransactionType?: string = ''

  dateTransactionCreated?: string = (new Date()).toISOString().split('.')[0] + 'Z' //  + (new Date()).getTimezoneOffset() * NUMBER_OF_MILLISECONDS_IN_A_MINUTE
  dateTransactionProcessed?: string

  dateTransactionScheduledForProcessing?: string
  dateTransactionReScheduledForProcessing?: string

  dateTransactionScheduledForCancellingRecurrence?: string

  isProcessed: boolean = false
  isBeingProcessed: boolean = false
  dateLastBeingProcessed?: string

  isRecurringTransaction: boolean = false
  millisecondRecurringDelay: number = 0
  numberOfTimesRecurred: number = 0
  numberOfTimesRecurredSinceRestart: number = 0

  dateRecurrenceCancelled?: string
  dateRecurrenceRestarted?: string

  originalTimeNumberUntilNextRecurrence?: number = 0
  originalTimeScaleUntilNextRecurrence?: string = ''

  numberOfTimesProcessingFailed?: number = 0
  latestReasonForProcessingFailed?: string = ''

  constructor (defaultOption?: DigitalCurrencyTransaction) {
    Object.assign(this, defaultOption)
  }
}

const SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME = 'SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME'



export {
  DigitalCurrencyTransaction,
  SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME
}


