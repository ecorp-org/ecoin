




import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'
import { deleteItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-1'

import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


import { getTransactionProcessingInformationTableAlgorithm } from '../algorithms-type-9-initialize-digital-currency-transaction-list/service-for-helper-algorithms/helper-algorithms-1-get-transaction-processing-information-table'


// import { getUTCDate1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'
// import { getMillisecondTimePartitionList1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { NUMBER_OF_MILLISECONDS_IN_A_MINUTE } from '../../../app-type-1-general/general-type-3-constants/constants-3-time'

async function updateDigitalCurrencyTransaction1 (digitalCurrencyTransactionId: string, propertyId: string, propertyValue: string) {

  if (!propertyId) {
    throw Error('Update digital currency transaction requires a property id parameter')
  }

  let digitalCurrencyTransaction = await getItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId) as DigitalCurrencyTransaction

  if (!digitalCurrencyTransaction) {
    throw Error(`Digital Currency Transaction Doesn't Exist`)
  }

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const { dateTransactionScheduledToBeProcessedTimePartition, dateTransactionRecurredTimePartition } = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

  let isAllowedPropertyUpdate = false

  switch (propertyId) {
    case 'dateTransactionReScheduledForProcessing':
      const newReSchedulingDate = (new Date(propertyValue)).toISOString().split('.')[0] + 'Z'
      const newReSchedulingDateTimePartition = getMillisecondTimePartitionList2(newReSchedulingDate)

      const isTransactionDateTooMuchInThePast = (new Date(newReSchedulingDate)).getTime() - (new Date(currentDate)).getTime() < -(5 * NUMBER_OF_MILLISECONDS_IN_A_MINUTE) // Before 5 minutes in the past
      const isValidReSchedulingDate = !isTransactionDateTooMuchInThePast && newReSchedulingDate
      if (!isValidReSchedulingDate) {
        throw Error(`Cannot update digital currency transaction with invalid re-scheduling date: ${isTransactionDateTooMuchInThePast ? 'date is too far in the past' : newReSchedulingDate}`)
      }

      await deleteItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${dateTransactionScheduledToBeProcessedTimePartition.millisecondDay}/${dateTransactionScheduledToBeProcessedTimePartition.millisecondHour}/${dateTransactionScheduledToBeProcessedTimePartition.millisecondMinute}/${dateTransactionScheduledToBeProcessedTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
      await deleteItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${dateTransactionRecurredTimePartition.millisecondDay}/${dateTransactionRecurredTimePartition.millisecondHour}/${dateTransactionRecurredTimePartition.millisecondMinute}/${dateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
      await deleteItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${dateTransactionRecurredTimePartition.millisecondDay}/${dateTransactionRecurredTimePartition.millisecondHour}/${dateTransactionRecurredTimePartition.millisecondMinute}/${dateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)

      await updateItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId, {
        'isProcessed': false,
        'dateTransactionScheduledForProcessing': newReSchedulingDate,
        'dateRecurrenceRestarted': newReSchedulingDate,
        'dateTransactionReScheduledForProcessing': 0,
        'numberOfTimesRecurredSinceRestart': 0,
        'dateRecurrenceCancelled': 0,
        'dateTransactionScheduledForCancellingRecurrence': 0
      })

      await updateItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${newReSchedulingDateTimePartition.millisecondDay}/${newReSchedulingDateTimePartition.millisecondHour}/${newReSchedulingDateTimePartition.millisecondMinute}/${newReSchedulingDateTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, { 'isProcessed': false, 'dateCreated': newReSchedulingDate })

      isAllowedPropertyUpdate = true
      break
    case 'dateTransactionScheduledForCancellingRecurrence':
      if (propertyValue) {
        const newCancellingRecurrenceDate = (new Date(propertyValue)).toISOString().split('.')[0] + 'Z'
        const isCancellingDateTooMuchInThePast = (new Date(newCancellingRecurrenceDate)).getTime() - (new Date(currentDate)).getTime() < -(5 * NUMBER_OF_MILLISECONDS_IN_A_MINUTE) // Before 5 minutes in the past
        const isValidReCancellingDate = !isCancellingDateTooMuchInThePast
        if (!isValidReCancellingDate) {
          throw Error(`Cannot update digital currency transaction with invalid cancelling date: ${isCancellingDateTooMuchInThePast ? 'date is too far in the past' : ''}`)
        }
        await updateItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId, {
          'dateTransactionScheduledForCancellingRecurrence': newCancellingRecurrenceDate
        })

        if (newCancellingRecurrenceDate <= currentDate) {
          await updateItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId, {
            'dateRecurrenceCancelled': newCancellingRecurrenceDate
          })
        }
      } else {
        await deleteItem1(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransactionId}`, 'dateTransactionScheduledForCancellingRecurrence')
      }
      isAllowedPropertyUpdate = true
      break
    default:
      isAllowedPropertyUpdate = false
  }

  if (!isAllowedPropertyUpdate) {
    throw Error(`Cannot update transaction property ${propertyId}`)
  }

  // update the transaction
  // await updateItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId, { [propertyId]: propertyValue })

  // return the digital currency transaction
  digitalCurrencyTransaction = await getItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId) as DigitalCurrencyTransaction
  return digitalCurrencyTransaction
}

const updateDigitalCurrencyTransaction1Algorithm = {
  function: updateDigitalCurrencyTransaction1
}


export {
  updateDigitalCurrencyTransaction1Algorithm
}






