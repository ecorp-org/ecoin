

import { getItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { updateItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'

import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function updateDigitalCurrencyAccount2FirebaseAdmin (digitalCurrencyTransactionId: string, propertyId: string, propertyValue: string) {

  let digitalCurrencyTransaction: DigitalCurrencyTransaction | null = null

  digitalCurrencyTransaction = await getItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId)

  await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId, { [propertyId]: propertyValue })

  return digitalCurrencyTransaction
}


export {
  updateDigitalCurrencyAccount2FirebaseAdmin
}

