

// import 'mocha'
// import { expect } from 'chai'

// import { getTransactionProcessingInformationTableAlgorithm } from '../helper-algorithms-1-get-transaction-processing-information-table'

// import { initializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-initialize-resources'
// import { uninitializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-2-uninitialize-resources'
// import { createDigitalCurrencyAccountList } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-5-create-digital-currency-account/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-account-list'
// import { createDigitalCurrencyTransactionList } from '../../../algorithms-type-1-create-digital-currency-transaction/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-transaction-list'

// import { DigitalCurrencyAccount } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
// import { DigitalCurrencyTransaction } from '../../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

// import {
//   // NUMBER_OF_MILLISECONDS_IN_A_MINUTE,
//   // NUMBER_OF_MILLISECONDS_IN_AN_HOUR,
//   NUMBER_OF_MILLISECONDS_IN_A_DAY
// } from '../../../../../app-type-1-general/general-type-3-constants/constants-3-time'

// // import { getMillisecondTimePartitionList1 } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'




// describe('get-transaction-processing-information-table', () => {
//   let digitalCurrencyAccountList: DigitalCurrencyAccount[] = []
//   let digitalCurrencyTransactionList: DigitalCurrencyTransaction[] = []

//   beforeEach(async function () {
//     this.timeout(30000)
//     await initializeResources()
//     const accountListObject = await createDigitalCurrencyAccountList({
//       numberOfAccounts: 2,
//       minimumAmountOfCurrency: 10000
//     })
//     digitalCurrencyAccountList = accountListObject.digitalCurrencyAccountList
//   })

//   afterEach(async function () {
//     this.timeout(30000)
//     await uninitializeResources()
//     digitalCurrencyAccountList = []
//     digitalCurrencyTransactionList = []
//   })

//   it('should return the expected transaction recurrence date #1 - 0 Recurrence', async () => {
//     // Create Transaction
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions: 1,
//       oldestTransactionDate: new Date((new Date()).getTime() - 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY),
//       newestTransactionDate: new Date(),
//       minimumMillisecondRecurringTransactionDelay: 100,
//       maximumMillisecondRecurringTransactionDelay: 200
//     })

//     // Suppose N number of recurrences occurred
//     let digitalCurrencyTransaction = digitalCurrencyTransactionList[0]
//     digitalCurrencyTransaction.numberOfTimesRecurred = 0

//     const processingInformationTable = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

//     expect(processingInformationTable.dateTransactionRecurred.getTime()).to.equal(digitalCurrencyTransaction.dateTransactionCreated!)
//   })

//   it('should return the expected transaction recurrence date #2 - N Recurrence', async () => {
//     // Create Transaction
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions: 1,
//       oldestTransactionDate: new Date((new Date()).getTime() - 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY),
//       newestTransactionDate: new Date(),
//       minimumMillisecondRecurringTransactionDelay: 100,
//       maximumMillisecondRecurringTransactionDelay: 200
//     })

//     let digitalCurrencyTransaction = digitalCurrencyTransactionList[0]
//     digitalCurrencyTransaction.numberOfTimesRecurred = Math.floor(Math.random() * 1000)

//     const processingInformationTable = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

//     const expectedDateTransactionRecurred = digitalCurrencyTransaction.dateTransactionCreated! + digitalCurrencyTransaction.numberOfTimesRecurred * digitalCurrencyTransaction.millisecondRecurringDelay
//     expect(processingInformationTable.dateTransactionRecurred.getTime()).to.equal(expectedDateTransactionRecurred)
//   })

//   it('should return the expected transaction recurrence date #3 - N Recurrence and Scheduled Delay', async () => {

//     const maximumNumberOfMillisecondsIntoTheFuture = 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY

//     // Create Transaction
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions: 1,
//       oldestTransactionDate: new Date((new Date()).getTime() - 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY),
//       newestTransactionDate: new Date(),
//       minimumMillisecondRecurringTransactionDelay: 100,
//       maximumMillisecondRecurringTransactionDelay: 200,
//       maximumMillisecondTimeFromNowForScheduledDate: maximumNumberOfMillisecondsIntoTheFuture
//     })

//     let digitalCurrencyTransaction = digitalCurrencyTransactionList[0]
//     digitalCurrencyTransaction.numberOfTimesRecurred = Math.floor(Math.random() * 1000)

//     expect(digitalCurrencyTransaction.dateTransactionCreated!).to.be.lessThan(digitalCurrencyTransaction.dateTransactionScheduledForProcessing!)

//     const processingInformationTable = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

//     const expectedDateTransactionRecurred = digitalCurrencyTransaction.dateTransactionScheduledForProcessing! + digitalCurrencyTransaction.numberOfTimesRecurred * digitalCurrencyTransaction.millisecondRecurringDelay
//     expect(processingInformationTable.dateTransactionRecurred.getTime()).to.equal(expectedDateTransactionRecurred)
//   })

//   it('should return the expected transaction recurrence date #4 - N Recurrence and Scheduled Delay and Restart', async () => {

//     const maximumNumberOfMillisecondsIntoTheFuture = 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY

//     // Create Transaction
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions: 1,
//       oldestTransactionDate: new Date((new Date()).getTime() - 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY),
//       newestTransactionDate: new Date(),
//       minimumMillisecondRecurringTransactionDelay: 100,
//       maximumMillisecondRecurringTransactionDelay: 200,
//       maximumMillisecondTimeFromNowForScheduledDate: maximumNumberOfMillisecondsIntoTheFuture
//     })

//     let digitalCurrencyTransaction = digitalCurrencyTransactionList[0]
//     digitalCurrencyTransaction.dateRecurrenceRestarted = (new Date(digitalCurrencyTransaction.dateTransactionScheduledForProcessing! + Math.floor(Math.random() * maximumNumberOfMillisecondsIntoTheFuture))).getTime()
//     digitalCurrencyTransaction.numberOfTimesRecurred = Math.floor(Math.random() * 1000)
//     digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart = Math.floor(Math.random() * 1000)

//     expect(digitalCurrencyTransaction.dateTransactionCreated!).to.be.lessThan(digitalCurrencyTransaction.dateTransactionScheduledForProcessing!)

//     const processingInformationTable = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

//     const expectedDateTransactionRecurred = digitalCurrencyTransaction.dateRecurrenceRestarted! + digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart * digitalCurrencyTransaction.millisecondRecurringDelay
//     expect(processingInformationTable.dateTransactionRecurred.getTime()).to.equal(expectedDateTransactionRecurred)
//   })

//   it('should return the expected transaction recurrence date #5 - N Recurrence and Rescheduled Processing Date', async () => {

//     const maximumNumberOfMillisecondsIntoTheFuture = 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY

//     // Create Transaction
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions: 1,
//       oldestTransactionDate: new Date((new Date()).getTime() - 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY),
//       newestTransactionDate: new Date(),
//       minimumMillisecondRecurringTransactionDelay: 100,
//       maximumMillisecondRecurringTransactionDelay: 200,
//       maximumMillisecondTimeFromNowForScheduledDate: maximumNumberOfMillisecondsIntoTheFuture
//     })

//     let digitalCurrencyTransaction = digitalCurrencyTransactionList[0]
//     digitalCurrencyTransaction.dateTransactionReScheduledForProcessing = (new Date(digitalCurrencyTransaction.dateTransactionScheduledForProcessing! + Math.floor(Math.random() * maximumNumberOfMillisecondsIntoTheFuture))).getTime()
//     digitalCurrencyTransaction.numberOfTimesRecurred = Math.floor(Math.random() * 1000)
//     digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart = Math.floor(Math.random() * 1000)

//     expect(digitalCurrencyTransaction.dateTransactionCreated!).to.be.lessThan(digitalCurrencyTransaction.dateTransactionReScheduledForProcessing!)

//     const processingInformationTable = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

//     const expectedDateTransactionRecurred = digitalCurrencyTransaction.dateTransactionReScheduledForProcessing! + digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart * digitalCurrencyTransaction.millisecondRecurringDelay
//     expect(processingInformationTable.dateTransactionRecurred.getTime()).to.equal(expectedDateTransactionRecurred)
//   })
// })




