


import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'


// Check Mark (Completed), Warning (Pending)
      // Error (Failed), Error (Stopped With Error)
      // Pause (Paused), Check Mark (Active)

const TRANSACTION_PROCESSING_COMPLETE = 'complete'
const TRANSACTION_PROCESSING_PENDING = 'pending'
const TRANSACTION_PROCESSING_FAILED = 'failed'
const TRANSACTION_PROCESSING_FAILED_WITH_RECURRING_TRANSACTION = 'failed-with-recurring-transaction'
const TRANSACTION_PROCESSING_STOPPED_WITH_RECURRING_TRANSACTION = 'stopped-with-recurring-transaction'
const TRANSACTION_PROCESSING_ACTIVE_WITH_RECURRING_TRANSACTION = 'active-with-recurring-transaction'


function getTransactionProcessingStatus (digitalCurrencyTransaction: DigitalCurrencyTransaction) {

  if (digitalCurrencyTransaction.isRecurringTransaction) {
    if (digitalCurrencyTransaction.latestReasonForProcessingFailed) {
      return TRANSACTION_PROCESSING_FAILED_WITH_RECURRING_TRANSACTION
    }

    if (digitalCurrencyTransaction.dateRecurrenceCancelled) {
      return TRANSACTION_PROCESSING_STOPPED_WITH_RECURRING_TRANSACTION
    }

    return digitalCurrencyTransaction.isProcessed ? TRANSACTION_PROCESSING_ACTIVE_WITH_RECURRING_TRANSACTION : TRANSACTION_PROCESSING_PENDING
  } else {
    if (digitalCurrencyTransaction.latestReasonForProcessingFailed) {
      return TRANSACTION_PROCESSING_FAILED
    }
    return digitalCurrencyTransaction.isProcessed ? TRANSACTION_PROCESSING_COMPLETE : TRANSACTION_PROCESSING_PENDING
  }
}

const getTransactionProcessingStatusAlgorithm = {
  function: getTransactionProcessingStatus
}

export {
  getTransactionProcessingStatusAlgorithm,
  TRANSACTION_PROCESSING_COMPLETE,
  TRANSACTION_PROCESSING_PENDING,
  TRANSACTION_PROCESSING_FAILED,
  TRANSACTION_PROCESSING_FAILED_WITH_RECURRING_TRANSACTION,
  TRANSACTION_PROCESSING_STOPPED_WITH_RECURRING_TRANSACTION,
  TRANSACTION_PROCESSING_ACTIVE_WITH_RECURRING_TRANSACTION
}

