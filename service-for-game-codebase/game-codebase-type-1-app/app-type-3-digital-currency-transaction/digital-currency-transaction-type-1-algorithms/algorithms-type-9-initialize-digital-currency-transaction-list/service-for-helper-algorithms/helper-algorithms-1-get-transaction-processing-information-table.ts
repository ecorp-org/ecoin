
import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import { getMillisecondTimePartitionList2 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


function getTransactionProcessingInformationTable (digitalCurrencyTransaction: DigitalCurrencyTransaction) {
  const dateTransactionScheduledToBeProcessed = digitalCurrencyTransaction.dateTransactionReScheduledForProcessing ? new Date(digitalCurrencyTransaction.dateTransactionReScheduledForProcessing) : (digitalCurrencyTransaction.dateTransactionScheduledForProcessing ? new Date(digitalCurrencyTransaction.dateTransactionScheduledForProcessing) : new Date(digitalCurrencyTransaction.dateTransactionCreated!))
  const dateTransactionScheduledToBeProcessedTimePartition = getMillisecondTimePartitionList2(dateTransactionScheduledToBeProcessed.toISOString().split('.')[0] + 'Z')
  const dateTransactionRecurrenceRestarted = digitalCurrencyTransaction.dateRecurrenceRestarted ? new Date(digitalCurrencyTransaction.dateRecurrenceRestarted) : dateTransactionScheduledToBeProcessed
  // const dateTransactionRecurrenceRestartedTimePartition = getMillisecondTimePartitionList1(dateTransactionRecurrenceRestarted.getTime())
  const numberOfRecurrencesSinceRestarting = digitalCurrencyTransaction.dateRecurrenceRestarted ? digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart : digitalCurrencyTransaction.numberOfTimesRecurred
  const dateTransactionRecurred = new Date(dateTransactionRecurrenceRestarted.getTime() + (numberOfRecurrencesSinceRestarting * digitalCurrencyTransaction.millisecondRecurringDelay))
  const dateTransactionRecurredTimePartition = getMillisecondTimePartitionList2(dateTransactionRecurred.toISOString().split('.')[0] + 'Z')
  return {
    dateTransactionRecurred,
    dateTransactionScheduledToBeProcessed,

    dateTransactionRecurrenceRestarted,

    dateTransactionRecurredTimePartition,
    dateTransactionScheduledToBeProcessedTimePartition,

    numberOfRecurrencesSinceRestarting
  }
}

const getTransactionProcessingInformationTableAlgorithm = {
  function: getTransactionProcessingInformationTable
}

export {
  getTransactionProcessingInformationTableAlgorithm
}
