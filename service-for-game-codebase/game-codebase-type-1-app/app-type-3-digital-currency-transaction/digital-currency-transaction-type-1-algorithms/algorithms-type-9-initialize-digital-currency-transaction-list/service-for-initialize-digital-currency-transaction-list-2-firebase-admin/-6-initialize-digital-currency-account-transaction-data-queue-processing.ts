
import { getNumberWithFixedDecmial1 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-9-get-number-with-fixed-decimal/get-number-with-fixed-decimal-1'

import { DigitalCurrencyAccount } from '../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { updateItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { getItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'
import { createItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-2-firebase-admin'

import { getDigitalCurrencyAccountById2FirebaseAdmin } from '../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-15-get-digital-currency-account-by-id/get-digital-currency-account-by-id-2-firebase-admin'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID
} from '../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  NUMBER_OF_MILLISECONDS_IN_A_MINUTE
} from '../../../../app-type-1-general/general-type-3-constants/constants-3-time'

// import { getUTCDate1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'
// import { getMillisecondTimePartitionList1 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


let variableTable: {
  NUMBER_OF_MILLISECONDS_TO_DELETE_UNUSED_ACCOUNT_DATA: number
} = {
  NUMBER_OF_MILLISECONDS_TO_DELETE_UNUSED_ACCOUNT_DATA: 5 * NUMBER_OF_MILLISECONDS_IN_A_MINUTE
}

async function initializeDigitalCurrencyAccountTransactionDataQueueProcessing () {
  const initializeTransactionDataQueueAccountIdVariableTable = await getItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue`, 'accountIdVariableTable') || {}

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'

  for (let accountId in initializeTransactionDataQueueAccountIdVariableTable) {
    if (!initializeTransactionDataQueueAccountIdVariableTable.hasOwnProperty(accountId)) {
      continue
    }
    const accountObject = initializeTransactionDataQueueAccountIdVariableTable[accountId]

    const numberOfItemsToProcess = Object.keys(accountObject.dataQueue || {}).length

    // Delete empty data queue entries
    if (numberOfItemsToProcess === 0) {
      continue
    }

    // Ensure the data queue is processed at least once
    // each minute
    if (accountObject.isDataQueueBeingProcessed) {
      const hasDataToProcess = numberOfItemsToProcess > 0
      if (hasDataToProcess) {
        const millisecondTimeUntilLastIsProcessingChecked = accountObject.millisecondTimeUntilLastIsProcessingChecked

        if (!millisecondTimeUntilLastIsProcessingChecked) {
          await updateItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue/accountIdVariableTable`, accountId, { millisecondTimeUntilLastIsProcessingChecked: currentDate })
          continue
        }

        if ((new Date(currentDate)).getTime() - millisecondTimeUntilLastIsProcessingChecked < NUMBER_OF_MILLISECONDS_IN_A_MINUTE) {
          continue
        } else {
          await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue/accountIdVariableTable/${accountId}`, 'millisecondTimeUntilLastIsProcessingChecked')
        }
      }
    }

    // Continue to process the data queue
    await updateItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue/accountIdVariableTable`, accountId, { isDataQueueBeingProcessed: true })

    const transactionDataList = Object.keys(accountObject.dataQueue || {}).map((transactionDateId: string) => {
      return {
        transactionDateId,
        ...accountObject.dataQueue[transactionDateId]
      }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemB.dateCreated)).getTime() - (new Date(itemA.dateCreated)).getTime()
    })

    for (let i = 0; i < transactionDataList.length; i++) {
      const transactionObject = transactionDataList[i]
      await initializeDigitalCurrencyAccountTransactionUpdate(transactionObject.digitalCurrencySenderAccountId, accountId, transactionObject.transactionId, transactionObject.transactionCurrencyAmount, transactionObject.isRecurringTransaction, transactionObject.isCreatingCurrencyTransaction, transactionObject.isRefundPayment)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue/accountIdVariableTable/${accountId}/dataQueue`, transactionObject.transactionDateId)
    }

    await updateItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue/accountIdVariableTable`, accountId, { isDataQueueBeingProcessed: false })
  }
}

async function initializeDigitalCurrencyAccountTransactionUpdate (digitalCurrencySenderAccountId: string, digitalCurrencyRecipientAccountId: string, digitalCurrencyTransactionId: string, currencyAmount: number, isRecurringTransaction?: boolean, isCreatingCurrencyTransaction?: boolean, isRefundPayment?: boolean) {
  // if currency amount would be less than 0, return
  // apply transaction account to currency amount
  // remove the data queue item from the data queue list
  // apply account statistics
  // apply network statistics
  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const digitalCurrencyAccount = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyRecipientAccountId) as DigitalCurrencyAccount
  currencyAmount = getNumberWithFixedDecmial1(currencyAmount)

  if (!digitalCurrencyAccount) {
    if (digitalCurrencySenderAccountId && currencyAmount >= 0) {
      // Send Refund To The Sender Account Id
      await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue/accountIdVariableTable/${digitalCurrencySenderAccountId}/dataQueue`, `${digitalCurrencyTransactionId}-refund-${currentDate}`, {
        transactionId: digitalCurrencyTransactionId,
        transactionCurrencyAmount: currencyAmount,
        dateCreated: currentDate,
        isRefundPayment: true
      }, false, false, true)
    }
    return
  }

  const digitalCurrencyAccountCurrencyAmount = getNumberWithFixedDecmial1(digitalCurrencyAccount.accountCurrencyAmount)

  const latestDigitalCurrencyAmount = getNumberWithFixedDecmial1(digitalCurrencyAccountCurrencyAmount + currencyAmount)

  if (latestDigitalCurrencyAmount >= 0) {
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyRecipientAccountId, { 'accountCurrencyAmount': latestDigitalCurrencyAmount })
  }

  console.log('updated: ', latestDigitalCurrencyAmount, '; ', currencyAmount, `${isCreatingCurrencyTransaction ? '(ubi)' : ''}`)

  if (isRefundPayment) {
    return
  }

  // Account Statistics

  await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyRecipientAccountId}/totalDigitalCurrencyBalance/dataQueue`, `${digitalCurrencyTransactionId}-${currentDate}`, {
    currencyAmount: latestDigitalCurrencyAmount,
    dateCreated: currentDate
  }, false, false, true)

  if (currencyAmount >= 0) {
    // Received
    await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyRecipientAccountId}/amountOfDigitalCurrencyReceived/dataQueue`, `${digitalCurrencyTransactionId}-${currentDate}`, {
      currencyAmount: Math.abs(currencyAmount),
      dateCreated: currentDate
    }, false, false, true)

    if (isRecurringTransaction) {
      await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyRecipientAccountId}/totalRecurringTransactionIncome/dataQueue`, `${digitalCurrencyTransactionId}-${currentDate}`, {
        currencyAmount: Math.abs(currencyAmount),
        dateCreated: currentDate
      }, false, false, true)
    }
  } else if (currencyAmount < 0) {
    // Sent

    await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyRecipientAccountId}/amountOfDigitalCurrencySent/dataQueue`, `${digitalCurrencyTransactionId}-${currentDate}`, {
      currencyAmount: Math.abs(currencyAmount),
      dateCreated: currentDate
    }, false, false, true)

    if (isRecurringTransaction) {
      await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyRecipientAccountId}/totalRecurringTransactionExpenditure/dataQueue`, `${digitalCurrencyTransactionId}-${currentDate}`, {
        currencyAmount: Math.abs(currencyAmount),
        dateCreated: currentDate
      }, false, false, true)
    }
  }

  // Network Statistics

  if (currencyAmount < 0) {
    if (isCreatingCurrencyTransaction) {
      await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/amountOfDigitalCurrencyCreated/dataQueue`, `${digitalCurrencyTransactionId}-${currentDate}`, {
        currencyAmount: Math.abs(currencyAmount),
        dateCreated: currentDate
      }, false, false, true)
    } else {
      await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/amountOfDigitalCurrencySent/dataQueue`, `${digitalCurrencyTransactionId}-${currentDate}`, {
        currencyAmount: Math.abs(currencyAmount),
        dateCreated: currentDate
      }, false, false, true)
    }
  }
}

const initializeDigitalCurrencyAccountTransactionUpdateAlgorithm = {
  function: initializeDigitalCurrencyAccountTransactionUpdate
}

const initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm = {
  function: initializeDigitalCurrencyAccountTransactionDataQueueProcessing
}

export {
  variableTable,

  initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm,
  initializeDigitalCurrencyAccountTransactionUpdateAlgorithm
}

