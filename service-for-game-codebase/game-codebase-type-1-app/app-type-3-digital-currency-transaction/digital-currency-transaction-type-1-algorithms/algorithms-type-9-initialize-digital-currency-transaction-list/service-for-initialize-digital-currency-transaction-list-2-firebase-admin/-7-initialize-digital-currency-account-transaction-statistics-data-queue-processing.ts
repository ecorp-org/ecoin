
import { getNumberWithFixedDecmial1 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-9-get-number-with-fixed-decimal/get-number-with-fixed-decimal-1'

import { updateItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { getItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID
} from '../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


import { getMillisecondTimePartitionList2 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


async function initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessing () {
  const initializeAccountTransactionStatisticsDataQueueAccountIdVariableTable = await getItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue`, 'accountIdVariableTable') || {}

  for (let accountId in initializeAccountTransactionStatisticsDataQueueAccountIdVariableTable) {
    if (!initializeAccountTransactionStatisticsDataQueueAccountIdVariableTable.hasOwnProperty(accountId)) {
      continue
    }

    const accountObject = initializeAccountTransactionStatisticsDataQueueAccountIdVariableTable[accountId]

    let totalDigitalCurrencyBalanceDataQueue = accountObject.totalDigitalCurrencyBalance ? accountObject.totalDigitalCurrencyBalance.dataQueue : {}
    let amountOfDigitalCurrencyReceivedDataQueue = accountObject.amountOfDigitalCurrencyReceived ? accountObject.amountOfDigitalCurrencyReceived.dataQueue : {}
    let totalRecurringTransactionIncomeDataQueue = accountObject.totalRecurringTransactionIncome ? accountObject.totalRecurringTransactionIncome.dataQueue : {}
    let amountOfDigitalCurrencySentDataQueue = accountObject.amountOfDigitalCurrencySent ? accountObject.amountOfDigitalCurrencySent.dataQueue : {}
    let totalRecurringTransactionExpenditureDataQueue = accountObject.totalRecurringTransactionExpenditure ? accountObject.totalRecurringTransactionExpenditure.dataQueue : {}

    let numberOfTransactionsCreatedDataQueue = accountObject.numberOfTransactionsCreated ? accountObject.numberOfTransactionsCreated.dataQueue : {}
    let numberOfTransactionsReceivedDataQueue = accountObject.numberOfTransactionsReceived ? accountObject.numberOfTransactionsReceived.dataQueue : {}
    let numberOfTransactionsDeletedDataQueue = accountObject.numberOfTransactionsDeleted ? accountObject.numberOfTransactionsDeleted.dataQueue : {}
    // let numberOfActiveTransactionsDataQueue = accountObject.numberOfActiveTransactions ? accountObject.numberOfActiveTransactions.dataQueue : {}
    let numberOfRecurringTransactionsCreatedDataQueue = accountObject.numberOfRecurringTransactionsCreated ? accountObject.numberOfRecurringTransactionsCreated.dataQueue : {}
    let numberOfRecurringTransactionsReceivedDataQueue = accountObject.numberOfRecurringTransactionsReceived ? accountObject.numberOfRecurringTransactionsReceived.dataQueue : {}
    let numberOfRecurringTransactionsDeletedDataQueue = accountObject.numberOfRecurringTransactionsDeleted ? accountObject.numberOfRecurringTransactionsDeleted.dataQueue : {}
    // let numberOfActiveRecurringTransactionsDataQueue = accountObject.numberOfActiveRecurringTransactions ? accountObject.numberOfActiveRecurringTransactions.dataQueue : {}
    let numberOfAccountsSentATransactionToDataQueue = accountObject.numberOfAccountsSentATransactionTo ? accountObject.numberOfAccountsSentATransactionTo.dataQueue : {}
    let numberOfAccountsReceivedATransactionFromDataQueue = accountObject.numberOfAccountsReceivedATransactionFrom ? accountObject.numberOfAccountsReceivedATransactionFrom.dataQueue : {}

    let totalDigitalCurrencyBalanceDataList = Object.keys(totalDigitalCurrencyBalanceDataQueue).map((transactionDateId: string) => {
      return { transactionDateId, ...totalDigitalCurrencyBalanceDataQueue[transactionDateId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let amountOfDigitalCurrencyReceivedDataList = Object.keys(amountOfDigitalCurrencyReceivedDataQueue).map((transactionDateId: string) => {
      return { transactionDateId, ...amountOfDigitalCurrencyReceivedDataQueue[transactionDateId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let totalRecurringTransactionIncomeDataList = Object.keys(totalRecurringTransactionIncomeDataQueue).map((transactionDateId: string) => {
      return { transactionDateId, ...totalRecurringTransactionIncomeDataQueue[transactionDateId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let amountOfDigitalCurrencySentDataList = Object.keys(amountOfDigitalCurrencySentDataQueue).map((transactionDateId: string) => {
      return { transactionDateId, ...amountOfDigitalCurrencySentDataQueue[transactionDateId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let totalRecurringTransactionExpenditureDataList = Object.keys(totalRecurringTransactionExpenditureDataQueue).map((transactionDateId: string) => {
      return { transactionDateId, ...totalRecurringTransactionExpenditureDataQueue[transactionDateId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    //
    let numberOfTransactionsCreatedDataList = Object.keys(numberOfTransactionsCreatedDataQueue).map((transactionId: string) => {
      return { transactionId, ...numberOfTransactionsCreatedDataQueue[transactionId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let numberOfTransactionsReceivedDataList = Object.keys(numberOfTransactionsReceivedDataQueue).map((transactionId: string) => {
      return { transactionId, ...numberOfTransactionsReceivedDataQueue[transactionId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let numberOfTransactionsDeletedDataList = Object.keys(numberOfTransactionsDeletedDataQueue).map((transactionId: string) => {
      return { transactionId, ...numberOfTransactionsDeletedDataQueue[transactionId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let numberOfRecurringTransactionsCreatedDataList = Object.keys(numberOfRecurringTransactionsCreatedDataQueue).map((transactionId: string) => {
      return { transactionId, ...numberOfRecurringTransactionsCreatedDataQueue[transactionId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let numberOfRecurringTransactionsReceivedDataList = Object.keys(numberOfRecurringTransactionsReceivedDataQueue).map((transactionId: string) => {
      return { transactionId, ...numberOfRecurringTransactionsReceivedDataQueue[transactionId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let numberOfRecurringTransactionsDeletedDataList = Object.keys(numberOfRecurringTransactionsDeletedDataQueue).map((transactionId: string) => {
      return { transactionId, ...numberOfRecurringTransactionsDeletedDataQueue[transactionId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
    })

    let numberOfAccountsSentATransactionToDataList = Object.keys(numberOfAccountsSentATransactionToDataQueue).map((accountDateId: string) => {
      return { accountDateId, ...numberOfAccountsSentATransactionToDataQueue[accountDateId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateAdded)).getTime() - (new Date(itemB.dateAdded)).getTime()
    })

    let numberOfAccountsReceivedATransactionFromDataList = Object.keys(numberOfAccountsReceivedATransactionFromDataQueue).map((accountDateId: string) => {
      return { accountDateId, ...numberOfAccountsReceivedATransactionFromDataQueue[accountDateId] }
    }).sort((itemA: any, itemB: any) => {
      return (new Date(itemA.dateAdded)).getTime() - (new Date(itemB.dateAdded)).getTime()
    })

    // Total Digital Currency Balance
    for (let i = 0; i < totalDigitalCurrencyBalanceDataList.length; i++) {
      const transactionObject = totalDigitalCurrencyBalanceDataList[i]
      if (!transactionObject || !transactionObject.transactionDateId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      // const totalDigitalCurrencyBalance = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/totalDigitalCurrencyBalanceVariableTable/timeTable/digitalCurrencyAmountTimeTable/${millisecondDay}`, `digitalCurrencyAmount`) || 0
      const newTotalDigitalCurrencyBalance = transactionObject.currencyAmount
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/totalDigitalCurrencyBalanceVariableTable/timeTable/digitalCurrencyAmountTimeTable`, `${millisecondDay}`, { 'digitalCurrencyAmount': newTotalDigitalCurrencyBalance }, false, true)
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'totalDigitalCurrencyBalance': newTotalDigitalCurrencyBalance })
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/totalDigitalCurrencyBalance/dataQueue`, transactionObject.transactionDateId)
    }

    // Amount of Digital Currency Received
    for (let i = 0; i < amountOfDigitalCurrencyReceivedDataList.length; i++) {
      const transactionObject = amountOfDigitalCurrencyReceivedDataList[i]
      if (!transactionObject || !transactionObject.transactionDateId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const amountOfDigitalCurrencyReceived = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `amountOfDigitalCurrencyReceived`) || 0
      const amountOfDigitalCurrencyReceivedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/amountOfDigitalCurrencyReceivedVariableTable/timeTable/digitalCurrencyAmountTimeTable/${millisecondDay}`, `digitalCurrencyAmount`) || 0
      const newAmountOfDigitalCurrencyReceived = getNumberWithFixedDecmial1(amountOfDigitalCurrencyReceived + transactionObject.currencyAmount)
      const newAmountOfDigitalCurrencyReceivedForToday = getNumberWithFixedDecmial1(amountOfDigitalCurrencyReceivedForToday + transactionObject.currencyAmount)
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'amountOfDigitalCurrencyReceived': newAmountOfDigitalCurrencyReceived })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/amountOfDigitalCurrencyReceivedVariableTable/timeTable/digitalCurrencyAmountTimeTable`, `${millisecondDay}`, { 'digitalCurrencyAmount': newAmountOfDigitalCurrencyReceivedForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/amountOfDigitalCurrencyReceived/dataQueue`, transactionObject.transactionDateId)
    }

    // Total Recurring Transaction Income
    for (let i = 0; i < totalRecurringTransactionIncomeDataList.length; i++) {
      const transactionObject = totalRecurringTransactionIncomeDataList[i]
      if (!transactionObject || !transactionObject.transactionDateId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const totalRecurringTransactionIncome = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `totalRecurringTransactionIncome`) || 0
      const totalRecurringTransactionIncomeForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/totalRecurringTransactionIncomeVariableTable/timeTable/digitalCurrencyAmountTimeTable/${millisecondDay}`, `digitalCurrencyAmount`) || 0
      const newTotalRecurringTransactionIncome = getNumberWithFixedDecmial1(totalRecurringTransactionIncome + transactionObject.currencyAmount)
      const newTotalRecurringTransactionIncomeForToday = getNumberWithFixedDecmial1(totalRecurringTransactionIncomeForToday + transactionObject.currencyAmount)
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'totalRecurringTransactionIncome': newTotalRecurringTransactionIncome })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/totalRecurringTransactionIncomeVariableTable/timeTable/digitalCurrencyAmountTimeTable`, `${millisecondDay}`, { 'digitalCurrencyAmount': newTotalRecurringTransactionIncomeForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/totalRecurringTransactionIncome/dataQueue`, transactionObject.transactionDateId)
    }

    // Amount of Digital Currency Sent
    for (let i = 0; i < amountOfDigitalCurrencySentDataList.length; i++) {
      const transactionObject = amountOfDigitalCurrencySentDataList[i]
      if (!transactionObject || !transactionObject.transactionDateId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const amountOfDigitalCurrencySent = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `amountOfDigitalCurrencySent`) || 0
      const amountOfDigitalCurrencySentForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/amountOfDigitalCurrencySentVariableTable/timeTable/digitalCurrencyAmountTimeTable/${millisecondDay}`, `digitalCurrencyAmount`) || 0
      const newAmountOfDigitalCurrencySent = getNumberWithFixedDecmial1(amountOfDigitalCurrencySent + transactionObject.currencyAmount)
      const newAmountOfDigitalCurrencySentForToday = getNumberWithFixedDecmial1(amountOfDigitalCurrencySentForToday + transactionObject.currencyAmount)
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'amountOfDigitalCurrencySent': newAmountOfDigitalCurrencySent })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/amountOfDigitalCurrencySentVariableTable/timeTable/digitalCurrencyAmountTimeTable`, `${millisecondDay}`, { 'digitalCurrencyAmount': newAmountOfDigitalCurrencySentForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/amountOfDigitalCurrencySent/dataQueue`, transactionObject.transactionDateId)
    }

    // Total Recurring Transaction Expenditure
    for (let i = 0; i < totalRecurringTransactionExpenditureDataList.length; i++) {
      const transactionObject = totalRecurringTransactionExpenditureDataList[i]
      if (!transactionObject || !transactionObject.transactionDateId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const totalRecurringTransactionExpenditure = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `totalRecurringTransactionExpenditure`) || 0
      const totalRecurringTransactionExpenditureForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/totalRecurringTransactionExpenditureVariableTable/timeTable/digitalCurrencyAmountTimeTable/${millisecondDay}`, `digitalCurrencyAmount`) || 0
      const newTotalRecurringTransactionExpenditure = getNumberWithFixedDecmial1(totalRecurringTransactionExpenditure + transactionObject.currencyAmount)
      const newTotalRecurringTransactionExpenditureForToday = getNumberWithFixedDecmial1(totalRecurringTransactionExpenditureForToday + transactionObject.currencyAmount)
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'totalRecurringTransactionExpenditure': newTotalRecurringTransactionExpenditure })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/totalRecurringTransactionExpenditureVariableTable/timeTable/digitalCurrencyAmountTimeTable`, `${millisecondDay}`, { 'digitalCurrencyAmount': newTotalRecurringTransactionExpenditureForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/totalRecurringTransactionExpenditure/dataQueue`, transactionObject.transactionDateId)
    }

    // Number of Transactions Created
    for (let i = 0; i < numberOfTransactionsCreatedDataList.length; i++) {
      const transactionObject = numberOfTransactionsCreatedDataList[i]
      if (!transactionObject || !transactionObject.transactionId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const numberOfTransactionsCreated = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfTransactionsCreated`) || 0
      const numberOfTransactionsCreatedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/createdTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
      const newNumberOfTransactionsCreated = numberOfTransactionsCreated + 1
      const newNumberOfTransactionsCreatedForToday = numberOfTransactionsCreatedForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfTransactionsCreated': newNumberOfTransactionsCreated })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/createdTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfTransactionsCreatedForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfTransactionsCreated/dataQueue`, transactionObject.transactionId)

      const numberOfActiveTransactions = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfActiveTransactions`) || 0
      const numberOfActiveTransactionsForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || numberOfActiveTransactions
      const newNumberOfActiveTransactions = numberOfActiveTransactions + 1
      const newNumberOfActiveTransactionsForToday = numberOfActiveTransactionsForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfActiveTransactions': newNumberOfActiveTransactions })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfActiveTransactionsForToday }, false, true)
    }

    // Number of Transactions Received
    for (let i = 0; i < numberOfTransactionsReceivedDataList.length; i++) {
      const transactionObject = numberOfTransactionsReceivedDataList[i]
      if (!transactionObject || !transactionObject.transactionId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const numberOfTransactionsReceived = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfTransactionsReceived`) || 0
      const numberOfTransactionsReceivedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
      const newNumberOfTransactionsReceived = numberOfTransactionsReceived + 1
      const newNumberOfTransactionsReceivedForToday = numberOfTransactionsReceivedForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfTransactionsReceived': newNumberOfTransactionsReceived })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfTransactionsReceivedForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfTransactionsReceived/dataQueue`, transactionObject.transactionId)
    }

    // Number of Transactions Deleted
    for (let i = 0; i < numberOfTransactionsDeletedDataList.length; i++) {
      const transactionObject = numberOfTransactionsDeletedDataList[i]
      if (!transactionObject || !transactionObject.transactionId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const numberOfTransactionsDeleted = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfTransactionsDeleted`) || 0
      const numberOfTransactionsDeletedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/deletedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
      const newNumberOfTransactionsDeleted = numberOfTransactionsDeleted + 1
      const newNumberOfTransactionsDeletedForToday = numberOfTransactionsDeletedForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfTransactionsDeleted': newNumberOfTransactionsDeleted })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/deletedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfTransactionsDeletedForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfTransactionsDeleted/dataQueue`, transactionObject.transactionId)

      const numberOfActiveTransactions = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfActiveTransactions`) || 0
      const numberOfActiveTransactionsForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || numberOfActiveTransactions
      const newNumberOfActiveTransactions = numberOfActiveTransactions - 1
      const newNumberOfActiveTransactionsForToday = numberOfActiveTransactionsForToday - 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfActiveTransactions': newNumberOfActiveTransactions })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfActiveTransactionsForToday }, false, true)
    }

    // Number of Recurring Transactions Created
    for (let i = 0; i < numberOfRecurringTransactionsCreatedDataList.length; i++) {
      const transactionObject = numberOfRecurringTransactionsCreatedDataList[i]
      if (!transactionObject || !transactionObject.transactionId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const numberOfRecurringTransactionsCreated = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfRecurringTransactionsCreated`) || 0
      const numberOfRecurringTransactionsCreatedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/createdRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
      const newNumberOfRecurringTransactionsCreated = numberOfRecurringTransactionsCreated + 1
      const newNumberOfRecurringTransactionsCreatedForToday = numberOfRecurringTransactionsCreatedForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfRecurringTransactionsCreated': newNumberOfRecurringTransactionsCreated })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/createdRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfRecurringTransactionsCreatedForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfRecurringTransactionsCreated/dataQueue`, transactionObject.transactionId)

      const numberOfActiveRecurringTransactions = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfActiveRecurringTransactions`) || 0
      const numberOfActiveRecurringTransactionsForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || numberOfActiveRecurringTransactions
      const newNumberOfActiveRecurringTransactions = numberOfActiveRecurringTransactions + 1
      const newNumberOfActiveRecurringTransactionsForToday = numberOfActiveRecurringTransactionsForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfActiveRecurringTransactions': newNumberOfActiveRecurringTransactions })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfActiveRecurringTransactionsForToday }, false, true)
    }

    // Number of Recurring Transactions Received
    for (let i = 0; i < numberOfRecurringTransactionsReceivedDataList.length; i++) {
      const transactionObject = numberOfRecurringTransactionsReceivedDataList[i]
      if (!transactionObject || !transactionObject.transactionId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const numberOfRecurringTransactionsReceived = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfRecurringTransactionsReceived`) || 0
      const numberOfRecurringTransactionsReceivedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
      const newNumberOfRecurringTransactionsReceived = numberOfRecurringTransactionsReceived + 1
      const newNumberOfRecurringTransactionsReceivedForToday = numberOfRecurringTransactionsReceivedForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfRecurringTransactionsReceived': newNumberOfRecurringTransactionsReceived })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfRecurringTransactionsReceivedForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfRecurringTransactionsReceived/dataQueue`, transactionObject.transactionId)
    }

    // Number of Recurring Transactions Deleted
    for (let i = 0; i < numberOfRecurringTransactionsDeletedDataList.length; i++) {
      const transactionObject = numberOfRecurringTransactionsDeletedDataList[i]
      if (!transactionObject || !transactionObject.transactionId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

      const numberOfRecurringTransactionsDeleted = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfRecurringTransactionsDeleted`) || 0
      const numberOfRecurringTransactionsDeletedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/deletedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
      const newNumberOfRecurringTransactionsDeleted = numberOfRecurringTransactionsDeleted + 1
      const newNumberOfRecurringTransactionsDeletedForToday = numberOfRecurringTransactionsDeletedForToday + 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfRecurringTransactionsDeleted': newNumberOfRecurringTransactionsDeleted })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/deletedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfRecurringTransactionsDeletedForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfRecurringTransactionsDeleted/dataQueue`, transactionObject.transactionId)

      const numberOfActiveRecurringTransactions = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}`, `numberOfActiveRecurringTransactions`) || 0
      const numberOfActiveRecurringTransactionsForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeRecurringTransactionsVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || numberOfActiveRecurringTransactions
      const newNumberOfActiveRecurringTransactions = numberOfActiveRecurringTransactions - 1
      const newNumberOfActiveRecurringTransactionsForToday = numberOfActiveRecurringTransactionsForToday - 1
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfActiveRecurringTransactions': newNumberOfActiveRecurringTransactions })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/activeRecurringTransactionsVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfActiveRecurringTransactionsForToday }, false, true)
    }

    // Number of Accounts Sent A Transaction To
    for (let i = 0; i < numberOfAccountsSentATransactionToDataList.length; i++) {
      const accountObject = numberOfAccountsSentATransactionToDataList[i]
      if (!accountObject || !accountObject.accountId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(accountObject.dateAdded)

      const accountsSentATransactionToVariableTable = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsSentATransactionToVariableTable`, `accountIdVariableTable`) || {}
      const numberOfAccountsSentATransactionToForTodayVariableTable = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsSentATransactionToVariableTable/timeTable/accountIdTimeTable/${millisecondDay}`, `accountIdVariableTable`) || {}
      const newNumberOfAccountsSentATransactionTo = Object.keys(accountsSentATransactionToVariableTable).length
      const newNumberOfAccountsSentATransactionToForToday = Object.keys(numberOfAccountsSentATransactionToForTodayVariableTable).length
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfAccountsSentATransactionTo': newNumberOfAccountsSentATransactionTo })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsSentATransactionToVariableTable/timeTable/numberOfProcessedAccountsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedAccounts': newNumberOfAccountsSentATransactionToForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfAccountsSentATransactionTo/dataQueue`, accountObject.accountId)
    }

    // Number of Accounts Received A Transaction From
    for (let i = 0; i < numberOfAccountsReceivedATransactionFromDataList.length; i++) {
      const accountObject = numberOfAccountsReceivedATransactionFromDataList[i]
      if (!accountObject || !accountObject.accountId) {
        continue
      }
      const { millisecondDay } = getMillisecondTimePartitionList2(accountObject.dateAdded)

      const accountsReceivedATransactionFromVariableTable = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsReceivedATransactionFromVariableTable`, `accountIdVariableTable`) || {}
      const numberOfAccountsReceivedATransactionFromForTodayVariableTable = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsReceivedATransactionFromVariableTable/timeTable/accountIdTimeTable/${millisecondDay}`, `accountIdVariableTable`) || {}
      const newNumberOfAccountsReceivedATransactionFrom = Object.keys(accountsReceivedATransactionFromVariableTable).length
      const newNumberOfAccountsReceivedATransactionFromForToday = Object.keys(numberOfAccountsReceivedATransactionFromForTodayVariableTable).length
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, accountId, { 'numberOfAccountsReceivedATransactionFrom': newNumberOfAccountsReceivedATransactionFrom })
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsReceivedATransactionFromVariableTable/timeTable/numberOfProcessedAccountsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedAccounts': newNumberOfAccountsReceivedATransactionFromForToday }, false, true)
      await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfAccountsReceivedATransactionFrom/dataQueue`, accountObject.accountId)
    }
  }
}


const initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessingAlgorithm = {
  function: initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessing
}



export {
  initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessingAlgorithm
}


