

import { getNumberWithFixedDecmial1 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-9-get-number-with-fixed-decimal/get-number-with-fixed-decimal-1'

import { DigitalCurrencyAccount } from '../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
import { DigitalCurrencyTransaction, SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import { updateItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'
import { createItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-2-firebase-admin'
// import { getItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'


import { getDigitalCurrencyAccountById2FirebaseAdmin } from '../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-15-get-digital-currency-account-by-id/get-digital-currency-account-by-id-2-firebase-admin'
import { deleteDigitalCurrencyTransaction2FirebaseAdmin } from '../../algorithms-type-2-delete-digital-currency-transaction/delete-digital-currency-transaction-2-firebase-admin'


import {
  SERVICE_DATA_QUEUE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID
} from '../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE
} from '../../../../app-type-1-general/general-type-3-constants/constants-5-service-type'


import {
  NUMBER_OF_MILLISECONDS_IN_A_MINUTE
} from '../../../../app-type-1-general/general-type-3-constants/constants-3-time'

import {
  administratorVariables
} from '../../../../app-type-1-general/general-type-3-constants/constants-4-administrator'

// import { getUTCDate1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'
import { getMillisecondTimePartitionList2 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { getTransactionProcessingInformationTableAlgorithm } from '../service-for-helper-algorithms/helper-algorithms-1-get-transaction-processing-information-table'


let variableTable: {
  NUMBER_OF_TIMES_FAILED_TRANSACTION_PROCESSING_IS_ALLOWED: number
  NUMBER_OF_MILLISECONDS_TO_TRY_TRANSACTION_PROCESSING_AGAIN: number
} = {
  NUMBER_OF_TIMES_FAILED_TRANSACTION_PROCESSING_IS_ALLOWED: 3,
  NUMBER_OF_MILLISECONDS_TO_TRY_TRANSACTION_PROCESSING_AGAIN: 2 * NUMBER_OF_MILLISECONDS_IN_A_MINUTE
}


async function initializeDigitalCurrencyTransaction (digitalCurrencyTransaction: DigitalCurrencyTransaction, withNumberOfRecurringIterations?: number) {
  // if transaction amount doesn't add up to distribution
    // is processed to true
    // return
  // if transaction amount is less than 0
    // is processed to true,
    // return
  // if is a recurring transaction and cancellation date exceeded
    // set is processed to true
    // return
  // get account sender
  // get account recipient table
  // if account sender doesn't have enough currency
    // add processing error for the transaction
    // if number of processing errors >= 3, return
    // move the transaction 1 minute into the future
    // return
  // for all of the accounts (sender, recipients)
    // add this transaction to the transactionProcessingDataQueue
      // (information about how much to add or subtract from the
      // account currency amount, date)
  // if is a recurring transaction
    // move the transaction to its next recurrence place

  // Transaction Validation
  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const { dateTransactionScheduledToBeProcessedTimePartition, dateTransactionRecurred, dateTransactionRecurredTimePartition } = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

  const isAdministratorTransaction = (digitalCurrencyTransaction.specialTransactionType ? true : false) || (digitalCurrencyTransaction.accountSenderId === administratorVariables.ADMINISTRATOR_ACCOUNT_ID)

  let hasValidationError = false
  let latestReasonForProcessingFailed = ''
  let allowTryTransactionAgainLater = true

  let millisecondDay = 0
  let millisecondHour = 0
  let millisecondMinute = 0
  let millisecondSecond = 0

  if (digitalCurrencyTransaction.isRecurringTransaction) {
    millisecondDay = dateTransactionRecurredTimePartition.millisecondDay
    millisecondHour = dateTransactionRecurredTimePartition.millisecondHour
    millisecondMinute = dateTransactionRecurredTimePartition.millisecondMinute
    millisecondSecond = dateTransactionRecurredTimePartition.millisecondSecond
  } else {
    millisecondDay = dateTransactionScheduledToBeProcessedTimePartition.millisecondDay
    millisecondHour = dateTransactionScheduledToBeProcessedTimePartition.millisecondHour
    millisecondMinute = dateTransactionScheduledToBeProcessedTimePartition.millisecondMinute
    millisecondSecond = dateTransactionScheduledToBeProcessedTimePartition.millisecondSecond
  }

  let accountRecipientTransactionSum = 0

  if (digitalCurrencyTransaction.isBeingProcessed) {
    if ((new Date(currentDate)).getTime() - (new Date(digitalCurrencyTransaction.dateLastBeingProcessed! || 0)).getTime() < 2 * NUMBER_OF_MILLISECONDS_IN_A_MINUTE) {
      return digitalCurrencyTransaction
    }
  }

  if ((withNumberOfRecurringIterations || 0) < 0) {
    return digitalCurrencyTransaction
  }

  if (!isAdministratorTransaction) {
    digitalCurrencyTransaction.isBeingProcessed = true
    digitalCurrencyTransaction.dateLastBeingProcessed = currentDate
    await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, {
      'isBeingProcessed': true,
      'dateLastBeingProcessed': currentDate
    })
  }

  if (!isAdministratorTransaction && !hasValidationError) {
    accountRecipientTransactionSum = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).map((accountId: string) => digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId].transactionCurrencyAmount || 0).reduce((value1: number, value2: number) => getNumberWithFixedDecmial1(getNumberWithFixedDecmial1(value1) + getNumberWithFixedDecmial1(value2)))
    if (accountRecipientTransactionSum !== digitalCurrencyTransaction.transactionCurrencyAmount) {
      latestReasonForProcessingFailed = 'Invalid Transaction Amount'
      hasValidationError = true
    }
  }

  if (!isAdministratorTransaction && !withNumberOfRecurringIterations && !hasValidationError) {
    const isAllRecipientAccountsDeleted = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).map((accountId: string) => digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId].dateAccountDeleted || 0).every((dateAccountDeleted: number) => dateAccountDeleted ? true : false)
    if (isAllRecipientAccountsDeleted) {
      latestReasonForProcessingFailed = 'All Recipient Accounts Have Been Deleted'
      hasValidationError = true
      allowTryTransactionAgainLater = false
    }
  }

  if (digitalCurrencyTransaction.transactionCurrencyAmount < 0 && !hasValidationError) {
    latestReasonForProcessingFailed = 'Transaction Amount Cannot Be Less Than 0'
    hasValidationError = true
  }

  if (digitalCurrencyTransaction.dateTransactionScheduledForCancellingRecurrence && !withNumberOfRecurringIterations && !hasValidationError && !isAdministratorTransaction) {
    if (digitalCurrencyTransaction.dateTransactionScheduledForCancellingRecurrence! <= currentDate) {
      hasValidationError = true
    }
  }

  // Sender Account Validation
  let senderAccount: DigitalCurrencyAccount | null = null

  if (!isAdministratorTransaction) {
    senderAccount = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyTransaction.accountSenderId)
  }

  if ((!senderAccount && !isAdministratorTransaction) && !hasValidationError) {
    latestReasonForProcessingFailed = 'Sender Account Doesn\'t Exist'
    hasValidationError = true
  }

  if (!isAdministratorTransaction && !hasValidationError) {
    if (senderAccount!.accountCurrencyAmount < accountRecipientTransactionSum || senderAccount!.accountCurrencyAmount < digitalCurrencyTransaction.transactionCurrencyAmount) {
      latestReasonForProcessingFailed = `Sender doesn\'t have enough funds to send this transaction amount.`
      hasValidationError = true
    }
  }

  // Respond To Invalid Transactions
  if (hasValidationError) {
    if (latestReasonForProcessingFailed) {
      digitalCurrencyTransaction.numberOfTimesProcessingFailed = (digitalCurrencyTransaction.numberOfTimesProcessingFailed || 0) + 1
      digitalCurrencyTransaction.latestReasonForProcessingFailed = latestReasonForProcessingFailed
    }

    if (!senderAccount) {
      await deleteDigitalCurrencyTransaction2FirebaseAdmin(digitalCurrencyTransaction.transactionId)
      return digitalCurrencyTransaction
    }

    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)

    if (digitalCurrencyTransaction.isRecurringTransaction) {
      const alternateDateTransactionRecurredTimePartition = getMillisecondTimePartitionList2((new Date(dateTransactionRecurred.getTime() + digitalCurrencyTransaction.millisecondRecurringDelay)).toISOString().split('.')[0] + 'Z')
      await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${dateTransactionRecurredTimePartition.millisecondDay}/${dateTransactionRecurredTimePartition.millisecondHour}/${dateTransactionRecurredTimePartition.millisecondMinute}/${dateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
      await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${alternateDateTransactionRecurredTimePartition.millisecondDay}/${alternateDateTransactionRecurredTimePartition.millisecondHour}/${alternateDateTransactionRecurredTimePartition.millisecondMinute}/${alternateDateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
    }

    // Schedule the transaction to be processed later
    if (latestReasonForProcessingFailed && ((digitalCurrencyTransaction.numberOfTimesProcessingFailed! || 0) <= variableTable.NUMBER_OF_TIMES_FAILED_TRANSACTION_PROCESSING_IS_ALLOWED) && allowTryTransactionAgainLater) {
      const nextScheduledProcessingDate = new Date(currentDate + variableTable.NUMBER_OF_MILLISECONDS_TO_TRY_TRANSACTION_PROCESSING_AGAIN)
      const nextScheduledProcessingDateTimePartition = getMillisecondTimePartitionList2(nextScheduledProcessingDate.toISOString().split('.')[0] + 'Z')

      if (digitalCurrencyTransaction.isRecurringTransaction) {
        digitalCurrencyTransaction.dateRecurrenceRestarted = nextScheduledProcessingDate.toISOString().split('.')[0] + 'Z'
        digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart = 0
        await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, {
          'dateRecurrenceRestarted': nextScheduledProcessingDate.getTime(),
          'numberOfTimesRecurredSinceRestart': 0
        })
      }

      digitalCurrencyTransaction.dateTransactionReScheduledForProcessing = nextScheduledProcessingDate.toISOString().split('.')[0] + 'Z'
      await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, {
        'dateTransactionReScheduledForProcessing': nextScheduledProcessingDate.getTime()
      })

      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${nextScheduledProcessingDateTimePartition.millisecondDay}/${nextScheduledProcessingDateTimePartition.millisecondHour}/${nextScheduledProcessingDateTimePartition.millisecondMinute}/${nextScheduledProcessingDateTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, { isProcessed: false, dateCreated: currentDate })
    } else {
      digitalCurrencyTransaction.dateRecurrenceCancelled = currentDate
      await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, {
        'dateRecurrenceCancelled': currentDate
      })

      // Add Account Notification for Stopped With Error
      if (latestReasonForProcessingFailed) {
        await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}/createdTransactions`, 'stoppedTransactionWithErrorTransactionIdVariableTable', {
          [digitalCurrencyTransaction.transactionId]: currentDate
        })
        for (let accountId in digitalCurrencyTransaction.accountRecipientIdVariableTable) {
          if (!digitalCurrencyTransaction.accountRecipientIdVariableTable.hasOwnProperty(accountId)) {
            continue
          }
          await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${accountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}/receivedTransactions`, 'stoppedTransactionWithErrorTransactionIdVariableTable', {
            [digitalCurrencyTransaction.transactionId]: currentDate
          })
        }
      }
    }

    digitalCurrencyTransaction.isBeingProcessed = false
    digitalCurrencyTransaction.isProcessed = true
    digitalCurrencyTransaction.dateTransactionProcessed = currentDate
    await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, {
      'isBeingProcessed': false,
      'isProcessed': true,
      'numberOfTimesProcessingFailed': digitalCurrencyTransaction.numberOfTimesProcessingFailed,
      'latestReasonForProcessingFailed': latestReasonForProcessingFailed,
      'dateTransactionProcessed': currentDate
    })

    return digitalCurrencyTransaction
  }

  // With Number of Recurring Iterations

  let numberOfAllowedRecurringIterations = 1
  if (withNumberOfRecurringIterations && digitalCurrencyTransaction.isRecurringTransaction && !isAdministratorTransaction) {
    // Get how many iterations can be applied to the sender account
    // without going into a negative balance.

    // If there's a remaining number of recurring iterations
    // That couldn't be applied
      // Treat this like an error

    const numberOfIterationsWithoutNegativeBalance = Math.floor(senderAccount!.accountCurrencyAmount / digitalCurrencyTransaction.transactionCurrencyAmount)
    numberOfAllowedRecurringIterations = Math.min(numberOfIterationsWithoutNegativeBalance, withNumberOfRecurringIterations)

    digitalCurrencyTransaction.numberOfTimesRecurred += numberOfAllowedRecurringIterations
    digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart += digitalCurrencyTransaction.dateRecurrenceRestarted ? numberOfAllowedRecurringIterations : 0

    const hasRemainingNumberOfIterations = (withNumberOfRecurringIterations - numberOfIterationsWithoutNegativeBalance) > 0 ? true : false

    if (hasRemainingNumberOfIterations) {
      latestReasonForProcessingFailed = `Sender doesn\'t have enough funds to send this transaction amount.`
      digitalCurrencyTransaction.numberOfTimesProcessingFailed = (digitalCurrencyTransaction.numberOfTimesProcessingFailed || 0) + 1
      digitalCurrencyTransaction.latestReasonForProcessingFailed = latestReasonForProcessingFailed
      await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, {
        'numberOfTimesProcessingFailed': digitalCurrencyTransaction.numberOfTimesProcessingFailed,
        'latestReasonForProcessingFailed': latestReasonForProcessingFailed,
        'dateRecurrenceCancelled': currentDate
      })
    }

    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransaction.transactionId, { 'numberOfTimesRecurred': digitalCurrencyTransaction.numberOfTimesRecurred })
    if (digitalCurrencyTransaction.dateRecurrenceRestarted) {
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransaction.transactionId, { 'numberOfTimesRecurredSinceRestart': digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart })
    }
    const alternateDateTransactionRecurredTimePartition = getMillisecondTimePartitionList2((new Date(dateTransactionRecurred.getTime() + digitalCurrencyTransaction.millisecondRecurringDelay)).toISOString().split('.')[0] + 'Z')
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${dateTransactionRecurredTimePartition.millisecondDay}/${dateTransactionRecurredTimePartition.millisecondHour}/${dateTransactionRecurredTimePartition.millisecondMinute}/${dateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${alternateDateTransactionRecurredTimePartition.millisecondDay}/${alternateDateTransactionRecurredTimePartition.millisecondHour}/${alternateDateTransactionRecurredTimePartition.millisecondMinute}/${alternateDateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)

    if (!hasRemainingNumberOfIterations && !digitalCurrencyTransaction.dateTransactionScheduledForCancellingRecurrence) {
      const newTransactionProcessingInformationTable = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)
      const newDateTransactionRecurredTimePartition = getMillisecondTimePartitionList2(newTransactionProcessingInformationTable.dateTransactionRecurred.toISOString().split('.')[0] + 'Z')
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${newDateTransactionRecurredTimePartition.millisecondDay}/${newDateTransactionRecurredTimePartition.millisecondHour}/${newDateTransactionRecurredTimePartition.millisecondMinute}/${newDateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, { isProcessed: false, dateCreated: currentDate })
    }

  } else if (withNumberOfRecurringIterations && digitalCurrencyTransaction.isRecurringTransaction && isAdministratorTransaction) {
    numberOfAllowedRecurringIterations = withNumberOfRecurringIterations
  }

  // Account Processing

  if (!isAdministratorTransaction) {
    digitalCurrencyTransaction.accountRecipientIdVariableTable[digitalCurrencyTransaction.accountSenderId] = {
      transactionCurrencyAmount: -digitalCurrencyTransaction.transactionCurrencyAmount
    }
  }

  for (let accountId in digitalCurrencyTransaction.accountRecipientIdVariableTable) {
    if (!digitalCurrencyTransaction.accountRecipientIdVariableTable.hasOwnProperty(accountId)) {
      continue
    }

    const accountObject = digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId]
    if (accountObject.dateAccountDeleted) {
      continue
    }

    await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionDataQueue/accountIdVariableTable/${accountId}/dataQueue`, `${digitalCurrencyTransaction.transactionId}-${currentDate}`, {
      transactionId: digitalCurrencyTransaction.transactionId,
      digitalCurrencySenderAccountId: !isAdministratorTransaction ? (digitalCurrencyTransaction.accountSenderId !== accountId ? digitalCurrencyTransaction.accountSenderId : '') : '',
      transactionCurrencyAmount: getNumberWithFixedDecmial1(accountObject.transactionCurrencyAmount * numberOfAllowedRecurringIterations),
      dateCreated: currentDate,
      isRecurringTransaction: digitalCurrencyTransaction.isRecurringTransaction,
      isCreatingCurrencyTransaction: (digitalCurrencyTransaction.specialTransactionType === SPECIAL_TRANSACTION_TYPE_UNIVERSAL_BASIC_INCOME) && isAdministratorTransaction
    }, false, false, true)
  }

  if (!isAdministratorTransaction) {
    delete digitalCurrencyTransaction.accountRecipientIdVariableTable[digitalCurrencyTransaction.accountSenderId]
  }

  // Transaction - Processing Complete

  if (!digitalCurrencyTransaction.isProcessed || (digitalCurrencyTransaction.dateTransactionReScheduledForProcessing && digitalCurrencyTransaction.numberOfTimesProcessingFailed)) {
    digitalCurrencyTransaction.isProcessed = true
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransaction.transactionId, { 'isProcessed': true })
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
  }

  // Transaction - Failed Processing Cleared
  if (digitalCurrencyTransaction.numberOfTimesProcessingFailed && !withNumberOfRecurringIterations) {
    digitalCurrencyTransaction.numberOfTimesProcessingFailed = 0
    digitalCurrencyTransaction.latestReasonForProcessingFailed = ''
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransaction.transactionId, { 'numberOfTimesProcessingFailed': 0 })
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.transactionId}`, 'dateRecurrenceCancelled')
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.transactionId}`, 'latestReasonForProcessingFailed')
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
  }

  // Recurring Transaction - Number Of Times Recurred

  if (digitalCurrencyTransaction.isRecurringTransaction && !isAdministratorTransaction && !withNumberOfRecurringIterations) {
    digitalCurrencyTransaction.numberOfTimesRecurred = (digitalCurrencyTransaction.numberOfTimesRecurred || 0) + 1
    digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart = digitalCurrencyTransaction.dateRecurrenceRestarted ? (digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart || 0) + 1 : 0
    const newDateTransactionRecurred = new Date(dateTransactionRecurred.getTime() + digitalCurrencyTransaction.millisecondRecurringDelay)
    const newDateTransactionRecurredTimePartition = getMillisecondTimePartitionList2(newDateTransactionRecurred.toISOString().split('.')[0] + 'Z')
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransaction.transactionId, { 'numberOfTimesRecurred': digitalCurrencyTransaction.numberOfTimesRecurred })
    if (digitalCurrencyTransaction.dateRecurrenceRestarted) {
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransaction.transactionId, { 'numberOfTimesRecurredSinceRestart': digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart })
    }
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${dateTransactionRecurredTimePartition.millisecondDay}/${dateTransactionRecurredTimePartition.millisecondHour}/${dateTransactionRecurredTimePartition.millisecondMinute}/${dateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${newDateTransactionRecurredTimePartition.millisecondDay}/${newDateTransactionRecurredTimePartition.millisecondHour}/${newDateTransactionRecurredTimePartition.millisecondMinute}/${newDateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, { isProcessed: false, dateCreated: currentDate })
  }

  if (!isAdministratorTransaction) {
    await updateItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, {
      'isBeingProcessed': false,
      'dateTransactionProcessed': currentDate
    })
    digitalCurrencyTransaction.isBeingProcessed = false
    digitalCurrencyTransaction.dateTransactionProcessed = currentDate
  }

  return digitalCurrencyTransaction
}





const initializeDigitalCurrencyTransactionAlgorithm = {
  function: initializeDigitalCurrencyTransaction
}

export {
  variableTable,
  initializeDigitalCurrencyTransactionAlgorithm
}


