


import { getItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { addItemEventListener2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-10-add-item-event-listener/add-item-event-listener-2-firebase-admin'
import { removeItemEventListener2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-11-remove-item-event-listener/remove-item-event-listener-2-firebase-admin'

import {
  // EVENT_ID_UPDATE,
  EVENT_ID_CREATE
} from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

import {
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  NUMBER_OF_MILLISECONDS_IN_A_MINUTE
} from '../../../../app-type-1-general/general-type-3-constants/constants-3-time'


let variableTable: {
  activeAccountIdVariableTable: { [accountId: string]: { accountId: string, dateAccountCreated: string, dateAddedToList: string } }
  newActiveAccountIdVariableTable: { [accountId: string]: { accountId: string, dateAccountCreated: string, dateAddedToList: string } }
  oldActiveAccountIdVariableTable: { [accountId: string]: { accountId: string, dateAccountDeleted: string, dateAddedToList: string } }

  addNewActiveAccountEventListenerId: any
  removeOldActiveAccountEventListenerId: any
  updateNewActiveAccountIdVariableTableIntervalId: any
  updateOldActiveAccountIdVariableTableIntervalId: any

  NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE: number
} = {
  activeAccountIdVariableTable: {},
  newActiveAccountIdVariableTable: {},
  oldActiveAccountIdVariableTable: {},

  addNewActiveAccountEventListenerId: null,
  removeOldActiveAccountEventListenerId: null,
  updateNewActiveAccountIdVariableTableIntervalId: null,
  updateOldActiveAccountIdVariableTableIntervalId: null,

  NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE: 10 * NUMBER_OF_MILLISECONDS_IN_A_MINUTE
}


async function initializeAllAccountsForAccountIdVariableTable () {
  // get all account ids
  // add account id to account id variable table
  // add event listener for created accounts
    // add account id to account id variable table
  // add event listener for deleted accounts
    // remove account id from account id variable table

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const createdAccountIdVariableTable = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable`, 'accountIdVariableTable') || []
  const deletedAccountIdVariableTable = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable`, 'accountIdVariableTable') || []

  const createdAccountList = Object.keys(createdAccountIdVariableTable).map((accountId: string) => { return { accountId, dateAccountCreated: createdAccountIdVariableTable[accountId], dateAddedToList: currentDate } })
  // const deletedAccountList = Object.keys(deletedAccountIdVariableTable).map((accountId: string) => { return { accountId, accountDateDeleted: deletedAccountIdVariableTable[accountId] } })

  const activeAccountList = createdAccountList.filter((accountObject: any) => !deletedAccountIdVariableTable[accountObject.accountId])
  variableTable.activeAccountIdVariableTable = activeAccountList.reduce((map: any, obj: any, _index: number) => (map[obj.accountId] = obj, map), {})

  uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm.function()

  // Add Event Listeners For New Accounts
  variableTable.addNewActiveAccountEventListenerId = await addItemEventListener2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable`, 'accountIdVariableTable', EVENT_ID_CREATE, (dateAccountCreated: any, options: any) => {
    const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
    const accountId = options.key
    console.log('new account created: ', accountId)
    variableTable.activeAccountIdVariableTable[accountId] = { accountId, dateAccountCreated, dateAddedToList: currentDate }
    variableTable.newActiveAccountIdVariableTable[accountId] = { accountId, dateAccountCreated, dateAddedToList: currentDate }
  })

  // Add Event Listeners For Deleted Accounts
  variableTable.removeOldActiveAccountEventListenerId = await addItemEventListener2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable`, 'accountIdVariableTable', EVENT_ID_CREATE, (dateAccountDeleted: any, options: any) => {
    const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
    const accountId = options.key
    console.log('old account deleted: ', accountId)
    delete variableTable.activeAccountIdVariableTable[accountId]
    variableTable.oldActiveAccountIdVariableTable[accountId] = { accountId, dateAccountDeleted, dateAddedToList: currentDate }
  })

  // Set Interval For Deleting New Accounts From Existing
  // New Account Id Variable Table
  variableTable.updateNewActiveAccountIdVariableTableIntervalId = setInterval(() => {
    // Remove All Account Ids Older Than N Minutes
    const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'

    for (let accountId in variableTable.newActiveAccountIdVariableTable) {
      if (!variableTable.newActiveAccountIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }
      const accountObject = variableTable.newActiveAccountIdVariableTable[accountId]
      if ((new Date(accountObject.dateAddedToList)).getTime() > (new Date(currentDate)).getTime() - variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE) {
        continue
      }

      delete variableTable.newActiveAccountIdVariableTable[accountId]
    }

  }, variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE)

  // Set Interval For Deleting Old Accounts From Existing
  // Old Account Id Variable Table
  variableTable.updateOldActiveAccountIdVariableTableIntervalId = setInterval(() => {
    // Remove All Account Ids Older Than N Minutes
    const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'

    for (let accountId in variableTable.oldActiveAccountIdVariableTable) {
      if (!variableTable.oldActiveAccountIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }
      const accountObject = variableTable.oldActiveAccountIdVariableTable[accountId]
      if ((new Date(accountObject.dateAddedToList)).getTime() > (new Date(currentDate)).getTime() - variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE) {
        continue
      }

      delete variableTable.oldActiveAccountIdVariableTable[accountId]
    }
  }, variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE)
}

function uninitializeAllAccountsForAccountIdVariableTableEventListener () {
  removeItemEventListener2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable`, 'accountIdVariableTable', EVENT_ID_CREATE, variableTable.addNewActiveAccountEventListenerId)
  clearInterval(variableTable.updateNewActiveAccountIdVariableTableIntervalId)
  removeItemEventListener2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable`, 'accountIdVariableTable', EVENT_ID_CREATE, variableTable.removeOldActiveAccountEventListenerId)
  clearInterval(variableTable.updateOldActiveAccountIdVariableTableIntervalId)
}


function getDigitalCurrencyAccountListByDateCreated ({
  startDateAccountCreated,
  endDateAccountCreated
}: {
  startDateAccountCreated?: string
  endDateAccountCreated?: string
}) {
  let accountList: Array<{ accountId: string, dateAccountCreated: string }> = []

  startDateAccountCreated = startDateAccountCreated || (new Date('1970-01-01T00:00:00Z')).toISOString().split('.')[0] + 'Z'
  endDateAccountCreated = endDateAccountCreated || (new Date()).toISOString().split('.')[0] + 'Z'

  for (let accountId in variableTable.activeAccountIdVariableTable) {
    if (!variableTable.activeAccountIdVariableTable.hasOwnProperty(accountId)) {
      continue
    }
    const accountObject = variableTable.activeAccountIdVariableTable[accountId]

    if (((new Date(accountObject.dateAccountCreated)).getTime() < (new Date(startDateAccountCreated)).getTime()) || ((new Date(accountObject.dateAccountCreated)).getTime() > (new Date(endDateAccountCreated)).getTime())) {
      continue
    }

    accountList.push({
      accountId,
      dateAccountCreated: accountObject.dateAccountCreated
    })
  }

  return accountList
}


const initializeAllAccountsForAccountIdVariableTableAlgorithm = {
  function: initializeAllAccountsForAccountIdVariableTable
}

const uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm = {
  function: uninitializeAllAccountsForAccountIdVariableTableEventListener
}

const getDigitalCurrencyAccountListByDateCreatedAlgorithm = {
  function: getDigitalCurrencyAccountListByDateCreated
}


export {
  variableTable,

  uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm,
  initializeAllAccountsForAccountIdVariableTableAlgorithm,
  getDigitalCurrencyAccountListByDateCreatedAlgorithm
}



