

import { getNumberWithFixedDecmial1 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-9-get-number-with-fixed-decimal/get-number-with-fixed-decimal-1'

import { updateItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { getItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID
} from '../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { getMillisecondTimePartitionList2 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

async function initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessing () {
  const initializeTransactionNetworkStatisticsDataQueueTable = await getItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}`, 'initializeTransactionNetworkStatisticsDataQueue') || {}

  const numberOfTransactionsCreatedDataQueue = initializeTransactionNetworkStatisticsDataQueueTable.numberOfTransactionsCreated ? initializeTransactionNetworkStatisticsDataQueueTable.numberOfTransactionsCreated.dataQueue : {}
  const numberOfRecurringTransactionsCreatedDataQueue = initializeTransactionNetworkStatisticsDataQueueTable.numberOfRecurringTransactionsCreated ? initializeTransactionNetworkStatisticsDataQueueTable.numberOfRecurringTransactionsCreated.dataQueue : {}
  const numberOfTransactionsDeletedDataQueue = initializeTransactionNetworkStatisticsDataQueueTable.numberOfTransactionsDeleted ? initializeTransactionNetworkStatisticsDataQueueTable.numberOfTransactionsDeleted.dataQueue : {}
  const numberOfRecurringTransactionsDeletedDataQueue = initializeTransactionNetworkStatisticsDataQueueTable.numberOfRecurringTransactionsDeleted ? initializeTransactionNetworkStatisticsDataQueueTable.numberOfRecurringTransactionsDeleted.dataQueue : {}

  const amountOfDigitalCurrencyCreatedDataQueue = initializeTransactionNetworkStatisticsDataQueueTable.amountOfDigitalCurrencyCreated ? initializeTransactionNetworkStatisticsDataQueueTable.amountOfDigitalCurrencyCreated.dataQueue : {}
  const amountOfDigitalCurrencySentDataQueue = initializeTransactionNetworkStatisticsDataQueueTable.amountOfDigitalCurrencySent ? initializeTransactionNetworkStatisticsDataQueueTable.amountOfDigitalCurrencySent.dataQueue : {}

  const numberOfTransactionsCreatedDataList = Object.keys(numberOfTransactionsCreatedDataQueue).map((transactionId: string) => {
    return { transactionId, ...numberOfTransactionsCreatedDataQueue[transactionId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
  })

  const numberOfRecurringTransactionsCreatedDataList = Object.keys(numberOfRecurringTransactionsCreatedDataQueue).map((transactionId: string) => {
    return { transactionId, ...numberOfRecurringTransactionsCreatedDataQueue[transactionId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
  })

  const numberOfTransactionsDeletedDataList = Object.keys(numberOfTransactionsDeletedDataQueue).map((transactionId: string) => {
    return { transactionId, ...numberOfTransactionsDeletedDataQueue[transactionId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
  })

  const numberOfRecurringTransactionsDeletedDataList = Object.keys(numberOfRecurringTransactionsDeletedDataQueue).map((transactionId: string) => {
    return { transactionId, ...numberOfRecurringTransactionsDeletedDataQueue[transactionId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
  })

  //
  const amountOfDigitalCurrencyCreatedDataList = Object.keys(amountOfDigitalCurrencyCreatedDataQueue).map((transactionDateId: string) => {
    return { transactionDateId, ...amountOfDigitalCurrencyCreatedDataQueue[transactionDateId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
  })

  const amountOfDigitalCurrencySentDataList = Object.keys(amountOfDigitalCurrencySentDataQueue).map((transactionDateId: string) => {
    return { transactionDateId, ...amountOfDigitalCurrencySentDataQueue[transactionDateId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemA.dateCreated)).getTime() - (new Date(itemB.dateCreated)).getTime()
  })

  // Number of Transactions Created
  for (let i = 0; i < numberOfTransactionsCreatedDataList.length; i++) {
    const transactionObject = numberOfTransactionsCreatedDataList[i]
    if (!transactionObject || !transactionObject.transactionId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

    const numberOfTransactionsCreated = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, `numberOfTransactionsCreated`) || 0
    const numberOfTransactionsCreatedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
    const newNumberOfTransactionsCreated = numberOfTransactionsCreated + 1
    const newNumberOfTransactionsCreatedForToday = numberOfTransactionsCreatedForToday + 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfTransactionsCreated': newNumberOfTransactionsCreated })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfTransactionsCreatedForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfTransactionsCreated/dataQueue`, transactionObject.transactionId)
  }

  // Number of Recurring Transactions Created
  for (let i = 0; i < numberOfRecurringTransactionsCreatedDataList.length; i++) {
    const transactionObject = numberOfRecurringTransactionsCreatedDataList[i]
    if (!transactionObject || !transactionObject.transactionId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

    const numberOfRecurringTransactionsCreated = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, `numberOfRecurringTransactionsCreated`) || 0
    const numberOfRecurringTransactionsCreatedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
    const newNumberOfRecurringTransactionsCreated = numberOfRecurringTransactionsCreated + 1
    const newNumberOfRecurringTransactionsCreatedForToday = numberOfRecurringTransactionsCreatedForToday + 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfRecurringTransactionsCreated': newNumberOfRecurringTransactionsCreated })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfRecurringTransactionsCreatedForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfRecurringTransactionsCreated/dataQueue`, transactionObject.transactionId)
  }

  // Number of Transactions Deleted
  for (let i = 0; i < numberOfTransactionsDeletedDataList.length; i++) {
    const transactionObject = numberOfTransactionsDeletedDataList[i]
    if (!transactionObject || !transactionObject.transactionId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

    const numberOfTransactionsDeleted = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, `numberOfTransactionsDeleted`) || 0
    const numberOfTransactionsDeletedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
    const newNumberOfTransactionsDeleted = numberOfTransactionsDeleted + 1
    const newNumberOfTransactionsDeletedForToday = numberOfTransactionsDeletedForToday + 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfTransactionsDeleted': newNumberOfTransactionsDeleted })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfTransactionsDeletedForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfTransactionsDeleted/dataQueue`, transactionObject.transactionId)
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, transactionObject.transactionId)
  }

  // Number of Recurring Transactions Deleted
  for (let i = 0; i < numberOfRecurringTransactionsDeletedDataList.length; i++) {
    const transactionObject = numberOfRecurringTransactionsDeletedDataList[i]
    if (!transactionObject || !transactionObject.transactionId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

    const numberOfRecurringTransactionsDeleted = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, `numberOfRecurringTransactionsDeleted`) || 0
    const numberOfRecurringTransactionsDeletedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${millisecondDay}`, `numberOfProcessedTransactions`) || 0
    const newNumberOfRecurringTransactionsDeleted = numberOfRecurringTransactionsDeleted + 1
    const newNumberOfRecurringTransactionsDeletedForToday = numberOfRecurringTransactionsDeletedForToday + 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfRecurringTransactionsDeleted': newNumberOfRecurringTransactionsDeleted })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedTransactions': newNumberOfRecurringTransactionsDeletedForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfRecurringTransactionsDeleted/dataQueue`, transactionObject.transactionId)
  }

  // Amount of Digital Currency Created
  for (let i = 0; i < amountOfDigitalCurrencyCreatedDataList.length; i++) {
    const transactionObject = amountOfDigitalCurrencyCreatedDataList[i]
    if (!transactionObject || !transactionObject.transactionDateId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

    const amountOfDigitalCurrencyCreated = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, `amountOfDigitalCurrencyCreated`) || 0
    const amountOfDigitalCurrencyCreatedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/amountOfDigitalCurrencyCreatedVariableTable/timeTable/digitalCurrencyAmountTimeTable/${millisecondDay}`, `digitalCurrencyAmount`) || 0
    const newAmountOfDigitalCurrencyCreated = amountOfDigitalCurrencyCreated + transactionObject.currencyAmount
    const newAmountOfDigitalCurrencyCreatedForToday = amountOfDigitalCurrencyCreatedForToday + transactionObject.currencyAmount
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, '', { 'amountOfDigitalCurrencyCreated': newAmountOfDigitalCurrencyCreated })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/amountOfDigitalCurrencyCreatedVariableTable/timeTable/digitalCurrencyAmountTimeTable`, `${millisecondDay}`, { 'digitalCurrencyAmount': newAmountOfDigitalCurrencyCreatedForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/amountOfDigitalCurrencyCreated/dataQueue`, transactionObject.transactionDateId)
  }

  // Amount of Digital Currency Sent
  for (let i = 0; i < amountOfDigitalCurrencySentDataList.length; i++) {
    const transactionObject = amountOfDigitalCurrencySentDataList[i]
    if (!transactionObject || !transactionObject.transactionDateId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(transactionObject.dateCreated)

    const amountOfDigitalCurrencySent = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, `amountOfDigitalCurrencySent`) || 0
    const amountOfDigitalCurrencySentForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/amountOfDigitalCurrencySentVariableTable/timeTable/digitalCurrencyAmountTimeTable/${millisecondDay}`, `digitalCurrencyAmount`) || 0
    const newAmountOfDigitalCurrencySent = getNumberWithFixedDecmial1(amountOfDigitalCurrencySent + transactionObject.currencyAmount)
    const newAmountOfDigitalCurrencySentForToday = getNumberWithFixedDecmial1(amountOfDigitalCurrencySentForToday + transactionObject.currencyAmount)
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}`, '', { 'amountOfDigitalCurrencySent': newAmountOfDigitalCurrencySent })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/amountOfDigitalCurrencySentVariableTable/timeTable/digitalCurrencyAmountTimeTable`, `${millisecondDay}`, { 'digitalCurrencyAmount': newAmountOfDigitalCurrencySentForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/amountOfDigitalCurrencySent/dataQueue`, transactionObject.transactionDateId)
  }
}


const initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessingAlgorithm = {
  function: initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessing
}


export {
  initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessingAlgorithm
}


