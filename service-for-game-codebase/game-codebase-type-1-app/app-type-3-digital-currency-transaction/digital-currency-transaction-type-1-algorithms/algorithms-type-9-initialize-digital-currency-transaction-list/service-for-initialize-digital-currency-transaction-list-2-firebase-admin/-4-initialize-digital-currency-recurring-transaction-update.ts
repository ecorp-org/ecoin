
import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  variableTable,
  getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm
} from './-1-initialize-digital-currency-transaction-list'

import { initializeDigitalCurrencyTransactionAlgorithm } from './-5-initialize-digital-currency-transaction'

import { initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm } from './-6-initialize-digital-currency-account-transaction-data-queue-processing'

import { getTransactionProcessingInformationTableAlgorithm } from '../service-for-helper-algorithms/helper-algorithms-1-get-transaction-processing-information-table'

async function updateDigitalCurrencyRecurringTransactionList (untilMillisecondTimeAgo?: number, withDataQueueProcessing?: boolean) {
  // get the latestDaemonShutdownDate
  // for each transaction since the latestDaemonShutdownDate - DefaultHours
    // initializeDigitalCurrencyRecurringTransactionUpdate

  let digitalCurrencyTransactionList = await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_RECURRING_TRANSACTION, untilMillisecondTimeAgo)
  let scheduledDigitalCurrencyTransactionList = await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, untilMillisecondTimeAgo, true)
  digitalCurrencyTransactionList = digitalCurrencyTransactionList.concat(scheduledDigitalCurrencyTransactionList)

  let isProcessedTransactionVariableTable: { [digitalCurrencyTransactionId: string]: boolean } = {}

  for (let i = 0; i < digitalCurrencyTransactionList.length; i++) {
    const digitalCurrencyTransaction = digitalCurrencyTransactionList[i]
    if (isProcessedTransactionVariableTable[digitalCurrencyTransaction.transactionId]) {
      continue
    }
    isProcessedTransactionVariableTable[digitalCurrencyTransaction.transactionId] = true
    await initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm.function(digitalCurrencyTransaction)
    if (withDataQueueProcessing) {
      await initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm.function()
    }
  }

  console.log('number of updated recurring transactions: ', Object.keys(isProcessedTransactionVariableTable).length)
}

async function initializeDigitalCurrencyRecurringTransactionUpdate (digitalCurrencyTransaction: DigitalCurrencyTransaction) {
  // get the latest date to process (now or date cancelled)
  // get the number of times recurred
  // get expected number of times recurred
  // get the difference between the expected number and the actual number
  // for each unit of difference
    // initialize digital currency transaction
      // with number of recurring iterations

  if (!digitalCurrencyTransaction.isRecurringTransaction) {
    return digitalCurrencyTransaction
  }

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const latestDateToProcess = digitalCurrencyTransaction.dateRecurrenceCancelled ? (new Date(digitalCurrencyTransaction.dateRecurrenceCancelled)).getTime() < (new Date(currentDate)).getTime() ? (new Date(digitalCurrencyTransaction.dateRecurrenceCancelled)).getTime() : (new Date(currentDate)).getTime() : (new Date(currentDate)).getTime()
  const { dateTransactionRecurrenceRestarted, numberOfRecurrencesSinceRestarting } = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)
  const expectedNumberOfTimesRecurredSinceRestarting = Math.floor((latestDateToProcess - dateTransactionRecurrenceRestarted.getTime()) / digitalCurrencyTransaction.millisecondRecurringDelay) + 1
  const numberOfUpdates = expectedNumberOfTimesRecurredSinceRestarting - numberOfRecurrencesSinceRestarting

  digitalCurrencyTransaction = await initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction, numberOfUpdates)

  return digitalCurrencyTransaction
}

const updateDigitalCurrencyRecurringTransactionListAlgorithm = {
  function: updateDigitalCurrencyRecurringTransactionList
}

const initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm = {
  function: initializeDigitalCurrencyRecurringTransactionUpdate
}


export {
  updateDigitalCurrencyRecurringTransactionListAlgorithm,
  initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm
}
