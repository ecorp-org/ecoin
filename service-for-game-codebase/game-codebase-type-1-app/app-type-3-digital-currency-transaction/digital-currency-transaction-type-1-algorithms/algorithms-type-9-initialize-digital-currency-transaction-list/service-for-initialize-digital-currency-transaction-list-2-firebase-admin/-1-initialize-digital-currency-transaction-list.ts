

import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import { getItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'

import { getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm } from '../../algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-2-firebase-admin'

import {
  DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID
} from '../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  // NUMBER_OF_MILLISECONDS_IN_A_MINUTE,
  // NUMBER_OF_MILLISECONDS_IN_AN_HOUR,
  NUMBER_OF_MILLISECONDS_IN_A_DAY
} from '../../../../app-type-1-general/general-type-3-constants/constants-3-time'

// import { getUTCDate1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'
import { getMillisecondTimePartitionList2 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { initializeDigitalCurrencyTransactionAlgorithm } from './-5-initialize-digital-currency-transaction'

import { initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm } from './-6-initialize-digital-currency-account-transaction-data-queue-processing'

let variableTable: {
  NUMBER_OF_MILLISECONDS_TRANSACTION_PROCESSING_BUFFER: number

  TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION: string
  TRANSACTION_TYPE_ID_RECURRING_TRANSACTION: string
} = {

  NUMBER_OF_MILLISECONDS_TRANSACTION_PROCESSING_BUFFER: 3 * NUMBER_OF_MILLISECONDS_IN_A_DAY,

  TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION: 'scheduledTransaction',
  TRANSACTION_TYPE_ID_RECURRING_TRANSACTION: 'recurringTransaction'
}

async function initializeDigitalCurrencyTransactionList (untilMillisecondTimeAgo?: number, isRecurringTransaction?: boolean, withDataQueueProcessing?: boolean) {
  const digitalCurrencyTransactionList = await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, untilMillisecondTimeAgo, isRecurringTransaction)
  for (let i = 0; i < digitalCurrencyTransactionList.length; i++) {
    const digitalCurrencyTransaction = digitalCurrencyTransactionList[i]
    await initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction)
    if (withDataQueueProcessing) {
      await initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm.function()
    }
  }
}

async function initializeDigitalCurrencyRecurringTransactionList (untilMillisecondTimeAgo?: number, withDataQueueProcessing?: boolean) {
  const digitalCurrencyTransactionList = await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_RECURRING_TRANSACTION, untilMillisecondTimeAgo)
  for (let i = 0; i < digitalCurrencyTransactionList.length; i++) {
    const digitalCurrencyTransaction = digitalCurrencyTransactionList[i]
    await initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction)
    if (withDataQueueProcessing) {
      await initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm.function()
    }
  }
}

async function getDigitalCurrencyTransactionListByTransactionTypeId (transactionTypeId: string, untilMillisecondTimeAgo?: number, isRecurringTransaction?: boolean) {
  // For now until last 10 minutes (or last millisecondTimeAgo)
    // Get transactions from the last 10 minutes
    // For each transaction
      // if transaction has been processed continue
      // initialize transaction

  let transactionTypeStoreId = ''
  switch (transactionTypeId) {
    case variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION:
      transactionTypeStoreId = 'scheduledTransactionVariableTable'
      break
    case variableTable.TRANSACTION_TYPE_ID_RECURRING_TRANSACTION:
      transactionTypeStoreId = 'recurringTransactionVariableTable'
      break
    default:
      break
  }

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const millisecondCurrentDate = NUMBER_OF_MILLISECONDS_IN_A_DAY + (new Date(currentDate)).getTime()
  // console.log('current date: ', currentDate)
  const currentDateTimePartition = getMillisecondTimePartitionList2(currentDate)
  untilMillisecondTimeAgo = untilMillisecondTimeAgo || variableTable.NUMBER_OF_MILLISECONDS_TRANSACTION_PROCESSING_BUFFER
  const oldestDate = (new Date((new Date(currentDate)).getTime() - untilMillisecondTimeAgo)).toISOString().split('.')[0] + 'Z'
  // const oldestDateTimePartition = getMillisecondTimePartitionList1(oldestDate)

  const numberOfDaysBetweenCurrentDateAndOldestDate = Math.floor(((new Date(currentDate)).getTime() - (new Date(oldestDate)).getTime()) / NUMBER_OF_MILLISECONDS_IN_A_DAY) + 2

  let digitalCurrencyTransactionList: DigitalCurrencyTransaction[] = []

  // console.log('time partition: ', currentDateTimePartition.millisecondDay, currentDateTimePartition.millisecondHour, currentDateTimePartition.millisecondMinute, currentDateTimePartition.millisecondSecond)

  for (let i = 0; i < numberOfDaysBetweenCurrentDateAndOldestDate; i++) {
    const millisecondDay = currentDateTimePartition.millisecondDay - i * NUMBER_OF_MILLISECONDS_IN_A_DAY
    const transactionTimeTableStoreId = `${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/${transactionTypeStoreId}/timeTable/transactionIdTimeTable`
    const transactionTimeTableItemId = `${millisecondDay}`

    // console.log('current date: ', currentDate)
    // console.log('hello-0: ', transactionTimeTableItemId, '; ', i)

    const createdTransactionTimeTable = await getItem2FirebaseAdmin(transactionTimeTableStoreId, transactionTimeTableItemId)
    if (!createdTransactionTimeTable) {
      continue
    }

    for (let millisecondHour in createdTransactionTimeTable) {
      if (!createdTransactionTimeTable.hasOwnProperty(millisecondHour)) {
        continue
      }
      for (let millisecondMinute in createdTransactionTimeTable[millisecondHour]) {
        if (!createdTransactionTimeTable[millisecondHour].hasOwnProperty(millisecondMinute)) {
          continue
        }
        for (let millisecondSecond in createdTransactionTimeTable[millisecondHour][millisecondMinute]) {
          if (!createdTransactionTimeTable[millisecondHour][millisecondMinute].hasOwnProperty(millisecondSecond)) {
            continue
          }

          let transactionDate = millisecondDay + parseInt(millisecondHour, 10) + parseInt(millisecondMinute, 10) + parseInt(millisecondSecond, 10)
          if (transactionDate > millisecondCurrentDate) {
            continue
          }

          const transactionIdVariableTable = createdTransactionTimeTable[millisecondHour][millisecondMinute][millisecondSecond].transactionIdVariableTable || {}

          for (let transactionId in transactionIdVariableTable) {
            if (!transactionIdVariableTable.hasOwnProperty(transactionId)) {
              continue
            }
            const transactionObject = transactionIdVariableTable[transactionId] || {}
            if (transactionObject.isProcessed || transactionObject.dateCreated <= oldestDate) {
              continue
            }

            const digitalCurrencyTransaction = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(transactionId)
            if (!digitalCurrencyTransaction) {
              await deleteItem2FirebaseAdmin(`${transactionTimeTableStoreId}/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, transactionId)
              continue
            }

            if (isRecurringTransaction !== undefined && isRecurringTransaction && !digitalCurrencyTransaction.isRecurringTransaction) {
              continue
            } else if (isRecurringTransaction !== undefined && !isRecurringTransaction && digitalCurrencyTransaction.isRecurringTransaction) {
              continue
            }

            digitalCurrencyTransactionList.push(digitalCurrencyTransaction)
          }

        }
      }
    }
  }

  return digitalCurrencyTransactionList
}


const initializeDigitalCurrencyTransactionListAlgorithm = {
  function: initializeDigitalCurrencyTransactionList
}

const initializeDigitalCurrencyRecurringTransactionListAlgorithm = {
  function: initializeDigitalCurrencyRecurringTransactionList
}

const getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm = {
  function: getDigitalCurrencyTransactionListByTransactionTypeId
}

export {
  variableTable,

  initializeDigitalCurrencyTransactionListAlgorithm,
  initializeDigitalCurrencyRecurringTransactionListAlgorithm,
  getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm
}



