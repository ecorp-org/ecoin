

import { DigitalCurrencyTransaction } from '../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import { initializeDigitalCurrencyTransactionAlgorithm } from './-5-initialize-digital-currency-transaction'

import {
  variableTable,
  getDigitalCurrencyAccountListByDateCreatedAlgorithm
} from './-3-initialize-all-accounts-for-account-id-variable-table'

async function initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts (digitalCurrencyTransaction: DigitalCurrencyTransaction, initializeFromPreviousReference?: boolean) {
  digitalCurrencyTransaction = getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm.function(digitalCurrencyTransaction, initializeFromPreviousReference)
  await initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction)
  return digitalCurrencyTransaction
}

async function updateDigitalCurrencyAccountWithMissedRecurringTransactionAmountForAllAccounts (digitalCurrencyTransaction: DigitalCurrencyTransaction, startDateForMissedTransactions: string, endDateForMissedTransactions?: string) {

  endDateForMissedTransactions = endDateForMissedTransactions || (new Date()).toISOString().split('.')[0] + 'Z'

  const transactionForAllAccountsBeforeStartDate = Object.assign({}, digitalCurrencyTransaction)
  transactionForAllAccountsBeforeStartDate.accountRecipientIdVariableTable = {}

  // Update All Accounts Before The Start Date With The Full
  // Missed Transaction Amount
  const accountListBeforeStartDate = getDigitalCurrencyAccountListByDateCreatedAlgorithm.function({ endDateAccountCreated: startDateForMissedTransactions })

  const numberOfRecurringTransactionIterationsSinceStartDate = Math.floor(((new Date(endDateForMissedTransactions)).getTime() - (new Date(startDateForMissedTransactions)).getTime()) / digitalCurrencyTransaction.millisecondRecurringDelay)

  for (let i = 0; i < accountListBeforeStartDate.length; i++) {
    const accountObject = accountListBeforeStartDate[i]
    transactionForAllAccountsBeforeStartDate.accountRecipientIdVariableTable[accountObject.accountId] = {
      transactionCurrencyAmount: digitalCurrencyTransaction.transactionCurrencyAmount
    }
  }

  await initializeDigitalCurrencyTransactionAlgorithm.function(transactionForAllAccountsBeforeStartDate, numberOfRecurringTransactionIterationsSinceStartDate)

  // Update The Accounts Between The Start Date And The
  // End Date With The Respective Missed Transaction Amount
  const accountListBetweenStartAndEndDate = getDigitalCurrencyAccountListByDateCreatedAlgorithm.function({ startDateAccountCreated: startDateForMissedTransactions, endDateAccountCreated: endDateForMissedTransactions })

  for (let i = 0; i < accountListBetweenStartAndEndDate.length; i++) {
    const accountObject = accountListBetweenStartAndEndDate[i]
    const transactionForAccountBetweenStartAndEndDate = Object.assign({}, digitalCurrencyTransaction)
    transactionForAccountBetweenStartAndEndDate.accountRecipientIdVariableTable = {}
    const numberOfRecurringTransactionIterationsSinceAccountWasCreated = Math.floor(((new Date(endDateForMissedTransactions)).getTime() - (new Date(accountObject.dateAccountCreated)).getTime()) / digitalCurrencyTransaction.millisecondRecurringDelay)

    transactionForAccountBetweenStartAndEndDate.accountRecipientIdVariableTable[accountObject.accountId] = {
      transactionCurrencyAmount: digitalCurrencyTransaction.transactionCurrencyAmount
    }

    await initializeDigitalCurrencyTransactionAlgorithm.function(transactionForAccountBetweenStartAndEndDate, numberOfRecurringTransactionIterationsSinceAccountWasCreated)
  }

  return digitalCurrencyTransaction
}


function getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts (digitalCurrencyTransaction: DigitalCurrencyTransaction, initializeFromPreviousReference?: boolean) {
  // create map for transaction recipient table with all accounts
    // from account id variable table
  // set transaction recpient table to include the transaction
    // amount for that account
  // initialize transaction

  if (initializeFromPreviousReference) {
    // Add new active accounts
    for (let accountId in variableTable.newActiveAccountIdVariableTable) {
      if (!variableTable.newActiveAccountIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }

      digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId] = {
        transactionCurrencyAmount: digitalCurrencyTransaction.transactionCurrencyAmount
      }
    }

    // Remove old active accounts
    for (let accountId in variableTable.oldActiveAccountIdVariableTable) {
      if (!variableTable.oldActiveAccountIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }

      delete digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId]
    }

  } else {
    for (let accountId in variableTable.activeAccountIdVariableTable) {
      if (!variableTable.activeAccountIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }

      digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId] = {
        transactionCurrencyAmount: digitalCurrencyTransaction.transactionCurrencyAmount
      }
    }
  }

  return digitalCurrencyTransaction
}

const initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm = {
  function: initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts
}

const updateDigitalCurrencyAccountWithMissedRecurringTransactionAmountForAllAccountsAlgorithm = {
  function: updateDigitalCurrencyAccountWithMissedRecurringTransactionAmountForAllAccounts
}

const getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm = {
  function: getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccounts
}

export {
  initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm,
  updateDigitalCurrencyAccountWithMissedRecurringTransactionAmountForAllAccountsAlgorithm,
  getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm
}




