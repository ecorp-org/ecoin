
// import 'mocha'
// import { expect } from 'chai'

// import {
//   variableTable,
//   initializeAllAccountsForAccountIdVariableTableAlgorithm,
//   uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm
// } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-3-initialize-all-accounts-for-account-id-variable-table'

// import { initializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-initialize-resources'
// import { uninitializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-2-uninitialize-resources'
// import { createDigitalCurrencyAccountList } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-5-create-digital-currency-account/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-account-list'
// import { deleteDigitalCurrencyAccount1 } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-9-delete-digital-currency-account/delete-digital-currency-account-1'

// import { DigitalCurrencyAccount } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'


// describe('initialize-all-accounts-for-account-id-variable-table', () => {

//   const originalMillisecondsToDeleteActiveAccountIdFromVariableTable = variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE

//   beforeEach(async function () {
//     this.timeout(30000)
//     await initializeResources()
//     variableTable.activeAccountIdVariableTable = {}
//     variableTable.newActiveAccountIdVariableTable = {}
//     variableTable.oldActiveAccountIdVariableTable = {}
//     variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE = originalMillisecondsToDeleteActiveAccountIdFromVariableTable
//   })

//   afterEach(async function () {
//     this.timeout(30000)
//     await uninitializeResources()
//     variableTable.activeAccountIdVariableTable = {}
//     variableTable.newActiveAccountIdVariableTable = {}
//     variableTable.oldActiveAccountIdVariableTable = {}
//     variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE = originalMillisecondsToDeleteActiveAccountIdFromVariableTable
//     uninitializeAllAccountsForAccountIdVariableTableEventListenerAlgorithm.function()
//   })

//   it('should add N accounts to the active account id variable table', async () => {
//     const numberOfAccounts = 10
//     const { digitalCurrencyAccountList } = await createDigitalCurrencyAccountList({ numberOfAccounts, minimumAmountOfCurrency: 2 })

//     await initializeAllAccountsForAccountIdVariableTableAlgorithm.function()

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(digitalCurrencyAccountList.length)
//   }).timeout(10 * 1000)

//   it('should add N new accounts to the active account id variable table', async () => {
//     const numberOfAccounts = 10
//     const numberOfNewAccounts = 10
//     const accountListObject1 = await createDigitalCurrencyAccountList({ numberOfAccounts, minimumAmountOfCurrency: 11 })

//     await initializeAllAccountsForAccountIdVariableTableAlgorithm.function()

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(accountListObject1.digitalCurrencyAccountList.length)
//     expect(Object.keys(variableTable.newActiveAccountIdVariableTable).length).to.equal(0)

//     const accountListObject2 = await createDigitalCurrencyAccountList({ numberOfAccounts: numberOfNewAccounts, minimumAmountOfCurrency: 11 })

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(numberOfAccounts + numberOfNewAccounts)
//     expect(Object.keys(variableTable.newActiveAccountIdVariableTable).length).to.equal(accountListObject2.digitalCurrencyAccountList.length)
//   }).timeout(10 * 1000)

//   it('should remove N old accounts from the active account id variable table', async () => {
//     const numberOfAccounts = 10
//     const numberOfOldAccounts = 5
//     const { serviceAccountList, digitalCurrencyAccountList } = await createDigitalCurrencyAccountList({ numberOfAccounts, minimumAmountOfCurrency: 22 })

//     await initializeAllAccountsForAccountIdVariableTableAlgorithm.function()

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(digitalCurrencyAccountList.length)
//     expect(Object.keys(variableTable.oldActiveAccountIdVariableTable).length).to.equal(0)

//     let oldAccountIdList = getNRandomElements(digitalCurrencyAccountList, numberOfOldAccounts).map((digitalCurrencyAccount: DigitalCurrencyAccount) => digitalCurrencyAccount.accountId)

//     for (let i = 0; i < oldAccountIdList.length; i++) {
//       const oldAccountId = oldAccountIdList[i]
//       const serviceAccountId = (serviceAccountList.filter((serviceAccount: any) => serviceAccount.digitalCurrencyAccountIdTable[oldAccountId] ? true : false)[0] || {}).serviceAccountId
//       await deleteDigitalCurrencyAccount1(serviceAccountId, oldAccountId)
//     }

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(numberOfAccounts - numberOfOldAccounts)
//     expect(Object.keys(variableTable.oldActiveAccountIdVariableTable).length).to.equal(numberOfOldAccounts)
//   }).timeout(10 * 1000)

//   it('should delete new accounts after N minutes', async () => {
//     const numberOfAccounts = 10
//     const numberOfNewAccounts = 10
//     const numberOfMillisecondsToWaitBeforeDeleting = 100
//     variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE = numberOfMillisecondsToWaitBeforeDeleting
//     const accountListObject1 = await createDigitalCurrencyAccountList({ numberOfAccounts, minimumAmountOfCurrency: 98 })

//     await initializeAllAccountsForAccountIdVariableTableAlgorithm.function()

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(accountListObject1.digitalCurrencyAccountList.length)
//     expect(Object.keys(variableTable.newActiveAccountIdVariableTable).length).to.equal(0)

//     // Create New Accounts
//     await createDigitalCurrencyAccountList({ numberOfAccounts: numberOfNewAccounts, minimumAmountOfCurrency: 99 })

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(numberOfAccounts + numberOfNewAccounts)

//     await new Promise((resolve) => setTimeout(resolve, 3 * numberOfMillisecondsToWaitBeforeDeleting))

//     expect(Object.keys(variableTable.newActiveAccountIdVariableTable).length).to.equal(0)
//   }).timeout(10 * 1000)

//   it('should delete old accounts after N minutes', async () => {
//     const numberOfAccounts = 10
//     const numberOfOldAccounts = 5
//     const numberOfMillisecondsToWaitBeforeDeleting = 100
//     variableTable.NUMBER_OF_MILLISECONDS_TO_DELETE_ACTIVE_ACCOUNT_ID_FROM_VARIABLE_TABLE = numberOfMillisecondsToWaitBeforeDeleting
//     const { serviceAccountList, digitalCurrencyAccountList } = await createDigitalCurrencyAccountList({ numberOfAccounts, minimumAmountOfCurrency: 9191 })

//     await initializeAllAccountsForAccountIdVariableTableAlgorithm.function()

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(digitalCurrencyAccountList.length)
//     expect(Object.keys(variableTable.oldActiveAccountIdVariableTable).length).to.equal(0)

//     let oldAccountIdList = getNRandomElements(digitalCurrencyAccountList, numberOfOldAccounts).map((digitalCurrencyAccount: DigitalCurrencyAccount) => digitalCurrencyAccount.accountId)

//     for (let i = 0; i < oldAccountIdList.length; i++) {
//       const oldAccountId = oldAccountIdList[i]
//       const serviceAccountId = (serviceAccountList.filter((serviceAccount: any) => serviceAccount.digitalCurrencyAccountIdTable[oldAccountId] ? true : false)[0] || {}).serviceAccountId
//       await deleteDigitalCurrencyAccount1(serviceAccountId, oldAccountId)
//     }

//     await new Promise((resolve) => setTimeout(resolve, 3 * numberOfMillisecondsToWaitBeforeDeleting))

//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(numberOfAccounts - numberOfOldAccounts)
//     expect(Object.keys(variableTable.oldActiveAccountIdVariableTable).length).to.equal(0)
//   }).timeout(10 * 1000)
// })


// function getNRandomElements (list: any[], n: number) {
//   let newList = []
//   let randomNumberList = []
//   while (newList.length < n) {
//     const latestRandomNumber = Math.floor(Math.random() * list.length)
//     if (randomNumberList.indexOf(latestRandomNumber) >= 0) {
//       continue
//     }
//     randomNumberList.push(latestRandomNumber)
//     newList.push(list[latestRandomNumber])
//   }
//   return newList
// }


