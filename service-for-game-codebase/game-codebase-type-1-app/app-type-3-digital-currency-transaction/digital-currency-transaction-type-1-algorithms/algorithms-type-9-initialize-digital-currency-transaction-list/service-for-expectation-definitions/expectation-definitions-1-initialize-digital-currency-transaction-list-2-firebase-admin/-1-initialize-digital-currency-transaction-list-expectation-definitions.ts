


// import 'mocha'
// import { expect } from 'chai'

// import {
//   variableTable,
//   getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm
// } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-1-initialize-digital-currency-transaction-list'
// import { initializeDigitalCurrencyTransactionAlgorithm } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-5-initialize-digital-currency-transaction'

// import { initializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-initialize-resources'
// import { uninitializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-2-uninitialize-resources'
// import { createDigitalCurrencyAccountList } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-5-create-digital-currency-account/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-account-list'
// import { createDigitalCurrencyTransactionList } from '../../../algorithms-type-1-create-digital-currency-transaction/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-transaction-list'
// import { getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm } from '../../../algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-2-firebase-admin'


// import { DigitalCurrencyAccount } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
// import { DigitalCurrencyTransaction } from '../../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

// import {
//   NUMBER_OF_MILLISECONDS_IN_A_MINUTE,
//   NUMBER_OF_MILLISECONDS_IN_AN_HOUR,
//   NUMBER_OF_MILLISECONDS_IN_A_DAY
// } from '../../../../../app-type-1-general/general-type-3-constants/constants-3-time'

// // import { getMillisecondTimePartitionList1 } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


// describe('initialize-digital-currency-transaction-list', () => {
//   let digitalCurrencyAccountList: DigitalCurrencyAccount[] = []
//   let digitalCurrencyTransactionList: DigitalCurrencyTransaction[] = []
//   let getDigitalCurrencyTransactionByIdRequestList: string[] = []
//   let isEnableDatabaseAnalytics: boolean = false
//   let databaseAnalyticsReferenceRequestArgumentList: string[] = []

//   const originalInitializeDigitalCurrencyTransactionAlgorithmFunction = initializeDigitalCurrencyTransactionAlgorithm.function
//   const originalGetDigitalCurrencyTransactionById2FirebaseAdminAlgorithmFunction = getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function

//   beforeEach(async function () {
//     this.timeout(30000)
//     await initializeResources({
//       firebaseOptions: {
//         realtimeDatabaseAnalyticsCallbackFunction: (...argumentList: any) => {
//           if (!isEnableDatabaseAnalytics) {
//             return
//           }
//           if (argumentList[0].functionName !== 'ref') {
//             return
//           }

//           databaseAnalyticsReferenceRequestArgumentList.push(argumentList[0].argumentList[0])
//         }
//       }
//     })
//     const accountListObject = await createDigitalCurrencyAccountList({ numberOfAccounts: 2, minimumAmountOfCurrency: 10000 })
//     digitalCurrencyAccountList = accountListObject.digitalCurrencyAccountList
//     ;(initializeDigitalCurrencyTransactionAlgorithm.function as any) = () => { /* */ }
//     ;(getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function as any) = (transactionId: string) => {
//       getDigitalCurrencyTransactionByIdRequestList.push(transactionId)
//       return originalGetDigitalCurrencyTransactionById2FirebaseAdminAlgorithmFunction(transactionId)
//     }
//   })

//   afterEach(async function () {
//     this.timeout(30000)
//     await uninitializeResources()
//     digitalCurrencyAccountList = []
//     digitalCurrencyTransactionList = []
//     getDigitalCurrencyTransactionByIdRequestList = []
//     databaseAnalyticsReferenceRequestArgumentList = []
//     initializeDigitalCurrencyTransactionAlgorithm.function = originalInitializeDigitalCurrencyTransactionAlgorithmFunction
//     getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function = originalGetDigitalCurrencyTransactionById2FirebaseAdminAlgorithmFunction
//   })

//   describe('getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm', () => {
//     it('should get all transactions from the last N minutes', async () => {
//       const n = 10
//       const nNumberOfMinutesAgo = n * NUMBER_OF_MILLISECONDS_IN_A_MINUTE
//       const numberOfTransactions = 40
//       const currentDate = new Date()

//       digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//         numberOfTransactions: numberOfTransactions,
//         oldestTransactionDate: new Date(currentDate.getTime() - 2 * nNumberOfMinutesAgo),
//         newestTransactionDate: currentDate,
//         textMemorandumList: ['initialize-digital-currency-transaction-list']
//       })

//       isEnableDatabaseAnalytics = true
//       const approximateStartTime = new Date()
//       const nMinutesAgo = new Date(approximateStartTime.getTime() - nNumberOfMinutesAgo)
//       await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, nNumberOfMinutesAgo)
//       isEnableDatabaseAnalytics = false

//       const transactionsBeforeNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! <= nMinutesAgo.getTime())
//       const transactionsAfterNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! > nMinutesAgo.getTime())

//       const transactionsIdBeforeNMinutesAgo = transactionsBeforeNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)
//       const transactionsIdAfterNMinutesAgo = transactionsAfterNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)

//       const doesContainAnyTransactionsBeforeNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.some((transactionId: string) => transactionsIdBeforeNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesContainEveryTransactionsAfterNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.every((transactionId: string) => transactionsIdAfterNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesEveryRequestOccurAfterNMinutesAgo = transactionsIdAfterNMinutesAgo.every((transactionId: string) => getDigitalCurrencyTransactionByIdRequestList.indexOf(transactionId) >= 0)

//       // Expect Proper Transactions To Be Retrieved
//       expect(doesContainAnyTransactionsBeforeNMinutesAgo).to.equal(false)
//       expect(doesContainEveryTransactionsAfterNMinutesAgo).to.equal(true)
//       expect(doesEveryRequestOccurAfterNMinutesAgo).to.be.equal(true)
//     }).timeout(10 * 1000)

//     it('should get all transactions from the last N hours', async () => {
//       const n = 10
//       const nNumberOfMinutesAgo = n * NUMBER_OF_MILLISECONDS_IN_AN_HOUR
//       const numberOfTransactions = 40
//       const currentDate = new Date()

//       digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//         numberOfTransactions: numberOfTransactions,
//         oldestTransactionDate: new Date(currentDate.getTime() - 2 * nNumberOfMinutesAgo),
//         newestTransactionDate: currentDate,
//         textMemorandumList: ['initialize-digital-currency-transaction-list']
//       })

//       isEnableDatabaseAnalytics = true
//       const approximateStartTime = new Date()
//       const nMinutesAgo = new Date(approximateStartTime.getTime() - nNumberOfMinutesAgo)
//       await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, nNumberOfMinutesAgo)
//       isEnableDatabaseAnalytics = false

//       const transactionsBeforeNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! <= nMinutesAgo.getTime())
//       const transactionsAfterNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! > nMinutesAgo.getTime())

//       const transactionsIdBeforeNMinutesAgo = transactionsBeforeNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)
//       const transactionsIdAfterNMinutesAgo = transactionsAfterNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)

//       const doesContainAnyTransactionsBeforeNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.some((transactionId: string) => transactionsIdBeforeNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesContainEveryTransactionsAfterNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.every((transactionId: string) => transactionsIdAfterNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesEveryRequestOccurAfterNMinutesAgo = transactionsIdAfterNMinutesAgo.every((transactionId: string) => getDigitalCurrencyTransactionByIdRequestList.indexOf(transactionId) >= 0)

//       // Expect Proper Transactions To Be Retrieved
//       expect(doesContainAnyTransactionsBeforeNMinutesAgo).to.equal(false)
//       expect(doesContainEveryTransactionsAfterNMinutesAgo).to.equal(true)
//       expect(doesEveryRequestOccurAfterNMinutesAgo).to.be.equal(true)
//     }).timeout(10 * 1000)

//     it('should get all transactions from the last N days', async () => {
//       const n = 10
//       const nNumberOfMinutesAgo = n * NUMBER_OF_MILLISECONDS_IN_A_DAY
//       const numberOfTransactions = 40
//       const currentDate = new Date()

//       digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//         numberOfTransactions: numberOfTransactions,
//         oldestTransactionDate: new Date(currentDate.getTime() - 2 * nNumberOfMinutesAgo),
//         newestTransactionDate: currentDate,
//         textMemorandumList: ['initialize-digital-currency-transaction-list']
//       })

//       isEnableDatabaseAnalytics = true
//       const approximateStartTime = new Date()
//       const nMinutesAgo = new Date(approximateStartTime.getTime() - nNumberOfMinutesAgo)
//       await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, nNumberOfMinutesAgo)
//       isEnableDatabaseAnalytics = false

//       const transactionsBeforeNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! <= nMinutesAgo.getTime())
//       const transactionsAfterNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! > nMinutesAgo.getTime())

//       const transactionsIdBeforeNMinutesAgo = transactionsBeforeNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)
//       const transactionsIdAfterNMinutesAgo = transactionsAfterNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)

//       const doesContainAnyTransactionsBeforeNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.some((transactionId: string) => transactionsIdBeforeNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesContainEveryTransactionsAfterNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.every((transactionId: string) => transactionsIdAfterNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesEveryRequestOccurAfterNMinutesAgo = transactionsIdAfterNMinutesAgo.every((transactionId: string) => getDigitalCurrencyTransactionByIdRequestList.indexOf(transactionId) >= 0)

//       // Expect Proper Transactions To Be Retrieved
//       expect(doesContainAnyTransactionsBeforeNMinutesAgo).to.equal(false)
//       expect(doesContainEveryTransactionsAfterNMinutesAgo).to.equal(true)
//       expect(doesEveryRequestOccurAfterNMinutesAgo).to.be.equal(true)
//     }).timeout(10 * 1000)

//     it('should get all transactions from the last X days, Y hours and Z minutes ago', async () => {
//       const x = 10 // days ago
//       const y = 100 // hours ago
//       const z = 1000 // minutes ago

//       const nNumberOfMinutesAgo = x * NUMBER_OF_MILLISECONDS_IN_A_DAY + y * NUMBER_OF_MILLISECONDS_IN_AN_HOUR + z * NUMBER_OF_MILLISECONDS_IN_A_MINUTE
//       const numberOfTransactions = 40
//       const currentDate = new Date()

//       digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//         numberOfTransactions: numberOfTransactions,
//         oldestTransactionDate: new Date(currentDate.getTime() - 2 * nNumberOfMinutesAgo),
//         newestTransactionDate: currentDate,
//         textMemorandumList: ['initialize-digital-currency-transaction-list']
//       })

//       isEnableDatabaseAnalytics = true
//       const approximateStartTime = new Date()
//       const nMinutesAgo = new Date(approximateStartTime.getTime() - nNumberOfMinutesAgo)
//       await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, nNumberOfMinutesAgo)
//       isEnableDatabaseAnalytics = false

//       const transactionsBeforeNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! <= nMinutesAgo.getTime())
//       const transactionsAfterNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! > nMinutesAgo.getTime())

//       const transactionsIdBeforeNMinutesAgo = transactionsBeforeNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)
//       const transactionsIdAfterNMinutesAgo = transactionsAfterNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)

//       const doesContainAnyTransactionsBeforeNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.some((transactionId: string) => transactionsIdBeforeNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesContainEveryTransactionsAfterNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.every((transactionId: string) => transactionsIdAfterNMinutesAgo.indexOf(transactionId) >= 0)
//       const doesEveryRequestOccurAfterNMinutesAgo = transactionsIdAfterNMinutesAgo.every((transactionId: string) => getDigitalCurrencyTransactionByIdRequestList.indexOf(transactionId) >= 0)

//       // Expect Proper Transactions To Be Retrieved
//       expect(doesContainAnyTransactionsBeforeNMinutesAgo).to.equal(false)
//       expect(doesContainEveryTransactionsAfterNMinutesAgo).to.equal(true)
//       expect(doesEveryRequestOccurAfterNMinutesAgo).to.be.equal(true)
//     }).timeout(10 * 1000)

//     it('should get scheduled recurring transactions', async () => {
//       const numberOfTransactions = 20
//       const numberOfRecurringTransactions = 20

//       digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//         numberOfTransactions,
//         numberOfRecurringTransactions,
//         textMemorandumList: ['initialize-digital-currency-transaction-list']
//       })

//       const retrievedDigitalCurrencyAccountList = await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, undefined, true)

//       const isEveryRetrievedTransactionARecurringTransaction = retrievedDigitalCurrencyAccountList.every((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.isRecurringTransaction)

//       expect(isEveryRetrievedTransactionARecurringTransaction).to.equal(true)
//       expect(retrievedDigitalCurrencyAccountList.length).to.equal(numberOfRecurringTransactions)
//     }).timeout(10 * 1000)

//     it('should get scheduled non-recurring transactions', async () => {
//       const numberOfTransactions = 20
//       const numberOfRecurringTransactions = 20

//       digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//         numberOfTransactions,
//         numberOfRecurringTransactions,
//         textMemorandumList: ['initialize-digital-currency-transaction-list']
//       })

//       const retrievedDigitalCurrencyAccountList = await getDigitalCurrencyTransactionListByTransactionTypeIdAlgorithm.function(variableTable.TRANSACTION_TYPE_ID_SCHEDULED_TRANSACTION, undefined, false)

//       const isEveryRetrievedTransactionANonRecurringTransaction = retrievedDigitalCurrencyAccountList.every((digitalCurrencyTransaction: DigitalCurrencyTransaction) => !digitalCurrencyTransaction.isRecurringTransaction)

//       expect(isEveryRetrievedTransactionANonRecurringTransaction).to.equal(true)
//       expect(retrievedDigitalCurrencyAccountList.length).to.equal(numberOfTransactions)
//     }).timeout(10 * 1000)
//   })
// })


