
import 'mocha'
import { expect } from 'chai'

import {
  variableTable,
  initializeDigitalCurrencyTransactionAlgorithm
} from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-5-initialize-digital-currency-transaction'

import {
  initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm
} from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-6-initialize-digital-currency-account-transaction-data-queue-processing'

import { initializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-initialize-resources'
import { uninitializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-2-uninitialize-resources'
import { createDigitalCurrencyAccountList } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-5-create-digital-currency-account/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-account-list'
import { createDigitalCurrencyTransactionList } from '../../../algorithms-type-1-create-digital-currency-transaction/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-transaction-list'
import { getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm } from '../../../algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-2-firebase-admin'
import { getDigitalCurrencyAccountById2FirebaseAdmin } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-15-get-digital-currency-account-by-id/get-digital-currency-account-by-id-2-firebase-admin'

// import { getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm } from '../../../algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-2-firebase-admin'

import { DigitalCurrencyAccount } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
import { DigitalCurrencyTransaction } from '../../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  NUMBER_OF_MILLISECONDS_IN_A_DAY
} from '../../../../../app-type-1-general/general-type-3-constants/constants-3-time'

// import { getMillisecondTimePartitionList1 } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


let digitalCurrencyAccountList: DigitalCurrencyAccount[] = []
let digitalCurrencyTransactionList: DigitalCurrencyTransaction[] = []

describe('initialize-digital-currency-transaction', () => {

  const initialAccountDigitalCurrencyAmount: number = 10000

  beforeEach(async function () {
    this.timeout(30000)
    await initializeResources()
    const accountListObject = await createDigitalCurrencyAccountList({
      numberOfAccounts: 2,
      minimumAmountOfCurrency: initialAccountDigitalCurrencyAmount,
      maximumAmountOfCurrency: initialAccountDigitalCurrencyAmount
    })
    digitalCurrencyAccountList = accountListObject.digitalCurrencyAccountList

  })

  afterEach(async function () {
    this.timeout(30000)
    await uninitializeResources()
    digitalCurrencyAccountList = []
    digitalCurrencyTransactionList = []
  })

  // it('should initialize scheduled transaction', async () => {
  //   const nNumberOfMinutesAgo = Math.floor(1 + Math.random() * 10) * NUMBER_OF_MILLISECONDS_IN_A_DAY
  //   const numberOfTransactions = 40
  //   const minimumDigitalCurrencyTransactionAmount = 1 + Math.floor(Math.random() * 10)

  //   const digitalCurrencyAccountSenderId = digitalCurrencyAccountList[0].accountId
  //   const digitalCurrencyAccountRecipientId = digitalCurrencyAccountList[1].accountId

  //   // Create Transactions
  //   digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
  //     numberOfTransactions,
  //     oldestTransactionDate: new Date((new Date()).getTime() - nNumberOfMinutesAgo),
  //     newestTransactionDate: new Date(),
  //     accountSenderId: digitalCurrencyAccountSenderId,
  //     minimumDigitalCurrencyAmount: minimumDigitalCurrencyTransactionAmount,
  //     maximumDigitalCurrencyAmount: minimumDigitalCurrencyTransactionAmount,
  //     textMemorandumList: ['initialize-digital-currency-transaction']
  //   })

  //   // Initialize Transaction
  //   const smallAmountOfTimeToWait = 5 // 5 milliseconds
  //   const smallAmountOfTimeAfterInitialized = 10 * 1000 // 10 seconds

  //   for (let i = 0; i < digitalCurrencyTransactionList.length; i++) {
  //     let digitalCurrencyTransaction = digitalCurrencyTransactionList[i]
  //     expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(false)

  //     const beforeTransactionProcessed = (new Date()).getTime()

  //     const initializePromise = initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction)

  //     // Wait Until Transaction is Being Processed
  //     await new Promise((resolve) => setTimeout(resolve, smallAmountOfTimeToWait))

  //     digitalCurrencyTransaction = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)
  //     expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(true)
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.greaterThanOrEqual(beforeTransactionProcessed)
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.lessThan(beforeTransactionProcessed + smallAmountOfTimeAfterInitialized)

  //     // Transaction Has Been Processed
  //     digitalCurrencyTransaction = await initializePromise

  //     const afterTransactionProcessed = (new Date()).getTime()

  //     // Expectations Before Network Request
  //     expect(digitalCurrencyTransaction.accountSenderId).to.not.equal('')
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.greaterThanOrEqual(beforeTransactionProcessed)
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.lessThanOrEqual(afterTransactionProcessed)
  //     expect(digitalCurrencyTransaction.dateRecurrenceCancelled).to.equal(undefined)
  //     expect(digitalCurrencyTransaction.dateRecurrenceRestarted).to.equal(undefined)
  //     expect(typeof digitalCurrencyTransaction.dateTransactionCreated).to.equal('number')
  //     expect(digitalCurrencyTransaction.dateTransactionCreated).to.be.greaterThan(0)

  //     expect(digitalCurrencyTransaction.dateTransactionProcessed).to.be.greaterThan(0)
  //     expect(digitalCurrencyTransaction.dateTransactionReScheduledForProcessing).to.equal(undefined)
  //     expect(digitalCurrencyTransaction.dateTransactionScheduledForCancellingRecurrence).to.equal(undefined)
  //     expect(digitalCurrencyTransaction.dateTransactionScheduledForProcessing).to.equal(undefined)

  //     expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(false)
  //     expect(digitalCurrencyTransaction.isProcessed).to.equal(true)
  //     expect(digitalCurrencyTransaction.isRecurringTransaction).to.equal(false)

  //     expect(digitalCurrencyTransaction.latestReasonForProcessingFailed).to.equal('')
  //     expect(digitalCurrencyTransaction.millisecondRecurringDelay).to.equal(0)
  //     expect(digitalCurrencyTransaction.numberOfTimesProcessingFailed).to.equal(0)
  //     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.equal(0)
  //     expect(digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart).to.equal(0)
  //     expect(digitalCurrencyTransaction.onBehalfOfAccountId).to.equal('')
  //     // expect(digitalCurrencyTransaction.originalTimeNumberUntilNextRecurrence)
  //     // expect(digitalCurrencyTransaction.originalTimeScaleUntilNextRecurrence)
  //     expect(digitalCurrencyTransaction.specialTransactionType).to.equal('')
  //     // expect(digitalCurrencyTransaction.textMemorandum)
  //     expect(digitalCurrencyTransaction.transactionCurrencyAmount).to.equal(minimumDigitalCurrencyTransactionAmount)
  //     expect(digitalCurrencyTransaction.transactionId).to.not.equal('')
  //     // expect(digitalCurrencyTransaction.transactionName)
  //     expect(digitalCurrencyTransaction.transactionOrderIndex).to.equal(i + 1)
  //     // expect(digitalCurrencyTransaction.transactionType)

  //     const digitalCurrencyTransactionAfterNetworkRequest = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)

  //     expect(digitalCurrencyTransaction).to.deep.equal(digitalCurrencyTransactionAfterNetworkRequest)
  //   }

  //   await initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm.function()

  //   let digitalCurrencyAccountSender = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyAccountSenderId)
  //   let digitalCurrencyAccountRecipient = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyAccountRecipientId)

  //   const totalTransactionDigitalCurrencyAmount = digitalCurrencyTransactionList.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionCurrencyAmount).reduce((a: number, b: number) => a + b)

  //   expect(totalTransactionDigitalCurrencyAmount).to.be.greaterThan(0)
  //   expect(digitalCurrencyAccountSender.accountCurrencyAmount).to.equal(initialAccountDigitalCurrencyAmount - totalTransactionDigitalCurrencyAmount)
  //   expect(digitalCurrencyAccountRecipient.accountCurrencyAmount).to.equal(initialAccountDigitalCurrencyAmount + totalTransactionDigitalCurrencyAmount)
  // }).timeout(30 * 1000)

  // it('should initialize scheduled recurring transaction', async () => {
  //   const nNumberOfMinutesAgo = Math.floor(1 + Math.random() * 10) * NUMBER_OF_MILLISECONDS_IN_A_DAY
  //   const numberOfRecurringTransactions = 40
  //   const minimumDigitalCurrencyTransactionAmount = 1 + Math.floor(Math.random() * 10)
  //   const minimumMillisecondRecurringTransactionDelay = 100
  //   const maximumMillisecondRecurringTransactionDelay = 200

  //   const digitalCurrencyAccountSenderId = digitalCurrencyAccountList[0].accountId
  //   const digitalCurrencyAccountRecipientId = digitalCurrencyAccountList[1].accountId

  //   // Create Transactions
  //   digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
  //     numberOfRecurringTransactions,
  //     oldestTransactionDate: new Date((new Date()).getTime() - nNumberOfMinutesAgo),
  //     newestTransactionDate: new Date(),
  //     accountSenderId: digitalCurrencyAccountSenderId,
  //     minimumDigitalCurrencyAmount: minimumDigitalCurrencyTransactionAmount,
  //     maximumDigitalCurrencyAmount: minimumDigitalCurrencyTransactionAmount,
  //     minimumMillisecondRecurringTransactionDelay,
  //     maximumMillisecondRecurringTransactionDelay,
  //     textMemorandumList: ['initialize-digital-currency-transaction']
  //   })

  //   // Initialize Transaction
  //   const smallAmountOfTimeToWait = 5 // 5 milliseconds
  //   const smallAmountOfTimeAfterInitialized = 10 * 1000 // 10 seconds

  //   for (let i = 0; i < digitalCurrencyTransactionList.length; i++) {
  //     let digitalCurrencyTransaction = digitalCurrencyTransactionList[i]
  //     expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(false)

  //     const beforeTransactionProcessed = (new Date()).getTime()

  //     const initializePromise = initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction)

  //     // Wait Until Transaction is Being Processed
  //     await new Promise((resolve) => setTimeout(resolve, smallAmountOfTimeToWait))

  //     digitalCurrencyTransaction = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)
  //     expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(true)
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.greaterThanOrEqual(beforeTransactionProcessed)
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.lessThan(beforeTransactionProcessed + smallAmountOfTimeAfterInitialized)

  //     // Transaction Has Been Processed
  //     digitalCurrencyTransaction = await initializePromise

  //     const afterTransactionProcessed = (new Date()).getTime()

  //     // Expectations Before Network Request
  //     expect(digitalCurrencyTransaction.accountSenderId).to.not.equal('')
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.greaterThanOrEqual(beforeTransactionProcessed)
  //     expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.lessThanOrEqual(afterTransactionProcessed)
  //     expect(digitalCurrencyTransaction.dateRecurrenceCancelled).to.equal(undefined)
  //     expect(digitalCurrencyTransaction.dateRecurrenceRestarted).to.equal(undefined)
  //     expect(typeof digitalCurrencyTransaction.dateTransactionCreated).to.equal('number')
  //     expect(digitalCurrencyTransaction.dateTransactionCreated).to.be.greaterThan(0)

  //     expect(digitalCurrencyTransaction.dateTransactionProcessed).to.be.greaterThan(0)
  //     expect(digitalCurrencyTransaction.dateTransactionReScheduledForProcessing).to.equal(undefined)
  //     expect(digitalCurrencyTransaction.dateTransactionScheduledForCancellingRecurrence).to.equal(undefined)
  //     expect(digitalCurrencyTransaction.dateTransactionScheduledForProcessing).to.equal(undefined)

  //     expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(false)
  //     expect(digitalCurrencyTransaction.isProcessed).to.equal(true)
  //     expect(digitalCurrencyTransaction.isRecurringTransaction).to.equal(true)

  //     expect(digitalCurrencyTransaction.latestReasonForProcessingFailed).to.equal('')
  //     expect(digitalCurrencyTransaction.millisecondRecurringDelay).to.be.greaterThanOrEqual(minimumMillisecondRecurringTransactionDelay)
  //     expect(digitalCurrencyTransaction.millisecondRecurringDelay).to.be.lessThanOrEqual(maximumMillisecondRecurringTransactionDelay)
  //     expect(digitalCurrencyTransaction.numberOfTimesProcessingFailed).to.equal(0)
  //     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.equal(1)
  //     expect(digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart).to.equal(0)
  //     expect(digitalCurrencyTransaction.onBehalfOfAccountId).to.equal('')
  //     // expect(digitalCurrencyTransaction.originalTimeNumberUntilNextRecurrence)
  //     // expect(digitalCurrencyTransaction.originalTimeScaleUntilNextRecurrence)
  //     expect(digitalCurrencyTransaction.specialTransactionType).to.equal('')
  //     // expect(digitalCurrencyTransaction.textMemorandum)
  //     expect(digitalCurrencyTransaction.transactionCurrencyAmount).to.equal(minimumDigitalCurrencyTransactionAmount)
  //     expect(digitalCurrencyTransaction.transactionId).to.not.equal('')
  //     // expect(digitalCurrencyTransaction.transactionName)
  //     expect(digitalCurrencyTransaction.transactionOrderIndex).to.equal(i + 1)
  //     // expect(digitalCurrencyTransaction.transactionType)

  //     const digitalCurrencyTransactionAfterNetworkRequest = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)

  //     expect(digitalCurrencyTransaction).to.deep.equal(digitalCurrencyTransactionAfterNetworkRequest)
  //   }

  //   await initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm.function()

  //   let digitalCurrencyAccountSender = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyAccountSenderId)
  //   let digitalCurrencyAccountRecipient = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyAccountRecipientId)

  //   const totalTransactionDigitalCurrencyAmount = digitalCurrencyTransactionList.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionCurrencyAmount).reduce((a: number, b: number) => a + b)

  //   expect(totalTransactionDigitalCurrencyAmount).to.be.greaterThan(0)
  //   expect(digitalCurrencyAccountSender.accountCurrencyAmount).to.equal(initialAccountDigitalCurrencyAmount - totalTransactionDigitalCurrencyAmount)
  //   expect(digitalCurrencyAccountRecipient.accountCurrencyAmount).to.equal(initialAccountDigitalCurrencyAmount + totalTransactionDigitalCurrencyAmount)
  // }).timeout(30 * 1000)

  // it('should initialize non-scheduled recurring transaction #1 - One Account Sender And One Account Recipient', async () => {
  //   //
  // }).timeout(30 * 1000)

  // it('should initialize non-scheduled recurring transaction #2 - One Account Sender And One Account Recipient + Deleted Account Sender', async () => {
  //   //
  // }).timeout(30 * 1000)

  // it('should initialize non-scheduled recurring transaction #2 - One Account Sender And One Account Recipient + Deleted Account Recipient', async () => {
  //   //
  // }).timeout(30 * 1000)

  // it('should initialize non-scheduled recurring transaction #2 - Multiple Interacting Parties', async () => {
  //   //
  // }).timeout(30 * 1000)

  // it('should initialize administrator recurring transaction')
  it('should create error for invalid transaction #1 - Not Enough Currency', async () => {
    const nNumberOfMinutesAgo = Math.floor(1 + Math.random() * 10) * NUMBER_OF_MILLISECONDS_IN_A_DAY
    const numberOfRecurringTransactions = 2
    const minimumDigitalCurrencyTransactionAmount = initialAccountDigitalCurrencyAmount + 1
    const minimumMillisecondRecurringTransactionDelay = 100
    const maximumMillisecondRecurringTransactionDelay = 200

    const numberOfErrors = 10

    const digitalCurrencyAccountSenderId = digitalCurrencyAccountList[0].accountId
    const digitalCurrencyAccountRecipientId = digitalCurrencyAccountList[1].accountId

    // Create Transactions
    digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
      numberOfRecurringTransactions,
      oldestTransactionDate: new Date((new Date()).getTime() - nNumberOfMinutesAgo),
      newestTransactionDate: new Date(),
      accountSenderId: digitalCurrencyAccountSenderId,
      minimumDigitalCurrencyAmount: minimumDigitalCurrencyTransactionAmount,
      maximumDigitalCurrencyAmount: minimumDigitalCurrencyTransactionAmount,
      minimumMillisecondRecurringTransactionDelay,
      maximumMillisecondRecurringTransactionDelay,
      textMemorandumList: ['initialize-digital-currency-transaction']
    })

    // Initialize Transaction
    const smallAmountOfTimeToWait = 5 // 5 milliseconds
    const smallAmountOfTimeAfterInitialized = 10 * 1000 // 10 seconds

    for (let i = 0; i < numberOfErrors; i++) {
      for (let j = 0; j < digitalCurrencyTransactionList.length; j++) {
        let digitalCurrencyTransaction = digitalCurrencyTransactionList[j]
        expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(false)

        const beforeTransactionProcessed = (new Date()).getTime()

        const initializePromise = initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction)

        // Wait Until Transaction is Being Processed
        await new Promise((resolve) => setTimeout(resolve, smallAmountOfTimeToWait))

        digitalCurrencyTransaction = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)
        expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(true)
        expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.greaterThanOrEqual(beforeTransactionProcessed)
        expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.lessThan(beforeTransactionProcessed + smallAmountOfTimeAfterInitialized)

        // Transaction Has Been Processed
        digitalCurrencyTransaction = await initializePromise

        const afterTransactionProcessed = (new Date()).getTime()

        // Expectations Before Network Request
        expect(digitalCurrencyTransaction.accountSenderId).to.not.equal('')
        expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.greaterThanOrEqual(beforeTransactionProcessed)
        expect(digitalCurrencyTransaction.dateLastBeingProcessed).to.be.lessThanOrEqual(afterTransactionProcessed)

        if (i + 1 <= variableTable.NUMBER_OF_TIMES_FAILED_TRANSACTION_PROCESSING_IS_ALLOWED) {
          expect(digitalCurrencyTransaction.dateRecurrenceCancelled).to.equal(undefined)
        } else {
          expect(digitalCurrencyTransaction.dateRecurrenceCancelled).to.be.greaterThan(0)
        }

        expect(digitalCurrencyTransaction.dateRecurrenceRestarted).to.be.greaterThan(0)
        expect(typeof digitalCurrencyTransaction.dateTransactionCreated).to.equal('number')
        expect(digitalCurrencyTransaction.dateTransactionCreated).to.be.greaterThan(0)

        expect(digitalCurrencyTransaction.dateTransactionProcessed).to.be.greaterThan(0)
        expect(digitalCurrencyTransaction.dateTransactionReScheduledForProcessing).to.be.greaterThan(0)
        expect(digitalCurrencyTransaction.dateTransactionScheduledForCancellingRecurrence).to.equal(undefined)
        expect(digitalCurrencyTransaction.dateTransactionScheduledForProcessing).to.equal(undefined)

        expect(digitalCurrencyTransaction.isBeingProcessed).to.equal(false)
        expect(digitalCurrencyTransaction.isProcessed).to.equal(true)
        expect(digitalCurrencyTransaction.isRecurringTransaction).to.equal(true)

        expect(digitalCurrencyTransaction.latestReasonForProcessingFailed).to.not.equal('')
        expect(digitalCurrencyTransaction.millisecondRecurringDelay).to.be.greaterThanOrEqual(minimumMillisecondRecurringTransactionDelay)
        expect(digitalCurrencyTransaction.millisecondRecurringDelay).to.be.lessThanOrEqual(maximumMillisecondRecurringTransactionDelay)
        expect(digitalCurrencyTransaction.numberOfTimesProcessingFailed).to.equal(i + 1)
        expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.equal(0)
        expect(digitalCurrencyTransaction.numberOfTimesRecurredSinceRestart).to.equal(0)
        expect(digitalCurrencyTransaction.onBehalfOfAccountId).to.equal('')
        // expect(digitalCurrencyTransaction.originalTimeNumberUntilNextRecurrence)
        // expect(digitalCurrencyTransaction.originalTimeScaleUntilNextRecurrence)
        expect(digitalCurrencyTransaction.specialTransactionType).to.equal('')
        // expect(digitalCurrencyTransaction.textMemorandum)
        expect(digitalCurrencyTransaction.transactionCurrencyAmount).to.equal(minimumDigitalCurrencyTransactionAmount)
        expect(digitalCurrencyTransaction.transactionId).to.not.equal('')
        // expect(digitalCurrencyTransaction.transactionName)
        expect(digitalCurrencyTransaction.transactionOrderIndex).to.equal(j + 1)
        // expect(digitalCurrencyTransaction.transactionType)

        const digitalCurrencyTransactionAfterNetworkRequest = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)

        expect(digitalCurrencyTransaction).to.deep.equal(digitalCurrencyTransactionAfterNetworkRequest)
      }
    }

    await initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm.function()

    let digitalCurrencyAccountSender = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyAccountSenderId)
    let digitalCurrencyAccountRecipient = await getDigitalCurrencyAccountById2FirebaseAdmin(digitalCurrencyAccountRecipientId)

    const totalTransactionDigitalCurrencyAmount = digitalCurrencyTransactionList.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionCurrencyAmount).reduce((a: number, b: number) => a + b)

    expect(totalTransactionDigitalCurrencyAmount).to.be.greaterThan(0)
    expect(digitalCurrencyAccountSender.accountCurrencyAmount).to.equal(initialAccountDigitalCurrencyAmount)
    expect(digitalCurrencyAccountRecipient.accountCurrencyAmount).to.equal(initialAccountDigitalCurrencyAmount)
  })
  // it('should create error for invalid transaction #2 - Multiple Number of Errors')
  // it('should update recurring transaction with number of recurring iterations #1 - Enough Currency For Full Update')
  // it('should update recurring transaction with number of recurring iterations #2 - Not Enough Currency For Full Update')

})


// function createAccountList () {
//   //
// }

// function createTransactionList () {
//   //
// }

// function deleteAccountList () {
//   //
// }

// function deleteTransactionList () {
//   //
// }

// function initializeTransactionList () {
//   //
// }

// function initializeTransactionListWithNumberOfIterations () {
//   //
// }

// function cancelTransactionList () {
//   //
// }

// function restartTransactionList () {
//   //
// }

// function waitPeriodOfTime () {
//  //
// }

// function startSimulationGroup () {
//   //
// }






