

// import 'mocha'
// import { expect } from 'chai'

// import {
//   initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm
// } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-2-initialize-digital-currency-transaction-with-account-recipients-as-all-accounts'

// import { variableTable } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-3-initialize-all-accounts-for-account-id-variable-table'

// import { DigitalCurrencyTransaction } from '../../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'


// describe('initialize-digital-currency-transaction-with-account-recipients-as-all-accounts', () => {

//   afterEach(() => {
//     variableTable.activeAccountIdVariableTable = {}
//     variableTable.newActiveAccountIdVariableTable = {}
//     variableTable.oldActiveAccountIdVariableTable = {}
//   })


//   it('should return transaction with account recipients receiving currency amount', async () => {
//     const numberOfAccounts = 10
//     let digitalCurrencyTransaction = new DigitalCurrencyTransaction()
//     digitalCurrencyTransaction.transactionCurrencyAmount = 10
//     expect(Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).length).to.equal(0)

//     // Create Active Account
//     for (let i = 0; i < numberOfAccounts; i++) {
//       const accountId = `account-${i}`
//       const currentDate = (new Date()).getTime()
//       variableTable.activeAccountIdVariableTable[accountId] = { accountId, dateAccountCreated: currentDate, dateAddedToList: currentDate }
//     }

//     // Add Account Recipients
//     digitalCurrencyTransaction = await initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm.function(digitalCurrencyTransaction)

//     const numberOfAccountRecipients = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).length
//     const doesEveryTransactionAmountEqualExpectedAmount = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).map((accountId: any) => digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId].transactionCurrencyAmount).every((currencyAmount: number) => currencyAmount === digitalCurrencyTransaction.transactionCurrencyAmount)

//     expect(numberOfAccountRecipients).to.equal(numberOfAccounts)
//     expect(doesEveryTransactionAmountEqualExpectedAmount).to.equal(true)
//   })

//   it('should initialize transaction account recipient list from previous reference', async () => {
//     const numberOfAccounts = 10
//     const numberOfNewAccounts = 10
//     const numberOfOldAccounts = 5

//     let digitalCurrencyTransaction = new DigitalCurrencyTransaction()
//     digitalCurrencyTransaction.transactionCurrencyAmount = 10
//     expect(Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).length).to.equal(0)
//     expect(Object.keys(variableTable.activeAccountIdVariableTable).length).to.equal(0)
//     expect(Object.keys(variableTable.newActiveAccountIdVariableTable).length).to.equal(0)
//     expect(Object.keys(variableTable.oldActiveAccountIdVariableTable).length).to.equal(0)

//     // Create Active Account
//     for (let i = 0; i < numberOfAccounts; i++) {
//       const accountId = `account-${i}`
//       const currentDate = (new Date()).getTime()
//       variableTable.activeAccountIdVariableTable[accountId] = { accountId, dateAccountCreated: currentDate, dateAddedToList: currentDate }
//     }

//     // Add Account Recipients
//     digitalCurrencyTransaction = await initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm.function(digitalCurrencyTransaction)

//     let numberOfAccountRecipients = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).length
//     let doesEveryTransactionAmountEqualExpectedAmount = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).map((accountId: any) => digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId].transactionCurrencyAmount).every((currencyAmount: number) => currencyAmount === digitalCurrencyTransaction.transactionCurrencyAmount)
//     expect(numberOfAccountRecipients).to.equal(numberOfAccounts)
//     expect(doesEveryTransactionAmountEqualExpectedAmount).to.equal(true)

//     // Create New Active Accounts
//     for (let i = numberOfAccounts; i < numberOfAccounts + numberOfNewAccounts; i++) {
//       const accountId = `account-${i}`
//       const currentDate = (new Date()).getTime()
//       variableTable.newActiveAccountIdVariableTable[accountId] = { accountId, dateAccountCreated: currentDate, dateAddedToList: currentDate }
//     }

//     // Create Old Active Accounts
//     for (let i = 0; i < numberOfOldAccounts; i++) {
//       let accountIndex = null
//       while (!accountIndex || variableTable.oldActiveAccountIdVariableTable[`account-${accountIndex}`]) {
//         accountIndex = Math.floor(Math.random() * (numberOfAccounts + numberOfNewAccounts))
//       }
//       const accountId = `account-${accountIndex}`
//       const currentDate = (new Date()).getTime()
//       variableTable.oldActiveAccountIdVariableTable[accountId] = { accountId, dateAccountDeleted: currentDate, dateAddedToList: currentDate }
//     }

//     // Add Account Recipients From Previous Reference
//     digitalCurrencyTransaction = await initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm.function(digitalCurrencyTransaction, true)

//     numberOfAccountRecipients = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).length
//     doesEveryTransactionAmountEqualExpectedAmount = Object.keys(digitalCurrencyTransaction.accountRecipientIdVariableTable).map((accountId: any) => digitalCurrencyTransaction.accountRecipientIdVariableTable[accountId].transactionCurrencyAmount).every((currencyAmount: number) => currencyAmount === digitalCurrencyTransaction.transactionCurrencyAmount)

//     expect(numberOfAccountRecipients).to.equal(numberOfAccounts + numberOfNewAccounts - numberOfOldAccounts)
//     expect(doesEveryTransactionAmountEqualExpectedAmount).to.equal(true)
//   })
// })



