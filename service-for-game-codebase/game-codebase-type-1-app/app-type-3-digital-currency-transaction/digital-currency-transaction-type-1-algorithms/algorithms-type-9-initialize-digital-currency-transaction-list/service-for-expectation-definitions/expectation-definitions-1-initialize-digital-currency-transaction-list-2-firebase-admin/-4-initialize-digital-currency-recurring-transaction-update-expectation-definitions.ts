
// import 'mocha'
// import { expect } from 'chai'

// import {
//   updateDigitalCurrencyRecurringTransactionListAlgorithm,
//   initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm
// } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-4-initialize-digital-currency-recurring-transaction-update'

// import { initializeDigitalCurrencyTransactionListAlgorithm } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-1-initialize-digital-currency-transaction-list'
// import { initializeDigitalCurrencyTransactionAlgorithm } from '../../service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-5-initialize-digital-currency-transaction'

// import { initializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-initialize-resources'
// import { uninitializeResources } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-2-uninitialize-resources'
// import { createDigitalCurrencyAccountList } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-5-create-digital-currency-account/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-account-list'
// import { createDigitalCurrencyTransactionList } from '../../../algorithms-type-1-create-digital-currency-transaction/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-1-create-digital-currency-transaction-list'

// import { getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm } from '../../../algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-2-firebase-admin'

// import { DigitalCurrencyAccount } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
// import { DigitalCurrencyTransaction } from '../../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

// import {
//   // NUMBER_OF_MILLISECONDS_IN_A_MINUTE,
//   // NUMBER_OF_MILLISECONDS_IN_AN_HOUR,
//   NUMBER_OF_MILLISECONDS_IN_A_DAY
// } from '../../../../../app-type-1-general/general-type-3-constants/constants-3-time'

// // import { getMillisecondTimePartitionList1 } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


// describe('update-digital-currency-recurring-transaction-list', () => {
//   let digitalCurrencyAccountList: DigitalCurrencyAccount[] = []
//   let digitalCurrencyTransactionList: DigitalCurrencyTransaction[] = []
//   let getDigitalCurrencyTransactionByIdRequestList: string[] = []

//   const originalInitializeDigitalCurrencyRecurringTransactionUpdateAlgorithmFunction = initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm.function
//   const originalGetDigitalCurrencyTransactionById2FirebaseAdminAlgorithmFunction = getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function

//   beforeEach(async function () {
//     this.timeout(30000)
//     await initializeResources()
//     const accountListObject = await createDigitalCurrencyAccountList({
//       numberOfAccounts: 2,
//       minimumAmountOfCurrency: 10000
//     })
//     digitalCurrencyAccountList = accountListObject.digitalCurrencyAccountList

//     ;(initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm.function as any) = () => { /* */ }
//     ;(getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function as any) = (transactionId: string) => {
//       getDigitalCurrencyTransactionByIdRequestList.push(transactionId)
//       return originalGetDigitalCurrencyTransactionById2FirebaseAdminAlgorithmFunction(transactionId)
//     }
//   })

//   afterEach(async function () {
//     this.timeout(30000)
//     await uninitializeResources()
//     digitalCurrencyAccountList = []
//     digitalCurrencyTransactionList = []
//     initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm.function = originalInitializeDigitalCurrencyRecurringTransactionUpdateAlgorithmFunction
//     getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function = originalGetDigitalCurrencyTransactionById2FirebaseAdminAlgorithmFunction
//   })

//   it('should get all recurring transactions', async () => {
//     const nNumberOfMinutesAgo = Math.floor(Math.random() * 10) * NUMBER_OF_MILLISECONDS_IN_A_DAY
//     const numberOfRecurringTransactions = 10
//     const minimumMillisecondRecurringTransactionDelay = 100
//     const maximumMillisecondRecurringTransactionDelay = 200
//     const currentDate = new Date()

//     // Create Transactions
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions,
//       oldestTransactionDate: new Date(currentDate.getTime() - 2 * nNumberOfMinutesAgo),
//       newestTransactionDate: currentDate,
//       minimumMillisecondRecurringTransactionDelay: minimumMillisecondRecurringTransactionDelay,
//       maximumMillisecondRecurringTransactionDelay: maximumMillisecondRecurringTransactionDelay,
//       textMemorandumList: ['update-digital-currency-recurring-transaction-list']
//     })

//     // Process Transactions
//     await initializeDigitalCurrencyTransactionListAlgorithm.function(nNumberOfMinutesAgo)

//     // Update Recurring Transaction List
//     const approximateStartTime = new Date()
//     const nMinutesAgo = new Date(approximateStartTime.getTime() - nNumberOfMinutesAgo)
//     await updateDigitalCurrencyRecurringTransactionListAlgorithm.function(nNumberOfMinutesAgo)

//     const transactionsBeforeNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! <= nMinutesAgo.getTime())
//     const transactionsAfterNMinutesAgo = digitalCurrencyTransactionList.filter((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.dateTransactionCreated! > nMinutesAgo.getTime())

//     const transactionsIdBeforeNMinutesAgo = transactionsBeforeNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)
//     const transactionsIdAfterNMinutesAgo = transactionsAfterNMinutesAgo.map((digitalCurrencyTransaction: DigitalCurrencyTransaction) => digitalCurrencyTransaction.transactionId)

//     const doesContainAnyTransactionsBeforeNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.some((transactionId: string) => transactionsIdBeforeNMinutesAgo.indexOf(transactionId) >= 0)
//     const doesContainEveryTransactionsAfterNMinutesAgo = getDigitalCurrencyTransactionByIdRequestList.every((transactionId: string) => transactionsIdAfterNMinutesAgo.indexOf(transactionId) >= 0)
//     const doesEveryRequestOccurAfterNMinutesAgo = transactionsIdAfterNMinutesAgo.every((transactionId: string) => getDigitalCurrencyTransactionByIdRequestList.indexOf(transactionId) >= 0)

//     // Expect Proper Transactions To Be Retrieved
//     expect(transactionsBeforeNMinutesAgo.length + transactionsAfterNMinutesAgo.length).to.equal(numberOfRecurringTransactions)
//     expect(doesContainAnyTransactionsBeforeNMinutesAgo).to.equal(false)
//     expect(doesContainEveryTransactionsAfterNMinutesAgo).to.equal(true)
//     expect(doesEveryRequestOccurAfterNMinutesAgo).to.be.equal(true)
//   }).timeout(10 * 1000)
// })


// describe('initialize-digital-currency-recurring-transaction-update', () => {
//   let digitalCurrencyAccountList: DigitalCurrencyAccount[] = []
//   let digitalCurrencyTransactionList: DigitalCurrencyTransaction[] = []

//   beforeEach(async function () {
//     this.timeout(30000)
//     await initializeResources()
//     const accountListObject = await createDigitalCurrencyAccountList({
//       numberOfAccounts: 2,
//       minimumAmountOfCurrency: 10000
//     })
//     digitalCurrencyAccountList = accountListObject.digitalCurrencyAccountList
//   })

//   afterEach(async function () {
//     this.timeout(30000)
//     await uninitializeResources()
//     digitalCurrencyAccountList = []
//     digitalCurrencyTransactionList = []
//   })

//   it('should update the transaction to the latest expected amount after a pause #1 - 0 Processing Before', async () => {
//     const numberOfRecurringTransactions = 1
//     const minimumMillisecondRecurringTransactionDelay = 100
//     const maximumMillisecondRecurringTransactionDelay = 200
//     const currentDate = new Date()

//     const processingWaitingPeriodCycleAmount = 1 + Math.floor(Math.random() * 5)

//     // Create Transactions
//     const timeBeforeStarting = (new Date()).getTime()
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions,
//       oldestTransactionDate: currentDate,
//       newestTransactionDate: currentDate,
//       minimumMillisecondRecurringTransactionDelay: minimumMillisecondRecurringTransactionDelay,
//       maximumMillisecondRecurringTransactionDelay: maximumMillisecondRecurringTransactionDelay
//     })

//     let digitalCurrencyTransaction = digitalCurrencyTransactionList[0]

//     // Don't Process Transaction For N Cycles
//     await new Promise((resolve) => setTimeout(resolve, processingWaitingPeriodCycleAmount * digitalCurrencyTransaction.millisecondRecurringDelay))

//     // Update
//     digitalCurrencyTransaction = await initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm.function(digitalCurrencyTransaction)
//     const timeAfterStarting = (new Date()).getTime()

//     // Expect The Update To Update To The Latest Cycle
//     const minimumNumberOfTimesRecurred = processingWaitingPeriodCycleAmount - 1
//     const maximumNumberOfTimesRecurred = Math.floor((timeAfterStarting - timeBeforeStarting) / digitalCurrencyTransaction.millisecondRecurringDelay) + 1
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.greaterThanOrEqual(minimumNumberOfTimesRecurred)
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.lessThanOrEqual(maximumNumberOfTimesRecurred)

//     digitalCurrencyTransaction = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.greaterThanOrEqual(minimumNumberOfTimesRecurred)
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.lessThanOrEqual(maximumNumberOfTimesRecurred)
//   }).timeout(10 * 1000)

//   it('should update the transaction to the latest expected amount after a pause #2 - N Processing Before', async () => {
//     const numberOfRecurringTransactions = 1
//     const minimumMillisecondRecurringTransactionDelay = 100
//     const maximumMillisecondRecurringTransactionDelay = 200
//     const currentDate = new Date()

//     const initialProcessingNumberAmount = 1 + Math.floor(Math.random() * 5)
//     const processingWaitingPeriodCycleAmount = 1 + Math.floor(Math.random() * 5)

//     // Create Transactions
//     const timeBeforeStarting = (new Date()).getTime()
//     digitalCurrencyTransactionList = await createDigitalCurrencyTransactionList(digitalCurrencyAccountList, {
//       numberOfRecurringTransactions,
//       oldestTransactionDate: currentDate,
//       newestTransactionDate: currentDate,
//       minimumMillisecondRecurringTransactionDelay: minimumMillisecondRecurringTransactionDelay,
//       maximumMillisecondRecurringTransactionDelay: maximumMillisecondRecurringTransactionDelay
//     })

//     let digitalCurrencyTransaction = digitalCurrencyTransactionList[0]

//     // Process Transaction N Times
//     for (let i = 0; i < initialProcessingNumberAmount; i++) {
//       await initializeDigitalCurrencyTransactionAlgorithm.function(digitalCurrencyTransaction)
//       if (i + 1 >= initialProcessingNumberAmount) {
//         continue
//       }
//       await new Promise((resolve) => setTimeout(resolve, digitalCurrencyTransaction.millisecondRecurringDelay))
//     }

//     // Don't Process Transaction For M Cycles
//     await new Promise((resolve) => setTimeout(resolve, processingWaitingPeriodCycleAmount * digitalCurrencyTransaction.millisecondRecurringDelay))

//     // Update
//     digitalCurrencyTransaction = await initializeDigitalCurrencyRecurringTransactionUpdateAlgorithm.function(digitalCurrencyTransaction)
//     const timeAfterStarting = (new Date()).getTime()

//     // Expect The Update To Update To The Latest Cycle
//     const minimumNumberOfTimesRecurred = initialProcessingNumberAmount + processingWaitingPeriodCycleAmount - 1
//     const maximumNumberOfTimesRecurred = Math.floor((timeAfterStarting - timeBeforeStarting) / digitalCurrencyTransaction.millisecondRecurringDelay) + 1
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.greaterThanOrEqual(minimumNumberOfTimesRecurred)
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.lessThanOrEqual(maximumNumberOfTimesRecurred)

//     digitalCurrencyTransaction = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(digitalCurrencyTransaction.transactionId)
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.greaterThanOrEqual(minimumNumberOfTimesRecurred)
//     expect(digitalCurrencyTransaction.numberOfTimesRecurred).to.be.lessThanOrEqual(maximumNumberOfTimesRecurred)
//   })
// })

