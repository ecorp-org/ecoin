
import { addItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-10-add-item-event-listener/add-item-event-listener-1'

import {
  EVENT_ID_UPDATE,
  EVENT_ID_CREATE
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

import {
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { GetAccountTransactionStatisticsOptions } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-3-transaction-account-statistics/transaction-account-statistics-2'

import {
  // Values
  AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE,
  AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_VALUE,
  TOTAL_DIGITAL_CURRENCY_BALANCE_VALUE,

  NUMBER_OF_TRANSACTIONS_CREATED_VALUE,
  NUMBER_OF_TRANSACTIONS_RECEIVED_VALUE,
  NUMBER_OF_TRANSACTIONS_DELETED_VALUE,
  TOTAL_ACTIVE_TRANSACTIONS_VALUE,

  NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_VALUE,
  NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_VALUE,
  NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_VALUE,
  TOTAL_ACTIVE_RECURRING_TRANSACTIONS_VALUE,

  TOTAL_RECURRING_TRANSACTION_INCOME_VALUE,
  TOTAL_RECURRING_TRANSACTION_EXPENDITURE_VALUE,

  NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_VALUE,
  NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_VALUE,

  // Data Tables

  CREATED_TRANSACTION_DATA_TABLE,
  RECEIVED_TRANSACTION_DATA_TABLE,

  // Bar Graphs

  AMOUNT_OF_DIGITAL_CURRENCY_SENT_GRAPH,
  AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_GRAPH,
  TOTAL_DIGITAL_CURRENCY_BALANCE_GRAPH,

  NUMBER_OF_TRANSACTIONS_CREATED_GRAPH,
  NUMBER_OF_TRANSACTIONS_RECEIVED_GRAPH,
  NUMBER_OF_TRANSACTIONS_DELETED_GRAPH,
  TOTAL_ACTIVE_TRANSACTIONS_GRAPH,

  NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_GRAPH,
  NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_GRAPH,
  NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_GRAPH,
  TOTAL_ACTIVE_RECURRING_TRANSACTIONS_GRAPH,

  TOTAL_RECURRING_TRANSACTION_INCOME_GRAPH,
  TOTAL_RECURRING_TRANSACTION_EXPENDITURE_GRAPH,

  NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_GRAPH,
  NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_GRAPH
} from '../../digital-currency-transaction-type-3-constants/constants-3-transaction-account-statistics'

// import { getMillisecondTimePartitionList1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

// import { getUpdatedStartAndEndDate } from '../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-16-get-digital-currency-account-network-statistics/get-digital-currency-account-network-statistics-1'


async function addEventListenerToDigitalCurrencyAccountTransactionStatistics1 (statisticsType: string, eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  switch (statisticsType) {
    case AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE:
      return addEventListenerToAmountOfDigitalCurrencySentValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_VALUE:
      return addEventListenerToAmountOfDigitalCurrencyReceivedValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_DIGITAL_CURRENCY_BALANCE_VALUE:
      return addEventListenerToTotalDigitalCurrencyBalanceValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_CREATED_VALUE:
      return addEventListenerToNumberOfTransactionsCreatedValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_RECEIVED_VALUE:
      return addEventListenerToNumberOfTransactionsReceivedValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_DELETED_VALUE:
      return addEventListenerToNumberOfTransactionsDeletedValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_ACTIVE_TRANSACTIONS_VALUE:
      return addEventListenerToTotalActiveTransactionsValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_VALUE:
      return addEventListenerToNumberOfRecurringTransactionsCreatedValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_VALUE:
      return addEventListenerToNumberOfRecurringTransactionsReceivedValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_VALUE:
      return addEventListenerToNumberOfRecurringTransactionsDeletedValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_ACTIVE_RECURRING_TRANSACTIONS_VALUE:
      return addEventListenerToTotalActiveRecurringTransactionsValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_INCOME_VALUE:
      return addEventListenerToTotalRecurringTransactionIncomeValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_EXPENDITURE_VALUE:
      return addEventListenerToTotalRecurringTransactionExpenditureValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_VALUE:
      return addEventListenerToNumberOfAccountsSentATransactionToValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_VALUE:
      return addEventListenerToNumberOfAccountsReceivedATransactionFromValue(eventListenerCallbackFunction, statisticsOptions)
      break
    case CREATED_TRANSACTION_DATA_TABLE:
      return addEventListenerToCreatedTransactionDataTable(eventListenerCallbackFunction, statisticsOptions)
      break
    case RECEIVED_TRANSACTION_DATA_TABLE:
      return addEventListenerToReceivedTransactionDataTable(eventListenerCallbackFunction, statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_SENT_GRAPH:
      return addEventListenerToAmountOfDigitalCurrencySentGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_GRAPH:
      return addEventListenerToAmountOfDigitalCurrencyReceivedGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_DIGITAL_CURRENCY_BALANCE_GRAPH:
      return addEventListenerToTotalDigitalCurrencyBalanceGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_CREATED_GRAPH:
      return addEventListenerToNumberOfTransactionsCreatedGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_RECEIVED_GRAPH:
      return addEventListenerToNumberOfTransactionsReceivedGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_DELETED_GRAPH:
      return addEventListenerToNumberOfTransactionsDeletedGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_ACTIVE_TRANSACTIONS_GRAPH:
      return addEventListenerToTotalActiveTransactionsGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_GRAPH:
      return addEventListenerToNumberOfRecurringTransactionsCreatedGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_GRAPH:
      return addEventListenerToNumberOfRecurringTransactionsReceivedGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_GRAPH:
      return addEventListenerToNumberOfRecurringTransactionsDeletedGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_ACTIVE_RECURRING_TRANSACTIONS_GRAPH:
      return addEventListenerToTotalActiveRecurringTransactionsGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_INCOME_GRAPH:
      return addEventListenerToTotalRecurringTransactionIncomeGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_EXPENDITURE_GRAPH:
      return addEventListenerToTotalRecurringTransactionExpenditureGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_GRAPH:
      return addEventListenerToNumberOfAccountsSentATransactionToGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_GRAPH:
      return addEventListenerToNumberOfAccountsReceivedATransactionFromGraph(eventListenerCallbackFunction, statisticsOptions)
      break
    default:
      throw Error(`No Account Transaction Statistics of type: ${statisticsType}`)
  }
}


// Values

async function addEventListenerToAmountOfDigitalCurrencySentValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'amountOfDigitalCurrencySent', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToAmountOfDigitalCurrencyReceivedValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'amountOfDigitalCurrencyReceived', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToTotalDigitalCurrencyBalanceValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalDigitalCurrencyBalance', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfTransactionsCreatedValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionsCreated', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfTransactionsReceivedValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionsReceived', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfTransactionsDeletedValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionsDeleted', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToTotalActiveTransactionsValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfActiveTransactions', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfRecurringTransactionsCreatedValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsCreated', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfRecurringTransactionsReceivedValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsReceived', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfRecurringTransactionsDeletedValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsDeleted', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToTotalActiveRecurringTransactionsValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfActiveRecurringTransactions', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}


async function addEventListenerToTotalRecurringTransactionIncomeValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalRecurringTransactionIncome', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToTotalRecurringTransactionExpenditureValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalRecurringTransactionExpenditure', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfAccountsSentATransactionToValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfAccountsSentATransactionTo', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfAccountsReceivedATransactionFromValue (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfAccountsReceivedATransactionFrom', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}


// Data Tables

async function addEventListenerToCreatedTransactionDataTable (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/createdTransactionVariableTable`, 'transactionIdVariableTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const transactionId = options.key
    const dateCreated = parseInt(data, 10)
    eventListenerCallbackFunction({
      dateList: [dateCreated],
      dataList: [transactionId]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToReceivedTransactionDataTable (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/receivedTransactionVariableTable`, 'transactionIdVariableTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const transactionId = options.key
    const dateCreated = parseInt(data, 10)
    eventListenerCallbackFunction({
      dateList: [dateCreated],
      dataList: [transactionId]
    })
  })
  return itemEventListenerId
}

// Graph

async function addEventListenerToAmountOfDigitalCurrencySentGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/amountOfDigitalCurrencySentVariableTable/timeTable`, 'digitalCurrencyAmountTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const digitalCurrencyAmount = data.digitalCurrencyAmount
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [digitalCurrencyAmount]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToAmountOfDigitalCurrencyReceivedGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/amountOfDigitalCurrencyReceivedVariableTable/timeTable`, 'digitalCurrencyAmountTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const digitalCurrencyAmount = data.digitalCurrencyAmount
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [digitalCurrencyAmount]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToTotalDigitalCurrencyBalanceGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/totalDigitalCurrencyBalanceVariableTable/timeTable`, 'digitalCurrencyAmountTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const digitalCurrencyAmount = data.digitalCurrencyAmount
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [digitalCurrencyAmount]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfTransactionsCreatedGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/createdTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfTransactionsReceivedGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/receivedTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfTransactionsDeletedGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/deletedTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToTotalActiveTransactionsGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/activeTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfRecurringTransactionsCreatedGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/createdRecurringTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfRecurringTransactionsReceivedGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/receivedRecurringTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfRecurringTransactionsDeletedGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/deletedRecurringTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToTotalActiveRecurringTransactionsGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/activeRecurringTransactionVariableTable/timeTable`, 'numberOfProcessedTransactionsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedTransactions = data.numberOfProcessedTransactions
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedTransactions]
    })
  })
  return itemEventListenerId
}


async function addEventListenerToTotalRecurringTransactionIncomeGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/totalRecurringTransactionIncomeVariableTable/timeTable`, 'digitalCurrencyAmountTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const digitalCurrencyAmount = data.digitalCurrencyAmount
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [digitalCurrencyAmount]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToTotalRecurringTransactionExpenditureGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/totalRecurringTransactionExpenditureVariableTable/timeTable`, 'digitalCurrencyAmountTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const digitalCurrencyAmount = data.digitalCurrencyAmount
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [digitalCurrencyAmount]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfAccountsSentATransactionToGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/accountsSentATransactionToVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedAccounts = data.numberOfProcessedAccounts
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedAccounts]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfAccountsReceivedATransactionFromGraph (eventListenerCallbackFunction: any, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/accountsReceivedATransactionFromVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedAccounts = data.numberOfProcessedAccounts
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedAccounts]
    })
  })
  return itemEventListenerId
}





export {
  addEventListenerToDigitalCurrencyAccountTransactionStatistics1
}


