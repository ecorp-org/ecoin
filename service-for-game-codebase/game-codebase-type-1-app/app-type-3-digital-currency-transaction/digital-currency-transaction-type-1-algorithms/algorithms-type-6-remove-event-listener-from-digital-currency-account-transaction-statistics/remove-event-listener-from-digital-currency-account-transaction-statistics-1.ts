
import { removeItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-11-remove-item-event-listener/remove-item-event-listener-1'

import { EVENT_ID_UPDATE, EVENT_ID_CREATE } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

import {
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { GetAccountTransactionStatisticsOptions } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-3-transaction-account-statistics/transaction-account-statistics-2'

import {
  // Values
  AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE,
  AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_VALUE,
  TOTAL_DIGITAL_CURRENCY_BALANCE_VALUE,

  NUMBER_OF_TRANSACTIONS_CREATED_VALUE,
  NUMBER_OF_TRANSACTIONS_RECEIVED_VALUE,
  NUMBER_OF_TRANSACTIONS_DELETED_VALUE,
  TOTAL_ACTIVE_TRANSACTIONS_VALUE,

  NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_VALUE,
  NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_VALUE,
  NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_VALUE,
  TOTAL_ACTIVE_RECURRING_TRANSACTIONS_VALUE,

  TOTAL_RECURRING_TRANSACTION_INCOME_VALUE,
  TOTAL_RECURRING_TRANSACTION_EXPENDITURE_VALUE,

  NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_VALUE,
  NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_VALUE,

  // Data Tables

  CREATED_TRANSACTION_DATA_TABLE,
  RECEIVED_TRANSACTION_DATA_TABLE,

  // Bar Graphs

  AMOUNT_OF_DIGITAL_CURRENCY_SENT_GRAPH,
  AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_GRAPH,
  TOTAL_DIGITAL_CURRENCY_BALANCE_GRAPH,

  NUMBER_OF_TRANSACTIONS_CREATED_GRAPH,
  NUMBER_OF_TRANSACTIONS_DELETED_GRAPH,
  TOTAL_ACTIVE_TRANSACTIONS_GRAPH,

  NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_GRAPH,
  NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_GRAPH,
  TOTAL_ACTIVE_RECURRING_TRANSACTIONS_GRAPH,

  TOTAL_RECURRING_TRANSACTION_INCOME_GRAPH,
  TOTAL_RECURRING_TRANSACTION_EXPENDITURE_GRAPH,

  NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_GRAPH,
  NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_GRAPH
} from '../../digital-currency-transaction-type-3-constants/constants-3-transaction-account-statistics'

async function removeEventListenerFromDigitalCurrencyAccountTransactionStatistics1 (statisticsType: string, itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  switch (statisticsType) {
    case AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE:
      return removeEventListenerFromAmountOfDigitalCurrencySentValue(itemEventListenerId, statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_VALUE:
      return removeEventListenerFromAmountOfDigitalCurrencyReceivedValue(itemEventListenerId, statisticsOptions)
      break
    case TOTAL_DIGITAL_CURRENCY_BALANCE_VALUE:
      return removeEventListenerFromTotalDigitalCurrencyBalanceValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_CREATED_VALUE:
      return removeEventListenerFromNumberOfTransactionsCreatedValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_RECEIVED_VALUE:
      return removeEventListenerFromNumberOfTransactionsReceivedValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_DELETED_VALUE:
      return removeEventListenerFromNumberOfTransactionsDeletedValue(itemEventListenerId, statisticsOptions)
      break
    case TOTAL_ACTIVE_TRANSACTIONS_VALUE:
      return removeEventListenerFromTotalActiveTransactionsValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_VALUE:
      return removeEventListenerFromNumberOfRecurringTransactionsCreatedValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_VALUE:
      return removeEventListenerFromNumberOfRecurringTransactionsReceivedValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_VALUE:
      return removeEventListenerFromNumberOfRecurringTransactionsDeletedValue(itemEventListenerId, statisticsOptions)
      break
    case TOTAL_ACTIVE_RECURRING_TRANSACTIONS_VALUE:
      return removeEventListenerFromTotalActiveRecurringTransactionsValue(itemEventListenerId, statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_INCOME_VALUE:
      return removeEventListenerFromTotalRecurringTransactionIncomeValue(itemEventListenerId, statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_EXPENDITURE_VALUE:
      return removeEventListenerFromTotalRecurringTransactionExpenditureValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_VALUE:
      return removeEventListenerFromNumberOfAccountsSentATransactionToValue(itemEventListenerId, statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_VALUE:
      return removeEventListenerFromNumberOfAccountsReceivedATransactionFromValue(itemEventListenerId, statisticsOptions)
      break
    case CREATED_TRANSACTION_DATA_TABLE:
      return removeEventListenerFromCreatedTransactionDataTable(itemEventListenerId, statisticsOptions)
      break
    case RECEIVED_TRANSACTION_DATA_TABLE:
      return removeEventListenerFromReceivedTransactionDataTable(itemEventListenerId, statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_SENT_GRAPH:
      return
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_GRAPH:
      return
      break
    case TOTAL_DIGITAL_CURRENCY_BALANCE_GRAPH:
      return
      break
    case NUMBER_OF_TRANSACTIONS_CREATED_GRAPH:
      return
      break
    case NUMBER_OF_TRANSACTIONS_DELETED_GRAPH:
      return
      break
    case TOTAL_ACTIVE_TRANSACTIONS_GRAPH:
      return
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_GRAPH:
      return
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_GRAPH:
      return
      break
    case TOTAL_ACTIVE_RECURRING_TRANSACTIONS_GRAPH:
      return
      break
    case TOTAL_RECURRING_TRANSACTION_INCOME_GRAPH:
      return
      break
    case TOTAL_RECURRING_TRANSACTION_EXPENDITURE_GRAPH:
      return
      break
    case NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_GRAPH:
      return
      break
    case NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_GRAPH:
      return
      break
    default:
      throw Error(`No Account Transaction Statistics of type: ${statisticsType}`)
  }
}

// Values

async function removeEventListenerFromAmountOfDigitalCurrencySentValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'amountOfDigitalCurrencySent', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromAmountOfDigitalCurrencyReceivedValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'amountOfDigitalCurrencyReceived', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromTotalDigitalCurrencyBalanceValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalDigitalCurrencyBalance', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfTransactionsCreatedValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionCreated', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfTransactionsReceivedValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionReceived', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfTransactionsDeletedValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionDeleted', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromTotalActiveTransactionsValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalActiveTransactions', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfRecurringTransactionsCreatedValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsCreated', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfRecurringTransactionsReceivedValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsReceived', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfRecurringTransactionsDeletedValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsDeleted', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromTotalActiveRecurringTransactionsValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalActiveRecurringTransactions', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromTotalRecurringTransactionIncomeValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalRecurringTransactionIncome', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromTotalRecurringTransactionExpenditureValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalRecurringTransactionExpenditure', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfAccountsSentATransactionToValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfAccountsSentATransactionTo', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfAccountsReceivedATransactionFromValue (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfAccountsReceivedATransactionFrom', EVENT_ID_UPDATE, itemEventListenerId)
}

// Data Tables

async function removeEventListenerFromCreatedTransactionDataTable (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/createdTransactionVariableTable`, 'transactionIdVariableTable', EVENT_ID_CREATE, itemEventListenerId)
}

async function removeEventListenerFromReceivedTransactionDataTable (itemEventListenerId: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/receivedTransactionVariableTable`, 'transactionIdVariableTable', EVENT_ID_CREATE, itemEventListenerId)
}

// Bar Graphs





export {
  removeEventListenerFromDigitalCurrencyAccountTransactionStatistics1
}


