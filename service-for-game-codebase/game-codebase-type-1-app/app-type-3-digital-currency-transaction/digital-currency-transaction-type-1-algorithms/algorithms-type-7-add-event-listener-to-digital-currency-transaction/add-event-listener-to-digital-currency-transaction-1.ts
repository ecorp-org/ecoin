

import { addItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-10-add-item-event-listener/add-item-event-listener-1'

import {
  EVENT_ID_UPDATE
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

import {
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function addEventListenerToDigitalCurrencyTransaction1 (digitalCurrencyTransactionId: string, eventListenerCallbackFunction?: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransactionId, EVENT_ID_UPDATE, (data: any) => {
    if (eventListenerCallbackFunction) {
      eventListenerCallbackFunction(data)
    }
  })
  return itemEventListenerId
}


export {
  addEventListenerToDigitalCurrencyTransaction1
}


