


import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
// import { deleteItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-1'
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'
import { getItemIdList1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-12-get-item-id-list/get-item-id-list-1'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { getTransactionProcessingInformationTableAlgorithm } from '../algorithms-type-9-initialize-digital-currency-transaction-list/service-for-helper-algorithms/helper-algorithms-1-get-transaction-processing-information-table'

async function deleteDigitalCurrencyTransaction1 (digitalCurrencyTransactionId: string) {

  // check if digital currency transaction is there
  const digitalCurrencyTransaction = await getItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId) as DigitalCurrencyTransaction

  if (!digitalCurrencyTransaction) {
    return
  }

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const currentDateTimePartition = getMillisecondTimePartitionList2(currentDate)
  const { dateTransactionScheduledToBeProcessedTimePartition, dateTransactionRecurredTimePartition } = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)

  let millisecondDay = 0
  let millisecondHour = 0
  let millisecondMinute = 0
  let millisecondSecond = 0

  if (digitalCurrencyTransaction.isRecurringTransaction) {
    millisecondDay = dateTransactionRecurredTimePartition.millisecondDay
    millisecondHour = dateTransactionRecurredTimePartition.millisecondHour
    millisecondMinute = dateTransactionRecurredTimePartition.millisecondMinute
    millisecondSecond = dateTransactionRecurredTimePartition.millisecondSecond
  } else {
    millisecondDay = dateTransactionScheduledToBeProcessedTimePartition.millisecondDay
    millisecondHour = dateTransactionScheduledToBeProcessedTimePartition.millisecondHour
    millisecondMinute = dateTransactionScheduledToBeProcessedTimePartition.millisecondMinute
    millisecondSecond = dateTransactionScheduledToBeProcessedTimePartition.millisecondSecond
  }

  await updateItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId, {
    'dateRecurrenceCancelled': currentDate,
    'dateTransactionScheduledForCancellingRecurrence': currentDate
  })

  // const alternateDateTransactionRecurredTimePartition = getMillisecondTimePartitionList1((new Date(dateTransactionRecurred.getTime() + digitalCurrencyTransaction.millisecondRecurringDelay)).getTime())
  // await deleteItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
  // await deleteItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}/${millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)
  // await deleteItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/recurringTransactionVariableTable/timeTable/transactionIdTimeTable/${alternateDateTransactionRecurredTimePartition.millisecondDay}/${alternateDateTransactionRecurredTimePartition.millisecondHour}/${alternateDateTransactionRecurredTimePartition.millisecondMinute}/${alternateDateTransactionRecurredTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId)

  // Transaction Network Statistics - Deleted Transactions
  await createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfTransactionsDeleted/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: currentDate }, false, true)
  await createItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedTransactionVariableTable/timeTable/transactionIdTimeTable/${currentDateTimePartition.millisecondDay}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, currentDate)
  await createItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, currentDate)

  // Transaction Network Statistics - Deleted Recurring Transactions
  if (digitalCurrencyTransaction.isRecurringTransaction) {
    await createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfRecurringTransactionsDeleted/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: currentDate }, false, true)
    await updateItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedRecurringTransactionVariableTable/timeTable/transactionIdTimeTable/${currentDateTimePartition.millisecondDay}`, 'transactionIdVariableTable', { [digitalCurrencyTransaction.transactionId]: currentDate })
    // await createItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedRecurringTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, currentDate)
  }

  // Publish Account Transaction Statistics

  // Account Statistics - Transactions Deleted
  await createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyTransaction.accountSenderId}/numberOfTransactionsDeleted/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: currentDate }, false, true)
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/deletedTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, currentDate)
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/deletedTransactionVariableTable/timeTable/transactionIdTimeTable/${currentDateTimePartition.millisecondDay}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, currentDate)

  // Account Statistics - Recurring Transactions Deleted
  if (digitalCurrencyTransaction.isRecurringTransaction) {
    await createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyTransaction.accountSenderId}/numberOfRecurringTransactionsDeleted/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: currentDate }, false, true)
    await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/deletedRecurringTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, currentDate)
    await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/deletedRecurringTransactionVariableTable/timeTable/transactionIdTimeTable/${currentDateTimePartition.millisecondDay}`, 'transactionIdVariableTable', { [digitalCurrencyTransaction.transactionId]: currentDate })
  }

  return digitalCurrencyTransaction.transactionId
}

async function deleteDigitalCurrencyTransactionList1 (digitalCurrencyAccountId: string) {
  if (!digitalCurrencyAccountId) {
    return
  }
  const transactionIdList: string[] = await getItemIdList1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/createdTransactionVariableTable/transactionIdVariableTable`)
  for (let i = 0; i < transactionIdList.length; i++) {
    const transactionId = transactionIdList[i]
    await deleteDigitalCurrencyTransaction1(transactionId)
  }
}

export {
  deleteDigitalCurrencyTransaction1,
  deleteDigitalCurrencyTransactionList1
}




