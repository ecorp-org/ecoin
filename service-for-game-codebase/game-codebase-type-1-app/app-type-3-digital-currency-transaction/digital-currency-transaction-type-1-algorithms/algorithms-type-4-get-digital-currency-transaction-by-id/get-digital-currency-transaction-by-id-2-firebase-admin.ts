

import { getItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'

import {
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  DigitalCurrencyTransaction
} from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'


async function getDigitalCurrencyTransactionById2FirebaseAdmin (digitalCurrencyTransactionId: string): Promise<DigitalCurrencyTransaction | null> {

  const isQueuedToBeDeleted = (await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/deletedTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransactionId)) ? true : false
  if (isQueuedToBeDeleted) {
    return null
  }

  let digitalCurrencyTransaction = await getItem2FirebaseAdmin(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransactionId)

  return digitalCurrencyTransaction
}

const getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm = {
  function: getDigitalCurrencyTransactionById2FirebaseAdmin
}

export {
  getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm
}

