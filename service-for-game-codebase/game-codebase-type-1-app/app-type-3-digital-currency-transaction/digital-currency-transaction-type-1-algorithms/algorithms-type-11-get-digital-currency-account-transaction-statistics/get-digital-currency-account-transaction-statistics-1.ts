
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
// import { getItemIdList1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-12-get-item-id-list/get-item-id-list-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { GetAccountTransactionStatisticsOptions } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-3-transaction-account-statistics/transaction-account-statistics-2'

import {
  // Values
  AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE,
  AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_VALUE,
  TOTAL_DIGITAL_CURRENCY_BALANCE_VALUE,

  NUMBER_OF_TRANSACTIONS_CREATED_VALUE,
  NUMBER_OF_TRANSACTIONS_RECEIVED_VALUE,
  NUMBER_OF_TRANSACTIONS_DELETED_VALUE,
  TOTAL_ACTIVE_TRANSACTIONS_VALUE,

  NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_VALUE,
  NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_VALUE,
  NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_VALUE,
  TOTAL_ACTIVE_RECURRING_TRANSACTIONS_VALUE,

  TOTAL_RECURRING_TRANSACTION_INCOME_VALUE,
  TOTAL_RECURRING_TRANSACTION_EXPENDITURE_VALUE,

  NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_VALUE,
  NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_VALUE,

  // Data Tables

  CREATED_TRANSACTION_DATA_TABLE,
  RECEIVED_TRANSACTION_DATA_TABLE,

  // Bar Graphs

  AMOUNT_OF_DIGITAL_CURRENCY_SENT_GRAPH,
  AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_GRAPH,
  TOTAL_DIGITAL_CURRENCY_BALANCE_GRAPH,

  NUMBER_OF_TRANSACTIONS_CREATED_GRAPH,
  NUMBER_OF_TRANSACTIONS_RECEIVED_GRAPH,
  NUMBER_OF_TRANSACTIONS_DELETED_GRAPH,
  TOTAL_ACTIVE_TRANSACTIONS_GRAPH,

  NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_GRAPH,
  NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_GRAPH,
  NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_GRAPH,
  TOTAL_ACTIVE_RECURRING_TRANSACTIONS_GRAPH,

  TOTAL_RECURRING_TRANSACTION_INCOME_GRAPH,
  TOTAL_RECURRING_TRANSACTION_EXPENDITURE_GRAPH,

  NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_GRAPH,
  NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_GRAPH
} from '../../digital-currency-transaction-type-3-constants/constants-3-transaction-account-statistics'

import { getUpdatedStartAndEndDate } from '../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-16-get-digital-currency-account-network-statistics/get-digital-currency-account-network-statistics-1'

import {
  // MAXIMUM_NUMBER_OF_MILLISECONDS_AGO,
  NUMBER_OF_MILLISECONDS_IN_A_DAY
} from '../../../app-type-1-general/general-type-3-constants/constants-3-time'

import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


async function getDigitalCurrencyAccountTransactionStatistics1 (statisticsType: string, statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  switch (statisticsType) {
    case AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE:
      return getAmountOfDigitalCurrencySentValue(statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_VALUE:
      return getAmountOfDigitalCurrencyReceivedValue(statisticsOptions)
      break
    case TOTAL_DIGITAL_CURRENCY_BALANCE_VALUE:
      return getTotalDigitalCurrencyBalanceValue(statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_CREATED_VALUE:
      return getNumberOfTransactionsCreatedValue(statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_RECEIVED_VALUE:
      return getNumberOfTransactionsReceivedValue(statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_DELETED_VALUE:
      return getNumberOfTransactionsDeletedValue(statisticsOptions)
      break
    case TOTAL_ACTIVE_TRANSACTIONS_VALUE:
      return getTotalActiveTransactionsValue(statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_VALUE:
      return getNumberOfRecurringTransactionsCreatedValue(statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_VALUE:
      return getNumberOfRecurringTransactionsReceivedValue(statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_VALUE:
      return getNumberOfRecurringTransactionsDeletedValue(statisticsOptions)
      break
    case TOTAL_ACTIVE_RECURRING_TRANSACTIONS_VALUE:
      return getTotalActiveRecurringTransactionsValue(statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_INCOME_VALUE:
      return getTotalRecurringTransactionIncomeValue(statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_EXPENDITURE_VALUE:
      return getTotalRecurringTransactionExpenditureValue(statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_VALUE:
      return getNumberOfAccountsSentATransactionToValue(statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_VALUE:
      return getNumberOfAccountsReceivedATransactionFromValue(statisticsOptions)
      break
    case CREATED_TRANSACTION_DATA_TABLE:
      return getCreatedTransactionDataTable(statisticsOptions)
      break
    case RECEIVED_TRANSACTION_DATA_TABLE:
      return getReceivedTransactionDataTable(statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_SENT_GRAPH:
      return getAmountOfDigitalCurrencySentGraph(statisticsOptions)
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_RECEIVED_GRAPH:
      return getAmountOfDigitalCurrencyReceivedGraph(statisticsOptions)
      break
    case TOTAL_DIGITAL_CURRENCY_BALANCE_GRAPH:
      return getTotalDigitalCurrencyBalanceGraph(statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_CREATED_GRAPH:
      return getNumberOfTransactionsCreatedGraph(statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_RECEIVED_GRAPH:
      return getNumberOfTransactionsReceivedGraph(statisticsOptions)
      break
    case NUMBER_OF_TRANSACTIONS_DELETED_GRAPH:
      return getNumberOfTransactionsDeletedGraph(statisticsOptions)
      break
    case TOTAL_ACTIVE_TRANSACTIONS_GRAPH:
      return getTotalActiveTransactionsGraph(statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_CREATED_GRAPH:
      return getNumberOfRecurringTransactionsCreatedGraph(statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_RECEIVED_GRAPH:
      return getNumberOfRecurringTransactionsReceivedGraph(statisticsOptions)
      break
    case NUMBER_OF_RECURRING_TRANSACTIONS_DELETED_GRAPH:
      return getNumberOfRecurringTransactionsDeletedGraph(statisticsOptions)
      break
    case TOTAL_ACTIVE_RECURRING_TRANSACTIONS_GRAPH:
      return getTotalActiveRecurringTransactionsGraph(statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_INCOME_GRAPH:
      return getTotalRecurringTransactionIncomeGraph(statisticsOptions)
      break
    case TOTAL_RECURRING_TRANSACTION_EXPENDITURE_GRAPH:
      return getTotalRecurringTransactionExpenditureGraph(statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_SENT_A_TRANSACTION_TO_GRAPH:
      return getNumberOfAccountsSentATransactionToGraph(statisticsOptions)
      break
    case NUMBER_OF_ACCOUNTS_RECEIVED_A_TRANSACTION_FROM_GRAPH:
      return getNumberOfAccountsReceivedATransactionFromGraph(statisticsOptions)
      break
    default:
      throw Error(`No Account Transaction Statistics of type: ${statisticsType}`)
  }
}

// Values

async function getAmountOfDigitalCurrencySentValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const amountOfDigitalCurrencySent = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'amountOfDigitalCurrencySent')
  return amountOfDigitalCurrencySent
}

async function getAmountOfDigitalCurrencyReceivedValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const amountOfDigitalCurrencyReceived = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'amountOfDigitalCurrencyReceived')
  return amountOfDigitalCurrencyReceived
}

async function getTotalDigitalCurrencyBalanceValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const totalDigitalCurrencyBalance = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalDigitalCurrencyBalance')
  return totalDigitalCurrencyBalance
}

async function getNumberOfTransactionsCreatedValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfTransactionsCreated = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionsCreated')
  return numberOfTransactionsCreated
}

async function getNumberOfTransactionsReceivedValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfTransactionsReceived = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionsReceived')
  return numberOfTransactionsReceived
}

async function getNumberOfTransactionsDeletedValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfTransactionsDeleted = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfTransactionsDeleted')
  return numberOfTransactionsDeleted
}

async function getTotalActiveTransactionsValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const totalActiveTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfActiveTransactions')
  return totalActiveTransactions
}

async function getNumberOfRecurringTransactionsCreatedValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfRecurringTransactionsCreated = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsCreated')
  return numberOfRecurringTransactionsCreated
}

async function getNumberOfRecurringTransactionsReceivedValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfRecurringTransactionsReceived = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsReceived')
  return numberOfRecurringTransactionsReceived
}

async function getNumberOfRecurringTransactionsDeletedValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfRecurringTransactionsDeleted = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfRecurringTransactionsDeleted')
  return numberOfRecurringTransactionsDeleted
}

async function getTotalActiveRecurringTransactionsValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const totalActiveRecurringTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfActiveRecurringTransactions')
  return totalActiveRecurringTransactions
}

async function getTotalRecurringTransactionIncomeValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const totalRecurringTransactionIncome = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalRecurringTransactionIncome')
  return totalRecurringTransactionIncome
}

async function getTotalRecurringTransactionExpenditureValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const totalRecurringTransactionExpenditure = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'totalRecurringTransactionExpenditure')
  return totalRecurringTransactionExpenditure
}

async function getNumberOfAccountsSentATransactionToValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfAccountsSentATransactionTo = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfAccountsSentATransactionTo')
  return numberOfAccountsSentATransactionTo
}

async function getNumberOfAccountsReceivedATransactionFromValue (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  const numberOfAccountsReceivedATransactionFrom = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}`, 'numberOfAccountsReceivedATransactionFrom')
  return numberOfAccountsReceivedATransactionFrom
}

// Data Tables

async function getCreatedTransactionDataTable (statisticsOptions?: GetAccountTransactionStatisticsOptions) {

  let dateList: number[] = []
  let dataList: string[] = []
  let itemDataList: any[] = []

  if (!statisticsOptions || !statisticsOptions.digitalCurrencyAccountId) {
    return {
      dateList,
      dataList,
      itemDataList
    }
  }

  const transactionIdVariableTable = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions.digitalCurrencyAccountId}/createdTransactionVariableTable`, 'transactionIdVariableTable', statisticsOptions.getItem1Options) || {}

  for (let transactionId in transactionIdVariableTable) {
    if (!transactionIdVariableTable.hasOwnProperty(transactionId)) {
      continue
    }
    const dateTransactionCreated = transactionIdVariableTable[transactionId]
    dateList.push(getMillisecondTimePartitionList2(dateTransactionCreated).millisecondDay)
    dataList.push(transactionId)
    itemDataList.push({
      dateCreated: dateTransactionCreated
    })
  }

  return {
    dateList,
    dataList,
    itemDataList
  }
}

async function getReceivedTransactionDataTable (statisticsOptions?: GetAccountTransactionStatisticsOptions) {

  let dateList: number[] = []
  let dataList: string[] = []
  let itemDataList: any[] = []

  if (!statisticsOptions || !statisticsOptions.digitalCurrencyAccountId) {
    return {
      dateList,
      dataList,
      itemDataList
    }
  }

  const transactionIdVariableTable = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions.digitalCurrencyAccountId}/receivedTransactionVariableTable`, 'transactionIdVariableTable', statisticsOptions.getItem1Options) || {}

  for (let transactionId in transactionIdVariableTable) {
    if (!transactionIdVariableTable.hasOwnProperty(transactionId)) {
      continue
    }
    const dateTransactionCreated = transactionIdVariableTable[transactionId]
    dateList.push(getMillisecondTimePartitionList2(dateTransactionCreated).millisecondDay)
    dataList.push(transactionId)
    itemDataList.push({
      dateCreated: dateTransactionCreated
    })
  }

  return {
    dateList,
    dataList,
    itemDataList
  }
}

// Graphs

async function getAmountOfDigitalCurrencySentGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const digitalCurrencyAmount = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/amountOfDigitalCurrencySentVariableTable/timeTable/digitalCurrencyAmountTimeTable/${dateNumber}`, 'digitalCurrencyAmount') || 0
    dataList.push(digitalCurrencyAmount)
  }

  return {
    dateList,
    dataList
  }
}

async function getAmountOfDigitalCurrencyReceivedGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const digitalCurrencyAmount = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/amountOfDigitalCurrencyReceivedVariableTable/timeTable/digitalCurrencyAmountTimeTable/${dateNumber}`, 'digitalCurrencyAmount') || 0
    dataList.push(digitalCurrencyAmount)
  }

  return {
    dateList,
    dataList
  }
}

async function getTotalDigitalCurrencyBalanceGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const digitalCurrencyAmount = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/totalDigitalCurrencyBalanceVariableTable/timeTable/digitalCurrencyAmountTimeTable/${dateNumber}`, 'digitalCurrencyAmount') || 0
    dataList.push(digitalCurrencyAmount)
  }

  return {
    dateList,
    dataList
  }
}


async function getNumberOfTransactionsCreatedGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfProcessedTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/createdTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || 0
    dataList.push(numberOfProcessedTransactions)
  }

  return {
    dateList,
    dataList
  }
}

async function getNumberOfTransactionsReceivedGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfProcessedTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/receivedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || 0
    dataList.push(numberOfProcessedTransactions)
  }

  return {
    dateList,
    dataList
  }
}

async function getNumberOfTransactionsDeletedGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfProcessedTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/deletedTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || 0
    dataList.push(numberOfProcessedTransactions)
  }

  return {
    dateList,
    dataList
  }
}

async function getTotalActiveTransactionsGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  let i = 0
  for (let dateNumber = endDateMillisecondPartitionList.millisecondDay; dateNumber <= startDateMillisecondPartitionList.millisecondDay; dateNumber += NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = getMillisecondTimePartitionList2((new Date(dateNumber)).toISOString().split('.')[0] + 'Z').millisecondDay
    dateList.push(millisecondDate)
    const numberOfPreviousProcessedTransactions: number = dataList.length > 0 ? dataList[i - 1] || 0 : 0
    const numberOfProcessedTransactions: number = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/activeTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || numberOfPreviousProcessedTransactions
    dataList.push(numberOfProcessedTransactions)
    i++
  }

  return {
    dateList: dateList.reverse(),
    dataList: dataList.reverse()
  }
}

async function getNumberOfRecurringTransactionsCreatedGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfProcessedTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/createdRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || 0
    dataList.push(numberOfProcessedTransactions)
  }

  return {
    dateList,
    dataList
  }
}

async function getNumberOfRecurringTransactionsReceivedGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfProcessedTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/receivedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || 0
    dataList.push(numberOfProcessedTransactions)
  }

  return {
    dateList,
    dataList
  }
}

async function getNumberOfRecurringTransactionsDeletedGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfProcessedTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/deletedRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || 0
    dataList.push(numberOfProcessedTransactions)
  }

  return {
    dateList,
    dataList
  }
}

async function getTotalActiveRecurringTransactionsGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  let i = 0
  for (let dateNumber = endDateMillisecondPartitionList.millisecondDay; dateNumber <= startDateMillisecondPartitionList.millisecondDay; dateNumber += NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = getMillisecondTimePartitionList2((new Date(dateNumber)).toISOString().split('.')[0] + 'Z').millisecondDay
    dateList.push(millisecondDate)
    const numberOfPreviousProcessedTransactions: number = dataList.length > 0 ? dataList[i - 1] || 0 : 0
    const numberOfProcessedTransactions: number = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/activeRecurringTransactionVariableTable/timeTable/numberOfProcessedTransactionsTimeTable/${dateNumber}`, 'numberOfProcessedTransactions') || numberOfPreviousProcessedTransactions
    dataList.push(numberOfProcessedTransactions)
    i++
  }

  return {
    dateList: dateList.reverse(),
    dataList: dataList.reverse()
  }
}

async function getTotalRecurringTransactionIncomeGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const digitalCurrencyAmount = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/totalRecurringTransactionIncomeVariableTable/timeTable/digitalCurrencyAmountTimeTable/${dateNumber}`, 'digitalCurrencyAmount') || 0
    dataList.push(digitalCurrencyAmount)
  }

  return {
    dateList,
    dataList
  }
}

async function getTotalRecurringTransactionExpenditureGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const digitalCurrencyAmount = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/totalRecurringTransactionExpenditureVariableTable/timeTable/digitalCurrencyAmountTimeTable/${dateNumber}`, 'digitalCurrencyAmount') || 0
    dataList.push(digitalCurrencyAmount)
  }

  return {
    dateList,
    dataList
  }
}

async function getNumberOfAccountsSentATransactionToGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const digitalCurrencyAmount = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/accountsSentATransactionToVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${dateNumber}`, 'numberOfProcessedAccounts') || 0
    dataList.push(digitalCurrencyAmount)
  }

  return {
    dateList,
    dataList
  }
}

async function getNumberOfAccountsReceivedATransactionFromGraph (statisticsOptions?: GetAccountTransactionStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const digitalCurrencyAmount = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${statisticsOptions!.digitalCurrencyAccountId!}/accountsReceivedATransactionFromVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${dateNumber}`, 'numberOfProcessedAccounts') || 0
    dataList.push(digitalCurrencyAmount)
  }

  return {
    dateList,
    dataList
  }
}



export {
  getDigitalCurrencyAccountTransactionStatistics1
}

