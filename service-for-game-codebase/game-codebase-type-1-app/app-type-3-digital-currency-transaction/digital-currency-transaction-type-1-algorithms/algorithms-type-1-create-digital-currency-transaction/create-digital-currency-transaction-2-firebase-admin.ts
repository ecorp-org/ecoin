
import { createItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-2-firebase-admin'

import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


import { createId1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-2-create-id/create-id-1'

async function createDigitalCurrencyTransaction2FirebaseAdmin (digitalCurrencyTransactionInformation: DigitalCurrencyTransaction) {

  let digitalCurrencyTransaction = new DigitalCurrencyTransaction(digitalCurrencyTransactionInformation)
  digitalCurrencyTransaction.transactionId = digitalCurrencyTransaction.transactionId || createId1()

  await createItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction, false, false, true)

  // Don't Do Any Further Processing For Special Transactions
  if (digitalCurrencyTransaction.specialTransactionType) {
    return digitalCurrencyTransaction
  }

  return digitalCurrencyTransaction
}


export {
  createDigitalCurrencyTransaction2FirebaseAdmin
}


