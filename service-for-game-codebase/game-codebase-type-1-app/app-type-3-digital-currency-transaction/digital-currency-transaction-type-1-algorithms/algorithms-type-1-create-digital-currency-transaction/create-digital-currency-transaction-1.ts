

import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import { DigitalCurrencyTransaction } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { createId1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-2-create-id/create-id-1'

// import { getUTCDate1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'
import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { getTransactionProcessingInformationTableAlgorithm } from '../algorithms-type-9-initialize-digital-currency-transaction-list/service-for-helper-algorithms/helper-algorithms-1-get-transaction-processing-information-table'


async function createDigitalCurrencyTransaction1 (digitalCurrencyTransactionInformation: DigitalCurrencyTransaction) {

  const currentDate = (new Date()).getTime()

  // Get Transaction Order Index
  const numberOfTransactionCreated = ((await getItem1(DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID, 'numberOfTransactionsCreated')) || 0) + (Object.keys(await getItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfTransactionsCreated`, 'dataQueue') || {}).length)

  // Create Transaction
  const digitalCurrencyTransaction = new DigitalCurrencyTransaction(digitalCurrencyTransactionInformation)
  digitalCurrencyTransaction.transactionId = createId1()
  digitalCurrencyTransaction.transactionOrderIndex = numberOfTransactionCreated + 1

  // Publish Transaction

  await createItem1(DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction)

  // Publish Transaction Network Statistics

  // Transaction Network Statistics - Created Transactions
  createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfTransactionsCreated/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })

  const transactionProcessingInformationTable = getTransactionProcessingInformationTableAlgorithm.function(digitalCurrencyTransaction)
  const dateTransactionScheduledToBeProcessedTimePartition = transactionProcessingInformationTable.dateTransactionScheduledToBeProcessedTimePartition
  const { millisecondDay } = getMillisecondTimePartitionList2(digitalCurrencyTransaction.dateTransactionCreated!)
  updateItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/scheduledTransactionVariableTable/timeTable/transactionIdTimeTable/${dateTransactionScheduledToBeProcessedTimePartition.millisecondDay}/${dateTransactionScheduledToBeProcessedTimePartition.millisecondHour}/${dateTransactionScheduledToBeProcessedTimePartition.millisecondMinute}/${dateTransactionScheduledToBeProcessedTimePartition.millisecondSecond}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, { 'isProcessed': false, 'dateCreated': digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
  createItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated, false, true).catch((_errorMessage: any) => { /* */ })
  createItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated).catch((_errorMessage: any) => { /* */ })

  // Transaction Network Statistics - Created Recurring Transactions
  if (digitalCurrencyTransaction.isRecurringTransaction) {
    createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeTransactionNetworkStatisticsDataQueue/numberOfRecurringTransactionsCreated/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
    createItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdRecurringTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated, false, true).catch((_errorMessage: any) => { /* */ })
    updateItem1(`${DIGITAL_CURRENCY_TRANSACTION_NETWORK_STATISTICS_STORE_ID}/createdRecurringTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}`, 'transactionIdVariableTable', { [digitalCurrencyTransaction.transactionId]: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
  }

  // Publish Account Transaction Statistics

  // Account Statistics - Transactions Created
  createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyTransaction.accountSenderId}/numberOfTransactionsCreated/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
  createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/createdTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated, false, true).catch((_errorMessage: any) => { /* */ })
  createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/createdTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated).catch((_errorMessage: any) => { /* */ })

  // Account Statistics - Recurring Transactions Created
  if (digitalCurrencyTransaction.isRecurringTransaction) {
    createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyTransaction.accountSenderId}/numberOfRecurringTransactionsCreated/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
    createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/createdRecurringTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated).catch((_errorMessage: any) => { /* */ })
    updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/createdRecurringTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}`, 'transactionIdVariableTable', { [digitalCurrencyTransaction.transactionId]: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
  }

  // Publish Recipient Account Transaction Statistics
  for (let accountId in digitalCurrencyTransaction.accountRecipientIdVariableTable) {
    if (!digitalCurrencyTransaction.accountRecipientIdVariableTable.hasOwnProperty(accountId)) {
      continue
    }

    // Account Statistics - Number of Received Transactions
    createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfTransactionsReceived/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
    createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated, false, true).catch((_errorMessage: any) => { /* */ })
    createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated).catch((_errorMessage: any) => { /* */ })

    // Account Statistics - Recurring Transactions Created
    if (digitalCurrencyTransaction.isRecurringTransaction) {
      createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfRecurringTransactionsReceived/dataQueue`, `${digitalCurrencyTransaction.transactionId}`, { transactionId: digitalCurrencyTransaction.transactionId, dateCreated: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
      createItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedRecurringTransactionVariableTable/transactionIdVariableTable`, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated, false, true).catch((_errorMessage: any) => { /* */ })
      updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/receivedRecurringTransactionVariableTable/timeTable/transactionIdTimeTable/${millisecondDay}`, 'transactionIdVariableTable', { [digitalCurrencyTransaction.transactionId]: digitalCurrencyTransaction.dateTransactionCreated }).catch((_errorMessage: any) => { /* */ })
    }

    // Account Statistics - Number of Accounts Sent A Transaction To
    createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${digitalCurrencyTransaction.accountSenderId}/numberOfAccountsSentATransactionTo/dataQueue`, `${accountId}`, { accountId, dateAdded: currentDate }).catch((_errorMessage: any) => { /* */ })
    updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/accountsSentATransactionToVariableTable/timeTable/accountIdTimeTable/${millisecondDay}`, 'accountIdVariableTable', { [accountId]: currentDate }).catch((_errorMessage: any) => { /* */ })
    updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyTransaction.accountSenderId}/accountsSentATransactionToVariableTable`, 'accountIdVariableTable', { [accountId]: currentDate }).catch((_errorMessage: any) => { /* */ })

    createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountTransactionStatisticsDataQueue/accountIdVariableTable/${accountId}/numberOfAccountsReceivedATransactionFrom/dataQueue`, `${digitalCurrencyTransaction.accountSenderId}`, { accountId: digitalCurrencyTransaction.accountSenderId, dateAdded: currentDate }).catch((_errorMessage: any) => { /* */ })
    updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsReceivedATransactionFromVariableTable`, 'accountIdVariableTable', { [digitalCurrencyTransaction.accountSenderId]: currentDate }).catch((_errorMessage: any) => { /* */ })
    updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${accountId}/accountsReceivedATransactionFromVariableTable/timeTable/accountIdTimeTable/${millisecondDay}`, 'accountIdVariableTable', { [digitalCurrencyTransaction.accountSenderId]: currentDate }).catch((_errorMessage: any) => { /* */ })
  }

  return digitalCurrencyTransaction
}



const createDigitalCurrencyTransaction1Algorithm = {
  function: createDigitalCurrencyTransaction1
}

export {
  createDigitalCurrencyTransaction1Algorithm
}


