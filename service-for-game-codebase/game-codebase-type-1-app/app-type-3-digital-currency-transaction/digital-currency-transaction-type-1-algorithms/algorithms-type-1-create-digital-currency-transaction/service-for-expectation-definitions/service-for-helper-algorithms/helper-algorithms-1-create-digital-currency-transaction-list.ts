

import { createDigitalCurrencyTransaction1Algorithm } from '../../create-digital-currency-transaction-1'

import { DigitalCurrencyAccount } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
import { DigitalCurrencyTransaction } from '../../../../digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

// import { getMillisecondTimePartitionList1 } from '../../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


class CreateDigitalCurrencyTransactionListOptions {
  numberOfTransactions?: number
  numberOfRecurringTransactions?: number

  oldestTransactionDate?: Date = new Date()
  newestTransactionDate?: Date = new Date()

  minimumMillisecondRecurringTransactionDelay?: number = 0
  maximumMillisecondRecurringTransactionDelay?: number = 0

  maximumMillisecondTimeFromNowForScheduledDate?: number = 0

  minimumDigitalCurrencyAmount?: number = 0
  maximumDigitalCurrencyAmount?: number = 0

  accountSenderId?: string = ''

  textMemorandumList?: string[] = []
}


async function createDigitalCurrencyTransactionList (digitalCurrencyAccountList: DigitalCurrencyAccount[], options?: CreateDigitalCurrencyTransactionListOptions) {

  if (digitalCurrencyAccountList.length < 2) {
    throw Error(`Cannot create transaction list with less than 2 accounts`)
  }

  if (!options) {
    options = new CreateDigitalCurrencyTransactionListOptions()
  }

  options.numberOfTransactions = options.numberOfTransactions || 0
  options.numberOfRecurringTransactions = options.numberOfRecurringTransactions || 0
  options.oldestTransactionDate = options.oldestTransactionDate || new Date()
  options.newestTransactionDate = options.newestTransactionDate || new Date()
  options.minimumMillisecondRecurringTransactionDelay = options.minimumMillisecondRecurringTransactionDelay || 0
  options.maximumMillisecondRecurringTransactionDelay = options.maximumMillisecondRecurringTransactionDelay || 0
  options.maximumMillisecondTimeFromNowForScheduledDate = options.maximumMillisecondTimeFromNowForScheduledDate || 0
  options.minimumDigitalCurrencyAmount = options.minimumDigitalCurrencyAmount || 0
  options.maximumDigitalCurrencyAmount = options.maximumDigitalCurrencyAmount || 0
  options.accountSenderId = options.accountSenderId || ''
  options.textMemorandumList = options.textMemorandumList || []

  let digitalCurrencyTransactionList = []

  for (let i = 0; i < options.numberOfTransactions + options.numberOfRecurringTransactions; i++) {
    let senderAccount = null
    let indexOfSenderAccount = 0
    if (options!.accountSenderId) {
      senderAccount = digitalCurrencyAccountList.filter((digitalCurrencyAccount: DigitalCurrencyAccount, index: number) => {
        const isSenderDigitalCurrencyAccount = digitalCurrencyAccount.accountId === options!.accountSenderId
        if (isSenderDigitalCurrencyAccount) {
          indexOfSenderAccount = index
        }
        return isSenderDigitalCurrencyAccount
      })[0]
    } else {
      indexOfSenderAccount = Math.floor(Math.random() * digitalCurrencyAccountList.length)
      senderAccount = digitalCurrencyAccountList[indexOfSenderAccount]
    }

    const recipientAccountList = [...digitalCurrencyAccountList.slice(0, indexOfSenderAccount), ...digitalCurrencyAccountList.slice(indexOfSenderAccount + 1, digitalCurrencyAccountList.length)]
    const transactionCurrencyAmount = options.minimumDigitalCurrencyAmount + Math.floor(Math.random() * (options.maximumDigitalCurrencyAmount - options.minimumDigitalCurrencyAmount))
    const currencyPaymentAmountList: number[] = [...new Array(recipientAccountList.length)].map(() => (transactionCurrencyAmount / recipientAccountList.length))

    // Create Transaction Information
    const digitalCurrencyTransactionInformation = new DigitalCurrencyTransaction()
    digitalCurrencyTransactionInformation.dateTransactionCreated = options.oldestTransactionDate!.getTime() + Math.floor(Math.random() * (options.newestTransactionDate!.getTime() - options.oldestTransactionDate!.getTime()))
    const isScheduledForLaterProcessing = options.maximumMillisecondTimeFromNowForScheduledDate ? true : false

    if (isScheduledForLaterProcessing) {
      const transactionDate = new Date(digitalCurrencyTransactionInformation.dateTransactionCreated + Math.floor(Math.random() * options.maximumMillisecondTimeFromNowForScheduledDate!))
      digitalCurrencyTransactionInformation.dateTransactionScheduledForProcessing = transactionDate.getTime()
    }

    const textMemorandumRandomIndex = Math.floor(Math.random() * options.textMemorandumList.length)

    digitalCurrencyTransactionInformation.accountRecipientIdVariableTable = {}
    digitalCurrencyTransactionInformation.accountSenderId = senderAccount ? senderAccount.accountId : ''

    digitalCurrencyTransactionInformation.isRecurringTransaction = i >= options.numberOfTransactions
    digitalCurrencyTransactionInformation.onBehalfOfAccountId = ''
    digitalCurrencyTransactionInformation.textMemorandum = options.textMemorandumList[textMemorandumRandomIndex] || ''
    digitalCurrencyTransactionInformation.transactionCurrencyAmount = transactionCurrencyAmount
    digitalCurrencyTransactionInformation.transactionType = 'General'

    if (digitalCurrencyTransactionInformation.isRecurringTransaction) {
      digitalCurrencyTransactionInformation.millisecondRecurringDelay = options.minimumMillisecondRecurringTransactionDelay + Math.floor(Math.random() * (options.maximumMillisecondRecurringTransactionDelay - options.minimumMillisecondRecurringTransactionDelay))
      digitalCurrencyTransactionInformation.originalTimeNumberUntilNextRecurrence = 1
      digitalCurrencyTransactionInformation.originalTimeScaleUntilNextRecurrence = 'Second'

      // if (!isUnlimitedNumberOfRecurrencePayments) {
      //   // digitalCurrencyTransactionInformation.dateRecurrenceCancelled = (new Date(this.transactionCancellationDate)).getTime()
      // }
    }

    for (let i = 0; i < recipientAccountList.length; i++) {
      const recpientAccountId = recipientAccountList[i].accountId
      const currencyPaymentAmount = currencyPaymentAmountList[i]
      digitalCurrencyTransactionInformation.accountRecipientIdVariableTable[recpientAccountId] = {
        transactionCurrencyAmount: currencyPaymentAmount
      }
    }

    // Create Transaction
    const digitalCurrencyTransaction = await createDigitalCurrencyTransaction1Algorithm.function(digitalCurrencyTransactionInformation)

    digitalCurrencyTransactionList.push(digitalCurrencyTransaction)
  }

  return digitalCurrencyTransactionList
}


export {
  createDigitalCurrencyTransactionList,
  CreateDigitalCurrencyTransactionListOptions
}




