
import { GetTransactionNetworkStatisticsOptions } from '../../digital-currency-transaction-type-2-data-structures/data-structures-type-2-transaction-network-statistics/transaction-network-statistics-2'


import {
  // Digital Currency Statistics
  AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE,
  AMOUNT_OF_DIGITAL_CURRENCY_CREATED_VALUE,
  AMOUNT_OF_DIGITAL_CURRENCY_DELETED_VALUE,
  AMOUNT_OF_DIGITAL_CURRENCY_ACTIVE_VALUE,

  // Transaction Statistics
  NUMBER_OF_CREATED_TRANSACTIONS_VALUE,
  NUMBER_OF_CREATED_TRANSACTIONS_GRAPH,
  RECENTLY_CREATED_TRANSACTIONS_DATA_TABLE,

  NUMBER_OF_DELETED_TRANSACTIONS_VALUE,
  NUMBER_OF_DELETED_TRANSACTIONS_GRAPH,
  RECENTLY_DELETED_TRANSACTIONS_DATA_TABE,

  NUMBER_OF_ACTIVE_TRANSACTIONS_VALUE,
  NUMBER_OF_ACTIVE_TRANSACTIONS_GRAPH,

  // Recurring Transaction Statistics
  NUMBER_OF_CREATED_RECURRING_TRANSACTIONS_VALUE,
  NUMBER_OF_CREATED_RECURRING_TRANSACTIONS_GRAPH,
  RECENTLY_CREATED_RECURRING_TRANSACTIONS_DATA_TABLE,

  NUMBER_OF_DELETED_RECURRING_TRANSACTIONS_VALUE,
  NUMBER_OF_DELETED_RECURRING_TRANSACTIONS_GRAPH,
  RECENTLY_DELETED_RECURRING_TRANSACTIONS_DATA_TABE,

  NUMBER_OF_ACTIVE_RECURRING_TRANSACTIONS_VALUE,
  NUMBER_OF_ACTIVE_RECURRING_TRANSACTIONS_GRAPH
} from '../../digital-currency-transaction-type-3-constants/constants-2-transaction-network-statistics'



async function getDigitalCurrencyTransactionNetworkStatistics1 (statisticsType: string, statisticsOptions?: GetTransactionNetworkStatisticsOptions) {
  switch (statisticsType) {
    case AMOUNT_OF_DIGITAL_CURRENCY_SENT_VALUE:
      return
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_CREATED_VALUE:
      return
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_DELETED_VALUE:
      return
      break
    case AMOUNT_OF_DIGITAL_CURRENCY_ACTIVE_VALUE:
      return
      break
    case NUMBER_OF_CREATED_TRANSACTIONS_VALUE:
      return
      break
    case NUMBER_OF_CREATED_TRANSACTIONS_GRAPH:
      return
      break
    case RECENTLY_CREATED_TRANSACTIONS_DATA_TABLE:
      return
      break
    case NUMBER_OF_DELETED_TRANSACTIONS_VALUE:
      return
      break
    case NUMBER_OF_DELETED_TRANSACTIONS_GRAPH:
      return
      break
    case RECENTLY_DELETED_TRANSACTIONS_DATA_TABE:
      return
      break
    case NUMBER_OF_ACTIVE_TRANSACTIONS_VALUE:
      return
      break
    case NUMBER_OF_ACTIVE_TRANSACTIONS_GRAPH:
      return
      break
    case NUMBER_OF_CREATED_RECURRING_TRANSACTIONS_VALUE:
      return
      break
    case NUMBER_OF_CREATED_RECURRING_TRANSACTIONS_GRAPH:
      return
      break
    case RECENTLY_CREATED_RECURRING_TRANSACTIONS_DATA_TABLE:
      return
      break
    case NUMBER_OF_DELETED_RECURRING_TRANSACTIONS_VALUE:
      return
      break
    case NUMBER_OF_DELETED_RECURRING_TRANSACTIONS_GRAPH:
      return
      break
    case RECENTLY_DELETED_RECURRING_TRANSACTIONS_DATA_TABE:
      return
      break
    case NUMBER_OF_ACTIVE_RECURRING_TRANSACTIONS_VALUE:
      return
      break
    case NUMBER_OF_ACTIVE_RECURRING_TRANSACTIONS_GRAPH:
      return
      break
    default:
      throw Error(`No Network Statistics of type: ${statisticsType}`)
  }
}


export {
  getDigitalCurrencyTransactionNetworkStatistics1
}

