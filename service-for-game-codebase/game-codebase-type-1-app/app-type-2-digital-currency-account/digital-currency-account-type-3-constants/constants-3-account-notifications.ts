



const CREATED_TRANSACTIONS: string = 'createdTransactions'
const RECEIVED_TRANSACTIONS: string = 'receivedTransactions'


export {
  CREATED_TRANSACTIONS,
  RECEIVED_TRANSACTIONS
}

