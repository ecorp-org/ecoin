

const NUMBER_OF_CREATED_ACCOUNTS_VALUE = 'NUMBER_OF_CREATED_ACCOUNTS_VALUE'
const NUMBER_OF_CREATED_ACCOUNTS_GRAPH = 'NUMBER_OF_CREATED_ACCOUNTS_GRAPH'
const RECENTLY_CREATED_ACCOUNTS_DATA_TABLE = 'RECENTLY_CREATED_ACCOUNTS_DATA_TABLE'

const NUMBER_OF_DELETED_ACCOUNTS_VALUE = 'NUMBER_OF_DELETED_ACCOUNTS_VALUE'
const NUMBER_OF_DELETED_ACCOUNTS_GRAPH = 'NUMBER_OF_DELETED_ACCOUNTS_GRAPH'
const RECENTLY_DELETED_ACCOUNTS_DATA_TABLE = 'RECENTLY_DELETED_ACCOUNTS_DATA_TABLE'

const NUMBER_OF_ACTIVE_ACCOUNTS_VALUE = 'NUMBER_OF_ACTIVE_ACCOUNTS_VALUE'
const NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH = 'NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH'


export {
  NUMBER_OF_CREATED_ACCOUNTS_VALUE,
  NUMBER_OF_CREATED_ACCOUNTS_GRAPH,
  RECENTLY_CREATED_ACCOUNTS_DATA_TABLE,

  NUMBER_OF_DELETED_ACCOUNTS_GRAPH,
  NUMBER_OF_DELETED_ACCOUNTS_VALUE,
  RECENTLY_DELETED_ACCOUNTS_DATA_TABLE,

  NUMBER_OF_ACTIVE_ACCOUNTS_VALUE,
  NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH
}

