


class DigitalCurrencyAccountNetworkStatistics {
  numberOfAccountsCreated?: number
  numberOfAccountsDeleted?: number
  numberOfActiveAccounts?: number

  // delete all of accountIdVariableTable after 10 days
  // delete all time table keys besides within the last 3 months
  createdAccountVariableTable?: TimeTableWithAccountId

  updatedAccountVariableTable?: TimeTableWithAccountId

  deletedAccountVariableTable?: TimeTableWithAccountId

  activeAccountVariableTable?: TimeTableWithAccountId

  recommendedAccountVariableTable?: {
    [accountId: string]: boolean
  }

  accountUsernameVariableTable?: {
    [username: string]: string
  }
}

class TimeTableWithAccountId {
  accountIdVariableTable: { [accountId: string]: boolean } = {}
  timeTable: {
    accountIdTimeTable: {
      [dateId: number]: {
        [hourId: number]: {
          [minuteId: number]: {
            accountIdVariableTable: {
              [itemId: string]: number // Date
            }
          }
        }
      }
    }
    numberOfProcessedAccountsTimeTable: {
      [dateId: number]: {
        [hourId: number]: {
          [minuteId: number]: {
            numberOfProcessedAccounts: number
          }
        }
      }
    }
  }
}





const digitalCurrencyAccountNetworkStatisticsPropertyNameList = Object.getOwnPropertyNames(new DigitalCurrencyAccountNetworkStatistics())

const NUMBER_OF_ACCOUNTS_CREATED = digitalCurrencyAccountNetworkStatisticsPropertyNameList[0]
const NUMBER_OF_ACCOUNTS_DELETED = digitalCurrencyAccountNetworkStatisticsPropertyNameList[1]


export {
  DigitalCurrencyAccountNetworkStatistics,

  NUMBER_OF_ACCOUNTS_CREATED,
  NUMBER_OF_ACCOUNTS_DELETED
}


