

class DigitalCurrencyAccountSettings {

  // Transaction Settings
  showUniversalBasicIncomeTransactionNotifications: boolean = false

  // Product Resource Settings

  // Job Employment Settings

  // Currency Exchange Settings

  payPalAccountUrl: string = ''

  stripeApiKey: string = ''

  paymentMethodKeyValueList: Array<{
    paymentMethodName: string
    paymentMethodValue: any
  }> = []

  // Digital Currency Alternative Settings

  // Network Privileges Settings

  blockedAccountVariableTable: {
    [digitalCurrencyAccountId: string]: boolean
  } = {}

  // Application Settings

  showTransactionNotifications: boolean = true

  // Software Updates Settings

}

export {
  DigitalCurrencyAccountSettings
}


