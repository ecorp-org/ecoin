


class DigitalCurrencyAccountNotification {
  notificationType: string = ''
  notificationMessage: string = ''
}

const NOTIFICATION_TYPE_SUCCESS = 'success'
const NOTIFICATION_TYPE_WARNING = 'warning'
const NOTIFICATION_TYPE_FAILURE = 'failure'

export {
  DigitalCurrencyAccountNotification,
  NOTIFICATION_TYPE_SUCCESS,
  NOTIFICATION_TYPE_WARNING,
  NOTIFICATION_TYPE_FAILURE
}

