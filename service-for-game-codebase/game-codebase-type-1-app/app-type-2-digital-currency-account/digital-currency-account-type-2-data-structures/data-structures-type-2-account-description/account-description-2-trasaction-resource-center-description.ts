
import { GeneralUserDescription, Promotion } from './account-description-1-basic-description'


class DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription {
  accountId: string = ''

  placesWhereTransactionsAreAcceptedFrom: string = ''
  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []
  willingToReceivePromotionsByName: Promotion[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}

export {
  DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription
}

