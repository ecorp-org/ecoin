
import { GeneralUserDescription, Promotion } from './account-description-1-basic-description'

class DigitalCurrencyAccountDescriptionForProductResourceCenterDescription {
  accountId: string = ''

  businessDescription: string = ''
  productGenreList: string[] = []
  estimatedTimeToDeliverProduct: string = ''

  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []
  willingToReceivePromotionsByName: Promotion[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}


export {
  DigitalCurrencyAccountDescriptionForProductResourceCenterDescription
}

