
import { GeneralUserDescription, Promotion } from './account-description-1-basic-description'

class DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription {
  accountId: string = ''

  currencyExchangeDescription: string = ''
  currencyTradeList: string[] = []
  estimatedTimeToDeliverTradePayout: string = ''

  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []
  willingToReceivePromotionsByName: Promotion[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}

export {
  DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription
}

