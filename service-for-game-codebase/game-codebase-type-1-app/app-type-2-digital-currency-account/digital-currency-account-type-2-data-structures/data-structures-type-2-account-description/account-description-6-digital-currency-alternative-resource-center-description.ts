
import { GeneralUserDescription, Promotion } from './account-description-1-basic-description'

class DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription {
  accountId: string = ''

  businessDescription: string = ''
  businessFeatureList: string[] = []
  businessWebsiteUrl: string = ''

  doesRepresentDigitalCurrencyAlternative: boolean = false

  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []
  willingToReceivePromotionsByName: Promotion[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}

export {
  DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription
}

