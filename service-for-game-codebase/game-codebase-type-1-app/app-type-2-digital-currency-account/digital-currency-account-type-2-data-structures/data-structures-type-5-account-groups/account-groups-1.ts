

class AccountGroups1 {
  groupVariableTable: {
    [groupId: string]: Group
  } = {}
}

class Group {
  groupId: string = ''
  groupName: string = ''
  dateCreated?: any

  groupItemIdVariableTable: {
    [itemId: string]: GroupItem
  } = {}
}

class GroupItem {
  itemId: string = ''
  itemType: string = ''
  dateAdded?: any
}

const GROUP_ITEM_TYPE_ACCOUNT = 'account'

export {
  AccountGroups1,
  Group,
  GroupItem,
  GROUP_ITEM_TYPE_ACCOUNT
}


