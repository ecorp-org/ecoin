

import { projectVariables } from '../../../../../service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/project-variables-1-introduction'

class DigitalCurrencyAccount {
  accountId: string = ''

  firebaseAuthUid?: string = ''
  // publicCryptographicKey?: string = ''
  // privateCryptographicKey?: string = ''

  accountName: string = ''
  accountProfilePictureUrl: string = projectVariables.digitalCurrencyAccountDefaultThumbnailImageIconUrl
  accountUsername: string = ''

  accountCurrencyAmount: number = 0
  // latestProcessedTransactionId: string = ''

  dateAccountCreated: string = (new Date()).toISOString().split('.')[0] + 'Z'
  dateLastActive: string = (new Date()).toISOString().split('.')[0] + 'Z'

  constructor (defaultOption?: DigitalCurrencyAccount) {
    Object.assign(this, defaultOption)
  }
}

export {
  DigitalCurrencyAccount
}


