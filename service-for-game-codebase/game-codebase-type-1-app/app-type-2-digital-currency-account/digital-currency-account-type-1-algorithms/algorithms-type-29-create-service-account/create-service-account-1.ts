

import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import { createServiceAccount1 as createServiceAccountByFirebase } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-13-create-service-account/create-service-account-1'

import {
  SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'

import {
  ServiceAccountCreateInformation
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'

async function createServiceAccount1 (information?: ServiceAccountCreateInformation): Promise<ServiceAccount> {

  const serviceAccountInformation = await createServiceAccountByFirebase(information || {})

  if (!serviceAccountInformation) {
    throw Error(`Could not create service account`)
  }

  const serviceAccountId = serviceAccountInformation.user!.uid

  let serviceAccount = await getItem1(SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID, serviceAccountId) as ServiceAccount

  if (!serviceAccount) {
    serviceAccount = new ServiceAccount()
    serviceAccount.serviceAccountId = serviceAccountId
    serviceAccount.numberOfDigitalCurrencyAccounts = 0
    serviceAccount.activeDigitalCurrencyAccountId = ''
    serviceAccount.isAnonymousAccount = serviceAccountInformation.user!.isAnonymous || false
    serviceAccount.isEmailVerified = serviceAccountInformation.user!.emailVerified || false
    serviceAccount.emailAddress = serviceAccountInformation.user!.email || ''
    serviceAccount = await createItem1(SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID, serviceAccountId, serviceAccount, false, true)
    ;(serviceAccount as any).additionalInformation = serviceAccountInformation
  }

  if (serviceAccount.isAnonymousAccount !== serviceAccountInformation.user!.isAnonymous) {
    console.log('Not Anonymous Anymore!')
    serviceAccount = await updateItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId, { 'isAnonymousAccount': serviceAccountInformation.user!.isAnonymous })
  }

  return serviceAccount
}


export {
  createServiceAccount1
}


