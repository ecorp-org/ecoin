

import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'


import {
  DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function getDigitalCurrencyAccountGroupList1 (digitalCurrencyAccountId: string) {

  const groupVariableTable = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId) || {}

  return Object.keys(groupVariableTable).map(groupId => groupVariableTable[groupId]) || []
}

export {
  getDigitalCurrencyAccountGroupList1
}


