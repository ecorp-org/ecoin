

import { removeItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-11-remove-item-event-listener/remove-item-event-listener-1'

import {
  EVENT_ID_UPDATE
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

import {
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function removeEventListenerFromDigitalCurrencyAccount1 (digitalCurrencyAccountId: string, itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId, EVENT_ID_UPDATE, itemEventListenerId)
}

export {
  removeEventListenerFromDigitalCurrencyAccount1
}

