

import { isInitializedServiceAccount as isInitializedServiceAccountByFirebase } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-4-is-initialized-service-account/is-initialized-service-account-1'

async function isInitializedServiceAccount1 (): Promise<boolean> {
  return isInitializedServiceAccountByFirebase()
}

export {
  isInitializedServiceAccount1
}


