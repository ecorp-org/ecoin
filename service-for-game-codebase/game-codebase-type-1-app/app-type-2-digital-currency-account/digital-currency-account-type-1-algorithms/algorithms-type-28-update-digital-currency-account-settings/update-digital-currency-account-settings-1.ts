

import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'


import {
  DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

async function updateDigitalCurrencyAccountSettings1 (digitalCurrencyAccountId: string, propertyId: string, propertyValue: string) {

  if (!digitalCurrencyAccountId) {
    return
  }

  // update the account settings
  await updateItem1(DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID, digitalCurrencyAccountId, { [propertyId]: propertyValue })

  // return the digital currency account
  const digitalCurrencyAccountSettings = await getItem1(DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID, digitalCurrencyAccountId)
  return digitalCurrencyAccountSettings
}


export {
  updateDigitalCurrencyAccountSettings1
}


