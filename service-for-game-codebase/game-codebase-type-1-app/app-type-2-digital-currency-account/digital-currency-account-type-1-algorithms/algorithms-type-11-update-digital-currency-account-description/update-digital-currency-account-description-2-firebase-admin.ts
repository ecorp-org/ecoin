

import { getItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { updateItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'

import { updateItemSearchIndex1 } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-7-update-item-search-index/update-item-search-index-1'

import { algoliasearchVariableTree } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-4-variables/variables-1-algoliasearch'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'

import { getDigitalCurrencyAccountDescription2FirebaseAdmin } from '../algorithms-type-12-get-digital-currency-account-description/get-digital-currency-account-description-2'

import {
  DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  BASIC_DESCRIPTION,
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION,

  TRANSACTION_PROMOTION,
  PRODUCT_RESOURCE_PROMOTION,
  JOB_EMPLOYMENT_RESOURCE_PROMOTION,
  CURRENCY_EXCHANGE_RESOURCE_POMOTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_PROMOTION
} from '../../digital-currency-account-type-3-constants/constants-1-account-description'


async function updateDigitalCurrencyAccountDescription2FirebaseAdmin (digitalCurrencyAccountId: string, descriptionType: string, propertyId: string, propertyValue: any) {

  // ensure the description type is valid
  let isValidDescriptionType = false

  switch (descriptionType) {
    case BASIC_DESCRIPTION:
    case TRANSACTION_RESOURCE_CENTER_DESCRIPTION:
    case PRODUCT_RESOURCE_CENTER_DESCRIPTION:
    case JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION:
    case CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION:
    case DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION:
      isValidDescriptionType = true
  }

  if (!isValidDescriptionType) {
    throw Error(`Invalid Description Type`)
  }

  // edit the selected property id and update object
  // before publishing to firebase
  let selectedStoreId = `${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`
  let selectedPropertyId = descriptionType
  let selectedUpdateObject = { [propertyId]: propertyValue }

  if (typeof propertyValue === 'object' && !Array.isArray(propertyValue)) {
    selectedStoreId = `${selectedStoreId}/${selectedPropertyId}`
    selectedPropertyId = propertyId
    selectedUpdateObject = propertyValue
  }

  // handle the specific case for willing to receive promotions
  if (propertyId === 'willingToReceivePromotions') {
    const promotionType = getPromotionTypeForDescriptionType(descriptionType)
    if (propertyValue) {
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/willingToReceivePromotionsTable`, promotionType, { [digitalCurrencyAccountId]: true })
    } else {
      await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/willingToReceivePromotionsTable/${promotionType}`, digitalCurrencyAccountId)
    }
  }

  // handle the specific case for willing to receive promotions
  // of type
  if (propertyId === 'willingToReceivePromotionsOfType') {
    const promotionType = getPromotionTypeForDescriptionType(descriptionType)

    if (propertyValue && Array.isArray(propertyValue)) {
      // Remove from old promotion genre types
      const accountDescription = await getDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, descriptionType)

      if (accountDescription) {
        const previousPromotionGenreTypeList = accountDescription.willingToReceivePromotionsOfType || []

        for (let i = 0; i < previousPromotionGenreTypeList.length; i++) {
          const promotionGenreType: string = previousPromotionGenreTypeList[i]
          await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/willingToReceivePromotionsOfTypeTable/${promotionType}/${promotionGenreType}`, digitalCurrencyAccountId)
        }

        // Add to new promotion genre types
        for (let i = 0; i < propertyValue.length; i++) {
          const promotionGenreType: string = propertyValue[i]
          await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/willingToReceivePromotionsOfTypeTable/${promotionType}`, promotionGenreType, { [digitalCurrencyAccountId]: true })
        }
      }
    }
  }

  // handle the specific case for representing an
  // alternative digital currency
  if (propertyId === 'doesRepresentDigitalCurrencyAlternative') {
    if (propertyValue) {
      await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'doesRepresentDigitalCurrencyAlternativeVariableTable', { [digitalCurrencyAccountId]: true })
    } else {
      await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/doesRepresentDigitalCurrencyAlternativeVariableTable`, digitalCurrencyAccountId)
    }
  }

  // publish update based on the description type
  await updateItem2FirebaseAdmin(selectedStoreId, selectedPropertyId, selectedUpdateObject)

  // get the description object
  const descriptionObject = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, descriptionType)

  // update text search index
  const digitalCurrencyAccountSearchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]
  await updateItemSearchIndex1(digitalCurrencyAccountSearchIndexId, digitalCurrencyAccountId, {
    [descriptionType]: descriptionObject
  })

  return descriptionObject
}

function getPromotionTypeForDescriptionType (descriptionType: string): string {
  switch (descriptionType) {
    case TRANSACTION_RESOURCE_CENTER_DESCRIPTION:
      return TRANSACTION_PROMOTION
      break
    case PRODUCT_RESOURCE_CENTER_DESCRIPTION:
      return PRODUCT_RESOURCE_PROMOTION
      break
    case JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION:
      return JOB_EMPLOYMENT_RESOURCE_PROMOTION
      break
    case CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION:
      return CURRENCY_EXCHANGE_RESOURCE_POMOTION
      break
    case DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION:
      return DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_PROMOTION
      break
    default:
      return ''
  }
}


export {
  updateDigitalCurrencyAccountDescription2FirebaseAdmin,
  getPromotionTypeForDescriptionType
}



