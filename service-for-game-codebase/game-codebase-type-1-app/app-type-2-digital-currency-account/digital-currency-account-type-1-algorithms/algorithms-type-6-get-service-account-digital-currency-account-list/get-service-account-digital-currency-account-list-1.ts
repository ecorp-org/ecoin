
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import {
  SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'
import { DigitalCurrencyAccount } from '../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'


async function getServiceAccountDigitalCurrencyAccountList1 (serviceAccountId: string): Promise<DigitalCurrencyAccount[]> {
  const serviceAccount = await getItem1(SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID, serviceAccountId) as ServiceAccount

  if (!serviceAccount) {
    return []
  }

  let digitalCurrencyAccountList: DigitalCurrencyAccount[] = []

  for (let accountId in serviceAccount.digitalCurrencyAccountIdTable) {
    if (!serviceAccount.digitalCurrencyAccountIdTable.hasOwnProperty(accountId)) {
      continue
    }
    const digitalCurrencyAccount = await getItem1(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, accountId)

    if (!digitalCurrencyAccount) {
      continue
    }

    // set new date last active
    await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, accountId, { 'dateLastActive': (new Date()).getTime() })

    digitalCurrencyAccountList.push(digitalCurrencyAccount)
  }

  return digitalCurrencyAccountList
}


export {
  getServiceAccountDigitalCurrencyAccountList1
}

