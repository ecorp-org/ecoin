

import { getItemListBySearchText1 } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-3-get-item-list-by-search-text/get-item-list-by-search-text-1'

import { algoliasearchVariableTree } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-4-variables/variables-1-algoliasearch'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'

import { GetItemListOptions } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-2-data-structures/data-structures-1-get-item-list-options'

async function getDigitalCurrencyAccountBySearchText1 (searchText: string, options?: GetItemListOptions) {
  const digitalCurrencyAccountSearchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]
  const searchResultList = await getItemListBySearchText1(digitalCurrencyAccountSearchIndexId, searchText, options)
  return searchResultList
}


export {
  getDigitalCurrencyAccountBySearchText1
}





