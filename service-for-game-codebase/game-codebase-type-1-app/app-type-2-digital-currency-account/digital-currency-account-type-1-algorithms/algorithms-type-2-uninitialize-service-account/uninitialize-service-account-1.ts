

import { uninitializeServiceAccount1 as uninitializeServiceAccountWithFirebase } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-3-uninitialize-service-account/uninitialize-service-account-1'

async function uninitializeServiceAccount1 () {
  return uninitializeServiceAccountWithFirebase()
}

export {
  uninitializeServiceAccount1
}
