
import { addItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-10-add-item-event-listener/add-item-event-listener-1'

import {
  EVENT_ID_UPDATE,
  EVENT_ID_CREATE
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

import {
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  NUMBER_OF_CREATED_ACCOUNTS_GRAPH,
  NUMBER_OF_CREATED_ACCOUNTS_VALUE,
  NUMBER_OF_DELETED_ACCOUNTS_GRAPH,
  NUMBER_OF_DELETED_ACCOUNTS_VALUE,
  NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH,
  NUMBER_OF_ACTIVE_ACCOUNTS_VALUE,
  RECENTLY_CREATED_ACCOUNTS_DATA_TABLE,
  RECENTLY_DELETED_ACCOUNTS_DATA_TABLE
} from '../../digital-currency-account-type-3-constants/constants-2-account-network-statistics'


async function addEventListenerToDigitalCurrencyAccountNetworkStatistics1 (statisticsType: string, eventListenerCallbackFunction: any) {
  switch (statisticsType) {
    case NUMBER_OF_CREATED_ACCOUNTS_VALUE:
      return addEventListenerToNumberOfCreatedAccountsValue(eventListenerCallbackFunction)
      break
    case NUMBER_OF_DELETED_ACCOUNTS_VALUE:
      return addEventListenerToNumberOfDeletedAccountsValue(eventListenerCallbackFunction)
      break
    case NUMBER_OF_ACTIVE_ACCOUNTS_VALUE:
      return addEventListenerToNumberOfActiveAccountsValue(eventListenerCallbackFunction)
      break
    case NUMBER_OF_CREATED_ACCOUNTS_GRAPH:
      return addEventListenerToNumberOfCreatedAccountsGraph(eventListenerCallbackFunction)
      break
    case NUMBER_OF_DELETED_ACCOUNTS_GRAPH:
      return addEventListenerToNumberOfDeletedAccountsGraph(eventListenerCallbackFunction)
      break
    case NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH:
      return addEventListenerToNumberOfActiveAccountsGraph(eventListenerCallbackFunction)
      break
    case RECENTLY_CREATED_ACCOUNTS_DATA_TABLE:
      return addEventListenerToRecentlyCreatedAccountsDataTable(eventListenerCallbackFunction)
      break
    case RECENTLY_DELETED_ACCOUNTS_DATA_TABLE:
      return addEventListenerToRecentlyDeletedAccountsDataTable(eventListenerCallbackFunction)
      break
    default:
      throw Error(`No Network Statistics of type: ${statisticsType}`)
  }
}

// Values

async function addEventListenerToNumberOfCreatedAccountsValue (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'numberOfAccountsCreated', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfDeletedAccountsValue (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'numberOfAccountsDeleted', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfActiveAccountsValue (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'numberOfActiveAccounts', EVENT_ID_UPDATE, (data: any) => {
    eventListenerCallbackFunction(data)
  })
  return itemEventListenerId
}

// Graphs

async function addEventListenerToNumberOfCreatedAccountsGraph (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedAccounts = data.numberOfProcessedAccounts
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedAccounts]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfDeletedAccountsGraph (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedAccounts = data.numberOfProcessedAccounts
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedAccounts]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToNumberOfActiveAccountsGraph (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/activeAccountVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const millisecondDay = parseInt(options.key, 10)
    const numberOfProcessedAccounts = data.numberOfProcessedAccounts
    eventListenerCallbackFunction({
      dateList: [millisecondDay],
      dataList: [numberOfProcessedAccounts]
    })
  })
  return itemEventListenerId
}

// Data Table - Recently

async function addEventListenerToRecentlyCreatedAccountsDataTable (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable`, 'accountIdVariableTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const accountId = options.key
    const dateCreated = parseInt(data, 10)
    eventListenerCallbackFunction({
      dateList: [dateCreated],
      dataList: [accountId]
    })
  })
  return itemEventListenerId
}

async function addEventListenerToRecentlyDeletedAccountsDataTable (eventListenerCallbackFunction: any) {
  const itemEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable`, 'accountIdVariableTable', EVENT_ID_CREATE, (data: any, options: any) => {
    if (!data || !options) {
      return
    }
    const accountId = options.key
    const dateCreated = parseInt(data, 10)
    eventListenerCallbackFunction({
      dateList: [dateCreated],
      dataList: [accountId]
    })
  })
  return itemEventListenerId
}


export {
  addEventListenerToDigitalCurrencyAccountNetworkStatistics1
}

