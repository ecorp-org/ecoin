
import { removeItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-11-remove-item-event-listener/remove-item-event-listener-1'

import {
  EVENT_ID_UPDATE,
  EVENT_ID_CREATE
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

import {
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  NUMBER_OF_CREATED_ACCOUNTS_GRAPH,
  NUMBER_OF_CREATED_ACCOUNTS_VALUE,
  NUMBER_OF_DELETED_ACCOUNTS_GRAPH,
  NUMBER_OF_DELETED_ACCOUNTS_VALUE,
  NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH,
  NUMBER_OF_ACTIVE_ACCOUNTS_VALUE,
  RECENTLY_CREATED_ACCOUNTS_DATA_TABLE,
  RECENTLY_DELETED_ACCOUNTS_DATA_TABLE
} from '../../digital-currency-account-type-3-constants/constants-2-account-network-statistics'


async function removeEventListenerFromDigitalCurrencyAccountNetworkStatistics1 (statisticsType: string, itemEventListenerId: string) {
  switch (statisticsType) {
    case NUMBER_OF_CREATED_ACCOUNTS_VALUE:
      return removeEventListenerFromNumberOfCreatedAccountsValue(itemEventListenerId)
      break
    case NUMBER_OF_DELETED_ACCOUNTS_VALUE:
      return removeEventListenerFromNumberOfDeletedAccountsValue(itemEventListenerId)
      break
    case NUMBER_OF_ACTIVE_ACCOUNTS_VALUE:
      return removeEventListenerFromNumberOfActiveAccountsValue(itemEventListenerId)
      break
    case NUMBER_OF_CREATED_ACCOUNTS_GRAPH:
      return removeEventListenerFromNumberOfCreatedAccountsGraph(itemEventListenerId)
      break
    case NUMBER_OF_DELETED_ACCOUNTS_GRAPH:
      return removeEventListenerFromNumberOfDeletedAccountsGraph(itemEventListenerId)
      break
    case NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH:
      return removeEventListenerFromNumberOfActiveAccountsGraph(itemEventListenerId)
      break
    case RECENTLY_CREATED_ACCOUNTS_DATA_TABLE:
      return removeEventListenerFromRecentlyCreatedAccountsDataTable(itemEventListenerId)
      break
    case RECENTLY_DELETED_ACCOUNTS_DATA_TABLE:
      return removeEventListenerFromRecentlyDeletedAccountsDataTable(itemEventListenerId)
      break
    default:
      throw Error(`No Network Statistics of type: ${statisticsType}`)
  }
}

// Values

async function removeEventListenerFromNumberOfCreatedAccountsValue (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'numberOfAccountsCreated', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfDeletedAccountsValue (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'numberOfAccountsDeleted', EVENT_ID_UPDATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfActiveAccountsValue (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'numberOfActiveAccounts', EVENT_ID_UPDATE, itemEventListenerId)
}

// Graphs

async function removeEventListenerFromNumberOfCreatedAccountsGraph (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfDeletedAccountsGraph (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, itemEventListenerId)
}

async function removeEventListenerFromNumberOfActiveAccountsGraph (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/numberOfActiveAccountsVariableTable/timeTable`, 'numberOfProcessedAccountsTimeTable', EVENT_ID_CREATE, itemEventListenerId)
}

// Data Tables

async function removeEventListenerFromRecentlyCreatedAccountsDataTable (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable`, 'accountIdTimeTable', EVENT_ID_CREATE, itemEventListenerId)
}

async function removeEventListenerFromRecentlyDeletedAccountsDataTable (itemEventListenerId: string) {
  removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable`, 'accountIdTimeTable', EVENT_ID_CREATE, itemEventListenerId)
}


export {
  removeEventListenerFromDigitalCurrencyAccountNetworkStatistics1
}


