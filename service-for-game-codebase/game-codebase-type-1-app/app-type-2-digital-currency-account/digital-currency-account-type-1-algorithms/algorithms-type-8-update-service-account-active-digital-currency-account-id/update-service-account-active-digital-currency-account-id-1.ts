
import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import { SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID } from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { getServiceAccount1 } from '../algorithms-type-7-get-service-account/get-service-account-1'


async function updateServiceAccountActiveDigitalCurrencyAccountId1 (serviceAccountId: string, activeDigitalCurrencyAccountId: string): Promise<ServiceAccount> {
  await updateItem1(SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID, serviceAccountId, { 'activeDigitalCurrencyAccountId' : activeDigitalCurrencyAccountId || '' })
  return getServiceAccount1(serviceAccountId)
}

export {
  updateServiceAccountActiveDigitalCurrencyAccountId1
}


