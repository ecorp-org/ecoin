

import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'

import { ServiceAccountInitializationInformation } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'

import { initializeServiceAccount1 as initializeServiceAccountWithFirebase } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-2-initialize-service-account/initialize-service-account-1'

import {
  SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { createServiceAccount1 } from '../algorithms-type-29-create-service-account/create-service-account-1'

import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'


async function initializeServiceAccount1 (accountInformation: ServiceAccountInitializationInformation) {

  const serviceAccountInformation = await initializeServiceAccountWithFirebase(accountInformation)

  if (!serviceAccountInformation) {
    throw Error(`Could not initialize service account`)
  }

  let serviceAccount = await getItem1(SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID, serviceAccountInformation.user!.uid) as ServiceAccount

  if (!serviceAccount) {
    serviceAccount = await createServiceAccount1({
      emailAddress: accountInformation.emailAddress,
      password: accountInformation.password,
      isAnonymous: accountInformation.isAnonymousSignIn
    })
  }

  return serviceAccount
}


export {
  initializeServiceAccount1
}

