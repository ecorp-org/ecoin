

import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
import { getItemIdList1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-12-get-item-id-list/get-item-id-list-1'

import { Group } from '../../digital-currency-account-type-2-data-structures/data-structures-type-5-account-groups/account-groups-1'

import { createId1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-2-create-id/create-id-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


const MAXIMUM_NUMBER_OF_GROUPS = 10

async function createDigitalCurrencyAccountGroup1 (digitalCurrencyAccountId: string, groupName: string) {

  const numberOfGroupsCreated = (await getItemIdList1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`)).length

  if (numberOfGroupsCreated >= MAXIMUM_NUMBER_OF_GROUPS) {
    throw Error(`Cannot create more than ${MAXIMUM_NUMBER_OF_GROUPS}`)
  }

  const group = new Group()
  group.groupId = createId1()
  group.groupName = groupName
  group.dateCreated = (new Date()).getTime()

  // publish group
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, group.groupId, group)

  return group
}

export {
  createDigitalCurrencyAccountGroup1,
  MAXIMUM_NUMBER_OF_GROUPS
}

