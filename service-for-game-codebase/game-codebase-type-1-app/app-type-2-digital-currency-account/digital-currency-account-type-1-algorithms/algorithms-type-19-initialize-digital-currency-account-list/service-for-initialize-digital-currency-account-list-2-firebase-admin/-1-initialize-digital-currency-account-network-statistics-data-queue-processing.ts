

import { updateItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { getItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'



import { getMillisecondTimePartitionList2 } from '../../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { deleteDigitalCurrencyAccount2FirebaseAdmin } from '../../algorithms-type-9-delete-digital-currency-account/delete-digital-currency-account-2-firebase-admin'

async function initializeDigitalCurrencyAccountNetworkStatisticsDataQueueProcessing () {

  const initializeAccountNetworkStatisticsDataQueueTable = await getItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}`, 'initializeAccountNetworkStatisticsDataQueue') || {}

  const numberOfAccountsCreatedDataQueue = initializeAccountNetworkStatisticsDataQueueTable.numberOfAccountsCreated ? initializeAccountNetworkStatisticsDataQueueTable.numberOfAccountsCreated.dataQueue : {}
  const numberOfAccountsDeletedDataQueue = initializeAccountNetworkStatisticsDataQueueTable.numberOfAccountsDeleted ? initializeAccountNetworkStatisticsDataQueueTable.numberOfAccountsDeleted.dataQueue : {}

  const numberOfAccountsCreatedDataList = Object.keys(numberOfAccountsCreatedDataQueue).map((accountId: string) => {
    return { accountId, ...numberOfAccountsCreatedDataQueue[accountId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemB.dateCreated)).getTime() - (new Date(itemA.dateCreated)).getTime()
  })

  const numberOfAccountsDeletedDataList = Object.keys(numberOfAccountsDeletedDataQueue).map((accountId: string) => {
    return { accountId, ...numberOfAccountsDeletedDataQueue[accountId] }
  }).sort((itemA: any, itemB: any) => {
    return (new Date(itemB.dateCreated)).getTime() - (new Date(itemA.dateCreated)).getTime()
  })

  // Number of Accounts Created
  for (let i = 0; i < numberOfAccountsCreatedDataList.length; i++) {
    const accountObject = numberOfAccountsCreatedDataList[i]
    if (!accountObject || !accountObject.accountId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(accountObject.dateCreated)

    const numberOfAccountsCreated = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, `numberOfAccountsCreated`) || 0
    const numberOfAccountsCreatedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${millisecondDay}`, `numberOfProcessedAccounts`) || 0
    const newNumberOfAccountsCreated = numberOfAccountsCreated + 1
    const newNumberOfAccountsCreatedForToday = numberOfAccountsCreatedForToday + 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfAccountsCreated': newNumberOfAccountsCreated })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/`, `${millisecondDay}`, { 'numberOfProcessedAccounts': newNumberOfAccountsCreatedForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountNetworkStatisticsDataQueue/numberOfAccountsCreated/dataQueue`, accountObject.accountId)

    const numberOfActiveAccounts = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, `numberOfActiveAccounts`) || 0
    const numberOfActiveAccountsForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/activeAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${millisecondDay}`, `numberOfProcessedAccounts`) || numberOfActiveAccounts
    const newNumberOfActiveAccounts = numberOfActiveAccounts + 1
    const newNumberOfActiveAccountsForToday = numberOfActiveAccountsForToday + 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfActiveAccounts': newNumberOfActiveAccounts })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/activeAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedAccounts': newNumberOfActiveAccountsForToday })
  }

  // Number of Accounts Deleted
  for (let i = 0; i < numberOfAccountsDeletedDataList.length; i++) {
    const accountObject = numberOfAccountsDeletedDataList[i]
    if (!accountObject || !accountObject.accountId) {
      continue
    }
    const { millisecondDay } = getMillisecondTimePartitionList2(accountObject.dateCreated)

    await deleteDigitalCurrencyAccount2FirebaseAdmin(accountObject.accountId, true)

    const numberOfAccountsDeleted = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, `numberOfAccountsDeleted`) || 0
    const numberOfAccountsDeletedForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${millisecondDay}`, `numberOfProcessedAccounts`) || 0
    const newNumberOfAccountsDeleted = numberOfAccountsDeleted + 1
    const newNumberOfAccountsDeletedForToday = numberOfAccountsDeletedForToday + 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfAccountsDeleted': newNumberOfAccountsDeleted })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedAccounts': newNumberOfAccountsDeletedForToday })
    await deleteItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountNetworkStatisticsDataQueue/numberOfAccountsDeleted/dataQueue`, accountObject.accountId)

    const numberOfActiveAccounts = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, `numberOfActiveAccounts`) || 0
    const numberOfActiveAccountsForToday = await getItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/activeAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${millisecondDay}`, `numberOfProcessedAccounts`) || numberOfActiveAccounts
    const newNumberOfActiveAccounts = numberOfActiveAccounts - 1
    const newNumberOfActiveAccountsForToday = numberOfActiveAccountsForToday - 1
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, '', { 'numberOfActiveAccounts': newNumberOfActiveAccounts })
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/activeAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable`, `${millisecondDay}`, { 'numberOfProcessedAccounts': newNumberOfActiveAccountsForToday })
  }
}



const initializeDigitalCurrencyAccountNetworkStatisticsDataQueueProcessingAlgorithm = {
  function: initializeDigitalCurrencyAccountNetworkStatisticsDataQueueProcessing
}




export {
  initializeDigitalCurrencyAccountNetworkStatisticsDataQueueProcessingAlgorithm
}


