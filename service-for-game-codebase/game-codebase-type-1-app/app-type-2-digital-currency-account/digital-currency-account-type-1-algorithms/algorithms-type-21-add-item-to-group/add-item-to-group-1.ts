
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import { GroupItem } from '../../digital-currency-account-type-2-data-structures/data-structures-type-5-account-groups/account-groups-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function addItemToGroup1 (digitalCurrencyAccountId: string, groupId: string, groupItem: GroupItem) {

  if (!digitalCurrencyAccountId || !groupId || !groupItem) {
    return
  }

  groupItem.dateAdded = new Date()

  // add group item
  await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/${groupId}`, 'groupItemIdVariableTable', { [groupItem.itemId]: groupItem })

  return groupItem
}

export {
  addItemToGroup1
}
