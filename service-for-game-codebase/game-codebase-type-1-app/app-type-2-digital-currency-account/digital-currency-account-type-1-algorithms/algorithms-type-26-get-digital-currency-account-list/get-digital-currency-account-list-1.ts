
import { getItemIdList1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-12-get-item-id-list/get-item-id-list-1'

import { getDigitalCurrencyAccountById1 } from '../algorithms-type-15-get-digital-currency-account-by-id/get-digital-currency-account-by-id-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  TRANSACTION_PROMOTION,
  PRODUCT_RESOURCE_PROMOTION,
  JOB_EMPLOYMENT_RESOURCE_PROMOTION,
  CURRENCY_EXCHANGE_RESOURCE_POMOTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_PROMOTION
} from '../../digital-currency-account-type-3-constants/constants-1-account-description'


const LIST_TYPE_WILLING_TO_RECEIVE_PROMOTIONS = 'willingToReceivePromotions'
const LIST_TYPE_DIGITAL_CURRENCY_ALTERNATIVE = 'digitalCurrencyAlternative'

const DEFAULT_MAX_NUMBER_OF_ACCOUNTS_TO_GET = 30


class GetDigitalCurrencyAccountListOptions {
  promotionType?: string = ''
  promotionGenreType?: string = ''
}

async function getDigitalCurrencyAccountList1 (listType: string, options: GetDigitalCurrencyAccountListOptions) {

  let digitalCurrencyAccountIdList = []
  let digitalCurrencyAccountList = []

  if (listType === LIST_TYPE_WILLING_TO_RECEIVE_PROMOTIONS) {
    if (getIsValidPromotionType(options.promotionType!) && options.promotionGenreType) {
      digitalCurrencyAccountIdList = await getItemIdList1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/willingToReceivePromotionsOfTypeTable/${options.promotionType}/${options.promotionGenreType}`, DEFAULT_MAX_NUMBER_OF_ACCOUNTS_TO_GET)
    } else {
      digitalCurrencyAccountIdList = await getItemIdList1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/willingToReceivePromotionsTable`, DEFAULT_MAX_NUMBER_OF_ACCOUNTS_TO_GET)
    }
  } else if (listType === LIST_TYPE_DIGITAL_CURRENCY_ALTERNATIVE) {
    digitalCurrencyAccountIdList = await getItemIdList1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/doesRepresentDigitalCurrencyAlternativeVariableTable`, DEFAULT_MAX_NUMBER_OF_ACCOUNTS_TO_GET)
  }

  for (let i = 0; i < digitalCurrencyAccountIdList.length; i++) {
    const digitalCurrencyAccountId = digitalCurrencyAccountIdList[i]
    const digitalCurrencyAccount = await getDigitalCurrencyAccountById1(digitalCurrencyAccountId)
    if (!digitalCurrencyAccount) {
      continue
    }
    digitalCurrencyAccountList.push(digitalCurrencyAccount)
  }

  return digitalCurrencyAccountList
}

function getIsValidPromotionType (promotionType: string) {
  let isValidPromotionType = false

  switch (promotionType) {
    case TRANSACTION_PROMOTION:
    case PRODUCT_RESOURCE_PROMOTION:
    case JOB_EMPLOYMENT_RESOURCE_PROMOTION:
    case CURRENCY_EXCHANGE_RESOURCE_POMOTION:
    case DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_PROMOTION:
      isValidPromotionType = true
  }

  return isValidPromotionType
}




export {
  getDigitalCurrencyAccountList1,
  GetDigitalCurrencyAccountListOptions,
  LIST_TYPE_WILLING_TO_RECEIVE_PROMOTIONS,
  LIST_TYPE_DIGITAL_CURRENCY_ALTERNATIVE
}


