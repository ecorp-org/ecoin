

import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  BASIC_DESCRIPTION,
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../digital-currency-account-type-3-constants/constants-1-account-description'

async function getDigitalCurrencyAccountDescription1 (digitalCurrencyAccountId: string, descriptionType: string) {

  // ensure the description type is valid
  let isValidDescriptionType = false

  switch (descriptionType) {
    case BASIC_DESCRIPTION:
    case TRANSACTION_RESOURCE_CENTER_DESCRIPTION:
    case PRODUCT_RESOURCE_CENTER_DESCRIPTION:
    case JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION:
    case CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION:
    case DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION:
      isValidDescriptionType = true
  }

  if (!isValidDescriptionType) {
    throw Error(`Invalid Description Type`)
  }

  // get the description object
  const descriptionObject = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, descriptionType)

  return descriptionObject
}


export {
  getDigitalCurrencyAccountDescription1
}


