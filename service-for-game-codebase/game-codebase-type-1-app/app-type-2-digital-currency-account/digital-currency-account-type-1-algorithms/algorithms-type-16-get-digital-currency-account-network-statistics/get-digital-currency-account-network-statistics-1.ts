

import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'


import {
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  NUMBER_OF_CREATED_ACCOUNTS_GRAPH,
  NUMBER_OF_CREATED_ACCOUNTS_VALUE,
  NUMBER_OF_DELETED_ACCOUNTS_GRAPH,
  NUMBER_OF_DELETED_ACCOUNTS_VALUE,
  RECENTLY_CREATED_ACCOUNTS_DATA_TABLE,
  RECENTLY_DELETED_ACCOUNTS_DATA_TABLE,
  NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH,
  NUMBER_OF_ACTIVE_ACCOUNTS_VALUE
} from '../../digital-currency-account-type-3-constants/constants-2-account-network-statistics'

// import { getUTCDate1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'
import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { GetAccountNetworkStatisticsOptions } from '../../digital-currency-account-type-2-data-structures/data-structures-type-4-account-network-statistics/account-network-statistics-2'

import {
  MAXIMUM_NUMBER_OF_MILLISECONDS_AGO,
  NUMBER_OF_MILLISECONDS_IN_A_DAY
} from '../../../app-type-1-general/general-type-3-constants/constants-3-time'



async function getDigitalCurrencyAccountNetworkStatistics1 (statisticsType: string, statisticsOptions?: GetAccountNetworkStatisticsOptions) {
  switch (statisticsType) {
    case NUMBER_OF_CREATED_ACCOUNTS_VALUE:
      return getNumberOfCreatedAccountsValue()
      break
    case NUMBER_OF_DELETED_ACCOUNTS_VALUE:
      return getNumberOfDeletedAccountsValue()
      break
    case NUMBER_OF_ACTIVE_ACCOUNTS_VALUE:
      return getNumberOfActiveAccountsValue()
      break
    case NUMBER_OF_CREATED_ACCOUNTS_GRAPH:
      return getNumberOfCreatedAccountsGraph(statisticsOptions)
      break
    case NUMBER_OF_DELETED_ACCOUNTS_GRAPH:
      return getNumberOfDeletedAccountsGraph(statisticsOptions)
      break
    case NUMBER_OF_ACTIVE_ACCOUNTS_GRAPH:
      return getNumberOfActiveAccountsGraph(statisticsOptions)
      break
    case RECENTLY_CREATED_ACCOUNTS_DATA_TABLE:
      return getRecentlyCreatedAccountsDataTable(statisticsOptions)
      break
    case RECENTLY_DELETED_ACCOUNTS_DATA_TABLE:
      return getRecentlyDeletedAccountsDataTable(statisticsOptions)
      break
    default:
      throw Error(`No Network Statistics of type: ${statisticsType}`)
  }
}

// Number of Created Accounts Value
async function getNumberOfCreatedAccountsValue () {
  const numberOfCreatedAccounts = await getItem1(DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID, 'numberOfAccountsCreated')
  return numberOfCreatedAccounts
}

// Number of Deleted Accounts Value
async function getNumberOfDeletedAccountsValue () {
  const numberOfDeletedAccounts = await getItem1(DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID, 'numberOfAccountsDeleted')
  return numberOfDeletedAccounts
}

// Number of Active Accounts Value
async function getNumberOfActiveAccountsValue () {
  const numberOfActiveAccounts = await getItem1(DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID, 'numberOfActiveAccounts')
  return numberOfActiveAccounts
}

// Number of Created Accounts Graph
async function getNumberOfCreatedAccountsGraph (statisticsOptions?: GetAccountNetworkStatisticsOptions) {

  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfCreatedAccounts = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${dateNumber}`, 'numberOfProcessedAccounts') || 0
    dataList.push(numberOfCreatedAccounts)
  }

  return {
    dateList,
    dataList
  }
}

// Number of Deleted Accounts Graph
async function getNumberOfDeletedAccountsGraph (statisticsOptions?: GetAccountNetworkStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = dateNumber
    dateList.push(millisecondDate)
    const numberOfDeletedAccounts = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${dateNumber}`, 'numberOfProcessedAccounts') || 0
    dataList.push(numberOfDeletedAccounts)
  }

  return {
    dateList,
    dataList
  }
}

// Number of Active Accounts Graph
async function getNumberOfActiveAccountsGraph (statisticsOptions?: GetAccountNetworkStatisticsOptions) {
  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date
  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  let i = 0
  for (let dateNumber = endDateMillisecondPartitionList.millisecondDay; dateNumber <= startDateMillisecondPartitionList.millisecondDay; dateNumber += NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const millisecondDate = getMillisecondTimePartitionList2((new Date(dateNumber)).toISOString().split('.')[0] + 'Z').millisecondDay
    dateList.push(millisecondDate)
    const numberOfPreviousActiveAccounts: number = dataList.length > 0 ? dataList[i - 1] || 0 : 0
    const numberOfActiveAccounts: number = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/activeAccountVariableTable/timeTable/numberOfProcessedAccountsTimeTable/${millisecondDate}`, 'numberOfProcessedAccounts') || numberOfPreviousActiveAccounts
    dataList.push(numberOfActiveAccounts)
    i++
  }

  return {
    dateList: dateList.reverse(),
    dataList: dataList.reverse()
  }
}



// Recently Created Accounts Data Table
async function getRecentlyCreatedAccountsDataTable (statisticsOptions?: GetAccountNetworkStatisticsOptions) {

  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const accountIdVariableTable = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable/accountIdTimeTable/${dateNumber}`, 'accountIdVariableTable') || {}
    for (let accountId in accountIdVariableTable) {
      if (!accountIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }
      dateList.push(dateNumber)
      dataList.push(accountId)
    }
  }

  return {
    dateList,
    dataList
  }
}

// Recently Deleted Accounts Data Table
async function getRecentlyDeletedAccountsDataTable (statisticsOptions?: GetAccountNetworkStatisticsOptions) {

  let dateList = []
  let dataList = []

  let { startDate, endDate } = getUpdatedStartAndEndDate(statisticsOptions)

  // Start Date is assumed to be greater than end date

  const startDateMillisecondPartitionList = getMillisecondTimePartitionList2(startDate.toISOString().split('.')[0] + 'Z')
  const endDateMillisecondPartitionList = getMillisecondTimePartitionList2(endDate.toISOString().split('.')[0] + 'Z')

  for (let dateNumber = startDateMillisecondPartitionList.millisecondDay; dateNumber >= endDateMillisecondPartitionList.millisecondDay; dateNumber -= NUMBER_OF_MILLISECONDS_IN_A_DAY) {
    const accountIdVariableTable = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable/accountIdTimeTable/${dateNumber}`, 'accountIdVariableTable') || {}
    for (let accountId in accountIdVariableTable) {
      if (!accountIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }
      dateList.push(dateNumber)
      dataList.push(accountId)
    }
  }

  return {
    dateList,
    dataList
  }
}

// Miscellaneous function
function getUpdatedStartAndEndDate (statisticsOptions?: GetAccountNetworkStatisticsOptions) {

  let startDate = statisticsOptions ? statisticsOptions.startDate : undefined
  let endDate = statisticsOptions ? statisticsOptions.endDate : undefined

  startDate = startDate ? new Date(startDate) : new Date()
  startDate = new Date(startDate.toISOString().split('.')[0] + 'Z')
  endDate = endDate ? new Date(endDate) : new Date((new Date()).getTime() - MAXIMUM_NUMBER_OF_MILLISECONDS_AGO)
  endDate = new Date(endDate.toISOString().split('.')[0] + 'Z')

  // startDate = getUTCDate1(startDate)
  // endDate = getUTCDate1(endDate)

  // Check if endDate is greater than start date, then switch
  // positions to make sure startDate is greater than endDate
  let temporaryDatePlaceholder
  if (endDate.getTime() > startDate.getTime()) {
    temporaryDatePlaceholder = endDate
    startDate = endDate
    endDate = temporaryDatePlaceholder
  }

  return {
    startDate,
    endDate
  }
}

export {
  getDigitalCurrencyAccountNetworkStatistics1,
  getUpdatedStartAndEndDate
}


