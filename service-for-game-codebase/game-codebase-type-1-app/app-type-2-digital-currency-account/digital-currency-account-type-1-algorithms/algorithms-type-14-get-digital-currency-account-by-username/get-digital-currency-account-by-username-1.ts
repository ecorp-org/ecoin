
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function getDigitalCurrencyAccountByUsername1 (username: string) {
  const digitalCurrencyAccountId = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/accountUsernameVariableTable`, username)
  const digitalCurrencyAccount = await getItem1(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId)
  return digitalCurrencyAccount
}

export {
  getDigitalCurrencyAccountByUsername1
}

