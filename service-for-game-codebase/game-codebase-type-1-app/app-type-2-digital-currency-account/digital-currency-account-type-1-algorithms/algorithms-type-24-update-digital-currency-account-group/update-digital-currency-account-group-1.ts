
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function updateDigitalCurrencyAccountGroup1 (digitalCurrencyAccountId: string, groupId: string, propertyId: string, propertyValue: any) {

  if (!digitalCurrencyAccountId || !groupId || !propertyId || !propertyValue) {
    return
  }

  await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, groupId, { [propertyId]: propertyValue })

  const group = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, groupId)

  return group
}


export {
  updateDigitalCurrencyAccountGroup1
}


