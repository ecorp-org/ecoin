
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'
import { deleteItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-1'

import { createImageCompression1 } from '../../../../game-codebase-type-2-resource/resource-type-3-client-compress/client-compress-type-1-algorithms/algorithms-type-1-create-image-compression/create-image-compression-1'

import { updateItemSearchIndex1 } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-7-update-item-search-index/update-item-search-index-1'

import { algoliasearchVariableTree } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-4-variables/variables-1-algoliasearch'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'

import { DigitalCurrencyAccount } from '../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

// import { getUTCDate1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-3-get-utc-date/get-utc-date-1'
// import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


async function updateDigitalCurrencyAccount1 (digitalCurrencyAccountId: string, propertyId: string, propertyValue: string) {

  let digitalCurrencyAccount = await getItem1(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId) as DigitalCurrencyAccount
  if (!digitalCurrencyAccount) {
    return digitalCurrencyAccount
  }

  const digitalCurrencyAccountSearchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]

  // handle the case for 'accountUsername'
  // 'accountUsername' - check to make sure the username isn't taken
  if (propertyId === 'accountUsername') {
    const alreadyTakenUsername = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/accountUsernameVariableTable`, propertyValue) ? true : false

    if (alreadyTakenUsername) {
      throw Error('The account username is already taken')
    }

    // delete the old username
    await deleteItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/accountUsernameVariableTable`, digitalCurrencyAccount.accountUsername)

    // 'accountUsername' - update the username
    await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'accountUsernameVariableTable', { [propertyValue]: digitalCurrencyAccountId })

    // await updateItemSearchIndex1(digitalCurrencyAccountSearchIndexId, digitalCurrencyAccountId, { [`specialCharacterItem_${propertyId}`]: propertyValue.replaceAll('-', ' ').replaceAll('_', ' ') })
  }

  // handle the case for 'accountProfilePictureUrl'

  if (propertyId === 'accountProfilePictureUrl') {
    propertyValue = await createImageCompression1(propertyValue)
    const profilePictureUpdateInformation = await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, 'accountProfilePicture', propertyValue, true)
    propertyValue = profilePictureUpdateInformation
  }

  // update the account
  await updateItem1(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId, { [propertyId]: propertyValue })

  // update the network statistics

  // network statistic - updated accounts
  // const currentDate = getUTCDate1(new Date())
  // const { millisecondDay, millisecondHour, millisecondMinute } = getMillisecondTimePartitionList2((currentDate.toISOString().split('.')[0] + 'Z'))
  // await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/updatedAccountVariableTable/timeTable/${millisecondDay}/${millisecondHour}/${millisecondMinute}`, 'accountIdVariableTable', { [digitalCurrencyAccountId]: true })
  await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/updatedAccountVariableTable`, 'accountIdVariableTable', { [digitalCurrencyAccountId]: true })

  // update text search index
  await updateItemSearchIndex1(digitalCurrencyAccountSearchIndexId, digitalCurrencyAccountId, { [propertyId]: propertyValue })

  // return the digital currency account
  digitalCurrencyAccount = await getItem1(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId) as DigitalCurrencyAccount
  return digitalCurrencyAccount
}


export {
  updateDigitalCurrencyAccount1
}

