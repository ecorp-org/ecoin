


// import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
// import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'


import {
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE,
  PRODUCT_RESOURCE_SERVICE_TYPE,
  JOB_EMPLOYMENT_SERVICE_TYPE,
  CURRENCY_EXCHANGE_SERVICE_TYPE,
  DIGITAL_CURRENCY_ALTERNATIVE_SERVICE_TYPE
} from '../../../app-type-1-general/general-type-3-constants/constants-5-service-type'

import {
  UpdateDigitalCurrencyAccountNotificationsOptions
} from '../../digital-currency-account-type-2-data-structures/data-structures-type-7-account-notification/account-notification-2'

import {
  CREATED_TRANSACTIONS,
  RECEIVED_TRANSACTIONS
} from '../../digital-currency-account-type-3-constants/constants-3-account-notifications'

async function updateDigitalCurrencyAccountNotifications1 (digitalCurrencyAccountId: string, serviceType: string, updateOptions: UpdateDigitalCurrencyAccountNotificationsOptions) {
  switch (serviceType) {
    case DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE:
      return updateDigitalCurrencyAccountTransactionNotifications(digitalCurrencyAccountId, updateOptions)
      break
    default:
      throw Error(`Cannot get account notifications for unknown service type: ${serviceType}`)
  }
}

async function updateDigitalCurrencyAccountTransactionNotifications (digitalCurrencyAccountId: string, updateOptions: UpdateDigitalCurrencyAccountNotificationsOptions) {
  switch (updateOptions.serviceItemId!) {
    case CREATED_TRANSACTIONS:
      await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}`, 'createdTransactions', {
        'stoppedTransactionWithErrorTransactionIdVariableTable': {}
      })
      break
    case RECEIVED_TRANSACTIONS:
      await updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}`, 'receivedTransactions', {
        'dateLastCheckedNotifications': (new Date()).getTime(),
        'stoppedTransactionWithErrorTransactionIdVariableTable': {}
      })
      break
    default:
      throw Error(`Unknown service item id for updating account transaction notification: ${updateOptions.serviceItemId}`)
  }
}

export {
  updateDigitalCurrencyAccountNotifications1
}



