

import { createServiceAccountRecoveryConfirmation1 as createServiceAccountRecoveryConfirmationFromFirebase } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-15-create-service-account-recovery-confirmation/create-service-account-recovery-confirmation-1'

import { ServiceAccountRecoveryConfirmationInformation } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'

async function createServiceAccountRecoveryConfirmation1 (recoveryConfirmationInformation: ServiceAccountRecoveryConfirmationInformation) {
  await createServiceAccountRecoveryConfirmationFromFirebase(recoveryConfirmationInformation)
}

export {
  createServiceAccountRecoveryConfirmation1
}

