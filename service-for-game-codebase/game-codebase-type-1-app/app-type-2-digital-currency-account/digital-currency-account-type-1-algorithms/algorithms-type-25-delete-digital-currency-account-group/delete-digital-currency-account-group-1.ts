
import { deleteItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function deleteDigitalCurrencyAccountGroup1 (digitalCurrencyAccountId: string, groupId: string) {

  if (!digitalCurrencyAccountId || !groupId) {
    return
  }

  await deleteItem1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, groupId)
}


export {
  deleteDigitalCurrencyAccountGroup1
}


