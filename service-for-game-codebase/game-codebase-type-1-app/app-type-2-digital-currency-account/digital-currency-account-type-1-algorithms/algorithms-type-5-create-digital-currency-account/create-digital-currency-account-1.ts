
import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import { addItemToSearchIndex1 } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-1-add-item-to-search-index/add-item-to-search-index-1'

import { algoliasearchVariableTree } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-4-variables/variables-1-algoliasearch'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  BASIC_DESCRIPTION,
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../digital-currency-account-type-3-constants/constants-1-account-description'

import {
  DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE
} from '../../../app-type-1-general/general-type-3-constants/constants-5-service-type'

import { DigitalCurrencyAccount } from '../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
import { DigitalCurrencyAccountDescription } from '../../digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-1-basic-description'
import { DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription } from '../../digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-2-trasaction-resource-center-description'
import { DigitalCurrencyAccountDescriptionForProductResourceCenterDescription } from '../../digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-3-product-resource-center-description'
import { DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription } from '../../digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-4-job-employment-resource-center-description'
import { DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription } from '../../digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-5-currency-exchange-resource-center-description'
import { DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription } from '../../digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-6-digital-currency-alternative-resource-center-description'

import { DigitalCurrencyAccountSettings } from '../../digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import { createId1 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-2-create-id/create-id-1'

import { getServiceAccount1 } from '../algorithms-type-7-get-service-account/get-service-account-1'

import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'


const MAXIMUM_NUMBER_OF_ACCOUNTS_ALLOWED = 3

async function createDigitalCurrencyAccount1 (serviceAccountId: string) {

  // get number of digital currency
  // accounts created by this accounts

  const serviceAccount = await getServiceAccount1(serviceAccountId)

  if (!serviceAccount) {
    throw Error(`Service Account Required to Create Digital Currency Account`)
  }

  if (serviceAccount.numberOfDigitalCurrencyAccounts >= MAXIMUM_NUMBER_OF_ACCOUNTS_ALLOWED) {
    throw Error(`Cannot create more than ${MAXIMUM_NUMBER_OF_ACCOUNTS_ALLOWED} accounts`)
  }

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const currentDateTimePartition = getMillisecondTimePartitionList2(currentDate)

  // get total number of accounts already created, else say 1
  const numberOfAccountsCreated = ((await getItem1(DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID, 'numberOfAccountsCreated')) || 0) + (Object.keys(await getItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountNetworkStatisticsDataQueue/numberOfAccountsCreated`, 'dataQueue') || {}).length)

  // create digital currency account object instance
  const digitalCurrencyAccount = new DigitalCurrencyAccount()
  const digitalCurrencyAccounDescription = new DigitalCurrencyAccountDescription()
  const digitalCurrencyAccounDescriptionForTransactionResourceCenterDescription = new DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription()
  const digitalCurrencyAccounDescriptionForProductResourceCenterDescription = new DigitalCurrencyAccountDescriptionForProductResourceCenterDescription()
  const digitalCurrencyAccounDescriptionForJobEmploymentResourceCenterDescription = new DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription()
  const digitalCurrencyAccounDescriptionForCurrencyExchangeResourceCenterDescription = new DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription()
  const digitalCurrencyAccounDescriptionForDigitalCurrencyAlternativeResourceCenterDescription = new DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription()

  digitalCurrencyAccount.accountId = createId1()
  digitalCurrencyAccount.accountUsername = digitalCurrencyAccount.accountId
  digitalCurrencyAccount.accountName = `Customer #${numberOfAccountsCreated + 1}`
  digitalCurrencyAccount.firebaseAuthUid = serviceAccountId

  // create digital currency account settings
  const digitalCurrencyAccountSettings = new DigitalCurrencyAccountSettings()

  // publish service account statistics

  await updateItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}/${serviceAccountId}`, 'digitalCurrencyAccountIdTable', { [digitalCurrencyAccount.accountId]: true }) // .catch((_errorMessage: any) => { /* */ })
  await updateItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId, { numberOfDigitalCurrencyAccounts: serviceAccount.numberOfDigitalCurrencyAccounts + 1 }) // .catch((_errorMessage: any) => { /* */ })
  await updateItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId, { 'activeDigitalCurrencyAccountId': digitalCurrencyAccount.accountId }) // .catch((_errorMessage: any) => { /* */ })

  // publish digital currency account
  await createItem1(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccount.accountId, digitalCurrencyAccount)

  // publish digital currency account description
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccount.accountId}`, BASIC_DESCRIPTION, digitalCurrencyAccounDescription) // .catch((_errorMessage: any) => { /* */ })
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccount.accountId}`, TRANSACTION_RESOURCE_CENTER_DESCRIPTION, digitalCurrencyAccounDescriptionForTransactionResourceCenterDescription) // .catch((_errorMessage: any) => { /* */ })
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccount.accountId}`, PRODUCT_RESOURCE_CENTER_DESCRIPTION, digitalCurrencyAccounDescriptionForProductResourceCenterDescription) // .catch((_errorMessage: any) => { /* */ })
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccount.accountId}`, JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION, digitalCurrencyAccounDescriptionForJobEmploymentResourceCenterDescription) // .catch((_errorMessage: any) => { /* */ })
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccount.accountId}`, CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION, digitalCurrencyAccounDescriptionForCurrencyExchangeResourceCenterDescription) // .catch((_errorMessage: any) => { /* */ })
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccount.accountId}`, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION, digitalCurrencyAccounDescriptionForDigitalCurrencyAlternativeResourceCenterDescription) // .catch((_errorMessage: any) => { /* */ })

  // publish digital currency account settings
  createItem1(DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID, digitalCurrencyAccount.accountId, digitalCurrencyAccountSettings).catch((_errorMessage: any) => { /* */ })

  // publish digital currency account statistics

  // statistics - username
  createItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}`, 'accountUsernameVariableTable', { [digitalCurrencyAccount.accountUsername]: digitalCurrencyAccount.accountId }).catch((_errorMessage: any) => { /* */ })

  // publish network statistics

  // Account Network Statistics - Created Account
  createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountNetworkStatisticsDataQueue/numberOfAccountsCreated/dataQueue`, `${digitalCurrencyAccount.accountId}`, { accountId: digitalCurrencyAccount.accountId, dateCreated: currentDate }).catch((_errorMessage: any) => { /* */ })
  createItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/timeTable/accountIdTimeTable/${currentDateTimePartition.millisecondDay}/accountIdVariableTable`, digitalCurrencyAccount.accountId, currentDate).catch((_errorMessage: any) => { /* */ })
  createItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/createdAccountVariableTable/accountIdVariableTable`, digitalCurrencyAccount.accountId, currentDate, false, true).catch((_errorMessage: any) => { /* */ })

  // account notifications
  updateItem1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccount.accountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}`, 'receivedTransactions', { 'dateLastCheckedNotifications': digitalCurrencyAccount.dateAccountCreated }).catch((_errorMessage: any) => { /* */ })

  // add text search index
  const digitalCurrencyAccountSearchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]
  addItemToSearchIndex1(digitalCurrencyAccountSearchIndexId, digitalCurrencyAccount.accountId, {
    ...digitalCurrencyAccount,
    [BASIC_DESCRIPTION]: digitalCurrencyAccounDescription,
    [TRANSACTION_RESOURCE_CENTER_DESCRIPTION]: digitalCurrencyAccounDescriptionForTransactionResourceCenterDescription,
    [PRODUCT_RESOURCE_CENTER_DESCRIPTION]: digitalCurrencyAccounDescriptionForProductResourceCenterDescription,
    [JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION]: digitalCurrencyAccounDescriptionForJobEmploymentResourceCenterDescription,
    [CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION]: digitalCurrencyAccounDescriptionForCurrencyExchangeResourceCenterDescription,
    [DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION]: digitalCurrencyAccounDescriptionForDigitalCurrencyAlternativeResourceCenterDescription
  }).catch((_errorMessage: any) => { /* */ })

  return digitalCurrencyAccount
}


export {
  createDigitalCurrencyAccount1
}



