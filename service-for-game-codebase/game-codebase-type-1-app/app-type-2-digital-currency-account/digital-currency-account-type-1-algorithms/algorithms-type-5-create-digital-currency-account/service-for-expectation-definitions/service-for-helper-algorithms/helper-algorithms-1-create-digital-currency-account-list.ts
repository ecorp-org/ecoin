


import { createServiceAccount1 } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-29-create-service-account/create-service-account-1'
import { createDigitalCurrencyAccount1 } from '../../../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-5-create-digital-currency-account/create-digital-currency-account-1'
import { getServiceAccount1 } from '../../../algorithms-type-7-get-service-account/get-service-account-1'

import { ServiceAccount } from '../../../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'
import { DigitalCurrencyAccount } from '../../../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { updateItem2FirebaseAdmin } from '../../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID } from '../../../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

class CreateDigitalCurrencyAccountListOptions {
  numberOfAccounts?: number

  minimumAmountOfCurrency?: number
  maximumAmountOfCurrency?: number
}


async function createDigitalCurrencyAccountList (options?: CreateDigitalCurrencyAccountListOptions) {

  if (!options) {
    options = new CreateDigitalCurrencyAccountListOptions()
  }

  options.numberOfAccounts = options.numberOfAccounts || 1
  options.minimumAmountOfCurrency = options.minimumAmountOfCurrency || 0
  options.maximumAmountOfCurrency = options.maximumAmountOfCurrency || 0


  let serviceAccountList: ServiceAccount[] = []
  let digitalCurrencyAccountList: DigitalCurrencyAccount[] = []

  for (let i = 0; i < options.numberOfAccounts; i++) {
    let serviceAccount = await createServiceAccount1({
      emailAddress: 'hello-world@hello-world.com',
      password: 'no-password'
    })
    const digitalCurrencyAccount = await createDigitalCurrencyAccount1(serviceAccount.serviceAccountId)

    const digitalCurrencyAmount = options.minimumAmountOfCurrency + Math.floor(Math.random() * (options.maximumAmountOfCurrency - options.minimumAmountOfCurrency))
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccount.accountId, { 'accountCurrencyAmount': digitalCurrencyAmount })

    serviceAccount = await getServiceAccount1(serviceAccount.serviceAccountId)
    serviceAccountList.push(serviceAccount)
    digitalCurrencyAccountList.push(digitalCurrencyAccount)
  }

  return {
    serviceAccountList,
    digitalCurrencyAccountList
  }
}


export {
  createDigitalCurrencyAccountList,
  CreateDigitalCurrencyAccountListOptions
}



