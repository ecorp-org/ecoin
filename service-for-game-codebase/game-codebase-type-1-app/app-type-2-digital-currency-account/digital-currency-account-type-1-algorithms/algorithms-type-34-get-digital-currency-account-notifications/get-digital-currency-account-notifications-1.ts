

import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'

import { getDigitalCurrencyAccountById1 } from '../algorithms-type-15-get-digital-currency-account-by-id/get-digital-currency-account-by-id-1'
import { getDigitalCurrencyAccountTransactionStatistics1 } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-11-get-digital-currency-account-transaction-statistics/get-digital-currency-account-transaction-statistics-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE,
  PRODUCT_RESOURCE_SERVICE_TYPE,
  JOB_EMPLOYMENT_SERVICE_TYPE,
  CURRENCY_EXCHANGE_SERVICE_TYPE,
  DIGITAL_CURRENCY_ALTERNATIVE_SERVICE_TYPE
} from '../../../app-type-1-general/general-type-3-constants/constants-5-service-type'

import {
  RECEIVED_TRANSACTION_DATA_TABLE
} from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-3-constants/constants-3-transaction-account-statistics'

import {
  DigitalCurrencyAccountNotification,
  NOTIFICATION_TYPE_SUCCESS,
  NOTIFICATION_TYPE_WARNING,
  NOTIFICATION_TYPE_FAILURE
} from '../../digital-currency-account-type-2-data-structures/data-structures-type-7-account-notification/account-notification-1'

import {
  numberOfSpecialTransactions
} from '../../../../../service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/project-variables-4-digital-currency'


async function getDigitalCurrencyAccountNotifications1 (digitalCurrencyAccountId: string, serviceType: string) {
  switch (serviceType) {
    case DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE:
      return getDigitalCurrencyAccountTransactionNotifications(digitalCurrencyAccountId)
      break
    default:
      throw Error(`Cannot get account notifications for unknown service type: ${serviceType}`)
  }
}

async function getDigitalCurrencyAccountTransactionNotifications (digitalCurrencyAccountId: string) {

  let notificationList: DigitalCurrencyAccountNotification[] = []

  const digitalCurrencyAccount = await getDigitalCurrencyAccountById1(digitalCurrencyAccountId)

  if (!digitalCurrencyAccount) {
    return notificationList
  }

  // Received Transaction Notifications
  const dateLastCheckedReceivedTransactions = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}/receivedTransactions`, 'dateLastCheckedNotifications') || 0

  const accountReceivedTransactionStatistics = await getDigitalCurrencyAccountTransactionStatistics1(RECEIVED_TRANSACTION_DATA_TABLE, {
    digitalCurrencyAccountId,
    startDate: dateLastCheckedReceivedTransactions
  })

  const numberOfSpecialTransactionReceived: number = dateLastCheckedReceivedTransactions === digitalCurrencyAccount.dateAccountCreated ? numberOfSpecialTransactions : 0

  let newlyReceivedTransactionIdList = (accountReceivedTransactionStatistics.dataList || []).filter((_dataItem: any, index: number) => {
    return accountReceivedTransactionStatistics.itemDataList[index].dateCreated > dateLastCheckedReceivedTransactions
  })

  if (newlyReceivedTransactionIdList.length > 0 || numberOfSpecialTransactionReceived) {
    const numberOfNewTransactionsReceived = newlyReceivedTransactionIdList.length + numberOfSpecialTransactionReceived
    notificationList.push({
      notificationType: NOTIFICATION_TYPE_SUCCESS,
      notificationMessage: `${numberOfNewTransactionsReceived} New Transaction${numberOfNewTransactionsReceived !== 1 ? 's' : ''} Received`
    })
  }


  // Created Transaction Stopped With Error Notifications
  const stoppedCreatedTransactionWithErrorTransactionIdVariableTable = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}/createdTransactions`, 'stoppedTransactionWithErrorTransactionIdVariableTable') || {}
  const numberOfStoppedCreatedTransactions = Object.keys(stoppedCreatedTransactionWithErrorTransactionIdVariableTable).length
  if (numberOfStoppedCreatedTransactions) {
    notificationList.push({
      notificationType: NOTIFICATION_TYPE_FAILURE,
      notificationMessage: `${numberOfStoppedCreatedTransactions} Sent Transaction${numberOfStoppedCreatedTransactions !== 1 ? 's' : ''} Stopped With Error`
    })
  }

  // Received Transaction Stopped With Error Notifications
  const stoppedReceivedTransactionWithErrorTransactionIdVariableTable = await getItem1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}/receivedTransactions`, 'stoppedTransactionWithErrorTransactionIdVariableTable') || {}
  const numberOfStoppedReceivedTransactions = Object.keys(stoppedReceivedTransactionWithErrorTransactionIdVariableTable).length
  if (numberOfStoppedReceivedTransactions) {
    notificationList.push({
      notificationType: NOTIFICATION_TYPE_FAILURE,
      notificationMessage: `${numberOfStoppedReceivedTransactions} Received Transaction${numberOfStoppedReceivedTransactions !== 1 ? 's' : ''} Stopped With Error`
    })
  }

  return notificationList
}


export {
  getDigitalCurrencyAccountNotifications1
}

