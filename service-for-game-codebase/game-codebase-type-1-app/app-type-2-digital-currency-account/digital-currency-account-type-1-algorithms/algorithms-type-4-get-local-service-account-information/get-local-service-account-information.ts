

import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'

import { getServiceAccount1 as getServiceAccountByFirebase } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-5-get-service-account/get-service-account-1'

import {
  SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'

import { createServiceAccount1 } from '../algorithms-type-29-create-service-account/create-service-account-1'


async function getLocalServiceAccountInformation1 () {
  const serviceAccountInformation = getServiceAccountByFirebase()

  let serviceAccount = await getItem1(SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID, serviceAccountInformation!.uid) as ServiceAccount
  if (!serviceAccount) {
    serviceAccount = await createServiceAccount1({})
  }

  return serviceAccount
}

export {
  getLocalServiceAccountInformation1
}



