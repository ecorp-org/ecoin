
import { createItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-1'
import { deleteItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-1'
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'
import { updateItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-1'
import { getItemIdList1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-12-get-item-id-list/get-item-id-list-1'

import { removeItemFromSearchIndex1 } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-5-remove-item-from-search-index/remove-item-from-search-index-1'

import { algoliasearchVariableTree } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-4-variables/variables-1-algoliasearch'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'
import { DigitalCurrencyAccount } from '../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { updateDigitalCurrencyAccountDescription1 } from '../algorithms-type-11-update-digital-currency-account-description/update-digital-currency-account-description-1'

import {
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../digital-currency-account-type-3-constants/constants-1-account-description'

import { deleteDigitalCurrencyTransactionList1 } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-2-delete-digital-currency-transaction/delete-digital-currency-transaction-1'
import { getDigitalCurrencyTransactionById1 } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-1'

async function deleteDigitalCurrencyAccount1 (serviceAccountId: string, digitalCurrencyAccountId: string) {

  // check if digital currency account is there
  const digitalCurrencyAccount = await getItem1(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId) as DigitalCurrencyAccount

  if (!digitalCurrencyAccount) {
    return
  }

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const currentDateTimePartition = getMillisecondTimePartitionList2(currentDate)

  // remove old values
  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, TRANSACTION_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, TRANSACTION_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, PRODUCT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, PRODUCT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])
  await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION, 'doesRepresentDigitalCurrencyAlternative', false)

  // Account Network Statistics - Deleted Accounts
  await createItem1(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountNetworkStatisticsDataQueue/numberOfAccountsDeleted/dataQueue`, `${digitalCurrencyAccount.accountId}`, { accountId: digitalCurrencyAccount.accountId, dateCreated: currentDate }, false, true)
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable/accountIdTimeTable/${currentDateTimePartition.millisecondDay}/accountIdVariableTable`, digitalCurrencyAccount.accountId, currentDate)
  await createItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/accountIdVariableTable`, digitalCurrencyAccount.accountId, currentDate, false, true)

  // remove account profile picture
  try {
    await deleteItem1(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, 'accountProfilePicture', true)
  } catch (_errorMessage) {
    //
  }

  // remove account description
  await deleteItem1(DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId)

  // remove account settings
  await deleteItem1(DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID, digitalCurrencyAccountId)

  // remove username
  await deleteItem1(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/accountUsernameVariableTable`, digitalCurrencyAccount.accountUsername)

  // remove groups
  await deleteItem1(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId)

  // remove notifications
  await deleteItem1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId)

  // remove text search index
  const digitalCurrencyAccountSearchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]
  await removeItemFromSearchIndex1(digitalCurrencyAccountSearchIndexId, digitalCurrencyAccountId)

  // update service account statistics
  await deleteItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}/${serviceAccountId}/digitalCurrencyAccountIdTable`, digitalCurrencyAccountId)
  let serviceAccount = await getItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId) as ServiceAccount
  await updateItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId, { 'numberOfDigitalCurrencyAccounts': Object.keys(serviceAccount.digitalCurrencyAccountIdTable).length })

  // set new active account
  const newActiveDigitalCurrencyAccountId = Object.keys(serviceAccount.digitalCurrencyAccountIdTable || {})[0] || ''
  await updateItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId, { 'activeDigitalCurrencyAccountId': newActiveDigitalCurrencyAccountId })

  // set received transactions to show account is deleted

  updateDigitalCurrencyReceivedTransactionList(digitalCurrencyAccountId).catch((_errorMessage: any) => { /* */ })
  deleteDigitalCurrencyTransactionList1(digitalCurrencyAccountId).catch((_errorMessage: any) => { /* */ })

  // return service account
  serviceAccount = await getItem1(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId) as ServiceAccount

  return serviceAccount
}

async function updateDigitalCurrencyReceivedTransactionList (digitalCurrencyAccountId: string) {
  const receivedTransactionIdList = await getItemIdList1(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/receivedTransactionVariableTable/transactionIdVariableTable`)
  for (let i = 0; i < receivedTransactionIdList.length; i++) {
    const transactionId = receivedTransactionIdList[i]
    const digitalCurrencyTransaction = await getDigitalCurrencyTransactionById1(transactionId)
    if (!digitalCurrencyTransaction || !digitalCurrencyTransaction.accountRecipientIdVariableTable[digitalCurrencyAccountId]) {
      continue
    }
    await updateItem1(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}/${transactionId}/accountRecipientIdVariableTable`, digitalCurrencyAccountId, { 'dateAccountDeleted': (new Date()).getTime() })
  }
}

export {
  deleteDigitalCurrencyAccount1
}

