


import { getItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'
import { createItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-6-create-item/create-item-2-firebase-admin'
import { updateItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'
import { getItemIdList2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-12-get-item-id-list/get-item-id-list-2-firebase-admin'

import { removeItemFromSearchIndex1 } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-5-remove-item-from-search-index/remove-item-from-search-index-1'

import { algoliasearchVariableTree } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-4-variables/variables-1-algoliasearch'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'

import {
  SERVICE_DATA_QUEUE_STORE_ID,
  SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID,
  DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import { ServiceAccount } from '../../../app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'
import { DigitalCurrencyAccount } from '../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { getMillisecondTimePartitionList2 } from '../../../app-type-1-general/general-type-1-algorithms/algorithms-type-4-get-millisecond-time-partition-list/get-millisecond-time-partition-list-1'

import { updateDigitalCurrencyAccountDescription2FirebaseAdmin } from '../algorithms-type-11-update-digital-currency-account-description/update-digital-currency-account-description-2-firebase-admin'

import {
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../digital-currency-account-type-3-constants/constants-1-account-description'

import { deleteDigitalCurrencyTransactionList2FirebaseAdmin } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-2-delete-digital-currency-transaction/delete-digital-currency-transaction-2-firebase-admin'
import { getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-2-firebase-admin'

async function deleteDigitalCurrencyAccount2FirebaseAdmin (digitalCurrencyAccountId: string, forceDelete?: boolean) {

  // check if digital currency account is there
  const digitalCurrencyAccount = await getItem2FirebaseAdmin(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId) as DigitalCurrencyAccount
  if (!digitalCurrencyAccount) {
    return
  }

  let serviceAccountId: string = digitalCurrencyAccount.firebaseAuthUid || ''

  const currentDate = (new Date()).toISOString().split('.')[0] + 'Z'
  const currentDateTimePartition = getMillisecondTimePartitionList2(currentDate)

  // remove old values
  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, TRANSACTION_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, TRANSACTION_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, PRODUCT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, PRODUCT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])

  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotions', false)
  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION, 'willingToReceivePromotionsOfType', [])
  await updateDigitalCurrencyAccountDescription2FirebaseAdmin(digitalCurrencyAccountId, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION, 'doesRepresentDigitalCurrencyAlternative', false)

  // Account Network Statistics - Deleted Accounts
  if (!forceDelete) {
    await createItem2FirebaseAdmin(`${SERVICE_DATA_QUEUE_STORE_ID}/initializeAccountNetworkStatisticsDataQueue/numberOfAccountsDeleted/dataQueue`, `${digitalCurrencyAccount.accountId}`, { accountId: digitalCurrencyAccount.accountId, dateCreated: currentDate }, false, true)
    await createItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/timeTable/accountIdTimeTable/${currentDateTimePartition.millisecondDay}/accountIdVariableTable`, digitalCurrencyAccount.accountId, currentDate)
    await createItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/deletedAccountVariableTable/accountIdVariableTable`, digitalCurrencyAccount.accountId, currentDate, false, true)
  }

  // remove account profile picture
  try {
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, 'accountProfilePicture', true)
  } catch (_errorMessage: any) {
    //
  }

  // remove account description
  await deleteItem2FirebaseAdmin(DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId)

  // remove account settings
  await deleteItem2FirebaseAdmin(DIGITAL_CURRENCY_ACCOUNT_SETTINGS_STORE_ID, digitalCurrencyAccountId)

  // remove username
  await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NETWORK_STATISTICS_STORE_ID}/accountUsernameVariableTable`, digitalCurrencyAccount.accountUsername)

  // remove groups
  await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_GROUPS_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId)

  // remove notifications
  await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId)

  // remove text search index
  const digitalCurrencyAccountSearchIndexId = algoliasearchVariableTree.itemSearchIndexNameToIndexIdMap[DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]
  await removeItemFromSearchIndex1(digitalCurrencyAccountSearchIndexId, digitalCurrencyAccountId)

  // update service account statistics
  await deleteItem2FirebaseAdmin(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}/${serviceAccountId}/digitalCurrencyAccountIdTable`, digitalCurrencyAccountId)
  let serviceAccount = await getItem2FirebaseAdmin(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId) as ServiceAccount
  await updateItem2FirebaseAdmin(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId, { 'numberOfDigitalCurrencyAccounts': Object.keys(serviceAccount.digitalCurrencyAccountIdTable).length })

  // set new active account
  const newActiveDigitalCurrencyAccountId = Object.keys(serviceAccount.digitalCurrencyAccountIdTable || {})[0] || ''
  await updateItem2FirebaseAdmin(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId, { 'activeDigitalCurrencyAccountId': newActiveDigitalCurrencyAccountId })

  const promiseToUpdateDigitalCurrencyReceivedTransactionList = updateDigitalCurrencyReceivedTransactionList(digitalCurrencyAccountId)
  const promiseToDeleteTransactionList = deleteDigitalCurrencyTransactionList2FirebaseAdmin(digitalCurrencyAccountId)

  if (forceDelete) {
    await promiseToUpdateDigitalCurrencyReceivedTransactionList
    await promiseToDeleteTransactionList
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId)
    await deleteItem2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}`, digitalCurrencyAccountId)
  }

  // return service account
  serviceAccount = await getItem2FirebaseAdmin(`${SERVICE_ACCOUNT_VARIABLE_TABLE_STORE_ID}`, serviceAccountId) as ServiceAccount

  return serviceAccount
}

async function updateDigitalCurrencyReceivedTransactionList (digitalCurrencyAccountId: string) {
  // set received transactions to show account is deleted
  const receivedTransactionIdList = await getItemIdList2FirebaseAdmin(`${DIGITAL_CURRENCY_ACCOUNT_TRANSACTION_STATISTICS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/receivedTransactionVariableTable/transactionIdVariableTable`)
  for (let i = 0; i < receivedTransactionIdList.length; i++) {
    const transactionId = receivedTransactionIdList[i]
    const digitalCurrencyTransaction = await getDigitalCurrencyTransactionById2FirebaseAdminAlgorithm.function(transactionId)
    if (!digitalCurrencyTransaction || !digitalCurrencyTransaction.accountRecipientIdVariableTable[digitalCurrencyAccountId]) {
      continue
    }
    await updateItem2FirebaseAdmin(`${DIGITAL_CURRENCY_TRANSACTION_VARIABLE_TABLE_STORE_ID}/${transactionId}/accountRecipientIdVariableTable`, digitalCurrencyAccountId, { 'dateAccountDeleted': (new Date()).getTime() })
  }
}

export {
  deleteDigitalCurrencyAccount2FirebaseAdmin
}



