
import { addItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-10-add-item-event-listener/add-item-event-listener-1'

import { getDigitalCurrencyAccountNotifications1 } from '../algorithms-type-34-get-digital-currency-account-notifications/get-digital-currency-account-notifications-1'

import { addEventListenerToDigitalCurrencyAccountTransactionStatistics1 } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-5-add-event-listener-to-digital-currency-account-transaction-statistics/add-event-listener-to-digital-currency-account-transaction-statistics-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE // ,
  // PRODUCT_RESOURCE_SERVICE_TYPE,
  // JOB_EMPLOYMENT_SERVICE_TYPE,
  // CURRENCY_EXCHANGE_SERVICE_TYPE,
  // DIGITAL_CURRENCY_ALTERNATIVE_SERVICE_TYPE
} from '../../../app-type-1-general/general-type-3-constants/constants-5-service-type'

import {
  RECEIVED_TRANSACTION_DATA_TABLE
} from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-3-constants/constants-3-transaction-account-statistics'

import {
  RECEIVED_TRANSACTIONS
} from '../../digital-currency-account-type-3-constants/constants-3-account-notifications'

import {
  EVENT_ID_UPDATE
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'

async function addEventListenerToDigitalCurrencyAccountNotifications1 (digitalCurrencyAccountId: string, serviceType: string, eventListenerCallbackFunction?: any) {
  switch (serviceType) {
    case DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE:
      return addEventListenerToDigitalCurrencyAccountTransactionNotifications(digitalCurrencyAccountId, serviceType, eventListenerCallbackFunction)
      break
    default:
      throw Error(`Cannot get account notifications for unknown service type: ${serviceType}`)
  }
}

async function addEventListenerToDigitalCurrencyAccountTransactionNotifications (digitalCurrencyAccountId: string, serviceType: string, eventListenerCallbackFunction?: any) {
  let eventListenerIdMap: { [notificationItemId: string]: string } = {}

  // Received Transaction
  const receivedTransactionDateLastCheckedEventListenerId = await addItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}`, `${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}`, EVENT_ID_UPDATE, async () => {
    const notificationList = await getDigitalCurrencyAccountNotifications1(digitalCurrencyAccountId, serviceType)

    if (eventListenerCallbackFunction) {
      eventListenerCallbackFunction(notificationList)
    }
  })
  const recievedTransactionEventListenerId = await addEventListenerToDigitalCurrencyAccountTransactionStatistics1(RECEIVED_TRANSACTION_DATA_TABLE, async () => {
    const notificationList = await getDigitalCurrencyAccountNotifications1(digitalCurrencyAccountId, serviceType)

    if (eventListenerCallbackFunction) {
      eventListenerCallbackFunction(notificationList)
    }
  }, {
    digitalCurrencyAccountId
  })

  eventListenerIdMap[RECEIVED_TRANSACTIONS] = receivedTransactionDateLastCheckedEventListenerId || ''
  eventListenerIdMap[RECEIVED_TRANSACTION_DATA_TABLE] = recievedTransactionEventListenerId || ''

  return eventListenerIdMap
}


export {
  addEventListenerToDigitalCurrencyAccountNotifications1
}



