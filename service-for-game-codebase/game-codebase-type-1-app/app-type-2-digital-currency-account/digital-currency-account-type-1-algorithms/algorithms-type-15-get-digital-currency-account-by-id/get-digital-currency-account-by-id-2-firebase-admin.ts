
import { getItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'

import {
  DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'

import {
  DigitalCurrencyAccount
} from '../../digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

async function getDigitalCurrencyAccountById2FirebaseAdmin (digitalCurrencyAccountId: string): Promise<DigitalCurrencyAccount> {
  const digitalCurrencyAccount = await getItem2FirebaseAdmin(DIGITAL_CURRENCY_ACCOUNT_VARIABLE_TABLE_STORE_ID, digitalCurrencyAccountId) as DigitalCurrencyAccount
  return digitalCurrencyAccount
}

export {
  getDigitalCurrencyAccountById2FirebaseAdmin
}

