
import { removeItemEventListener1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-11-remove-item-event-listener/remove-item-event-listener-1'

import { removeEventListenerFromDigitalCurrencyAccountTransactionStatistics1 } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-6-remove-event-listener-from-digital-currency-account-transaction-statistics/remove-event-listener-from-digital-currency-account-transaction-statistics-1'

import {
  DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


import {
  DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE // ,
  // PRODUCT_RESOURCE_SERVICE_TYPE,
  // JOB_EMPLOYMENT_SERVICE_TYPE,
  // CURRENCY_EXCHANGE_SERVICE_TYPE,
  // DIGITAL_CURRENCY_ALTERNATIVE_SERVICE_TYPE
} from '../../../app-type-1-general/general-type-3-constants/constants-5-service-type'

import {
  RECEIVED_TRANSACTION_DATA_TABLE
} from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-3-constants/constants-3-transaction-account-statistics'

import {
  RECEIVED_TRANSACTIONS
} from '../../digital-currency-account-type-3-constants/constants-3-account-notifications'

import {
  EVENT_ID_UPDATE
} from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-3-constants/constants-1-firebase-events'


async function removeEventListenerFromDigitalCurrencyAccountNotifications1 (digitalCurrencyAccountId: string, serviceType: string, eventListenerIdMap: { [notificationItemId: string]: string }) {
  switch (serviceType) {
    case DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE:
      return removeEventListenerFromDigitalCurrencyAccountTransactionNotifications(digitalCurrencyAccountId, eventListenerIdMap)
      break
    default:
      throw Error(`Cannot get account notifications for unknown service type: ${serviceType}`)
  }
}

async function removeEventListenerFromDigitalCurrencyAccountTransactionNotifications (digitalCurrencyAccountId: string, eventListenerIdMap: { [notificationItemId: string]: string }) {
  if (!eventListenerIdMap) {
    return
  }

  // Received Transactions
  const receivedTransactionDateLastCheckedEventListenerId = eventListenerIdMap[RECEIVED_TRANSACTIONS]
  if (receivedTransactionDateLastCheckedEventListenerId) {
    removeItemEventListener1(`${DIGITAL_CURRENCY_ACCOUNT_NOTIFICATIONS_VARIABLE_TABLE_STORE_ID}/${digitalCurrencyAccountId}/${DIGITAL_CURRENCY_TRANSACTION_SERVICE_TYPE}/receivedTransactions`, 'dateLastCheckedNotifications', EVENT_ID_UPDATE, receivedTransactionDateLastCheckedEventListenerId)
  }

  const receivedTransactionEventListenerId = eventListenerIdMap[RECEIVED_TRANSACTION_DATA_TABLE]
  if (receivedTransactionEventListenerId) {
    await removeEventListenerFromDigitalCurrencyAccountTransactionStatistics1(RECEIVED_TRANSACTION_DATA_TABLE, receivedTransactionEventListenerId, {
      digitalCurrencyAccountId
    })
  }
}

export {
  removeEventListenerFromDigitalCurrencyAccountNotifications1
}


