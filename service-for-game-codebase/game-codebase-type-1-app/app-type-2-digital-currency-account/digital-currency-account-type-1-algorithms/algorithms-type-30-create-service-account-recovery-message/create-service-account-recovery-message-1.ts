
import { createServiceAccountRecoveryMessage1 as createServiceAccountRecoveryMessageFromFirebase } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-14-create-service-account-recovery-message/create-service-account-recovery-message-1'
import { ServiceAccountRecoveryInformation } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'

async function createServiceAccountRecoveryMessage1 (recoveryInformation: ServiceAccountRecoveryInformation) {
  await createServiceAccountRecoveryMessageFromFirebase(recoveryInformation)
}

export {
  createServiceAccountRecoveryMessage1
}


