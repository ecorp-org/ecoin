


import { updateItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-8-update-item/update-item-2-firebase-admin'

import {
  SERVICE_DAEMON_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function updateServiceInformationByDaemonStatus2FirebaseAdmin (daemonId: string, propertyId: string, propertyValue: any) {
  if (!daemonId || !propertyId) {
    throw Error(`Cannot update daemon status without daemon id, property id or property value`)
  }
  return updateItem2FirebaseAdmin(SERVICE_DAEMON_VARIABLE_TABLE_STORE_ID, daemonId, { [propertyId]: propertyValue })
}

export {
  updateServiceInformationByDaemonStatus2FirebaseAdmin
}

