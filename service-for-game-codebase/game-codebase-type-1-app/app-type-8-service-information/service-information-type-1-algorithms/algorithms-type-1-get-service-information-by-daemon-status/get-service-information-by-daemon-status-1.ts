
import { getItem1 } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-1'

import {
  SERVICE_DAEMON_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function getServiceInformationByDaemonStatus1 (daemonId: string) {
  return getItem1(SERVICE_DAEMON_VARIABLE_TABLE_STORE_ID, daemonId)
}

export {
  getServiceInformationByDaemonStatus1
}
