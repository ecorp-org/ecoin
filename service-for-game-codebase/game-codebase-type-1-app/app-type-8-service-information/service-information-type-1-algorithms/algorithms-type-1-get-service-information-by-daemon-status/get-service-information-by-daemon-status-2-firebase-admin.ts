

import { getItem2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-7-get-item/get-item-2-firebase-admin'

import {
  SERVICE_DAEMON_VARIABLE_TABLE_STORE_ID
} from '../../../app-type-1-general/general-type-3-constants/constants-1-firebase-store-id'


async function getServiceInformationByDaemonStatus2FirebaseAdmin (daemonId: string) {
  return getItem2FirebaseAdmin(SERVICE_DAEMON_VARIABLE_TABLE_STORE_ID, daemonId)
}

export {
  getServiceInformationByDaemonStatus2FirebaseAdmin
}

