

const GENERAL_SECRET_VARIABLES = (JSON.parse(process.env.GENERAL_SECRET_VARIABLES || '{}') || {})

const administratorVariables = {
  ADMINISTRATOR_ACCOUNT_ID: GENERAL_SECRET_VARIABLES.ADMINISTRATOR_ACCOUNT_ID || 'administrator'
}


export {
  GENERAL_SECRET_VARIABLES,
  administratorVariables
}
