

class ServiceAccount {
  serviceAccountId: string = ''

  activeDigitalCurrencyAccountId: string = ''
  digitalCurrencyAccountIdTable: { [accountId: string]: boolean } = {}
  numberOfDigitalCurrencyAccounts: number = 0

  isAnonymousAccount: boolean = false
  isEmailVerified: boolean = false
  emailAddress: string = ''
}


export {
  ServiceAccount
}






