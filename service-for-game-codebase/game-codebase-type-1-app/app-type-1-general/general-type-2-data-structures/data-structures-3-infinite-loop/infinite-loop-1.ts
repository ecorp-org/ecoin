

class InfiniteLoopProgram {
  infiniteLoopId: string = ''
  isInfiniteLoopStarted: boolean = false
  dateStarted?: Date

  iterationIndex: number = 0

  millisecondRegularDelayTime: number = 0
  millisecondDelayToRestartSetInterval: number = 0

  specifiedStartDate: Date = new Date()
  millisecondTimeLeftToStart: number = 0

  timeoutId: any = null
  intervalId: any = null
  restartSetIntervalIntervalId: any = null

  initialAlgorithmFunctionArgumentList: any[] = []
  functionInvocationPromiseResultList: Promise<any>[] = []

  algorithmFunction: Function = () => {/* */}

  constructor (millisecondRegularDelayTime?: number) {
    if (millisecondRegularDelayTime && millisecondRegularDelayTime < 0) {
      millisecondRegularDelayTime = 0
    }
    this.millisecondRegularDelayTime = millisecondRegularDelayTime || 0
  }
}

export {
  InfiniteLoopProgram
}



