

function createId1 (additiveAmount?: number) {

  let resultNumber = ''
  additiveAmount = additiveAmount || 1

  if (additiveAmount <= 0) {
    additiveAmount = 1
  }

  for (let i = 0; i < additiveAmount; i++) {
    let randomNumber = (Math.random()).toString(16)
    randomNumber = randomNumber.slice('0.'.length, randomNumber.length)
    resultNumber = `${resultNumber}${randomNumber}`
  }

  return resultNumber
}


export {
  createId1
}

