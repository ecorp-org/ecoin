

// function getUTCDate1 (date?: Date) {
//   let calendarDate = date || new Date()
//   const calendarUTCDate = new Date(calendarDate.getUTCFullYear(), calendarDate.getUTCMonth(), calendarDate.getUTCDate(), calendarDate.getUTCHours(), calendarDate.getUTCMinutes(), calendarDate.getUTCSeconds(), calendarDate.getUTCMilliseconds())
//   return calendarUTCDate
// }


function getUTCDate1 (date: Date) {
  return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()))
}

// function convertDateToUTC (date: Date) {
//   return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds())
// }

// https://stackoverflow.com/questions/439630/create-a-date-with-a-set-timezone-without-using-a-string-representation


export {
  getUTCDate1
}

