


import { infiniteLoopProgramTable } from '../../general-type-4-variables/variables-1-infinite-loop/infinite-loop-1'


function stopInfiniteLoopProgram (infiniteLoopProgramId: string): boolean {
  const infiniteLoopProgram = infiniteLoopProgramTable[infiniteLoopProgramId]

  if (!infiniteLoopProgram) {
    return false
  }

  infiniteLoopProgram.isInfiniteLoopStarted = false

  clearTimeout(infiniteLoopProgram.timeoutId)
  clearInterval(infiniteLoopProgram.intervalId)

  infiniteLoopProgram.timeoutId = null
  infiniteLoopProgram.intervalId = null
  infiniteLoopProgram.millisecondTimeLeftToStart = 0

  return true
}

export {
  stopInfiniteLoopProgram
}


