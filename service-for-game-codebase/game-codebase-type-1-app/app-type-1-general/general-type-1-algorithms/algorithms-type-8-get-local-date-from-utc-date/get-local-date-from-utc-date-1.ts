

import {
  NUMBER_OF_MILLISECONDS_IN_A_MINUTE
} from '../../general-type-3-constants/constants-3-time'


function getLocalDateFromUTCDate1 (utcDate: Date): Date {
  const currentDate = new Date()
  const millisecondUTCDate = utcDate.getTime()
  const millisecondLocalDate = millisecondUTCDate - currentDate.getTimezoneOffset() * NUMBER_OF_MILLISECONDS_IN_A_MINUTE
  return new Date(millisecondLocalDate)
}


export {
  getLocalDateFromUTCDate1
}

