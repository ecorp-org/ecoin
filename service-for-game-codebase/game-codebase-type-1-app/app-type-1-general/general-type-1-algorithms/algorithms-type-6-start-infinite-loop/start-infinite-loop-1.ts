


import { InfiniteLoopProgram } from '../../general-type-2-data-structures/data-structures-3-infinite-loop/infinite-loop-1'
import { infiniteLoopProgramTable } from '../../general-type-4-variables/variables-1-infinite-loop/infinite-loop-1'

import { stopInfiniteLoopProgram } from '../algorithms-type-7-stop-infinite-loop/stop-infinite-loop-1'

function startInfiniteLoopProgram (infiniteLoopAlgorithmFunction: Function, startDate: Date, millisecondRegularDelayTime: number, initialAlgorithmFunctionArgumentList?: any[], sequentializeAsyncFunction?: boolean, millisecondDelayToRestartSetInterval?: number): InfiniteLoopProgram {
  let infiniteLoopProgram = new InfiniteLoopProgram()
  infiniteLoopProgram.infiniteLoopId = `${Math.random()}`
  infiniteLoopProgram.specifiedStartDate = startDate
  infiniteLoopProgram.millisecondRegularDelayTime = millisecondRegularDelayTime
  infiniteLoopProgram.millisecondDelayToRestartSetInterval = millisecondDelayToRestartSetInterval || 0
  infiniteLoopProgram.algorithmFunction = infiniteLoopAlgorithmFunction
  infiniteLoopProgram.initialAlgorithmFunctionArgumentList = initialAlgorithmFunctionArgumentList || []
  infiniteLoopProgramTable[infiniteLoopProgram.infiniteLoopId] = infiniteLoopProgram

  if (infiniteLoopProgram.millisecondDelayToRestartSetInterval) {

    stopInfiniteLoopProgram(infiniteLoopProgram.infiniteLoopId)

    infiniteLoopProgram = initializeMillisecondTimeLeftToStart(infiniteLoopProgram)

    if (sequentializeAsyncFunction) {
      infiniteLoopProgram = startWaitingForSequentialAsync(infiniteLoopProgram)
    } else {
      infiniteLoopProgram = startWaitingForNonSequentialAsyncFunction(infiniteLoopProgram)
    }

    infiniteLoopProgram.restartSetIntervalIntervalId = setInterval(() => {

      stopInfiniteLoopProgram(infiniteLoopProgram.infiniteLoopId)

      infiniteLoopProgram = initializeMillisecondTimeLeftToStart(infiniteLoopProgram)

      if (sequentializeAsyncFunction) {
        infiniteLoopProgram = startWaitingForSequentialAsync(infiniteLoopProgram)
      } else {
        infiniteLoopProgram = startWaitingForNonSequentialAsyncFunction(infiniteLoopProgram)
      }
    }, infiniteLoopProgram.millisecondDelayToRestartSetInterval)
  } else {

    infiniteLoopProgram = initializeMillisecondTimeLeftToStart(infiniteLoopProgram)

    // Start running the infinite loop program
    if (sequentializeAsyncFunction) {
      infiniteLoopProgram = startWaitingForSequentialAsync(infiniteLoopProgram)
    } else {
      infiniteLoopProgram = startWaitingForNonSequentialAsyncFunction(infiniteLoopProgram)
    }
  }

  return infiniteLoopProgram
}

function initializeMillisecondTimeLeftToStart (infiniteLoopProgram: InfiniteLoopProgram) {
  // Calculate the time in milliseconds from now of when to start the infinite loop program
  const delayTimeToStart = ((new Date(infiniteLoopProgram.specifiedStartDate)).getTime() - (new Date()).getTime())
  const isFutureStartDate = delayTimeToStart > 0
  const delayTimeToStartAtNextInterval = infiniteLoopProgram.millisecondRegularDelayTime - ((Math.abs(delayTimeToStart) / infiniteLoopProgram.millisecondRegularDelayTime) % 1) * infiniteLoopProgram.millisecondRegularDelayTime

  infiniteLoopProgram.millisecondTimeLeftToStart = Math.abs(delayTimeToStart) < 50 ? 0 : (isFutureStartDate ? delayTimeToStart : delayTimeToStartAtNextInterval)

  return infiniteLoopProgram
}

function startWaitingForSequentialAsync (infiniteLoopProgram: InfiniteLoopProgram) {
  // Set timeout to start when the start date is specified
  infiniteLoopProgram.timeoutId = setTimeout(() => {
    // Call the first iteration of the infinite loop
    // and initialize the argument list
    infiniteLoopProgram = sequentialAsyncFunctionSetIntervalFunction(infiniteLoopProgram)

    // Start the infinite loop today
    infiniteLoopProgram.dateStarted = new Date()
    infiniteLoopProgram.isInfiniteLoopStarted = true

    // Set interval to repeatedly loop through the iteration sequences
    infiniteLoopProgram.intervalId = setInterval(() => {
      infiniteLoopProgram = sequentialAsyncFunctionSetIntervalFunction(infiniteLoopProgram)
    }, infiniteLoopProgram.millisecondRegularDelayTime)
  }, infiniteLoopProgram.millisecondTimeLeftToStart)

  return infiniteLoopProgram
}

function sequentialAsyncFunctionSetIntervalFunction (infiniteLoopProgram: InfiniteLoopProgram) {
  const previousPromiseList = Promise.all(infiniteLoopProgram.functionInvocationPromiseResultList)

  const promise = new Promise<any>(async (resolve) => {
    await previousPromiseList
    infiniteLoopProgram.iterationIndex++
    const resultOfThisIteration = await infiniteLoopProgram.algorithmFunction(...(infiniteLoopProgram.initialAlgorithmFunctionArgumentList || []))
    infiniteLoopProgram.functionInvocationPromiseResultList.shift()
    resolve(resultOfThisIteration)
  })

  infiniteLoopProgram.functionInvocationPromiseResultList.push(promise)

  return infiniteLoopProgram
}

function startWaitingForNonSequentialAsyncFunction (infiniteLoopProgram: InfiniteLoopProgram) {
  infiniteLoopProgram.timeoutId = setTimeout(() => {
    infiniteLoopProgram.iterationIndex++
    infiniteLoopProgram.algorithmFunction(...(infiniteLoopProgram.initialAlgorithmFunctionArgumentList || []))

    // Start the infinite loop today
    infiniteLoopProgram.dateStarted = new Date()
    infiniteLoopProgram.isInfiniteLoopStarted = true

    infiniteLoopProgram.intervalId = setInterval(() => {
      infiniteLoopProgram.iterationIndex++
      infiniteLoopProgram.algorithmFunction(...(infiniteLoopProgram.initialAlgorithmFunctionArgumentList || []))
    }, infiniteLoopProgram.millisecondRegularDelayTime)
  }, infiniteLoopProgram.millisecondTimeLeftToStart)

  return infiniteLoopProgram
}




export {
  startInfiniteLoopProgram
}

