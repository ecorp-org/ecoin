

import 'mocha'
import { expect } from 'chai'

import { startInfiniteLoopProgram } from '../start-infinite-loop-1'
import { stopInfiniteLoopProgram } from '../../algorithms-type-7-stop-infinite-loop/stop-infinite-loop-1'

describe('start-infinite-loop-1', () => {

  it('should sequentialize async functions when sequentializeAsyncFunction is true', async () => {
    // let counter = 0
    let numberList: number[] = []

    const infiniteLoopProgram = startInfiniteLoopProgram(async () => {
      await new Promise((resolve) => setTimeout(resolve, Math.floor(Math.random() * 500)))
      numberList.push(infiniteLoopProgram.iterationIndex)
    }, new Date(), 100, [], true)

    await new Promise((resolve) => setTimeout(resolve, 2 * 1000))

    stopInfiniteLoopProgram(infiniteLoopProgram.infiniteLoopId)

    const naturalNumberSequenceList = [...(new Array(numberList.length))].map((_value, index) => index + 1)
    expect(numberList.length).to.equal(naturalNumberSequenceList.length)
    expect([...numberList]).to.deep.equal(naturalNumberSequenceList)
  }).timeout(3 * 1000)

  it('should not sequentialize async functions when sequentializeAsyncFunction is false', async () => {
    // let counter = 0
    let numberList: number[] = []

    const infiniteLoopProgram = startInfiniteLoopProgram(async () => {
      await new Promise((resolve) => setTimeout(resolve, Math.floor(Math.random() * 1000)))
      numberList.push(infiniteLoopProgram.iterationIndex)
    }, new Date(), 100, [], false)

    await new Promise((resolve) => setTimeout(resolve, 1 * 1000))

    stopInfiniteLoopProgram(infiniteLoopProgram.infiniteLoopId)

    const naturalNumberSequenceList = [...(new Array(numberList.length))].map((_value, index) => index + 1)
    expect(numberList.length).to.equal(naturalNumberSequenceList.length)
    expect([...numberList]).to.not.deep.equal(naturalNumberSequenceList)
  }).timeout(2 * 1000)

  it('should restart the setInterval with sequentializeAsyncFunction as true', async () => {

    let numberList: number[] = []
    let numberOfPromiseList: number[] = []

    const numberOfWaitingCycles = 20
    const numberOfDelaysPerWait = 1

    const maximumNumberOfMillisecondsToWaitBeforeResolving = 200
    const millisecondDelayForInfiniteLoop = 100
    const millisecondDelayToRestartSetInterval = 100

    const infiniteLoopProgram = startInfiniteLoopProgram(async () => {
      await new Promise((resolve) => setTimeout(resolve, Math.floor(Math.random() * maximumNumberOfMillisecondsToWaitBeforeResolving)))
      numberList.push(infiniteLoopProgram.iterationIndex)
      numberOfPromiseList.push(infiniteLoopProgram.functionInvocationPromiseResultList.length)
    }, new Date(), millisecondDelayForInfiniteLoop, [], true, millisecondDelayToRestartSetInterval)

    let previousSetIntervalId = null

    await new Promise((resolve) => setTimeout(resolve, numberOfDelaysPerWait * millisecondDelayToRestartSetInterval))

    for (let i = 0; i < numberOfWaitingCycles; i++) {
      previousSetIntervalId = infiniteLoopProgram.intervalId

      await new Promise((resolve) => setTimeout(resolve, numberOfDelaysPerWait * millisecondDelayToRestartSetInterval))

      const currentSetIntervalId = infiniteLoopProgram.intervalId

      if (!previousSetIntervalId || !currentSetIntervalId) {
        continue
      }

      expect(previousSetIntervalId).to.not.equal(null)
      expect(currentSetIntervalId).to.not.equal(null)
      expect(previousSetIntervalId).to.not.deep.equal(currentSetIntervalId)

      previousSetIntervalId = currentSetIntervalId
    }

    const naturalNumberSequenceList = [...(new Array(numberList.length))].map((_value, index) => index + 1)
    expect(numberList.length).to.equal(naturalNumberSequenceList.length)
    expect([...numberList]).to.deep.equal(naturalNumberSequenceList)

    const maximumNumberOfExpectedPromises = Math.floor((numberOfWaitingCycles * numberOfDelaysPerWait) * ((maximumNumberOfMillisecondsToWaitBeforeResolving - millisecondDelayForInfiniteLoop) / maximumNumberOfMillisecondsToWaitBeforeResolving)) + numberOfDelaysPerWait
    const isEveryNumberOfPromiseLessThanExpected = numberOfPromiseList.every((value: number) => value <= maximumNumberOfExpectedPromises)
    expect(isEveryNumberOfPromiseLessThanExpected).is.equal(true)

  }).timeout(10 * 1000)

  it('should restart the setInterval with sequentializeAsyncFunction as false', async () => {

    let numberList: number[] = []
    let numberOfPromiseList: number[] = []

    const numberOfWaitingCycles = 20
    const numberOfDelaysPerWait = 1

    const maximumNumberOfMillisecondsToWaitBeforeResolving = 200
    const millisecondDelayForInfiniteLoop = 100
    const millisecondDelayToRestartSetInterval = 100

    const infiniteLoopProgram = startInfiniteLoopProgram(async () => {
      await new Promise((resolve) => setTimeout(resolve, Math.floor(Math.random() * maximumNumberOfMillisecondsToWaitBeforeResolving)))
      numberList.push(infiniteLoopProgram.iterationIndex)
      numberOfPromiseList.push(infiniteLoopProgram.functionInvocationPromiseResultList.length)
    }, new Date(), millisecondDelayForInfiniteLoop, [], false, millisecondDelayToRestartSetInterval)

    let previousSetIntervalId = null

    await new Promise((resolve) => setTimeout(resolve, numberOfDelaysPerWait * millisecondDelayToRestartSetInterval))

    for (let i = 0; i < numberOfWaitingCycles; i++) {
      previousSetIntervalId = infiniteLoopProgram.intervalId

      await new Promise((resolve) => setTimeout(resolve, numberOfDelaysPerWait * millisecondDelayToRestartSetInterval))

      const currentSetIntervalId = infiniteLoopProgram.intervalId

      if (!previousSetIntervalId || !currentSetIntervalId) {
        continue
      }

      expect(previousSetIntervalId).to.not.equal(null)
      expect(currentSetIntervalId).to.not.equal(null)
      expect(previousSetIntervalId).to.not.deep.equal(currentSetIntervalId)

      previousSetIntervalId = currentSetIntervalId
    }

    const naturalNumberSequenceList = [...(new Array(numberList.length))].map((_value, index) => index + 1)
    expect(numberList.length).to.equal(naturalNumberSequenceList.length)
    expect([...numberList]).to.not.deep.equal(naturalNumberSequenceList)

    const isEveryNumberOfPromiseEqualToZero = numberOfPromiseList.every((value: number) => value === 0)
    expect(isEveryNumberOfPromiseEqualToZero).is.equal(true)

  }).timeout(10 * 1000)

})


