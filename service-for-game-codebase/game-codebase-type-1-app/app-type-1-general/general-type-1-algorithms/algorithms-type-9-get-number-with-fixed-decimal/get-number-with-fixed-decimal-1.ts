
import { numberOfDecimalPlacesAllowed } from '../../../../../service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/project-variables-4-digital-currency'


function getNumberWithFixedDecmial1 (numberValue: number) {
  return parseFloat(parseFloat(parseFloat(`${numberValue}`).toFixed(numberOfDecimalPlacesAllowed)).toPrecision(numberOfDecimalPlacesAllowed))
}



export {
  getNumberWithFixedDecmial1
}


