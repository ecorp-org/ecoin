

import onTerminatedProcess from 'death'

import { initializeResources2Daemon } from '../algorithms-type-1-initialize-resources/initialize-resources-2-daemon'

import { startInfiniteLoopProgram } from '../algorithms-type-6-start-infinite-loop/start-infinite-loop-1'

import { updateServiceInformationByDaemonStatus2FirebaseAdmin } from '../../../app-type-8-service-information/service-information-type-1-algorithms/algorithms-type-2-update-service-information-by-daemon-status/update-service-information-by-daemon-status-2-firebase-admin'
import { getServiceInformationByDaemonStatus2FirebaseAdmin } from '../../../app-type-8-service-information/service-information-type-1-algorithms/algorithms-type-1-get-service-information-by-daemon-status/get-service-information-by-daemon-status-2-firebase-admin'

import { createDigitalCurrencyTransaction2FirebaseAdmin } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-1-create-digital-currency-transaction/create-digital-currency-transaction-2-firebase-admin'

import {
  universalBasicIncomeTransaction
} from '../../../../../service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/project-variables-4-digital-currency'


// Initialize Digital Currency Transaction Algorithms

import {
  initializeDigitalCurrencyTransactionListAlgorithm,
  initializeDigitalCurrencyRecurringTransactionListAlgorithm
} from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-9-initialize-digital-currency-transaction-list/service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-1-initialize-digital-currency-transaction-list'

import {
  initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm,
  updateDigitalCurrencyAccountWithMissedRecurringTransactionAmountForAllAccountsAlgorithm,
  getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm
} from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-9-initialize-digital-currency-transaction-list/service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-2-initialize-digital-currency-transaction-with-account-recipients-as-all-accounts'

import { initializeAllAccountsForAccountIdVariableTableAlgorithm } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-9-initialize-digital-currency-transaction-list/service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-3-initialize-all-accounts-for-account-id-variable-table'

import { updateDigitalCurrencyRecurringTransactionListAlgorithm } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-9-initialize-digital-currency-transaction-list/service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-4-initialize-digital-currency-recurring-transaction-update'

import { initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-9-initialize-digital-currency-transaction-list/service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-6-initialize-digital-currency-account-transaction-data-queue-processing'
import { initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessingAlgorithm } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-9-initialize-digital-currency-transaction-list/service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-7-initialize-digital-currency-account-transaction-statistics-data-queue-processing'
import { initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessingAlgorithm } from '../../../app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-9-initialize-digital-currency-transaction-list/service-for-initialize-digital-currency-transaction-list-2-firebase-admin/-8-initialize-digital-currency-transaction-network-statistics-data-queue-processing'

import { initializeDigitalCurrencyAccountNetworkStatisticsDataQueueProcessingAlgorithm } from '../../../app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-19-initialize-digital-currency-account-list/service-for-initialize-digital-currency-account-list-2-firebase-admin/-1-initialize-digital-currency-account-network-statistics-data-queue-processing'

import {
  NUMBER_OF_MILLISECONDS_IN_A_DAY
} from '../../general-type-3-constants/constants-3-time'


import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'



// Variables

const variableTable = {
  daemonId: 'daemon-1',
  isAlreadyStarted: false,

  NUMBER_OF_MILLISECONDS_AGO_TO_UPDATE_DEFAULT_RECURRING_TRANSACTIONS: 30 * NUMBER_OF_MILLISECONDS_IN_A_DAY, // 30 days
  NUMBER_OF_MILLISECONDS_AGO_BEFORE_DATE_LAST_SHUTDOWN_TO_GET_RECURRING_TRANSACTIONS: 10 * NUMBER_OF_MILLISECONDS_IN_A_DAY
}


// Start the daemon
// initializeDaemon1().catch((errorMessage: any) => console.error(errorMessage))


async function initializeDaemon1 () {

  const currentDate = new Date()

  if (variableTable.isAlreadyStarted) {
    console.log('Already Started Daemon-1')
    return
  }

  variableTable.isAlreadyStarted = true

  console.log('Started Daemon-1 for: ', process.env.FILE_DISTRIBUTION_TYPE, ' at time: ', currentDate)

  // Initialize Resources

  const algoliasearchDigitalCurrencyAccountIndexId = process.env.NODE_ENV === 'development' ? 'development_DIGITAL_CURRENCY_ACCOUNT' : (process.env.NODE_ENV === 'production' ? 'production_DIGITAL_CURRENCY_ACCOUNT' : '')

  await initializeResources2Daemon({
    distributionType: process.env.FILE_DISTRIBUTION_TYPE,
    firebaseUseEmulator: process.env.FILE_DISTRIBUTION_TYPE === 'development',
    algoliasearchItemSearchIndexNameToIndexIdMap: {
      [DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]: algoliasearchDigitalCurrencyAccountIndexId
    }
  })

  // Initialize Daemon Status
  await initializeDaemonStatus()

  // Create Universal Basic Income Transaction
  await createDigitalCurrencyTransaction2FirebaseAdmin(universalBasicIncomeTransaction)

  // Initialize All Accounts
  await initializeAllAccountsForAccountIdVariableTableAlgorithm.function()

  const dateLastShutdown: string = (await getServiceInformationByDaemonStatus2FirebaseAdmin(variableTable.daemonId)).dateLastShutdown
  const dateLastShutdownWithBuffer: string = dateLastShutdown ? ((new Date(dateLastShutdown)).toISOString().split('.')[0] + 'Z') : ((new Date((new Date()).getTime() - variableTable.NUMBER_OF_MILLISECONDS_AGO_TO_UPDATE_DEFAULT_RECURRING_TRANSACTIONS)).toISOString().split('.')[0] + 'Z')
  const millisecondTimeAgoToProcessTransactions = ((new Date()).getTime() - (new Date(dateLastShutdownWithBuffer)).getTime()) + variableTable.NUMBER_OF_MILLISECONDS_AGO_BEFORE_DATE_LAST_SHUTDOWN_TO_GET_RECURRING_TRANSACTIONS

  // Apply Universal Basic Income Transaction
  let universalBasicIncomeTransactionResult = getDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm.function(universalBasicIncomeTransaction)
  startInfiniteLoopProgram(initializeDigitalCurrencyTransactionWithAccountRecipientsAsAllAccountsAlgorithm.function, new Date(universalBasicIncomeTransactionResult.dateTransactionCreated!), universalBasicIncomeTransactionResult.millisecondRecurringDelay, [universalBasicIncomeTransactionResult, true], false, NUMBER_OF_MILLISECONDS_IN_A_DAY)
  // Update Universal Basic Income Balance Since Last Shutdown
  if (dateLastShutdown) {
    await updateDigitalCurrencyAccountWithMissedRecurringTransactionAmountForAllAccountsAlgorithm.function(universalBasicIncomeTransaction, dateLastShutdown)
  }

  // Update Transaction List Since Last Shutdown
  await initializeDigitalCurrencyTransactionListAlgorithm.function(millisecondTimeAgoToProcessTransactions, false, true)
  await updateDigitalCurrencyRecurringTransactionListAlgorithm.function(millisecondTimeAgoToProcessTransactions, true)

  // Process Digital Currency Accounts
  startInfiniteLoopProgram(initializeDigitalCurrencyAccountNetworkStatisticsDataQueueProcessingAlgorithm.function, new Date(), 1000, [], true, NUMBER_OF_MILLISECONDS_IN_A_DAY)


  // Process Digital Currency Transactions
  startInfiniteLoopProgram(initializeDigitalCurrencyTransactionListAlgorithm.function, new Date(), 1000, [], true, NUMBER_OF_MILLISECONDS_IN_A_DAY)
  startInfiniteLoopProgram(initializeDigitalCurrencyRecurringTransactionListAlgorithm.function, new Date(), 1000, [], true, NUMBER_OF_MILLISECONDS_IN_A_DAY)
  startInfiniteLoopProgram(initializeDigitalCurrencyAccountTransactionDataQueueProcessingAlgorithm.function, new Date(), 1000, [], true, NUMBER_OF_MILLISECONDS_IN_A_DAY)
  startInfiniteLoopProgram(initializeDigitalCurrencyAccountTransactionStatisticsDataQueueProcessingAlgorithm.function, new Date(), 1000, [], true, NUMBER_OF_MILLISECONDS_IN_A_DAY)
  startInfiniteLoopProgram(initializeDigitalCurrencyTransactionNetworkStatisticsDataQueueProcessingAlgorithm.function, new Date(), 1000, [], true, NUMBER_OF_MILLISECONDS_IN_A_DAY)

}

async function initializeDaemonStatus () {
  await updateServiceInformationByDaemonStatus2FirebaseAdmin(variableTable.daemonId, 'isActive', true)

  onTerminatedProcess((code: any) => {
    updateServiceInformationByDaemonStatus2FirebaseAdmin(variableTable.daemonId, 'isActive', false)
    .then(() => {
      return updateServiceInformationByDaemonStatus2FirebaseAdmin(variableTable.daemonId, 'dateLastShutdown', ((new Date()).toISOString().split('.')[0] + 'Z'))
    })
    .then(() => {
      process.exit(code)
    }).catch((errorMessage) => {
      console.log(errorMessage)
      process.exit(code)
    })
  })
}


export {
  initializeDaemon1
}



