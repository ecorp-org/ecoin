
// const MILLISECONDS_IN_AN_EARTH_DAY = 24 * 60 * 60 * 1000
const MILLISECONDS_IN_AN_HOUR = 60 * 60 * 1000
const MILLISECONDS_IN_A_MINUTE = 60 * 1000
const MILLISECONDS_IN_A_SECOND = 1000

// setInterval(() => {
//   const currentDate = new Date()
//   const currentDateTimePartition = getMillisecondTimePartitionList2((currentDate.toISOString().split('.')[0] + 'Z'))

//   console.log('current date: ', (currentDate.toISOString().split('.')[0] + 'Z'))
//   console.log('time partition: ', currentDateTimePartition)
//   console.log('time: ', (new Date((currentDate.toISOString().split('.')[0] + 'Z'))).getTime())

// }, 1000)

function getMillisecondTimePartitionList1 (millisecondDateTime?: number): { millisecondDay: number, millisecondHour: number, millisecondMinute: number, millisecondSecond: number } {

  let date = millisecondDateTime ? new Date(millisecondDateTime) : new Date()
  // date = new Date(date.getTime() + date.getTimezoneOffset() * MILLISECONDS_IN_A_MINUTE)
  const timeValueList = date.toTimeString().substr(0, 8).split(':').map((timeValue: string) => parseInt(timeValue, 10))

  // console.log('time-1: ', date.toDateString(), '; ' , (new Date(`${date.toDateString()} 00:00:00`)).getTime())
  // console.log('time-2: ', date.toDateString(), '; ' , (new Date(`${date.toDateString()} 00:00:00Z`)).getTime())
  // console.log('time-2: ', date.toDateString(), '; ' , (new Date(`${date.toDateString()} 00:00:00Z`)).getTime())

  // console.log('date-1: ', millisecondDateTime, '; ', date.getTime())
  // console.log('date-2: ', date.toDateString(), '; ', date.getHours(), '; ', date.getUTCHours())
  // console.log('date-3: ', date.toDateString(), '; ', date.getMinutes(), '; ', date.getUTCMinutes())
  // console.log('date-4: ', date, '; ', date.getSeconds(), '; ', date.getUTCSeconds())
  // console.log('date-5: ', (new Date(`${date.toDateString()} 00:00:00-05:00`)).getTime(), '; ', (new Date(`${date.toDateString()} 00:00:00-05:00`)))
  // console.log('date-5: ', (new Date(`${date.toDateString()} 00:00:00-05:00`)).getTime())

  // console.log('date-5: ', date, '; ', date.getMinutes(), '; ', date.getUTCMinutes())



  // console.log('timezone offset: ', date.getTimezoneOffset())
  // console.log('time partition: ', date, '; ', millisecondDateTime, '; ', date.getTime(), '; ', date.toTimeString().substr(0, 8))
  // console.log('date partition: ', date.toDateString(), '; ', (new Date(`${date.toDateString()} 00:00:00Z`)), '; ', (new Date(`${date.toDateString()} 00:00:00Z`)).getTime())
  // const millisecondDay = (new Date(`${date.toDateString()} 00:00:00-05:00`)).getTime()
  const millisecondDay = (new Date(`${date.toDateString()} 00:00:00`)).getTime()
  const millisecondHour = timeValueList[0] * MILLISECONDS_IN_AN_HOUR
  const millisecondMinute = timeValueList[1] * MILLISECONDS_IN_A_MINUTE
  const millisecondSecond = timeValueList[2] * MILLISECONDS_IN_A_SECOND

  return {
    millisecondDay,
    millisecondHour,
    millisecondMinute,
    millisecondSecond
  }
}

function getMillisecondTimePartitionList2 (isoDateString?: string): { millisecondDay: number, millisecondHour: number, millisecondMinute: number, millisecondSecond: number } {

  let date = isoDateString ? new Date(isoDateString) : new Date()
  // date = new Date(date.getTime() + date.getTimezoneOffset() * MILLISECONDS_IN_A_MINUTE)
  const timeValueList = (date.toISOString().split('.')[0] + 'Z').substr(11, 8).split(':').map((timeValue: string) => parseInt(timeValue, 10))

  // console.log('time-1: ', date.toDateString(), '; ' , (new Date(`${date.toDateString()} 00:00:00`)).getTime())
  // console.log('time-2: ', date.toDateString(), '; ' , (new Date(`${date.toDateString()} 00:00:00Z`)).getTime())
  // console.log('time-2: ', date.toDateString(), '; ' , (new Date(`${date.toDateString()} 00:00:00Z`)).getTime())

  // console.log('date-1: ', millisecondDateTime, '; ', date.getTime())
  // console.log('date-2: ', date.toDateString(), '; ', date.getHours(), '; ', date.getUTCHours())
  // console.log('date-3: ', date.toDateString(), '; ', date.getMinutes(), '; ', date.getUTCMinutes())
  // console.log('date-4: ', date, '; ', date.getSeconds(), '; ', date.getUTCSeconds())
  // console.log('date-5: ', (new Date(`${date.toDateString()} 00:00:00-05:00`)).getTime(), '; ', (new Date(`${date.toDateString()} 00:00:00-05:00`)))
  // console.log('date-5: ', (new Date(`${date.toDateString()} 00:00:00-05:00`)).getTime())

  // console.log('date-5: ', date, '; ', date.getMinutes(), '; ', date.getUTCMinutes())



  // console.log('timezone offset: ', date.getTimezoneOffset())
  // console.log('time partition: ', date, '; ', millisecondDateTime, '; ', date.getTime(), '; ', date.toTimeString().substr(0, 8))
  // console.log('date partition: ', date.toDateString(), '; ', (new Date(`${date.toDateString()} 00:00:00Z`)), '; ', (new Date(`${date.toDateString()} 00:00:00Z`)).getTime())
  // const millisecondDay = (new Date(`${date.toDateString()} 00:00:00-05:00`)).getTime()
  const formattedMonth = Math.floor((date.getUTCMonth() + 1) / 10) === 0 ? `0${date.getUTCMonth() + 1}` : `${date.getUTCMonth() + 1}`
  const formattedDay = Math.floor((date.getUTCDate() + 1) / 10) === 0 ? `0${date.getUTCDate() + 1}` : `${date.getUTCDate() + 1}`
  const millisecondDay = (new Date(`${date.getUTCFullYear()}-${formattedMonth}-${formattedDay}T00:00:00Z`)).getTime()
  const millisecondHour = timeValueList[0] * MILLISECONDS_IN_AN_HOUR
  const millisecondMinute = timeValueList[1] * MILLISECONDS_IN_A_MINUTE
  const millisecondSecond = timeValueList[2] * MILLISECONDS_IN_A_SECOND

  return {
    millisecondDay,
    millisecondHour,
    millisecondMinute,
    millisecondSecond
  }
}


export {
  getMillisecondTimePartitionList1,
  getMillisecondTimePartitionList2
}

