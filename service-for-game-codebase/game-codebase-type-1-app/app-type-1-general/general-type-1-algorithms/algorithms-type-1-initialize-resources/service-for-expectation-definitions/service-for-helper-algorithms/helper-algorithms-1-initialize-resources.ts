


import { initializeFirebaseVariables2FirebaseAdmin, InitializeFirebaseVariables2FirebaseAdminOptions } from '../../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-1-initialize-firebase-variables/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-2-initialize-firebase-variables-2-firebase-admin'
import { initializeAlgoliasearchVariables1 } from '../../../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-6-initialize-algoliasearch-variables/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-initialize-algoliasearch-variables-1'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../../../app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'


class InitializeResourcesOption {
  firebaseOptions?: InitializeFirebaseVariables2FirebaseAdminOptions
}

async function initializeResources (options?: InitializeResourcesOption) {
  if (!options) {
    options = new InitializeResourcesOption()
  }
  // Initialize Firebase Variables
  await initializeFirebaseVariables2FirebaseAdmin(options!.firebaseOptions)
  await initializeAlgoliasearchVariables1({
    [DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]: 'default-account-index-id'
  })
}

export {
  initializeResources
}

