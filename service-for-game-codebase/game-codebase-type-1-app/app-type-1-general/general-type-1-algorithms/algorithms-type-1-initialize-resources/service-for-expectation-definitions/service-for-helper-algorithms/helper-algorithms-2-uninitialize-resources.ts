

import { uninitializeFirebaseVariables2FirebaseAdmin } from '../../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-1-initialize-firebase-variables/service-for-expectation-definitions/service-for-helper-algorithms/helper-algorithms-3-uninitialize-firebase-variables-2-firebase-admin'

async function uninitializeResources () {
  await uninitializeFirebaseVariables2FirebaseAdmin()
}

export {
  uninitializeResources
}



