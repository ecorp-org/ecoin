

import { initializeFirebaseVariables2FirebaseAdmin } from '../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-1-initialize-firebase-variables/initialize-firebase-variables-2-firebase-admin'

import { initializeAlgoliasearchVariables1 } from '../../../../game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-1-algorithms/algorithms-type-6-initialize-algoliasearch-variables/initialize-algoliasearch-variables-1'

import { ResourceInformation } from '../../general-type-2-data-structures/data-structures-2-resource-information/resource-information-1'


async function initializeResources2Daemon (resourceInformation: ResourceInformation) {
  await initializeFirebaseVariables2FirebaseAdmin({
    distributionType: resourceInformation.distributionType,
    useEmulator: resourceInformation.firebaseUseEmulator
  })

  initializeAlgoliasearchVariables1(resourceInformation.algoliasearchItemSearchIndexNameToIndexIdMap, resourceInformation.algoliasearchItemSearchIndexNameToOptionsMap)
}

export {
  initializeResources2Daemon
}

