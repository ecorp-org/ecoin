

import { initializeFirebaseVariables2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-1-initialize-firebase-variables/initialize-firebase-variables-2-firebase-admin'
import { deleteItem2FirebaseAdmin } from '../../../../../game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-1-algorithms/algorithms-type-9-delete-item/delete-item-2-firebase-admin'

import firebaseAdminPrivateKey from '../../../../../../service-for-secret-variables/secret-variables-type-1-firebase/ecoin369-firebase-adminsdk-bwhbg-834695107d.json'


process.env.FIREBASE_ADMIN_PRIVATE_KEY = JSON.stringify(firebaseAdminPrivateKey)


console.log('Starting delete production database')

;(async () => {
  await deleteAllItemsFromDatabase1ProductionDatabase()
})()

async function deleteAllItemsFromDatabase1ProductionDatabase () {
  await initializeFirebaseVariables2FirebaseAdmin({
    distributionType: 'production'
  })
  await deleteItem2FirebaseAdmin('', '')
  console.log('Deleted All Items From Production Database')
}

export {
  deleteAllItemsFromDatabase1ProductionDatabase
}



