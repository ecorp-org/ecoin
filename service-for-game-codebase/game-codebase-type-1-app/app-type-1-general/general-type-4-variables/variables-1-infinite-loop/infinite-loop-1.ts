


import { InfiniteLoopProgram } from '../../general-type-2-data-structures/data-structures-3-infinite-loop/infinite-loop-1'

const infiniteLoopProgramTable: { [infiniteLoopProgramId: string]: InfiniteLoopProgram } = {}

export {
  infiniteLoopProgramTable
}

