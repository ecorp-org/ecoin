
import Vue from 'vue'

import { MutationTree } from 'vuex'
import { DigitalCurrencyState } from '../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyAccount } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'
import { ServiceAccount } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-2-data-structures/data-structures-1-service-account/service-account-1'
import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import { Group, GroupItem } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-5-account-groups/account-groups-1'

import { DigitalCurrencyTransaction } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

// Account

const UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE = 'UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE'
const DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE = 'DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE'

const UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE = 'UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE'

const UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE = 'UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE'

const UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE = 'UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE'
const DELETE_FROM_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE = 'DELETE_FROM_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE'

const UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE = 'UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE'
const ADD_GROUP_ITEM_TO_GROUP = 'ADD_GROUP_ITEM_TO_GROUP'
const REMOVE_GROUP_ITEM_FROM_GROUP = 'REMOVE_GROUP_ITEM_FROM_GROUP'
const DELETE_DIGITAL_CURRENCY_ACCOUNT_GROUP = 'DELETE_DIGITAL_CURRENCY_ACCOUNT_GROUP'

const UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE = 'UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE'

// Transactions

const UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE = 'UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE'
const DELETE_FROM_DIGITAL_CURRENCY_TRANSACTION_TABLE = 'DELETE_FROM_DIGITAL_CURRENCY_TRANSACTION_TABLE'



import {
  BASIC_DESCRIPTION,
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-3-constants/constants-1-account-description'


const mutations: MutationTree<DigitalCurrencyState> = {

  // Account Mutations

  [UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE] (state: DigitalCurrencyState, digitalCurrencyAccount: DigitalCurrencyAccount) {
    if (!digitalCurrencyAccount) {
      return
    }
    Vue.set(state.serviceAccountDigitalCurrencyAccountTable, digitalCurrencyAccount.accountId, digitalCurrencyAccount)
  },

  [DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE] (state: DigitalCurrencyState, digitalCurrencyAccountId: string) {
    Vue.delete(state.serviceAccountDigitalCurrencyAccountTable, digitalCurrencyAccountId)
  },

  [UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, descriptionType, digitalCurrencyAccountDescription
  }: {
    digitalCurrencyAccountId: string, descriptionType: string, digitalCurrencyAccountDescription: any
  }) {
    if (!digitalCurrencyAccountId || !descriptionType || !digitalCurrencyAccountDescription) {
      return
    }

    if (descriptionType === BASIC_DESCRIPTION) {
      Vue.set(state.digitalCurrencyAccountDescriptionTable, digitalCurrencyAccountId, digitalCurrencyAccountDescription)
    } else if (descriptionType === TRANSACTION_RESOURCE_CENTER_DESCRIPTION) {
      Vue.set(state.digitalCurrencyAccountDescriptionForTransactionResourceCenterDescriptionTable, digitalCurrencyAccountId, digitalCurrencyAccountDescription)
    } else if (descriptionType === PRODUCT_RESOURCE_CENTER_DESCRIPTION) {
      Vue.set(state.digitalCurrencyAccountDescriptionForProductResourceCenterDescriptionTable, digitalCurrencyAccountId, digitalCurrencyAccountDescription)
    } else if (descriptionType === JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION) {
      Vue.set(state.digitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescriptionTable, digitalCurrencyAccountId, digitalCurrencyAccountDescription)
    } else if (descriptionType === CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION) {
      Vue.set(state.digitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescriptionTable, digitalCurrencyAccountId, digitalCurrencyAccountDescription)
    } else if (descriptionType === DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION) {
      Vue.set(state.digitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescriptionTable, digitalCurrencyAccountId, digitalCurrencyAccountDescription)
    }
  },

  [UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE] (state: DigitalCurrencyState, digitalCurrencyAccount: DigitalCurrencyAccount) {
    if (!digitalCurrencyAccount) {
      return
    }

    if (!state.digitalCurrencyAccountTable[digitalCurrencyAccount.accountId]) {
      Vue.set(state.digitalCurrencyAccountTable, digitalCurrencyAccount.accountId, digitalCurrencyAccount)
      return
    }

    for (let propertyId in digitalCurrencyAccount) {
      if (!digitalCurrencyAccount.hasOwnProperty(propertyId)) {
        continue
      }
      Vue.set(state.digitalCurrencyAccountTable[digitalCurrencyAccount.accountId], propertyId, (digitalCurrencyAccount as any)[propertyId])
    }
  },

  [UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, eventTypeId, eventListenerId
  }: { digitalCurrencyAccountId: string, eventTypeId: string, eventListenerId: string }) {
    if (!digitalCurrencyAccountId || !eventTypeId || !eventListenerId) {
      return
    }

    if (!state.digitalCurrencyAccountEventListenerIdTable[digitalCurrencyAccountId]) {
      state.digitalCurrencyAccountEventListenerIdTable[digitalCurrencyAccountId] = {}
    }

    Vue.set(state.digitalCurrencyAccountEventListenerIdTable[digitalCurrencyAccountId], eventTypeId, eventListenerId)
  },

  [DELETE_FROM_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, eventTypeId
  }: { digitalCurrencyAccountId: string, eventTypeId: string }) {
    if (!digitalCurrencyAccountId || !eventTypeId) {
      return
    }
    Vue.delete(state.digitalCurrencyAccountEventListenerIdTable[digitalCurrencyAccountId], eventTypeId)
  },

  [UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, group
  }: { digitalCurrencyAccountId: string, group: Group }) {
    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]) {
      Vue.set(state.digitalCurrencyAccountGroup, digitalCurrencyAccountId, {})
    }

    if (!group.groupItemIdVariableTable) {
      group.groupItemIdVariableTable = {}
    }

    Vue.set(state.digitalCurrencyAccountGroup[digitalCurrencyAccountId], group.groupId, group)
  },

  [ADD_GROUP_ITEM_TO_GROUP] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, groupId, groupItem
  }: { digitalCurrencyAccountId: string, groupId: string, groupItem: GroupItem }) {
    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]) {
      return
    }

    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId]) {
      return
    }

    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId].groupItemIdVariableTable) {
      Vue.set(state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId], 'groupItemIdVariableTable', {})
    }

    Vue.set(state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId].groupItemIdVariableTable, groupItem.itemId, groupItem)
  },

  [REMOVE_GROUP_ITEM_FROM_GROUP] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, groupId, itemId
  }: { digitalCurrencyAccountId: string, groupId: string, itemId: string }) {
    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]) {
      return
    }

    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId]) {
      return
    }

    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId].groupItemIdVariableTable) {
      Vue.set(state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId], 'groupItemIdVariableTable', {})
    }

    Vue.delete(state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId].groupItemIdVariableTable, itemId)
  },

  [DELETE_DIGITAL_CURRENCY_ACCOUNT_GROUP] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, groupId
  }: { digitalCurrencyAccountId: string, groupId: string }) {
    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]) {
      return
    }

    if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId]) {
      return
    }

    Vue.delete(state.digitalCurrencyAccountGroup[digitalCurrencyAccountId], groupId)
  },

  [UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE] (state: DigitalCurrencyState, {
    digitalCurrencyAccountId, digitalCurrencyAccountSettings
  }: { digitalCurrencyAccountId: string, digitalCurrencyAccountSettings: DigitalCurrencyAccountSettings }) {
    Vue.set(state.digitalCurrencyAccountSettings, digitalCurrencyAccountId, digitalCurrencyAccountSettings)
  },

  // Transaction Mutations
  [UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE] (state: DigitalCurrencyState, digitalCurrencyTransaction: DigitalCurrencyTransaction) {
    if (!digitalCurrencyTransaction || !digitalCurrencyTransaction.transactionId) {
      return
    }

    // Transaction Table
    Vue.set(state.digitalCurrencyTransactionTable, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction)

    // Account Transaction Table - Sender
    if (!state.digitalCurrencyAccountTransactionTable[digitalCurrencyTransaction.accountSenderId]) {
      Vue.set(state.digitalCurrencyAccountTransactionTable, digitalCurrencyTransaction.accountSenderId, {})
    }
    if (!state.digitalCurrencyAccountTransactionTable[digitalCurrencyTransaction.accountSenderId].createdTransactionTable) {
      Vue.set(state.digitalCurrencyAccountTransactionTable[digitalCurrencyTransaction.accountSenderId], 'createdTransactionTable', {})
    }
    Vue.set(state.digitalCurrencyAccountTransactionTable[digitalCurrencyTransaction.accountSenderId].createdTransactionTable, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated)

    // Account Transaction Table - Recipient
    for (let accountId in digitalCurrencyTransaction.accountRecipientIdVariableTable) {
      if (!digitalCurrencyTransaction.accountRecipientIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }
      if (!state.digitalCurrencyAccountTransactionTable[accountId]) {
        Vue.set(state.digitalCurrencyAccountTransactionTable, accountId, {})
      }
      if (!state.digitalCurrencyAccountTransactionTable[accountId].receivedTransactionTable) {
        Vue.set(state.digitalCurrencyAccountTransactionTable[accountId], 'receivedTransactionTable', {})
      }
      Vue.set(state.digitalCurrencyAccountTransactionTable[accountId].receivedTransactionTable, digitalCurrencyTransaction.transactionId, digitalCurrencyTransaction.dateTransactionCreated)
    }
  },

  [DELETE_FROM_DIGITAL_CURRENCY_TRANSACTION_TABLE] (state: DigitalCurrencyState, digitalCurrencyTransactionId: string) {
    if (!digitalCurrencyTransactionId) {
      return
    }

    const digitalCurrencyTransaction = state.digitalCurrencyTransactionTable[digitalCurrencyTransactionId]

    if (!digitalCurrencyTransaction) {
      return
    }

    Vue.delete(state.digitalCurrencyTransactionTable, digitalCurrencyTransactionId)
    Vue.delete(state.digitalCurrencyAccountTransactionTable[digitalCurrencyTransaction.accountSenderId].createdTransactionTable, digitalCurrencyTransactionId)

    for (let accountId in digitalCurrencyTransaction.accountRecipientIdVariableTable) {
      if (!digitalCurrencyTransaction.accountRecipientIdVariableTable.hasOwnProperty(accountId)) {
        continue
      }
      Vue.delete(state.digitalCurrencyAccountTransactionTable[accountId].receivedTransactionTable, digitalCurrencyTransactionId)
    }
  }
}

export {
  mutations,

  // Account
  UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE,
  DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE,

  UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE,
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE,

  UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE,
  DELETE_FROM_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE,

  UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE,
  ADD_GROUP_ITEM_TO_GROUP,
  REMOVE_GROUP_ITEM_FROM_GROUP,
  DELETE_DIGITAL_CURRENCY_ACCOUNT_GROUP,

  UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE,

  // Transaction

  UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE,
  DELETE_FROM_DIGITAL_CURRENCY_TRANSACTION_TABLE
}


