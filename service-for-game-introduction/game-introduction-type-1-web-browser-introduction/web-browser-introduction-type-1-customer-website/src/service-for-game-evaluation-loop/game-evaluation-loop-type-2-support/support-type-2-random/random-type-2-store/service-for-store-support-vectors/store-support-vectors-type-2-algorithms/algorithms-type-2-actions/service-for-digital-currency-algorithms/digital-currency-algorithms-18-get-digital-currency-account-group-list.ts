

import { getDigitalCurrencyAccountGroupList1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-23-get-digital-currency-account-group-list/get-digital-currency-account-group-list-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

async function getDigitalCurrencyAccountGroupList ({ commit }: DigitalCurrencyActionParam, digitalCurrencyAccountId: string) {

  const groupList = await getDigitalCurrencyAccountGroupList1(digitalCurrencyAccountId)

  for (let i = 0; i < groupList.length; i++) {
    const group = groupList[i]
    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE, {
      digitalCurrencyAccountId,
      group
    })
  }

  return groupList
}


export {
  getDigitalCurrencyAccountGroupList
}


