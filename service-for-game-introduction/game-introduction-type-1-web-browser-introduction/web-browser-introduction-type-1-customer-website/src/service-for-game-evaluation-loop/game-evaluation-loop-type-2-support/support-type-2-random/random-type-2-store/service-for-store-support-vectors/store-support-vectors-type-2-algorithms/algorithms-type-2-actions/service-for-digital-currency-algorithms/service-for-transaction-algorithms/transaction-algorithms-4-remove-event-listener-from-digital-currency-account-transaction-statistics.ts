

import { removeEventListenerFromDigitalCurrencyAccountTransactionStatistics1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-6-remove-event-listener-from-digital-currency-account-transaction-statistics/remove-event-listener-from-digital-currency-account-transaction-statistics-1'
import { GetAccountTransactionStatisticsOptions } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-3-transaction-account-statistics/transaction-account-statistics-2'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function removeEventListenerFromDigitalCurrencyAccountTransactionStatistics ({ }: DigitalCurrencyActionParam, {
  statisticsType, eventListenerId, statisticsOptions
}: {
  statisticsType: string,
  eventListenerId: string,
  statisticsOptions?: GetAccountTransactionStatisticsOptions
}) {
  await removeEventListenerFromDigitalCurrencyAccountTransactionStatistics1(statisticsType, eventListenerId, statisticsOptions)
}

export {
  removeEventListenerFromDigitalCurrencyAccountTransactionStatistics
}


