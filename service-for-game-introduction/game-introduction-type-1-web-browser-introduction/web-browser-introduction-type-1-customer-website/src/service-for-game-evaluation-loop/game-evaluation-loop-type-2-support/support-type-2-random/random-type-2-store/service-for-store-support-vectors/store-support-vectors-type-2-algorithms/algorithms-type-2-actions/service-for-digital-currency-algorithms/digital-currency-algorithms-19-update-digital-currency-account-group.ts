

import { updateDigitalCurrencyAccountGroup1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-24-update-digital-currency-account-group/update-digital-currency-account-group-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

async function updateDigitalCurrencyAccountGroup ({ commit }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, groupId, propertyId, propertyValue
}: { digitalCurrencyAccountId: string, groupId: string, propertyId: string, propertyValue: any }) {

  const group = await updateDigitalCurrencyAccountGroup1(digitalCurrencyAccountId, groupId, propertyId, propertyValue)

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE, {
    digitalCurrencyAccountId,
    group
  })

  return group
}


export {
  updateDigitalCurrencyAccountGroup
}


