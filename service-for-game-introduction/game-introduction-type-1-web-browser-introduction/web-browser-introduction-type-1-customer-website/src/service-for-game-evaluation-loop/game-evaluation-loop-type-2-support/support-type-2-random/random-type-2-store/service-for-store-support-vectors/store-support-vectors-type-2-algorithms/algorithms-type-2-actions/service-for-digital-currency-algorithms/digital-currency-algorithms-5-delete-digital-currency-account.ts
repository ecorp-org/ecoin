


import { deleteDigitalCurrencyAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-9-delete-digital-currency-account/delete-digital-currency-account-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  DIGITAL_CURRENCY_ACCOUNT_SERVICE_TYPE
} from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-3-constants/constants-5-service-type'


import {
  DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE,
  DELETE_FROM_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  SET_SERVICE_ACCOUNT,
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'

async function deleteDigitalCurrencyAccount ({ rootState, state, commit, dispatch }: DigitalCurrencyActionParam, digitalCurrencyAccountId: string) {
  if (!digitalCurrencyAccountId) {
    return
  }

  commit(DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccountId)
  commit(DELETE_FROM_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE, digitalCurrencyAccountId)
  commit(SET_SERVICE_ACCOUNT, { ...(rootState.serviceAccount || {}), activeDigitalCurrencyAccountId: '' }, { root: true })

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'delete',
    notificationMessage: `Account Deleted`,
    notificationDate: new Date()
  } as Notification, { root: true })

  const digitalCurrencyAccountEventListenerId = state.digitalCurrencyAccountEventListenerIdTable[digitalCurrencyAccountId]

  const serviceAccountId = rootState.serviceAccount ? rootState.serviceAccount.serviceAccountId : ''
  const serviceAccount = await deleteDigitalCurrencyAccount1(serviceAccountId, digitalCurrencyAccountId)
  commit(SET_SERVICE_ACCOUNT, serviceAccount, { root: true })

  await dispatch('removeEventListenerFromDigitalCurrencyAccount', { digitalCurrencyAccountId, eventTypeId: DIGITAL_CURRENCY_ACCOUNT_SERVICE_TYPE, eventListenerId: digitalCurrencyAccountEventListenerId })

  return serviceAccount
}


export {
  deleteDigitalCurrencyAccount
}

