

import { getDigitalCurrencyAccountDescription1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-12-get-digital-currency-account-description/get-digital-currency-account-description-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'
import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  BASIC_DESCRIPTION,
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-3-constants/constants-1-account-description'


async function getDigitalCurrencyAccountDescription ({ commit }: DigitalCurrencyActionParam, digitalCurrencyAccountId: string) {

  const accountDescription = await getDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, BASIC_DESCRIPTION)
  const accountDescriptionForTransactionResourceCenterDescription = await getDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, TRANSACTION_RESOURCE_CENTER_DESCRIPTION)
  const accountDescriptionForProductResourceCenterDescription = await getDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, PRODUCT_RESOURCE_CENTER_DESCRIPTION)
  const accountDescriptionForJobEmploymentResourceCenterDescription = await getDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION)
  const accountDescriptionForCurrencyExchangeResourceCenterDescription = await getDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION)
  const accountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription = await getDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION)

  // Add Digital Currency Account Description
  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
    digitalCurrencyAccountId: digitalCurrencyAccountId,
    descriptionType: BASIC_DESCRIPTION,
    digitalCurrencyAccountDescription: accountDescription
  })

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
    digitalCurrencyAccountId: digitalCurrencyAccountId,
    descriptionType: TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
    digitalCurrencyAccountDescription: accountDescriptionForTransactionResourceCenterDescription
  })

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
    digitalCurrencyAccountId: digitalCurrencyAccountId,
    descriptionType: PRODUCT_RESOURCE_CENTER_DESCRIPTION,
    digitalCurrencyAccountDescription: accountDescriptionForProductResourceCenterDescription
  })

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
    digitalCurrencyAccountId: digitalCurrencyAccountId,
    descriptionType: JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
    digitalCurrencyAccountDescription: accountDescriptionForJobEmploymentResourceCenterDescription
  })

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
    digitalCurrencyAccountId: digitalCurrencyAccountId,
    descriptionType: CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
    digitalCurrencyAccountDescription: accountDescriptionForCurrencyExchangeResourceCenterDescription
  })

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
    digitalCurrencyAccountId: digitalCurrencyAccountId,
    descriptionType: DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION,
    digitalCurrencyAccountDescription: accountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription
  })
}


export {
  getDigitalCurrencyAccountDescription
}

