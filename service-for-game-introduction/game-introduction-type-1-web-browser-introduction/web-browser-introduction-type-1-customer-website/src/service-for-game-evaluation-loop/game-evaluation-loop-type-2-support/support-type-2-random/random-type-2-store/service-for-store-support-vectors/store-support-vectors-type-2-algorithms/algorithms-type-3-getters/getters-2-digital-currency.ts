

import { GetterTree } from 'vuex'

import { RootState } from '../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import { DigitalCurrencyState } from '../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyAccount } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { Group } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-5-account-groups/account-groups-1'

import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import { DigitalCurrencyAccountDescription } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-1-basic-description'
import { DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-2-trasaction-resource-center-description'
import { DigitalCurrencyAccountDescriptionForProductResourceCenterDescription } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-3-product-resource-center-description'
import { DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-4-job-employment-resource-center-description'
import { DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-5-currency-exchange-resource-center-description'
import { DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-6-digital-currency-alternative-resource-center-description'

import { DigitalCurrencyTransaction } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'


import {
  BASIC_DESCRIPTION,
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-3-constants/constants-1-account-description'


const getters: GetterTree<DigitalCurrencyState, RootState> = {

  // Account

  getDigitalCurrencyAccount (state: DigitalCurrencyState): (digitalCurrencyAccountId: string) => DigitalCurrencyAccount {
    return (digitalCurrencyAccountId: string) => {
      const serviceDigitalCurrencyAccount = state.serviceAccountDigitalCurrencyAccountTable[digitalCurrencyAccountId]
      const digitalCurrencyAccount = state.digitalCurrencyAccountTable[digitalCurrencyAccountId]
      return serviceDigitalCurrencyAccount || digitalCurrencyAccount || new DigitalCurrencyAccount()
    }
  },

  getActiveDigitalCurrencyAccount (state: DigitalCurrencyState, _: any, rootState: RootState): DigitalCurrencyAccount {
    let accountId = ''
    if (rootState.serviceAccount) {
      accountId = rootState.serviceAccount.activeDigitalCurrencyAccountId
    }

    return state.serviceAccountDigitalCurrencyAccountTable[accountId] || {}
  },

  getDigitalCurrencyAccountDescription (state: DigitalCurrencyState): (digitalCurrencyAccountId: string, descriptionType: string) => any {
    return (digitalCurrencyAccountId: string, descriptionType: string) => {
      let descriptionObject: any

      if (descriptionType === BASIC_DESCRIPTION) {
        descriptionObject = state.digitalCurrencyAccountDescriptionTable[digitalCurrencyAccountId] || new DigitalCurrencyAccountDescription()
      } else if (descriptionType === TRANSACTION_RESOURCE_CENTER_DESCRIPTION) {
        descriptionObject = state.digitalCurrencyAccountDescriptionForTransactionResourceCenterDescriptionTable[digitalCurrencyAccountId] || new DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription()
      } else if (descriptionType === PRODUCT_RESOURCE_CENTER_DESCRIPTION) {
        descriptionObject = state.digitalCurrencyAccountDescriptionForProductResourceCenterDescriptionTable[digitalCurrencyAccountId] || new DigitalCurrencyAccountDescriptionForProductResourceCenterDescription()
      } else if (descriptionType === JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION) {
        descriptionObject = state.digitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescriptionTable[digitalCurrencyAccountId] || new DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription()
      } else if (descriptionType === CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION) {
        descriptionObject = state.digitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescriptionTable[digitalCurrencyAccountId] || new DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription()
      } else if (descriptionType === DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION) {
        descriptionObject = state.digitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescriptionTable[digitalCurrencyAccountId] || new DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription()
      }

      return descriptionObject
    }
  },

  getServiceAccountDigitalCurrencyAccountList (state: DigitalCurrencyState): DigitalCurrencyAccount[] {
    return Object.keys(state.serviceAccountDigitalCurrencyAccountTable).map((accountId: string) => {
      return state.serviceAccountDigitalCurrencyAccountTable[accountId]
    }).sort((digitalCurrencyAccountA: DigitalCurrencyAccount, digitalCurrencyAccountB: DigitalCurrencyAccount) => {
      return (new Date(digitalCurrencyAccountB.dateAccountCreated)).getTime() - (new Date(digitalCurrencyAccountA.dateAccountCreated)).getTime()
    })
  },

  getDigitalCurrencyAccountGroupList (state: DigitalCurrencyState): (digitalCurrencyAccountId: string) => Group[] {
    return (digitalCurrencyAccountId: string) => {
      if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]) {
        return []
      }
      return Object.keys(state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]).map((groupId: string) => {
        return state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId]
      }).sort((groupA: Group, groupB: Group) => groupB.dateCreated - groupA.dateCreated)
    }
  },

  getIsItemAlreadyInGroup (state: DigitalCurrencyState): (digitalCurrencyAccountId: string, groupId: string, itemId: string) => boolean {
    return (digitalCurrencyAccountId: string, groupId: string, itemId: string) => {

      if (!itemId) {
        return false
      }

      if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]) {
        return false
      }

      if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId]) {
        return false
      }

      return state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId].groupItemIdVariableTable[itemId] ? true : false
    }
  },

  getDigitalCurrencyAccountGroup (state: DigitalCurrencyState): (digitalCurrencyAccountId: string, groupId: string) => Group | null {
    return (digitalCurrencyAccountId: string, groupId: string) => {
      if (!digitalCurrencyAccountId || !groupId) {
        return null
      }

      if (!state.digitalCurrencyAccountGroup[digitalCurrencyAccountId]) {
        return null
      }

      return state.digitalCurrencyAccountGroup[digitalCurrencyAccountId][groupId]
    }
  },

  getDigitalCurrencyAccountSettings (state: DigitalCurrencyState): (digitalCurrencyAccountId: string) => DigitalCurrencyAccountSettings {
    return (digitalCurrencyAccountId: string) => {
      return state.digitalCurrencyAccountSettings[digitalCurrencyAccountId] || new DigitalCurrencyAccountSettings()
    }
  },

  getIsBlockedByDigitalCurrencyAccount (state: DigitalCurrencyState): (digitalCurrencyAccountId: string, blockedByAccountId: string) => boolean {
    return (digitalCurrencyAccountId: string, blockedByAccountId: string) => {
      const blockingAccountSettings = state.digitalCurrencyAccountSettings[blockedByAccountId]
      if (!blockingAccountSettings) {
        return false
      }

      if (!blockingAccountSettings.blockedAccountVariableTable) {
        return false
      }

      const isAccountBlocked = blockingAccountSettings.blockedAccountVariableTable[digitalCurrencyAccountId]
      return isAccountBlocked ? true : false
    }
  },

  // Transaction

  getDigitalCurrencyTransaction (state: DigitalCurrencyState): (digitalCurrencyTransactionId: string) => DigitalCurrencyTransaction {
    return (digitalCurrencyTransactionId: string) => {
      const digitalCurrencyTransaction = state.digitalCurrencyTransactionTable[digitalCurrencyTransactionId]
      return digitalCurrencyTransaction || new DigitalCurrencyTransaction()
    }
  }
}

export {
  getters
}





