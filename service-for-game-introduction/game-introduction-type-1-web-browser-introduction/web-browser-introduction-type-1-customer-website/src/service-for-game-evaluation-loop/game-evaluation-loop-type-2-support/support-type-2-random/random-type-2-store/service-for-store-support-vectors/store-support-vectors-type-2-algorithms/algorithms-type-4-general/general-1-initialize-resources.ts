
import { initializeResources1 } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-1-algorithms/algorithms-type-1-initialize-resources/initialize-resources-1'

import { DIGITAL_CURRENCY_ACCOUNT_INDEX_ID } from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-3-constants/constants-2-algoliasearch-index-id'

import {
  BASIC_DESCRIPTION,
  // TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-3-constants/constants-1-account-description'


/*
Algoliasearch / Attributes For Faceting:
https://www.algolia.com/doc/guides/managing-results/refine-results/filtering/how-to/filter-arrays/
*/


async function general1InitializeResources () {

  const algoliasearchDigitalCurrencyAccountIndexId = process.env.NODE_ENV === 'development' ? 'development_DIGITAL_CURRENCY_ACCOUNT' : (process.env.NODE_ENV === 'production' ? 'production_DIGITAL_CURRENCY_ACCOUNT' : '')
  const firebaseUseEmulator = (process.env.NODE_ENV === 'development') && (location.origin.indexOf('localhost') >= 0)

  await initializeResources1({
    distributionType: process.env.NODE_ENV,
    firebaseUseEmulator: firebaseUseEmulator,
    algoliasearchItemSearchIndexNameToIndexIdMap: {
      [DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]: algoliasearchDigitalCurrencyAccountIndexId
    },
    algoliasearchItemSearchIndexNameToOptionsMap: {
      [DIGITAL_CURRENCY_ACCOUNT_INDEX_ID]: {
        settings: {
          searchableAttributes: ['unordered(accountName)', 'unordered(accountUsername)', 'unordered(accountId)', `unordered(${BASIC_DESCRIPTION}.accountMinimumDescription)`, `unordered(${BASIC_DESCRIPTION}.accountMedianDescription)`],
          attributesForFaceting: [
            `${PRODUCT_RESOURCE_CENTER_DESCRIPTION}.willingToReceivePromotionsOfType`,
            `${JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION}.willingToReceivePromotionsOfType`,
            `${CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION}.willingToReceivePromotionsOfType`,
            `${DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION}.willingToReceivePromotionsOfType`
          ]
        }
      }
    }
  })
}


export {
  general1InitializeResources
}


