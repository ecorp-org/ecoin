



import { getDigitalCurrencyTransactionById1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-4-get-digital-currency-transaction-by-id/get-digital-currency-transaction-by-id-1'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import {
  UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE
} from '../../../algorithms-type-1-mutations/mutations-2-digital-currency'


async function getDigitalCurrencyTransactionById ({ rootState, commit, dispatch }: DigitalCurrencyActionParam, digitalCurrencyTransactionId: string) {
  const activeDigitalCurrencyAccountId = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''
  const digitalCurrencyTransaction = await getDigitalCurrencyTransactionById1(digitalCurrencyTransactionId)
  let isBlockedAccount = false

  if (!digitalCurrencyTransaction) {
    return
  }

  const accountSettings: DigitalCurrencyAccountSettings = await dispatch('getDigitalCurrencyAccountSettings', digitalCurrencyTransaction.accountSenderId)

  if (accountSettings) {
    if (accountSettings.blockedAccountVariableTable) {
      isBlockedAccount = accountSettings.blockedAccountVariableTable[activeDigitalCurrencyAccountId]
    }
  }

  if (isBlockedAccount) {
    return
  }

  commit(UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE, digitalCurrencyTransaction)

  return digitalCurrencyTransaction
}

export {
  getDigitalCurrencyTransactionById
}



