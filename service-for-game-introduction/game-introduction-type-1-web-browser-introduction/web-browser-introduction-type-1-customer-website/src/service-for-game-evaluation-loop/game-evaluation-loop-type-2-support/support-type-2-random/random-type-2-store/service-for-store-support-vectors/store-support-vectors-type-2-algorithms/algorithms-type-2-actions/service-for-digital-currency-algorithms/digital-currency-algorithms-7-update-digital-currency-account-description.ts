

import { updateDigitalCurrencyAccountDescription1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-11-update-digital-currency-account-description/update-digital-currency-account-description-1'

import { DigitalCurrencyAccount } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import {
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function updateDigitalCurrencyAccountDescription ({ commit }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, descriptionType, propertyId, propertyValue
}: {
  digitalCurrencyAccountId: string, descriptionType: string, propertyId: string, propertyValue: any
}): Promise<DigitalCurrencyAccount> {
  const digitalCurrencyAccountDescription = await updateDigitalCurrencyAccountDescription1(digitalCurrencyAccountId, descriptionType, propertyId, propertyValue)

  // Update account description
  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
    digitalCurrencyAccountId,
    descriptionType,
    digitalCurrencyAccountDescription
  })

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'edit',
    notificationMessage: `Account Updated`,
    notificationDate: new Date()
  } as Notification, { root: true })

  return digitalCurrencyAccountDescription
}


export {
  updateDigitalCurrencyAccountDescription
}

