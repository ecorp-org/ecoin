

import { getDigitalCurrencyAccountById1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-15-get-digital-currency-account-by-id/get-digital-currency-account-by-id-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'


async function getDigitalCurrencyAccountById ({ rootState, commit, dispatch }: DigitalCurrencyActionParam, digitalCurrencyAccountId: string) {
  const activeDigitalCurrencyAccountId = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''
  const digitalCurrencyAccount = await getDigitalCurrencyAccountById1(digitalCurrencyAccountId)
  let isBlockedAccount = false

  if (!digitalCurrencyAccount) {
    return
  }

  const accountSettings: DigitalCurrencyAccountSettings = await dispatch('getDigitalCurrencyAccountSettings', digitalCurrencyAccount.accountId)

  if (accountSettings) {
    if (accountSettings.blockedAccountVariableTable) {
      isBlockedAccount = accountSettings.blockedAccountVariableTable[activeDigitalCurrencyAccountId]
    }
  }

  if (isBlockedAccount) {
    return
  }

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)

  await dispatch('getDigitalCurrencyAccountDescription', digitalCurrencyAccount.accountId)
  await dispatch('getDigitalCurrencyAccountGroupList', digitalCurrencyAccount.accountId)

  return digitalCurrencyAccount
}

export {
  getDigitalCurrencyAccountById
}


