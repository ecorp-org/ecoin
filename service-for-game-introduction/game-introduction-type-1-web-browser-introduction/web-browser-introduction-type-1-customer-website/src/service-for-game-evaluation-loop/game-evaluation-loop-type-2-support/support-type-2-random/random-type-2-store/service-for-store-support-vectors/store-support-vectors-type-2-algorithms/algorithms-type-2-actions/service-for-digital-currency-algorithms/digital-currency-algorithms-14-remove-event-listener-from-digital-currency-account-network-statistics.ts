

import { removeEventListenerFromDigitalCurrencyAccountNetworkStatistics1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-18-remove-event-listener-from-digital-currency-account-network-statistics/remove-event-listener-from-digital-currency-account-network-statistics-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function removeEventListenerFromDigitalCurrencyAccountNetworkStatistics ({ }: DigitalCurrencyActionParam, {
  statisticsType, eventListenerId
}: { statisticsType: string, eventListenerId: string }) {
  await removeEventListenerFromDigitalCurrencyAccountNetworkStatistics1(statisticsType, eventListenerId)
}

export {
  removeEventListenerFromDigitalCurrencyAccountNetworkStatistics
}

