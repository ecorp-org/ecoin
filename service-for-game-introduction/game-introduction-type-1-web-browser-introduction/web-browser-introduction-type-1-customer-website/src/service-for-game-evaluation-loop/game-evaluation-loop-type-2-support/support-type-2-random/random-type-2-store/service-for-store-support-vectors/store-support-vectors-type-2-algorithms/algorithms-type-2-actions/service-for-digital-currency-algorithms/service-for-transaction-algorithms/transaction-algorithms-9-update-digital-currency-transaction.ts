



import { updateDigitalCurrencyTransaction1Algorithm } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-3-update-digital-currency-transaction/update-digital-currency-transaction-1'

import { DigitalCurrencyTransaction } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE
} from '../../../algorithms-type-1-mutations/mutations-2-digital-currency'


async function updateDigitalCurrencyTransaction ({ commit }: DigitalCurrencyActionParam, { digitalCurrencyTransactionId, propertyId, propertyValue }: {
  digitalCurrencyTransactionId: string, propertyId: string, propertyValue: any
}): Promise<DigitalCurrencyTransaction> {
  const digitalCurrencyTransaction = await updateDigitalCurrencyTransaction1Algorithm.function(digitalCurrencyTransactionId, propertyId, propertyValue)

  commit(UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE, digitalCurrencyTransaction)

  return digitalCurrencyTransaction
}


export {
  updateDigitalCurrencyTransaction
}



