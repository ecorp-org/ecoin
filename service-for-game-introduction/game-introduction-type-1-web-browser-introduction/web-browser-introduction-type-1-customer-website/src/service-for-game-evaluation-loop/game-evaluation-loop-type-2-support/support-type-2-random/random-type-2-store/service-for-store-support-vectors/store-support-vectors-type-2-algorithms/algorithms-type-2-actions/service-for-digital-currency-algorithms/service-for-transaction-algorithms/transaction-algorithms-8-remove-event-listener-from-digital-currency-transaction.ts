
import { removeEventListenerFromDigitalCurrencyTransaction1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-8-remove-event-listener-from-digital-currency-transaction/remove-event-listener-from-digital-currency-transaction-1'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function removeEventListenerFromDigitalCurrencyTransaction ({ }: DigitalCurrencyActionParam, {
  digitalCurrencyTransactionId, eventListenerId
}: { digitalCurrencyTransactionId: string, eventListenerId: string }) {
  await removeEventListenerFromDigitalCurrencyTransaction1(digitalCurrencyTransactionId, eventListenerId)
}

export {
  removeEventListenerFromDigitalCurrencyTransaction
}



