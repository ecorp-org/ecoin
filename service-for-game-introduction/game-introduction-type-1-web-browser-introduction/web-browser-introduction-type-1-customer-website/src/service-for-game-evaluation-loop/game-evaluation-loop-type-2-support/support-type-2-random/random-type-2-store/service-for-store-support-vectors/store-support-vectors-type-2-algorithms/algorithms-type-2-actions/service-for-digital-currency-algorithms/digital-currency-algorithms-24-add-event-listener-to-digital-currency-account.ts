
import { addEventListenerToDigitalCurrencyAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-32-add-event-listener-to-digital-currency-account/add-event-listener-to-digital-currency-account-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyAccount } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import {
  UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE,
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'



async function addEventListenerToDigitalCurrencyAccount ({ state, commit }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, eventListenerCallbackFunction
}: { digitalCurrencyAccountId: string, eventListenerCallbackFunction?: any }) {
  const eventListenerId = await addEventListenerToDigitalCurrencyAccount1(digitalCurrencyAccountId, (digitalCurrencyAccount: DigitalCurrencyAccount) => {

    const isServiceAccount = state.serviceAccountDigitalCurrencyAccountTable[digitalCurrencyAccountId] ? true : false

    if (isServiceAccount) {
      commit(UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)
    } else {
      commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)
    }

    if (eventListenerCallbackFunction) {
      eventListenerCallbackFunction(digitalCurrencyAccount)
    }
  })

  return eventListenerId
}

export {
  addEventListenerToDigitalCurrencyAccount
}

