


import { updateDigitalCurrencyAccountNotifications1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-35-update-digital-currency-account-notifications/update-digital-currency-account-notifications-1'

import { UpdateDigitalCurrencyAccountNotificationsOptions } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-7-account-notification/account-notification-2'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function updateDigitalCurrencyAccountNotifications ({ }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, serviceType, updateOptions
}: {
  digitalCurrencyAccountId: string,
  serviceType: string,
  updateOptions: UpdateDigitalCurrencyAccountNotificationsOptions
}) {
  await updateDigitalCurrencyAccountNotifications1(digitalCurrencyAccountId, serviceType, updateOptions)
}

export {
  updateDigitalCurrencyAccountNotifications
}


