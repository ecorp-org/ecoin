
import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'

import { RootState } from './service-for-store-support-vectors/store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import { mutations } from './service-for-store-support-vectors/store-support-vectors-type-2-algorithms/algorithms-type-1-mutations/mutations-1-general'
import { actions } from './service-for-store-support-vectors/store-support-vectors-type-2-algorithms/algorithms-type-2-actions/actions-1-general'

import { digitalCurrencyVuexModule } from './service-for-store-support-vectors/store-support-vectors-type-3-constants/constants-1-digital-currency'

import { general1InitializeResources } from './service-for-store-support-vectors/store-support-vectors-type-2-algorithms/algorithms-type-4-general/general-1-initialize-resources'

Vue.use(Vuex)

// Initialize Resources
general1InitializeResources().catch(() => {/* */})


// Vuex Store
const store: StoreOptions<RootState> = {
  state: {
    isMobileDevice: true,

    showNotificationList: false,
    notificationList: []
  },
  mutations,
  actions,
  modules: {
    digitalCurrency: digitalCurrencyVuexModule
  }
}

const vuexStore = new Vuex.Store<RootState>(store)

export default vuexStore

