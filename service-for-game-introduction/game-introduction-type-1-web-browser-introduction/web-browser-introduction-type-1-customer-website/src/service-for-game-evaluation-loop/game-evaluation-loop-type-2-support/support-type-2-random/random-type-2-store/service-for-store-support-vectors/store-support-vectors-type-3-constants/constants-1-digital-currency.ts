
// import Vuex, { Module } from 'vuex'
// import { Vue } from '../../../vue/constants/vue'
import { Module } from 'vuex'
import { DigitalCurrencyState } from '../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'
import { RootState } from '../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import { getters } from '../store-support-vectors-type-2-algorithms/algorithms-type-3-getters/getters-2-digital-currency'
import { mutations } from '../store-support-vectors-type-2-algorithms/algorithms-type-1-mutations/mutations-2-digital-currency'
import { actions } from '../store-support-vectors-type-2-algorithms/algorithms-type-2-actions/actions-2-digital-currency'

// Vue.use(Vuex)

const digitalCurrencyVuexModule: Module<DigitalCurrencyState, RootState> = {
  namespaced: true,
  state: {
    serviceAccountDigitalCurrencyAccountTable: {},
    digitalCurrencyAccountTable: {},
    digitalCurrencyAccountEventListenerIdTable: {},

    digitalCurrencyAccountDescriptionTable: {},
    digitalCurrencyAccountDescriptionForTransactionResourceCenterDescriptionTable: {},
    digitalCurrencyAccountDescriptionForProductResourceCenterDescriptionTable: {},
    digitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescriptionTable: {},
    digitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescriptionTable: {},
    digitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescriptionTable: {},

    digitalCurrencyAccountGroup: {},

    digitalCurrencyAccountSettings: {},

    digitalCurrencyAccountTransactionTable: {},

    digitalCurrencyTransactionTable: {}

    // yourDigitalCurrencyAccountTable: {},
    // otherDigitalCurrencyAccountTable: {},
    // digitalCurrencyAccountUsernameToAccountIdMap: {},

    // accountEventListenerTable: {},

    // transactionTableToAccountMap: {},
    // accountToTransactionMap: {},

    // showTransactionNotification: false,
    // activeCurrencyTransactionId: ''
  },
  getters,
  mutations,
  actions
}

export {
  digitalCurrencyVuexModule
}

