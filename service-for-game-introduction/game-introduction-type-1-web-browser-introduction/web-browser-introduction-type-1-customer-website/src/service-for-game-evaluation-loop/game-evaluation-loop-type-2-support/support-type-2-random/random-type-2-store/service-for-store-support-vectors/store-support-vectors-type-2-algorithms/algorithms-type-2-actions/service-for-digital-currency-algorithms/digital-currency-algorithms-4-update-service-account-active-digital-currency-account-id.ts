

import { updateServiceAccountActiveDigitalCurrencyAccountId1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-8-update-service-account-active-digital-currency-account-id/update-service-account-active-digital-currency-account-id-1'


import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  SET_SERVICE_ACCOUNT
} from '../../algorithms-type-1-mutations/mutations-1-general'

async function updateServiceAccountActiveDigitalCurrencyAccountId ({ rootState, commit }: DigitalCurrencyActionParam, activeDigitalCurrencyAccountId: string) {
  const serviceAccountId = rootState.serviceAccount ? rootState.serviceAccount.serviceAccountId : ''
  const serviceAccount = await updateServiceAccountActiveDigitalCurrencyAccountId1(serviceAccountId, activeDigitalCurrencyAccountId)
  commit(SET_SERVICE_ACCOUNT, serviceAccount, { root: true })
  return serviceAccount
}

export {
  updateServiceAccountActiveDigitalCurrencyAccountId
}

