

import { deleteDigitalCurrencyAccountGroup1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-25-delete-digital-currency-account-group/delete-digital-currency-account-group-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  DELETE_DIGITAL_CURRENCY_ACCOUNT_GROUP
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_GROUP
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function deleteDigitalCurrencyAccountGroup ({ commit }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, groupId
}: { digitalCurrencyAccountId: string, groupId: string }) {

  if (!digitalCurrencyAccountId || !groupId) {
    return
  }

  await deleteDigitalCurrencyAccountGroup1(digitalCurrencyAccountId, groupId)

  commit(DELETE_DIGITAL_CURRENCY_ACCOUNT_GROUP, {
    digitalCurrencyAccountId,
    groupId
  })

  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GROUP,
    notificationIconImageUrl: 'folder',
    notificationMessage: `Group Deleted`,
    notificationDate: new Date()
  } as Notification, { root: true })

}


export {
  deleteDigitalCurrencyAccountGroup
}
