

import { getDigitalCurrencyAccountNetworkStatistics1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-16-get-digital-currency-account-network-statistics/get-digital-currency-account-network-statistics-1'
import { GetAccountNetworkStatisticsOptions } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-4-account-network-statistics/account-network-statistics-2'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'


async function getDigitalCurrencyAccountNetworkStatistics ({ }: DigitalCurrencyActionParam, {
  statisticsType, statisticsOptions
}: { statisticsType: string, statisticsOptions?: GetAccountNetworkStatisticsOptions }) {
  const digitalCurrencyAccountNetworkStatistics = await getDigitalCurrencyAccountNetworkStatistics1(statisticsType, statisticsOptions)

  return digitalCurrencyAccountNetworkStatistics
}

export {
  getDigitalCurrencyAccountNetworkStatistics
}

