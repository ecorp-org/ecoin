
import { ActionTree } from 'vuex'

import { RootState } from '../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import { initializeServiceAccount } from './service-for-general-algorithms/general-algorithms-1-initialize-service-account'
import { uninitializeServiceAccount } from './service-for-general-algorithms/general-algorithms-2-uninitialize-service-account'
import { createServiceAccount } from './service-for-general-algorithms/general-algorithms-3-create-service-account'
import { createServiceAccountRecoveryMessage } from './service-for-general-algorithms/general-algorithms-4-create-service-account-recovery-message'
import { createServiceAccountRecoveryConfirmation } from './service-for-general-algorithms/general-algorithms-5-create-service-account-recovery-confirmation'
import { serviceInformation1GetServiceInformationByDaemonStatus } from './service-for-general-algorithms/service-for-service-information/service-information-1-get-service-information-by-daemon-status'


const actions: ActionTree<RootState, RootState> = {
  initializeServiceAccount,
  uninitializeServiceAccount,
  createServiceAccount,
  createServiceAccountRecoveryMessage,
  createServiceAccountRecoveryConfirmation,
  serviceInformation1GetServiceInformationByDaemonStatus
}

export {
  actions
}


