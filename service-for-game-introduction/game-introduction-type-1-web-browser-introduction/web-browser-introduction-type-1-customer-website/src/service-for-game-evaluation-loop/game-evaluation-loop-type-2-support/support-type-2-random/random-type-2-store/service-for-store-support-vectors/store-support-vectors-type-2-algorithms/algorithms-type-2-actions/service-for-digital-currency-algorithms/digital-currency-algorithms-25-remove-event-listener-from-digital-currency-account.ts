

import { removeEventListenerFromDigitalCurrencyAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-33-remove-event-listener-from-digital-currency-account/remove-event-listener-from-digital-currency-account-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function removeEventListenerFromDigitalCurrencyAccount ({ }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, eventListenerId
}: { digitalCurrencyAccountId: string, eventListenerId: string }) {
  await removeEventListenerFromDigitalCurrencyAccount1(digitalCurrencyAccountId, eventListenerId)
}

export {
  removeEventListenerFromDigitalCurrencyAccount
}


