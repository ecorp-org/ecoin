
import {
  getDigitalCurrencyAccountList1,
  GetDigitalCurrencyAccountListOptions
} from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-26-get-digital-currency-account-list/get-digital-currency-account-list-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'


async function getDigitalCurrencyAccountList ({ rootState, commit, dispatch }: DigitalCurrencyActionParam, {
  listType, options
}: { listType: string, options: GetDigitalCurrencyAccountListOptions }) {

  if (!listType) {
    return []
  }

  const activeDigitalCurrencyAccountId = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''
  const digitalCurrencyAccountResultList = await getDigitalCurrencyAccountList1(listType, options)

  let digitalCurrencyAccountList: any[] = []

  // Check account settings to see if blocked
  for (let i = 0; i < digitalCurrencyAccountResultList.length; i++) {
    const digitalCurrencyAccount = digitalCurrencyAccountResultList[i]
    let isBlockedAccount = false

    const accountSettings: DigitalCurrencyAccountSettings = await dispatch('getDigitalCurrencyAccountSettings', digitalCurrencyAccount.accountId)

    if (accountSettings) {
      if (accountSettings.blockedAccountVariableTable) {
        isBlockedAccount = accountSettings.blockedAccountVariableTable[activeDigitalCurrencyAccountId]
      }
    }

    if (isBlockedAccount) {
      continue
    }

    digitalCurrencyAccountList.push(digitalCurrencyAccount)
  }

  for (let i = 0; i < digitalCurrencyAccountList.length; i++) {
    const digitalCurrencyAccount = digitalCurrencyAccountList[i]
    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)

    await dispatch('getDigitalCurrencyAccountDescription', digitalCurrencyAccount.accountId)
    await dispatch('getDigitalCurrencyAccountGroupList', digitalCurrencyAccount.accountId)
  }

  return digitalCurrencyAccountList
}


export {
  getDigitalCurrencyAccountList
}

