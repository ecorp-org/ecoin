

import { deleteDigitalCurrencyTransaction1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-2-delete-digital-currency-transaction/delete-digital-currency-transaction-1'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  DELETE_FROM_DIGITAL_CURRENCY_TRANSACTION_TABLE
} from '../../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../../algorithms-type-1-mutations/mutations-1-general'

async function deleteDigitalCurrencyTransaction ({ commit }: DigitalCurrencyActionParam, digitalCurrencyTransactionId: string) {
  if (!digitalCurrencyTransactionId) {
    return
  }

  await deleteDigitalCurrencyTransaction1(digitalCurrencyTransactionId)
  commit(DELETE_FROM_DIGITAL_CURRENCY_TRANSACTION_TABLE, digitalCurrencyTransactionId)

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'delete',
    notificationMessage: `Transaction Deleted`,
    notificationDate: new Date()
  } as Notification, { root: true })

  return digitalCurrencyTransactionId
}


export {
  deleteDigitalCurrencyTransaction
}





