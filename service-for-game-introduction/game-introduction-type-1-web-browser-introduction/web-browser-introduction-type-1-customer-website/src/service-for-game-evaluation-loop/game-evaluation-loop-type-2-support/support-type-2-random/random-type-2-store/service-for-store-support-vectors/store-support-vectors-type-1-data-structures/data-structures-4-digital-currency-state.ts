

import { ActionContext } from 'vuex'

import { RootState } from './data-structures-1-root-state'

import { DigitalCurrencyAccount } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { DigitalCurrencyAccountDescription } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-1-basic-description'

import { DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-2-trasaction-resource-center-description'

import { DigitalCurrencyAccountDescriptionForProductResourceCenterDescription } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-3-product-resource-center-description'

import { DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-4-job-employment-resource-center-description'

import { DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-5-currency-exchange-resource-center-description'

import { DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-2-account-description/account-description-6-digital-currency-alternative-resource-center-description'

import { Group } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-5-account-groups/account-groups-1'

import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import { DigitalCurrencyTransaction } from '../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'


class DigitalCurrencyState {
  // Service Account

  serviceAccountDigitalCurrencyAccountTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccount
  } = {}

  // Account

  digitalCurrencyAccountTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccount
  } = {}

  // Account Event Listener

  digitalCurrencyAccountEventListenerIdTable: {
    [digitalCurrencyAccountId: string]: {
      [eventTypeId: string]: string
    }
  } = {}

  // Account Description

  digitalCurrencyAccountDescriptionTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccountDescription
  } = {}

  digitalCurrencyAccountDescriptionForTransactionResourceCenterDescriptionTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription
  } = {}

  digitalCurrencyAccountDescriptionForProductResourceCenterDescriptionTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccountDescriptionForProductResourceCenterDescription
  } = {}

  digitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescriptionTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription
  } = {}

  digitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescriptionTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription
  } = {}

  digitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescriptionTable: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription
  } = {}

  // Account Groups
  digitalCurrencyAccountGroup: {
    [digitalCurrencyAccountId: string]: {
      [groupId: string]: Group
    }
  } = {}

  // Account Settings
  digitalCurrencyAccountSettings: {
    [digitalCurrencyAccountId: string]: DigitalCurrencyAccountSettings
  } = {}

  // Account Transaction
  digitalCurrencyAccountTransactionTable: {
    [digitalCurrencyAccountId: string]: {
      createdTransactionTable: { [digitalCurrencyTransactionId: string]: number }
      receivedTransactionTable: { [digitalCurrencyTransactionId: string]: number }
    }
  } = {}


  // Transaction
  digitalCurrencyTransactionTable: {
    [digitalCurrencyTransactionId: string]: DigitalCurrencyTransaction
  } = {}


}

declare type DigitalCurrencyActionParam = ActionContext<DigitalCurrencyState, RootState>


export {
  DigitalCurrencyState,
  DigitalCurrencyActionParam,
  DigitalCurrencyAccount,
  DigitalCurrencyAccountDescription,
  DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription,
  DigitalCurrencyAccountDescriptionForProductResourceCenterDescription,
  DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription,
  DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription,
  DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription
}


