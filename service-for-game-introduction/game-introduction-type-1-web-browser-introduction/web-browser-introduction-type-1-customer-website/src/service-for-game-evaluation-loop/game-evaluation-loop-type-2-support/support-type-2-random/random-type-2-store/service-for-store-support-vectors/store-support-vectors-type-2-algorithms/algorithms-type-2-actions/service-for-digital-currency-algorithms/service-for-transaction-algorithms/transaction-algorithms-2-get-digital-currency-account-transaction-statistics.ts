

import { getDigitalCurrencyAccountTransactionStatistics1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-11-get-digital-currency-account-transaction-statistics/get-digital-currency-account-transaction-statistics-1'
import { GetAccountTransactionStatisticsOptions } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-3-transaction-account-statistics/transaction-account-statistics-2'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'


async function getDigitalCurrencyAccountTransactionStatistics ({ }: DigitalCurrencyActionParam, {
  statisticsType, statisticsOptions
}: { statisticsType: string, statisticsOptions?: GetAccountTransactionStatisticsOptions }) {
  const digitalCurrencyAccountTransactionStatistics = await getDigitalCurrencyAccountTransactionStatistics1(statisticsType, statisticsOptions)

  return digitalCurrencyAccountTransactionStatistics
}

export {
  getDigitalCurrencyAccountTransactionStatistics
}



