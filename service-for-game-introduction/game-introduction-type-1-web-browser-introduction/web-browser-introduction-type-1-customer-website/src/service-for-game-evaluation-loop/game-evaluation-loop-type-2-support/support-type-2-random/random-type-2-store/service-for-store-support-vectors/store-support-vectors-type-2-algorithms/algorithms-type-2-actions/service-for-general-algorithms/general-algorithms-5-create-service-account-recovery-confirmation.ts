


import { createServiceAccountRecoveryConfirmation1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-31-create-service-account-recovery-confirmation/create-service-account-recovery-confirmation-1'

import { ServiceAccountRecoveryConfirmationInformation } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'

import {
  RootActionParam,
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'

async function createServiceAccountRecoveryConfirmation ({ commit }: RootActionParam, recoveryConfirmationInformation: ServiceAccountRecoveryConfirmationInformation) {

  await createServiceAccountRecoveryConfirmation1(recoveryConfirmationInformation)

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'person',
    notificationMessage: `Password Reset`,
    notificationDate: new Date()
  } as Notification)
}

export {
  createServiceAccountRecoveryConfirmation
}

