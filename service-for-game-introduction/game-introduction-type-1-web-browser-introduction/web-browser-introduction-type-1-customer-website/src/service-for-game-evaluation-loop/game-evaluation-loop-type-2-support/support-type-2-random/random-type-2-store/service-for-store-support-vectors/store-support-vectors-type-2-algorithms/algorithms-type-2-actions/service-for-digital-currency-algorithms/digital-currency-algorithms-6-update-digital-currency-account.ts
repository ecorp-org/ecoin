

import { updateDigitalCurrencyAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-10-update-digital-currency-account/update-digital-currency-account-1'

import { DigitalCurrencyAccount } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function updateDigitalCurrencyAccount ({ commit }: DigitalCurrencyActionParam, { digitalCurrencyAccountId, propertyId, propertyValue }: {
  digitalCurrencyAccountId: string, propertyId: string, propertyValue: any
}): Promise<DigitalCurrencyAccount> {

  const digitalCurrencyAccount = await updateDigitalCurrencyAccount1(digitalCurrencyAccountId, propertyId, propertyValue)
  if (!digitalCurrencyAccount) {
    return digitalCurrencyAccount
  }

  commit(UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'edit',
    notificationMessage: `Account Updated`,
    notificationDate: new Date()
  } as Notification, { root: true })

  return digitalCurrencyAccount
}


export {
  updateDigitalCurrencyAccount
}





