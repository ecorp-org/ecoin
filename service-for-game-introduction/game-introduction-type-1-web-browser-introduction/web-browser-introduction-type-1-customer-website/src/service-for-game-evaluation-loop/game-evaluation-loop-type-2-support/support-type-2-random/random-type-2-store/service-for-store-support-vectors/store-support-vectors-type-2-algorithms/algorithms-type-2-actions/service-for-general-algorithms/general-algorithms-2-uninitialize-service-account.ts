

import { uninitializeServiceAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-2-uninitialize-service-account/uninitialize-service-account-1'

import { RootActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  SET_SERVICE_ACCOUNT
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function uninitializeServiceAccount ({ commit }: RootActionParam) {
  await uninitializeServiceAccount1()
  commit(SET_SERVICE_ACCOUNT, {})
}

export {
  uninitializeServiceAccount
}

