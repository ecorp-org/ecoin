

import { getServiceAccountDigitalCurrencyAccountList1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-6-get-service-account-digital-currency-account-list/get-service-account-digital-currency-account-list-1'
// import { getDigitalCurrencyAccountDescription1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-12-get-digital-currency-account-description/get-digital-currency-account-description-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  DIGITAL_CURRENCY_ACCOUNT_SERVICE_TYPE
} from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-1-general/general-type-3-constants/constants-5-service-type'

import {
  UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE,
  DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE,
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

async function getServiceAccountDigitalCurrencyAccountList ({ state, rootState, commit, dispatch }: DigitalCurrencyActionParam) {
  const serviceAccountId = rootState.serviceAccount ? rootState.serviceAccount.serviceAccountId : ''
  const digitalCurrencyAccountList = await getServiceAccountDigitalCurrencyAccountList1(serviceAccountId)

  // Remove Earlier List
  for (let digitalCurrencyAccountId in state.serviceAccountDigitalCurrencyAccountTable) {
    if (!state.serviceAccountDigitalCurrencyAccountTable.hasOwnProperty(digitalCurrencyAccountId)) {
      continue
    }
    commit(DELETE_FROM_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccountId)
  }

  // Update The Account
  for (let i = 0; i < digitalCurrencyAccountList.length; i++) {
    const digitalCurrencyAccount = digitalCurrencyAccountList[i]
    const digitalCurrencyAccountEventListenerId = await dispatch('addEventListenerToDigitalCurrencyAccount', { digitalCurrencyAccountId: digitalCurrencyAccount.accountId })
    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE, { digitalCurrencyAccountId: digitalCurrencyAccount.accountId, eventTypeId: DIGITAL_CURRENCY_ACCOUNT_SERVICE_TYPE, eventListenerId: digitalCurrencyAccountEventListenerId })
    commit(UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)
  }

  // Update The Account Description and Group List
  for (let i = 0; i < digitalCurrencyAccountList.length; i++) {
    await dispatch('getDigitalCurrencyAccountDescription', digitalCurrencyAccountList[i].accountId)
    await dispatch('getDigitalCurrencyAccountGroupList', digitalCurrencyAccountList[i].accountId)
    await dispatch('getDigitalCurrencyAccountSettings', digitalCurrencyAccountList[i].accountId)
  }

  return digitalCurrencyAccountList
}


export {
  getServiceAccountDigitalCurrencyAccountList
}

