

import { createServiceAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-29-create-service-account/create-service-account-1'

import { ServiceAccountCreateInformation } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'

import {
  RootActionParam,
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  SET_SERVICE_ACCOUNT,
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function createServiceAccount ({ commit }: RootActionParam, accountInformation: ServiceAccountCreateInformation) {
  const serviceAccount = await createServiceAccount1(accountInformation)
  commit(SET_SERVICE_ACCOUNT, serviceAccount)

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'person',
    notificationMessage: `New Service Account Created`,
    notificationDate: new Date()
  } as Notification)

  return serviceAccount
}

export {
  createServiceAccount
}


