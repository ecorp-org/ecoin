
import Vue from 'vue'
import { MutationTree } from 'vuex'
import { RootState, Notification } from '../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

const SET_SERVICE_ACCOUNT = 'SET_SERVICE_ACCOUNT'

const TOGGLE_IS_MOBILE_DEVICE = 'TOGGLE_IS_MOBILE_DEVICE'

const ADD_NOTIFICATION = 'ADD_NOTIFICATION'
const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION'
const TOGGLE_SHOW_NOTIFICATION_LIST = 'TOGGLE_SHOW_NOTIFICATION_LIST'


const mutations: MutationTree<RootState> = {

  [SET_SERVICE_ACCOUNT] (state, serviceAccount) {
    Vue.set(state, 'serviceAccount', serviceAccount)
  },

  [TOGGLE_IS_MOBILE_DEVICE] (state, isMobile: boolean) {
    state.isMobileDevice = isMobile
  },

  [ADD_NOTIFICATION] (state, notification: Notification) {
    // Vue.set(state.notificationList, state.notificationList.length, notification)

    state.notificationList.unshift(notification)

    // Remove the notification after 10 seconds
    setTimeout(() => {
      const indexOfNotification = state.notificationList.map((notificationItem: Notification) => notificationItem.notificationId).indexOf(notification.notificationId)

      if (indexOfNotification >= 0) {
        state.notificationList.splice(indexOfNotification, 1)
      }

      if (state.notificationList.length === 0) {
        state.showNotificationList = false
      }
    }, 10 * 1000)

    // No more than 10 notifications at a time
    if (state.notificationList.length > 10) {
      state.notificationList.pop()
    }

    state.showNotificationList = true
  },

  [REMOVE_NOTIFICATION] (state, notificationIndex: number) {
    state.notificationList.splice(notificationIndex, 1)

    if (state.notificationList.length === 0) {
      state.showNotificationList = false
    }
  },

  [TOGGLE_SHOW_NOTIFICATION_LIST] (state, isVisible?: boolean) {
    state.showNotificationList = isVisible !== undefined ? isVisible : !state.showNotificationList
  }
}

export {
  mutations,
  SET_SERVICE_ACCOUNT,
  TOGGLE_IS_MOBILE_DEVICE,
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  TOGGLE_SHOW_NOTIFICATION_LIST
}
