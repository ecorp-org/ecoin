

import { createDigitalCurrencyAccountGroup1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-20-create-digital-currency-account-group/create-digital-currency-account-group-1'

import { Group } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-5-account-groups/account-groups-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_GROUP
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'

async function createDigitalCurrencyAccountGroup ({ rootState, commit }: DigitalCurrencyActionParam, groupName: string): Promise<Group> {
  const activeDigitalCurrencyAccountId: string = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''
  const group = await createDigitalCurrencyAccountGroup1(activeDigitalCurrencyAccountId, groupName)

  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_GROUP_TABLE, {
    digitalCurrencyAccountId: activeDigitalCurrencyAccountId,
    group
  })

  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GROUP,
    notificationIconImageUrl: 'folder',
    notificationMessage: `Group Created`,
    notificationDate: group.dateCreated
  } as Notification, { root: true })

  return group
}


export {
  createDigitalCurrencyAccountGroup
}


