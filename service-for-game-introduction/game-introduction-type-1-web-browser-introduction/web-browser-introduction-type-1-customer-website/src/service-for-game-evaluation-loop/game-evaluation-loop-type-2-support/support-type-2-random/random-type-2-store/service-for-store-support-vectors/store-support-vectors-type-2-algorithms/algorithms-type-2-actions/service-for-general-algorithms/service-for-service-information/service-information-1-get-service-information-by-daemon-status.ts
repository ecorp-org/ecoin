

import { getServiceInformationByDaemonStatus1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-8-service-information/service-information-type-1-algorithms/algorithms-type-1-get-service-information-by-daemon-status/get-service-information-by-daemon-status-1'

import {
  RootActionParam
} from '../../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

async function serviceInformation1GetServiceInformationByDaemonStatus ({ }: RootActionParam, daemonId: string) {
  return getServiceInformationByDaemonStatus1(daemonId)
}

export {
  serviceInformation1GetServiceInformationByDaemonStatus
}

