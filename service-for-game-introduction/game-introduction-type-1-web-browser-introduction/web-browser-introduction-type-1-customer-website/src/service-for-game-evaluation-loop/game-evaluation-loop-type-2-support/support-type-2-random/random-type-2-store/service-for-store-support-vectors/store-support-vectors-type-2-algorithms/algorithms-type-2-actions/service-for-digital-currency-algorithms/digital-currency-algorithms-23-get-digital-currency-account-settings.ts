
import { getDigitalCurrencyAccountSettings1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-27-get-digital-currency-account-settings/get-digital-currency-account-settings-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

async function getDigitalCurrencyAccountSettings ({ commit }: DigitalCurrencyActionParam, digitalCurrencyAccountId: string) {

  const digitalCurrencyAccountSettings = await getDigitalCurrencyAccountSettings1(digitalCurrencyAccountId)

  // Add Digital Currency Account Description
  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE, {
    digitalCurrencyAccountId,
    digitalCurrencyAccountSettings
  })

  return digitalCurrencyAccountSettings
}


export {
  getDigitalCurrencyAccountSettings
}


