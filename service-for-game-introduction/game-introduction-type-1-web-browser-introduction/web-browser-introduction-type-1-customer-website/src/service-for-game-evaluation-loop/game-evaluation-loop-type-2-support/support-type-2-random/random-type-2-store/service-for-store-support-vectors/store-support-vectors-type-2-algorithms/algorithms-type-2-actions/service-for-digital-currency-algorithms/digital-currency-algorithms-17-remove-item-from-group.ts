


import { removeItemFromGroup1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-22-remove-item-from-group/remove-item-from-group-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  REMOVE_GROUP_ITEM_FROM_GROUP
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_GROUP
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function removeItemFromGroup ({ rootState, commit }: DigitalCurrencyActionParam, {
  groupId, itemId
}: { groupId: string, itemId: string }) {
  const activeDigitalCurrencyAccountId: string = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''

  if (!groupId || !itemId) {
    return
  }

  await removeItemFromGroup1(activeDigitalCurrencyAccountId, groupId, itemId)

  commit(REMOVE_GROUP_ITEM_FROM_GROUP, {
    digitalCurrencyAccountId: activeDigitalCurrencyAccountId,
    groupId,
    itemId
  })

  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GROUP,
    notificationIconImageUrl: 'folder',
    notificationMessage: `Removed Item From Group`,
    notificationDate: new Date()
  } as Notification, { root: true })

}


export {
  removeItemFromGroup
}
