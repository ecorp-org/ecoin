

import { addEventListenerToDigitalCurrencyAccountNetworkStatistics1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-17-add-event-listener-to-digital-currency-account-network-statistics/add-event-listener-to-digital-currency-account-network-statistics-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'


async function addEventListenerToDigitalCurrencyAccountNetworkStatistics ({ }: DigitalCurrencyActionParam, {
  statisticsType, eventListenerCallbackFunction
}: { statisticsType: string, eventListenerCallbackFunction: any }) {
  const eventListenerId = await addEventListenerToDigitalCurrencyAccountNetworkStatistics1(statisticsType, eventListenerCallbackFunction)

  return eventListenerId
}

export {
  addEventListenerToDigitalCurrencyAccountNetworkStatistics
}

