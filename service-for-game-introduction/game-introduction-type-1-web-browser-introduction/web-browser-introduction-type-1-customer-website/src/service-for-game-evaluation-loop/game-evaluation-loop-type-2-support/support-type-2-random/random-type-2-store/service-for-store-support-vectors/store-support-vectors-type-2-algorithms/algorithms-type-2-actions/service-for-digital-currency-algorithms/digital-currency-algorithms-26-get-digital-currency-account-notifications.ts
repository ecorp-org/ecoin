

import { getDigitalCurrencyAccountNotifications1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-34-get-digital-currency-account-notifications/get-digital-currency-account-notifications-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function getDigitalCurrencyAccountNotifications ({ }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, serviceType
}: {
  digitalCurrencyAccountId: string,
  serviceType: string
}) {
  const notificationList = await getDigitalCurrencyAccountNotifications1(digitalCurrencyAccountId, serviceType)
  return notificationList
}

export {
  getDigitalCurrencyAccountNotifications
}

