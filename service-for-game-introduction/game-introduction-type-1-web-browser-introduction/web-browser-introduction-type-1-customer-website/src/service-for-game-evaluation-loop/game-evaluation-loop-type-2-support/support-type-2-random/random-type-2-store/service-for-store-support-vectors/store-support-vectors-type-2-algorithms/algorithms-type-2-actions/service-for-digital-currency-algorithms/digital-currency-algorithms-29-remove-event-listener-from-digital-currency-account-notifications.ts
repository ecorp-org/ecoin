

import { removeEventListenerFromDigitalCurrencyAccountNotifications1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-37-remove-event-listener-from-digital-currency-account-notifications/remove-event-listener-from-digital-currency-account-notifications-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function removeEventListenerFromDigitalCurrencyAccountNotifications ({ }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, serviceType, eventListenerIdMap
}: {
  digitalCurrencyAccountId: string,
  serviceType: string,
  eventListenerIdMap: any
}) {
  await removeEventListenerFromDigitalCurrencyAccountNotifications1(digitalCurrencyAccountId, serviceType, eventListenerIdMap)
}

export {
  removeEventListenerFromDigitalCurrencyAccountNotifications
}

