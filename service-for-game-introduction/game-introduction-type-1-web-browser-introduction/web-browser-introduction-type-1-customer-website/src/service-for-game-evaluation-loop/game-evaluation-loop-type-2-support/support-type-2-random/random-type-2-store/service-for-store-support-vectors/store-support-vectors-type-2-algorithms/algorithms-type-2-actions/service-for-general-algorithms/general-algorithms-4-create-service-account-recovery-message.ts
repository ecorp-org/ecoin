

import { createServiceAccountRecoveryMessage1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-30-create-service-account-recovery-message/create-service-account-recovery-message-1'

import { ServiceAccountRecoveryInformation } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'

import {
  RootActionParam,
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'

async function createServiceAccountRecoveryMessage ({ commit }: RootActionParam, recoveryInformation: ServiceAccountRecoveryInformation) {

  await createServiceAccountRecoveryMessage1(recoveryInformation)

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'send',
    notificationMessage: `Password Reset Email Sent`,
    notificationDate: new Date()
  } as Notification)
}

export {
  createServiceAccountRecoveryMessage
}

