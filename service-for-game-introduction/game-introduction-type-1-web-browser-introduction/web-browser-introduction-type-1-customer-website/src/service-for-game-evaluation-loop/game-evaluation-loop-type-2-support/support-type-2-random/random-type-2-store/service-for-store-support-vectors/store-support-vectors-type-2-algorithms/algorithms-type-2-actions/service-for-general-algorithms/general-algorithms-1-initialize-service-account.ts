


import { initializeServiceAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-1-initialize-service-account/initialize-service-account-1'
import { isInitializedServiceAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-3-is-initialized-service-account/is-initialized-service-account-1'
import { getLocalServiceAccountInformation1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-4-get-local-service-account-information/get-local-service-account-information'

import { RootActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  SET_SERVICE_ACCOUNT
} from '../../algorithms-type-1-mutations/mutations-1-general'

import { ServiceAccountInitializationInformation } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-2-resource/resource-type-1-firebase/firebase-type-2-data-structures/data-structures-2-service-account'


async function initializeServiceAccount ({ commit, dispatch }: RootActionParam, serviceAccountInitializationInformation?: ServiceAccountInitializationInformation) {

  let { emailAddress, password, isAnonymousSignIn, isEmailAndPasswordSignIn, isGoogleSignIn } = serviceAccountInitializationInformation || {}

  let serviceAccount

  if (emailAddress !== undefined || isAnonymousSignIn !== undefined || isEmailAndPasswordSignIn !== undefined) {
    serviceAccount = await initializeServiceAccount1({
      emailAddress,
      password,
      isAnonymousSignIn,
      isEmailAndPasswordSignIn,
      isGoogleSignIn
    })
  } else {
    const isInitialized = await isInitializedServiceAccount1()

    if (isInitialized) {
      serviceAccount = await getLocalServiceAccountInformation1()
      console.log(`Already logged in`)
    } else {
      serviceAccount = await initializeServiceAccount1({
        isAnonymousSignIn: true
      })
      console.log(`New login`)
    }
  }

  commit(SET_SERVICE_ACCOUNT, serviceAccount)

  // Get Digital Currency Account List
  const digitalCurrencyAccountList = await dispatch('digitalCurrency/getServiceAccountDigitalCurrencyAccountList', null, { root: true })

  // Initialize the first account with anonymous users
  if (serviceAccount.isAnonymousAccount && digitalCurrencyAccountList.length === 0) {
    // Create an account
    await dispatch('digitalCurrency/createDigitalCurrencyAccount', null, { root: true })
  }

  return serviceAccount
}

export {
  initializeServiceAccount
}


