
import { ActionTree } from 'vuex'

import { RootState } from '../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'
import { DigitalCurrencyState } from '../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'


// Account
import { createDigitalCurrencyAccount } from './service-for-digital-currency-algorithms/digital-currency-algorithms-1-create-digital-currency-account'
import { getServiceAccountDigitalCurrencyAccountList } from './service-for-digital-currency-algorithms/digital-currency-algorithms-2-get-service-account-digital-currency-account-list'
import { getServiceAccount } from './service-for-digital-currency-algorithms/digital-currency-algorithms-3-get-service-account'
import { updateServiceAccountActiveDigitalCurrencyAccountId } from './service-for-digital-currency-algorithms/digital-currency-algorithms-4-update-service-account-active-digital-currency-account-id'
import { deleteDigitalCurrencyAccount } from './service-for-digital-currency-algorithms/digital-currency-algorithms-5-delete-digital-currency-account'
import { updateDigitalCurrencyAccount } from './service-for-digital-currency-algorithms/digital-currency-algorithms-6-update-digital-currency-account'
import { updateDigitalCurrencyAccountDescription } from './service-for-digital-currency-algorithms/digital-currency-algorithms-7-update-digital-currency-account-description'
import { getDigitalCurrencyAccountDescription } from './service-for-digital-currency-algorithms/digital-currency-algorithms-8-get-digital-currency-account-description'
import { getDigitalCurrencyAccountBySearchText } from './service-for-digital-currency-algorithms/digital-currency-algorithms-9-get-digital-currency-account-by-search-text'
import { getDigitalCurrencyAccountByUsername } from './service-for-digital-currency-algorithms/digital-currency-algorithms-10-get-digital-currency-account-by-username'
import { getDigitalCurrencyAccountById } from './service-for-digital-currency-algorithms/digital-currency-algorithms-11-get-digital-currency-account-by-id'
import { getDigitalCurrencyAccountNetworkStatistics } from './service-for-digital-currency-algorithms/digital-currency-algorithms-12-get-digital-currency-account-network-statistics'
import { addEventListenerToDigitalCurrencyAccountNetworkStatistics } from './service-for-digital-currency-algorithms/digital-currency-algorithms-13-add-event-listener-to-digital-currency-account-network-statistics'
import { removeEventListenerFromDigitalCurrencyAccountNetworkStatistics } from './service-for-digital-currency-algorithms/digital-currency-algorithms-14-remove-event-listener-from-digital-currency-account-network-statistics'
import { createDigitalCurrencyAccountGroup } from './service-for-digital-currency-algorithms/digital-currency-algorithms-15-create-digital-currency-account-group'
import { addItemToGroup } from './service-for-digital-currency-algorithms/digital-currency-algorithms-16-add-item-to-group'
import { removeItemFromGroup } from './service-for-digital-currency-algorithms/digital-currency-algorithms-17-remove-item-from-group'
import { getDigitalCurrencyAccountGroupList } from './service-for-digital-currency-algorithms/digital-currency-algorithms-18-get-digital-currency-account-group-list'
import { updateDigitalCurrencyAccountGroup } from './service-for-digital-currency-algorithms/digital-currency-algorithms-19-update-digital-currency-account-group'
import { deleteDigitalCurrencyAccountGroup } from './service-for-digital-currency-algorithms/digital-currency-algorithms-20-delete-digital-currency-account-group'
import { getDigitalCurrencyAccountList } from './service-for-digital-currency-algorithms/digital-currency-algorithms-21-get-digital-currency-account-list'
import { updateDigitalCurrencyAccountSettings } from './service-for-digital-currency-algorithms/digital-currency-algorithms-22-update-digital-currency-account-settings'
import { getDigitalCurrencyAccountSettings } from './service-for-digital-currency-algorithms/digital-currency-algorithms-23-get-digital-currency-account-settings'
import { addEventListenerToDigitalCurrencyAccount } from './service-for-digital-currency-algorithms/digital-currency-algorithms-24-add-event-listener-to-digital-currency-account'
import { removeEventListenerFromDigitalCurrencyAccount } from './service-for-digital-currency-algorithms/digital-currency-algorithms-25-remove-event-listener-from-digital-currency-account'
import { getDigitalCurrencyAccountNotifications } from './service-for-digital-currency-algorithms/digital-currency-algorithms-26-get-digital-currency-account-notifications'
import { updateDigitalCurrencyAccountNotifications } from './service-for-digital-currency-algorithms/digital-currency-algorithms-27-update-digital-currency-account-notifications'
import { addEventListenerToDigitalCurrencyAccountNotifications } from './service-for-digital-currency-algorithms/digital-currency-algorithms-28-add-event-listener-to-digital-currency-account-notifications'
import { removeEventListenerFromDigitalCurrencyAccountNotifications } from './service-for-digital-currency-algorithms/digital-currency-algorithms-29-remove-event-listener-from-digital-currency-account-notifications'


// Transaction

import { createDigitalCurrencyTransaction } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-1-create-digital-currency-transaction'
import { getDigitalCurrencyAccountTransactionStatistics } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-2-get-digital-currency-account-transaction-statistics'
import { addEventListenerToDigitalCurrencyAccountTransactionStatistics } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-3-add-event-listener-to-digital-currency-account-transaction-statistics'
import { removeEventListenerFromDigitalCurrencyAccountTransactionStatistics } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-4-remove-event-listener-from-digital-currency-account-transaction-statistics'
import { getDigitalCurrencyTransactionById } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-5-get-digital-currency-transaction-by-id'
import { deleteDigitalCurrencyTransaction } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-6-delete-digital-currency-transaction'
import { addEventListenerToDigitalCurrencyTransaction } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-7-add-event-listener-to-digital-currency-transaction'
import { removeEventListenerFromDigitalCurrencyTransaction } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-8-remove-event-listener-from-digital-currency-transaction'
import { updateDigitalCurrencyTransaction } from './service-for-digital-currency-algorithms/service-for-transaction-algorithms/transaction-algorithms-9-update-digital-currency-transaction'


const actions: ActionTree<DigitalCurrencyState, RootState> = {

  // Account
  createDigitalCurrencyAccount,
  getServiceAccountDigitalCurrencyAccountList,
  getServiceAccount,
  updateServiceAccountActiveDigitalCurrencyAccountId,
  deleteDigitalCurrencyAccount,
  updateDigitalCurrencyAccount,
  updateDigitalCurrencyAccountDescription,
  getDigitalCurrencyAccountDescription,
  getDigitalCurrencyAccountBySearchText,
  getDigitalCurrencyAccountByUsername,
  getDigitalCurrencyAccountById,
  getDigitalCurrencyAccountNetworkStatistics,
  addEventListenerToDigitalCurrencyAccountNetworkStatistics,
  removeEventListenerFromDigitalCurrencyAccountNetworkStatistics,
  createDigitalCurrencyAccountGroup,
  addItemToGroup,
  removeItemFromGroup,
  getDigitalCurrencyAccountGroupList,
  updateDigitalCurrencyAccountGroup,
  deleteDigitalCurrencyAccountGroup,
  getDigitalCurrencyAccountList,
  updateDigitalCurrencyAccountSettings,
  getDigitalCurrencyAccountSettings,
  addEventListenerToDigitalCurrencyAccount,
  removeEventListenerFromDigitalCurrencyAccount,
  getDigitalCurrencyAccountNotifications,
  updateDigitalCurrencyAccountNotifications,
  addEventListenerToDigitalCurrencyAccountNotifications,
  removeEventListenerFromDigitalCurrencyAccountNotifications,

  // Transaction
  createDigitalCurrencyTransaction,
  getDigitalCurrencyAccountTransactionStatistics,
  addEventListenerToDigitalCurrencyAccountTransactionStatistics,
  removeEventListenerFromDigitalCurrencyAccountTransactionStatistics,
  getDigitalCurrencyTransactionById,
  deleteDigitalCurrencyTransaction,
  addEventListenerToDigitalCurrencyTransaction,
  removeEventListenerFromDigitalCurrencyTransaction,
  updateDigitalCurrencyTransaction
}

export {
  actions
}


