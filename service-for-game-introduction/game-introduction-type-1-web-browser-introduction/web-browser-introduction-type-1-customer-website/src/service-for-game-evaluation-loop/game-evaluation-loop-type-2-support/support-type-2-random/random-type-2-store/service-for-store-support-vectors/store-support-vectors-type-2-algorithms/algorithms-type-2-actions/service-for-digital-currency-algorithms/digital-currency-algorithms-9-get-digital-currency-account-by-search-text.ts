
import { getDigitalCurrencyAccountBySearchText1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-13-get-digital-currency-account-by-search-text/get-digital-currency-account-by-search-text-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import { GetItemListOptions } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-2-resource/resource-type-2-algoliasearch/algoliasearch-type-2-data-structures/data-structures-1-get-item-list-options'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE,
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  BASIC_DESCRIPTION,
  TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
  PRODUCT_RESOURCE_CENTER_DESCRIPTION,
  JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
  CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
  DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION
} from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-3-constants/constants-1-account-description'


async function getDigitalCurrencyAccountBySearchText ({ rootState, commit, dispatch }: DigitalCurrencyActionParam, {
  searchText, options
}: { searchText: string, options?: GetItemListOptions }) {

  if (!searchText) {
    return []
  }

  const activeDigitalCurrencyAccountId = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''
  const digitalCurrencyAccountSearchResultList = await getDigitalCurrencyAccountBySearchText1(searchText, options)

  let digitalCurrencyAccountList: any[] = []

  // Check account settings to see if blocked
  for (let i = 0; i < digitalCurrencyAccountSearchResultList.length; i++) {
    const digitalCurrencyAccount = digitalCurrencyAccountSearchResultList[i]
    let isBlockedAccount = false

    const accountSettings: DigitalCurrencyAccountSettings = await dispatch('getDigitalCurrencyAccountSettings', digitalCurrencyAccount.accountId)

    if (accountSettings) {
      if (accountSettings.blockedAccountVariableTable) {
        isBlockedAccount = accountSettings.blockedAccountVariableTable[activeDigitalCurrencyAccountId]
      }
    }

    if (isBlockedAccount) {
      continue
    }

    digitalCurrencyAccountList.push(digitalCurrencyAccount)
  }


  for (let i = 0; i < digitalCurrencyAccountList.length; i++) {
    const digitalCurrencyAccount = digitalCurrencyAccountList[i]

    delete digitalCurrencyAccount['accountCurrencyAmount']

    // Commit descriptions
    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
      digitalCurrencyAccountId: digitalCurrencyAccount.accountId,
      descriptionType: BASIC_DESCRIPTION,
      digitalCurrencyAccountDescription: digitalCurrencyAccount[BASIC_DESCRIPTION]
    })
    delete digitalCurrencyAccount[BASIC_DESCRIPTION]

    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
      digitalCurrencyAccountId: digitalCurrencyAccount.accountId,
      descriptionType: TRANSACTION_RESOURCE_CENTER_DESCRIPTION,
      digitalCurrencyAccountDescription: digitalCurrencyAccount[TRANSACTION_RESOURCE_CENTER_DESCRIPTION]
    })
    delete digitalCurrencyAccount[TRANSACTION_RESOURCE_CENTER_DESCRIPTION]

    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
      digitalCurrencyAccountId: digitalCurrencyAccount.accountId,
      descriptionType: PRODUCT_RESOURCE_CENTER_DESCRIPTION,
      digitalCurrencyAccountDescription: digitalCurrencyAccount[PRODUCT_RESOURCE_CENTER_DESCRIPTION]
    })
    delete digitalCurrencyAccount[PRODUCT_RESOURCE_CENTER_DESCRIPTION]

    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
      digitalCurrencyAccountId: digitalCurrencyAccount.accountId,
      descriptionType: JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION,
      digitalCurrencyAccountDescription: digitalCurrencyAccount[JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION]
    })
    delete digitalCurrencyAccount[JOB_EMPLOYMENT_RESOURCE_CENTER_DESCRIPTION]

    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
      digitalCurrencyAccountId: digitalCurrencyAccount.accountId,
      descriptionType: CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION,
      digitalCurrencyAccountDescription: digitalCurrencyAccount[CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION]
    })
    delete digitalCurrencyAccount[CURRENCY_EXCHANGE_RESOURCE_CENTER_DESCRIPTION]

    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_DESCRIPTION_TABLE, {
      digitalCurrencyAccountId: digitalCurrencyAccount.accountId,
      descriptionType: DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION,
      digitalCurrencyAccountDescription: digitalCurrencyAccount[DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION]
    })
    delete digitalCurrencyAccount[DIGITAL_CURRENCY_ALTERNATIVE_RESOURCE_CENTER_DESCRIPTION]

    // Commit account
    commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)
  }
  return digitalCurrencyAccountList
}


export {
  getDigitalCurrencyAccountBySearchText
}

