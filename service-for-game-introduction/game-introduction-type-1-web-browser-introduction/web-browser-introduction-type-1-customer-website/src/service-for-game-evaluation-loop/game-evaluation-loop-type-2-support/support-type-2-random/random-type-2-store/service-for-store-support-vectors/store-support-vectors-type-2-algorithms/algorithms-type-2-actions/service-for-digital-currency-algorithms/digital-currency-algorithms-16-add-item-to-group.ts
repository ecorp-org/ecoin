

import { addItemToGroup1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-21-add-item-to-group/add-item-to-group-1'

import { GroupItem } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-5-account-groups/account-groups-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  ADD_GROUP_ITEM_TO_GROUP
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_GROUP
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function addItemToGroup ({ rootState, commit }: DigitalCurrencyActionParam, {
  groupId, itemId, itemType
}: { groupId: string, itemId: string, itemType?: string }) {
  const activeDigitalCurrencyAccountId: string = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''

  const groupItem = new GroupItem()
  groupItem.itemId = itemId
  groupItem.itemType = itemType || ''

  await addItemToGroup1(activeDigitalCurrencyAccountId, groupId, groupItem)

  commit(ADD_GROUP_ITEM_TO_GROUP, {
    digitalCurrencyAccountId: activeDigitalCurrencyAccountId,
    groupId,
    groupItem
  })

  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GROUP,
    notificationIconImageUrl: 'folder',
    notificationMessage: `Added Item to Group`,
    notificationDate: groupItem.dateAdded
  } as Notification, { root: true })

  return groupItem
}


export {
  addItemToGroup
}


