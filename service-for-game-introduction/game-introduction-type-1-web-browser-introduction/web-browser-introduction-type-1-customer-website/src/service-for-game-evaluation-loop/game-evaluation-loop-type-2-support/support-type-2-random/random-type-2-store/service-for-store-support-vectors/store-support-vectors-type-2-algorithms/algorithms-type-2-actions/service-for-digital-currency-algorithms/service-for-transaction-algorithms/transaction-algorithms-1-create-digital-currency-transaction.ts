
import { createDigitalCurrencyTransaction1Algorithm } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-1-create-digital-currency-transaction/create-digital-currency-transaction-1'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyTransaction } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE
} from '../../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_TRANSACTION
} from '../../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  ADD_NOTIFICATION
} from '../../../algorithms-type-1-mutations/mutations-1-general'


async function createDigitalCurrencyTransaction ({ commit }: DigitalCurrencyActionParam, digitalCurrencyTransactionInformation: DigitalCurrencyTransaction): Promise<DigitalCurrencyTransaction> {

  const digitalCurrencyTransaction = await createDigitalCurrencyTransaction1Algorithm.function(digitalCurrencyTransactionInformation)

  commit(UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE, digitalCurrencyTransaction)

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_TRANSACTION,
    notificationIconImageUrl: 'description',
    notificationMessage: `Transaction sent`,
    notificationMoreInformationUrl: `${location.origin}/transaction/${digitalCurrencyTransaction.transactionId}`,
    notificationDate: new Date(digitalCurrencyTransaction.dateTransactionCreated!)
  } as Notification, { root: true })

  return digitalCurrencyTransaction
}


export {
  createDigitalCurrencyTransaction
}

