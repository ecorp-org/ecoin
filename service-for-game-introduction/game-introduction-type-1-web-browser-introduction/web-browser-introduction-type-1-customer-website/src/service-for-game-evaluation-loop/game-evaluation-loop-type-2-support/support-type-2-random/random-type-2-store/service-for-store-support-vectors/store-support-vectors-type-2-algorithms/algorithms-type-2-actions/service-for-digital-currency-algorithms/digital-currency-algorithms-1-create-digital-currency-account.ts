
import { createDigitalCurrencyAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-5-create-digital-currency-account/create-digital-currency-account-1'
import { getServiceAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-7-get-service-account/get-service-account-1'

import { DigitalCurrencyAccount } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-1-service-data-structure/service-data-structure-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE,
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  Notification,
  NOTIFICATION_TYPE_ACCOUNT
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import {
  SET_SERVICE_ACCOUNT,
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'

async function createDigitalCurrencyAccount ({ rootState, commit, dispatch }: DigitalCurrencyActionParam): Promise<DigitalCurrencyAccount> {
  const serviceAccountId = rootState.serviceAccount ? rootState.serviceAccount.serviceAccountId : ''
  const digitalCurrencyAccount = await createDigitalCurrencyAccount1(serviceAccountId)
  const serviceAccount = await getServiceAccount1(serviceAccountId)
  const eventListenerId = await dispatch('addEventListenerToDigitalCurrencyAccount', { digitalCurrencyAccountId: digitalCurrencyAccount.accountId })

  // Add Service Account
  commit(SET_SERVICE_ACCOUNT, serviceAccount, { root: true })

  // Add Digital Currency Account
  commit(UPDATE_SERVICE_ACCOUNT_DIGITAL_CURRENCY_ACCOUNT_TABLE, digitalCurrencyAccount)
  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_EVENT_LISTENER_ID_TABLE, { digitalCurrencyAccountId: digitalCurrencyAccount.accountId, eventListenerId })

  // Add Digital Currency Account Description
  await dispatch('getDigitalCurrencyAccountDescription', digitalCurrencyAccount.accountId)
  await dispatch('getDigitalCurrencyAccountSettings', digitalCurrencyAccount.accountId)

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_ACCOUNT,
    notificationIconImageUrl: digitalCurrencyAccount.accountProfilePictureUrl,
    notificationMessage: `Congratulations, your new account has been created!`,
    notificationMoreInformationUrl: `${location.origin}/${digitalCurrencyAccount.accountUsername}`,
    notificationDate: new Date(digitalCurrencyAccount.dateAccountCreated)
  } as Notification, { root: true })

  return digitalCurrencyAccount
}


export {
  createDigitalCurrencyAccount
}

