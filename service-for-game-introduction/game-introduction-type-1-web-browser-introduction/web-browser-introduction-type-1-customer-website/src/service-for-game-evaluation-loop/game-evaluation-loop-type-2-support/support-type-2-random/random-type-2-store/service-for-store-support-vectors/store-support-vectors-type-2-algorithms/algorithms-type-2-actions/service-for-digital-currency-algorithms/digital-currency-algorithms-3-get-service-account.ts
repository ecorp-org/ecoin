
import { getServiceAccount1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-7-get-service-account/get-service-account-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  SET_SERVICE_ACCOUNT
} from '../../algorithms-type-1-mutations/mutations-1-general'

async function getServiceAccount ({ rootState, commit }: DigitalCurrencyActionParam) {
  const serviceAccount = await getServiceAccount1(rootState.serviceAccount!.serviceAccountId)

  commit(SET_SERVICE_ACCOUNT, serviceAccount, { root: true })

  return serviceAccount
}


export {
  getServiceAccount
}
