
import { addEventListenerToDigitalCurrencyAccountNotifications1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-36-add-event-listener-to-digital-currency-account-notifications/add-event-listener-to-digital-currency-account-notifications-1'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

async function addEventListenerToDigitalCurrencyAccountNotifications ({ }: DigitalCurrencyActionParam, {
  digitalCurrencyAccountId, serviceType, eventListenerCallbackFunction
}: {
  digitalCurrencyAccountId: string,
  serviceType: string,
  eventListenerCallbackFunction?: any
}) {
  const eventListenerIdMap = await addEventListenerToDigitalCurrencyAccountNotifications1(digitalCurrencyAccountId, serviceType, eventListenerCallbackFunction)
  return eventListenerIdMap
}

export {
  addEventListenerToDigitalCurrencyAccountNotifications
}


