

import { addEventListenerToDigitalCurrencyAccountTransactionStatistics1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-5-add-event-listener-to-digital-currency-account-transaction-statistics/add-event-listener-to-digital-currency-account-transaction-statistics-1'
import { GetAccountTransactionStatisticsOptions } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-3-transaction-account-statistics/transaction-account-statistics-2'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'


async function addEventListenerToDigitalCurrencyAccountTransactionStatistics ({ }: DigitalCurrencyActionParam, {
  statisticsType, eventListenerCallbackFunction, statisticsOptions
}: {
  statisticsType: string,
  eventListenerCallbackFunction: any,
  statisticsOptions?: GetAccountTransactionStatisticsOptions
}) {
  const eventListenerId = await addEventListenerToDigitalCurrencyAccountTransactionStatistics1(statisticsType, eventListenerCallbackFunction, statisticsOptions)

  return eventListenerId
}

export {
  addEventListenerToDigitalCurrencyAccountTransactionStatistics
}

