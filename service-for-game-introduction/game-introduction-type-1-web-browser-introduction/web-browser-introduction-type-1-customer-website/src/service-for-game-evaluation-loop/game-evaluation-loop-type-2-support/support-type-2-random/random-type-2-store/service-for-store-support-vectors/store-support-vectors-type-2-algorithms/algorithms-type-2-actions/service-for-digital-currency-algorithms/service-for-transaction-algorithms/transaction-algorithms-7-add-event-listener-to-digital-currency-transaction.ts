

import { addEventListenerToDigitalCurrencyTransaction1 } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-1-algorithms/algorithms-type-7-add-event-listener-to-digital-currency-transaction/add-event-listener-to-digital-currency-transaction-1'

import { DigitalCurrencyActionParam } from '../../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import { DigitalCurrencyTransaction } from '../../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-3-digital-currency-transaction/digital-currency-transaction-type-2-data-structures/data-structures-type-1-transaction/transaction-1'

import {
  UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE
} from '../../../algorithms-type-1-mutations/mutations-2-digital-currency'


async function addEventListenerToDigitalCurrencyTransaction ({ commit }: DigitalCurrencyActionParam, {
  digitalCurrencyTransactionId, eventListenerCallbackFunction
}: { digitalCurrencyTransactionId: string, eventListenerCallbackFunction?: any }) {
  const eventListenerId = await addEventListenerToDigitalCurrencyTransaction1(digitalCurrencyTransactionId, (digitalCurrencyTransaction: DigitalCurrencyTransaction) => {

    commit(UPDATE_DIGITAL_CURRENCY_TRANSACTION_TABLE, digitalCurrencyTransaction)

    if (eventListenerCallbackFunction) {
      eventListenerCallbackFunction(digitalCurrencyTransaction)
    }
  })

  return eventListenerId
}

export {
  addEventListenerToDigitalCurrencyTransaction
}



