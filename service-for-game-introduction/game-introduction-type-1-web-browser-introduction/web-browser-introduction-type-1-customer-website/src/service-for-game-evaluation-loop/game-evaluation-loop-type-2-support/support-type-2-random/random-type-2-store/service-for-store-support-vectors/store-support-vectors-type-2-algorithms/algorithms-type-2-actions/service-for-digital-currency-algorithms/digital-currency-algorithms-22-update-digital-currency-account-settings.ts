



import { updateDigitalCurrencyAccountSettings1 } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-1-algorithms/algorithms-type-28-update-digital-currency-account-settings/update-digital-currency-account-settings-1'

import { DigitalCurrencyAccountSettings } from '../../../../../../../../../../../../service-for-game-codebase/game-codebase-type-1-app/app-type-2-digital-currency-account/digital-currency-account-type-2-data-structures/data-structures-type-6-account-settings/account-settings-1'

import {
  Notification,
  NOTIFICATION_TYPE_GENERAL
} from '../../../store-support-vectors-type-1-data-structures/data-structures-1-root-state'

import { DigitalCurrencyActionParam } from '../../../store-support-vectors-type-1-data-structures/data-structures-4-digital-currency-state'

import {
  UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE
} from '../../algorithms-type-1-mutations/mutations-2-digital-currency'

import {
  ADD_NOTIFICATION
} from '../../algorithms-type-1-mutations/mutations-1-general'


async function updateDigitalCurrencyAccountSettings ({ rootState, commit }: DigitalCurrencyActionParam, {
  propertyId, propertyValue
}: {
  propertyId: string, propertyValue: any
}): Promise<DigitalCurrencyAccountSettings> {
  const digitalCurrencyAccountId = rootState.serviceAccount ? rootState.serviceAccount.activeDigitalCurrencyAccountId : ''
  const digitalCurrencyAccountSettings = await updateDigitalCurrencyAccountSettings1(digitalCurrencyAccountId, propertyId, propertyValue)

  // Update account description
  commit(UPDATE_DIGITAL_CURRENCY_ACCOUNT_SETTINGS_TABLE, {
    digitalCurrencyAccountId,
    digitalCurrencyAccountSettings
  })

  // Initiate Notification
  commit(ADD_NOTIFICATION, {
    notificationId: `${Math.random()}`,
    notificationType: NOTIFICATION_TYPE_GENERAL,
    notificationIconImageUrl: 'edit',
    notificationMessage: `Account Updated`,
    notificationDate: new Date()
  } as Notification, { root: true })

  return digitalCurrencyAccountSettings
}


export {
  updateDigitalCurrencyAccountSettings
}

