import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

import YourAccount1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-1-your-account/your-account-1.vue'
import NetworkStatus1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-2-network-status/network-status-1.vue'
import Settings1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-3-settings/settings-1.vue'
import About1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-4-about/about-1.vue'
import Account1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-5-account/account-1.vue'
import Transaction1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-6-transaction/transaction-1.vue'
import ProductResource1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-7-product-resource/product-resource-1.vue'
import JobEmploymentPosition1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-8-job-employment-position/job-employment-position-1.vue'
import CurrencyExchaangeTrade1 from '../../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-2-application-specific/application-specific-type-2-pages/pages-type-9-currency-exchange-trade/currency-exchange-trade-1.vue'


import {
  YOUR_ACCOUNT_PAGE_INFORMATION,
  NETWORK_STATUS_PAGE_INFORMATION,
  SETTINGS_PAGE_INFORMATION,
  ABOUT_PAGE_INFORMATION
} from '../../../../../../../../service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/project-variables-2-customer-website-page-information'


Vue.use(VueRouter)

const routes: RouteConfig[] = [
  { path: '/', name: YOUR_ACCOUNT_PAGE_INFORMATION.uid, component: YourAccount1 },
  { path: '/network-status', name: NETWORK_STATUS_PAGE_INFORMATION.uid, component: NetworkStatus1 },
  { path: '/settings', name: SETTINGS_PAGE_INFORMATION.uid, component: Settings1 },
  { path: '/about', name: ABOUT_PAGE_INFORMATION.uid, component: About1 },
  { path: '/:accountUsername', name: 'Account', component: Account1 },
  // { path: '/account/:accountId, name: 'AccountByAccountId', component: account1 },
  { path: '/transaction/:transactionId', name: 'Transaction', component: Transaction1 },
  { path: '/product-resource/:productResourceId', name: 'ProductResource', component: ProductResource1 },
  { path: '/job-employment-position/:jobEmploymentPositionId', name: 'JobEmploymentPosition', component: JobEmploymentPosition1 },
  { path: '/currency-exchange-trade/:currencyExchangeTradeId', name: 'CurrencyExchangeTrade', component: CurrencyExchaangeTrade1 }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (_to, _from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    return { x: 0, y: 0 }
  }
})

export default router
