

class DigitalCurrencyAccountDescription {
  accountId: string = ''

  accountMinimumDescription: string = ''
  accountMedianDescription: string = ''

  phoneNumber: string = ''
  emailAddress: string = ''
  homeWebsiteAddress: string = ''

  physicalLocationAddress: PhysicalLocationAddress = new PhysicalLocationAddress()

  socialMediaAccountList: SocialMediaAccount[] = []
  generalUserDescriptionList: GeneralUserDescription[] = []
}

class PhysicalLocationAddress {
  streetAndHouseNumber: string = ''
  cityTownName: string = ''
  stateProvinceName: string = ''
  zipCode: string = ''
  countryName: string = ''
}

class SocialMediaAccount {
  socialMediaItemName: string = ''
  socialMediaUrl: string = ''
}

class GeneralUserDescription {
  generalUserDescriptionTitle: string = ''
  generalUserDescriptionValue: string = ''
}


export {
  DigitalCurrencyAccountDescription,
  PhysicalLocationAddress,
  SocialMediaAccount,
  GeneralUserDescription
}




