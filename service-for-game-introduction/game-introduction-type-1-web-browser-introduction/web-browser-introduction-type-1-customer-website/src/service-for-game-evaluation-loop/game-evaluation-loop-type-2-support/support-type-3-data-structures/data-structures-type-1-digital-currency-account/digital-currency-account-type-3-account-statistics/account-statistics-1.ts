
// `Account Statistics`

// .......................................................................

// (1) Account Visited History (visited, transacted with) [`Data Table`]

class DigitalCurrencyAccountStatisticsForAccountVisitedHistory {
  //
}

// .......................................................................

// (1) Number of Accounts Sent A Transaction To [`Bar Graph`; number per day]

class DigitalCurrencyAccountStatisticsForNumberOfAccountsSentATransactionTo {
  //
}

// (2) Number of Accounts Received A Transaction From [`Bar Graph`; number per day]

class DigitalCurrencyAccountStatisticsForNumberOfAccountsReceivedATransactionFrom {
  //
}

// (3)

// .......................................................................

// (1) Total Ecoin Balance [`Bar Graph`; number per day]

class DigitalCurrencyAccountStatisticsForTotalDigialCurrencyBalance {
  //
}

// (2) Total Recurring Transaction Income [`Bar Graph`; number per day]

class DigitalCurrencyAccountStatisticsForTotalRecurringTransactionIncome {
  //
}


// (3) Total Recurring Transaction Exports [`Bar Graph`; number per day]

class DigitalCurrencyAccountStatisticsForTotalRecurringTransactionExports {
  //
}



export {
  DigitalCurrencyAccountStatisticsForAccountVisitedHistory,
  DigitalCurrencyAccountStatisticsForNumberOfAccountsSentATransactionTo,
  DigitalCurrencyAccountStatisticsForNumberOfAccountsReceivedATransactionFrom,
  DigitalCurrencyAccountStatisticsForTotalDigialCurrencyBalance,
  DigitalCurrencyAccountStatisticsForTotalRecurringTransactionIncome,
  DigitalCurrencyAccountStatisticsForTotalRecurringTransactionExports
}

