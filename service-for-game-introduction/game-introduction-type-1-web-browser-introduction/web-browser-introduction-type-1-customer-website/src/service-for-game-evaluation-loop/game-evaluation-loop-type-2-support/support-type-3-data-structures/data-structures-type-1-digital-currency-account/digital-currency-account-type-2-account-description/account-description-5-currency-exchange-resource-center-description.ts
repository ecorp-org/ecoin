
import { GeneralUserDescription } from './account-description-1-basic-description'

class DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription {
  accountId: string = ''

  currencyExchangeDescription: string = ''
  currencyTradeList: string[] = []
  estimatedTimeToDeliverTradePayout: string = ''

  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}

export {
  DigitalCurrencyAccountDescriptionForCurrencyExchangeResourceCenterDescription
}

