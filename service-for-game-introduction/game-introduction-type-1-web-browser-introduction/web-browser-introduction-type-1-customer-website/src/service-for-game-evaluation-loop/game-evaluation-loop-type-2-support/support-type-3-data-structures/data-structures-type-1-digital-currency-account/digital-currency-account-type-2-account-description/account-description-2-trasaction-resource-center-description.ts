
import { GeneralUserDescription } from './account-description-1-basic-description'


class DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription {
  accountId: string = ''

  placesWhereTransactionsAreAcceptedFrom: string = ''
  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}

export {
  DigitalCurrencyAccountDescriptionForTransactionResourceCenterDescription
}

