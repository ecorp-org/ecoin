
import { GeneralUserDescription } from './account-description-1-basic-description'

class DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription {
  accountId: string = ''

  companyWorkCulture: string = ''
  currentlyHiringPositionList: string[] = []
  estimatedTimeToHire: string = ''

  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}

export {
  DigitalCurrencyAccountDescriptionForJobEmploymentResourceCenterDescription
}

