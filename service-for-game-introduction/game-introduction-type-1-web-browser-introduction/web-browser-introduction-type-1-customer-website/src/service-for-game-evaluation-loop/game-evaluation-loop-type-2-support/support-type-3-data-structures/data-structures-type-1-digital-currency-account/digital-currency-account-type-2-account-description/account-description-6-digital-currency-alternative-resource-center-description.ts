
import { GeneralUserDescription } from './account-description-1-basic-description'

class DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription {
  accountId: string = ''

  businessDescription: string = ''
  businessFeatureList: string[] = []
  businessWebsiteUrl: string = ''

  willingToReceivePromotions: boolean = false
  willingToReceivePromotionsOfType: string[] = []

  generalUserDescriptionList: GeneralUserDescription[] = []
}

export {
  DigitalCurrencyAccountDescriptionForDigitalCurrencyAlternativeResourceCenterDescription
}

