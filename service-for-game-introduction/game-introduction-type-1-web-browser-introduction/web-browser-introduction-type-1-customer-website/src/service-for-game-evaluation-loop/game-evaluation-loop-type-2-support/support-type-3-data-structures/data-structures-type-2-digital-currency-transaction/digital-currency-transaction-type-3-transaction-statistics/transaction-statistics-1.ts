

// `Transaction Statistics`


// .......................................................................

// (1) Transaction History (sent, received) [`Data Table`]


class DigitalCurrencyTransactionStatisticsForTransactionHistory {
  latestProcessedTransactionId: string = ''
  numberOfProcessedTransactions: number = 0
  isAdminCurrentlyProcessingTransaction: boolean = false

  transactionIdVariableTree: { [transactionId: string]: boolean } = {}

  recurringTransactionIdVariableTree: {
    [recurringTransactionId: string]: { [transactionId: string]: boolean }
  } = {}

  processedTransactionVariableTree: {
    [transactionId: string]: ProcessedTransactionVariableTreeItem
  } = {}
}

class ProcessedTransactionVariableTreeItem {
  transactionOrderIndex: number = 0
  digitalCurrencyAmountBeforeTransaction: number = 0
}


// .......................................................................

// (1) Amount of Ecoin Sent [`Bar Graph`; number per day]


class DigitalCurrencyTransactionStatisticsForAmountOfDigitalCurrencySent {
  //
}



// (2) Amount of Ecoin Received [`Bar Graph`; number per day]

class DigitalCurrencyTransactionStatisticsForAmountOfDigitalCurrencyReceived {
  //
}



// .......................................................................

// (1) Number of Recurring Transactions Sent [`Bar Graph`; number per day]

class DigitalCurrencyTransactionStatisticsForNumberOfRecurringTransactionsSent {
  //
}


// (2) Number of Recurring Transactions Received [`Bar Graph`; number per day]

class DigitalCurrencyTransactionStatisticsForNumberOfRecurringTransactionsReceived {
  //
}

export {
  DigitalCurrencyTransactionStatisticsForTransactionHistory,
  DigitalCurrencyTransactionStatisticsForAmountOfDigitalCurrencySent,
  DigitalCurrencyTransactionStatisticsForAmountOfDigitalCurrencyReceived,
  DigitalCurrencyTransactionStatisticsForNumberOfRecurringTransactionsSent,
  DigitalCurrencyTransactionStatisticsForNumberOfRecurringTransactionsReceived
}

