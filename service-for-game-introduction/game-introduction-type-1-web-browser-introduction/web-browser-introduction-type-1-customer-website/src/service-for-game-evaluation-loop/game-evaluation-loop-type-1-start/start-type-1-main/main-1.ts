import Vue from 'vue'

import app1 from '../../../service-for-game-introduction/game-introduction-type-1-html-components/html-components-type-1-basic/basic-type-1-app/app-1.vue'

import '../../game-evaluation-loop-type-2-support/support-type-2-random/random-type-3-service-worker/service-for-service-worker-support-vectors/service-worker-support-vector-type-1-register'

import router1 from '../../game-evaluation-loop-type-2-support/support-type-2-random/random-type-1-router/router-1'
import store1 from '../../game-evaluation-loop-type-2-support/support-type-2-random/random-type-2-store/store-1'
import { vuetify } from '../../game-evaluation-loop-type-2-support/support-type-1-plugins/plugins-type-1-vuetify'

Vue.config.productionTip = false

new Vue({
  router: router1,
  store: store1,
  vuetify,
  render: (h) => h(app1)
}).$mount('#app')
