

const TRANSACTION_TYPE_LIST = [
  { transactionName: 'General', transactionIconId: 'person' },
  { transactionName: 'Voucher', transactionIconId: 'shopping_bag' },
  { transactionName: 'Gift', transactionIconId: 'card_giftcard' },
  { transactionName: 'Donation', transactionIconId: 'card_giftcard' },
  { transactionName: 'Product Resource Purchase', transactionIconId: 'shopping_cart' },
  { transactionName: 'Product Resource Promotion', transactionIconId: 'shopping_cart' },
  { transactionName: 'Currency Exchange Trade Purchase', transactionIconId: 'transfer_within_a_station' },
  { transactionName: 'Currency Exchange Trade Promotion', transactionIconId: 'transfer_within_a_station' },
  { transactionName: 'Job Employment Application Fee', transactionIconId: 'work' },
  { transactionName: 'Job Employment Application Promotion', transactionIconId: 'work' },
  { transactionName: 'Job Employment Salary', transactionIconId: 'work' },
  { transactionName: 'Coupon / Discount Promotion', transactionIconId: 'monetization_on' },
  { transactionName: 'Investment', transactionIconId: 'update' },
  { transactionName: 'Investment Return Payment', transactionIconId: 'update' },
  { transactionName: 'Refund Payment', transactionIconId: 'backspace' },
  { transactionName: 'Universal Basic Income', transactionIconId: 'restore' },
  { transactionName: 'Conditional Income', transactionIconId: 'restore' },
  { transactionName: 'Conditionless Income', transactionIconId: 'restore' },
  { transactionName: 'Digital Currency Alternative Purchase', transactionIconId: 'public' },
  { transactionName: 'Digital Currency Alternative Promotion', transactionIconId: 'public' }
]

const TRANSACTION_PROMOTION_TYPE_LIST = [
  'Product Resource Promotion',
  'Currency Exchange Promotion',
  'Job Employment Application Promotion',
  'Coupon / Discount Promotion',
  'Digital Currency Alternative Promotion'
]

const PRODUCT_RESOURCE_TYPE_LIST = [
  { productResourceName: 'Arts & Crafts', productResourceIconId: 'palette' },
  { productResourceName: 'Automotive', productResourceIconId: 'directions_car' },
  { productResourceName: 'Baby', productResourceIconId: 'child_care' },
  { productResourceName: 'Beauty & Personal Care', productResourceIconId: 'visibility' },
  { productResourceName: 'Books', productResourceIconId: 'menu_book' },
  { productResourceName: 'Computers', productResourceIconId: 'computer' },
  { productResourceName: 'Digital Music', productResourceIconId: 'headset' },
  { productResourceName: 'Electronics', productResourceIconId: 'speaker' },
  { productResourceName: 'Women\'s Fashion', productResourceIconId: 'checkroom' },
  { productResourceName: 'Men\'s Fashion', productResourceIconId: 'checkroom' },
  { productResourceName: 'Girls\' Fashion', productResourceIconId: 'checkroom' },
  { productResourceName: 'Boys\' Fashion', productResourceIconId: 'checkroom' },
  { productResourceName: 'Grocery & Gourmet Foods', productResourceIconId: 'restaurant' },
  { productResourceName: 'Health & Household', productResourceIconId: 'local_hospital' },
  { productResourceName: 'Home & Kitchen', productResourceIconId: 'home' },
  { productResourceName: 'Industrial & Scientific', productResourceIconId: 'construction' },
  { productResourceName: 'Luggage', productResourceIconId: 'work' },
  { productResourceName: 'Movies & TV', productResourceIconId: 'movie' },
  { productResourceName: 'Music, CDs & Vinyl', productResourceIconId: 'album' },
  { productResourceName: 'Pet Supplies', productResourceIconId: 'pets' },
  { productResourceName: 'Software', productResourceIconId: 'wysiwyg' },
  { productResourceName: 'Sports & Outdoors', productResourceIconId: 'sports_tennis' },
  { productResourceName: 'Tools & Home Improvement', productResourceIconId: 'handyman' },
  { productResourceName: 'Toys & Games', productResourceIconId: 'toys' },
  { productResourceName: 'Video Games', productResourceIconId: 'sports_esports' }
]

const JOB_EMPLOYMENT_POSITION_TYPE_LIST = [
  { jobEmploymentPositionName: 'Accounting / Finance', jobEmploymentPositionIconId: 'account_balance' },
  { jobEmploymentPositionName: 'Administrative', jobEmploymentPositionIconId: 'supervisor_account' },
  { jobEmploymentPositionName: 'Arts / Entertainment / Publishing', jobEmploymentPositionIconId: 'palette' },
  { jobEmploymentPositionName: 'Banking / Loans', jobEmploymentPositionIconId: 'account_balance' },
  { jobEmploymentPositionName: 'Computer / Internet', jobEmploymentPositionIconId: 'computer' },
  { jobEmploymentPositionName: 'Construction / Facilities', jobEmploymentPositionIconId: 'engineering' },
  { jobEmploymentPositionName: 'Customer Service', jobEmploymentPositionIconId: 'support_agent' },
  { jobEmploymentPositionName: 'Education / Training', jobEmploymentPositionIconId: 'school' },
  { jobEmploymentPositionName: 'Engineering / Architecture', jobEmploymentPositionIconId: 'engineering' },
  { jobEmploymentPositionName: 'Government / Military', jobEmploymentPositionIconId: 'gavel' },
  { jobEmploymentPositionName: 'Healthcare', jobEmploymentPositionIconId: 'local_hospital' },
  { jobEmploymentPositionName: 'Hospitality / Travel', jobEmploymentPositionIconId: 'map' },
  { jobEmploymentPositionName: 'Human Resources', jobEmploymentPositionIconId: 'support_agent' },
  { jobEmploymentPositionName: 'Insurance', jobEmploymentPositionIconId: 'admin_panel_settings' },
  { jobEmploymentPositionName: 'Law Enforcement / Security', jobEmploymentPositionIconId: 'local_police' },
  { jobEmploymentPositionName: 'Legal', jobEmploymentPositionIconId: 'policy' },
  { jobEmploymentPositionName: 'Manufacturing / Mechanical', jobEmploymentPositionIconId: 'construction' },
  { jobEmploymentPositionName: 'Marketing / Advertising / P.R.', jobEmploymentPositionIconId: 'storefront' },
  { jobEmploymentPositionName: 'Non-Profit / Volunteering', jobEmploymentPositionIconId: 'volunteer_activism' },
  { jobEmploymentPositionName: 'Pharmaceutical / Bio-Tech', jobEmploymentPositionIconId: 'local_pharmacy' },
  { jobEmploymentPositionName: 'Real Estate', jobEmploymentPositionIconId: 'home' },
  { jobEmploymentPositionName: 'Restaurant / Food Service', jobEmploymentPositionIconId: 'restaurant' },
  { jobEmploymentPositionName: 'Retail', jobEmploymentPositionIconId: 'storefront' },
  { jobEmploymentPositionName: 'Sales', jobEmploymentPositionIconId: 'point_of_sale' },
  { jobEmploymentPositionName: 'Telecommunications', jobEmploymentPositionIconId: 'local_phone' },
  { jobEmploymentPositionName: 'Transportation / Logistics', jobEmploymentPositionIconId: 'local_shipping' },
  { jobEmploymentPositionName: 'Upper Management / Consulting', jobEmploymentPositionIconId: 'leaderboard' }
]

const CURRENCY_EXCHANGE_CURRENCY_TYPE_LIST = [
  { currencyExchangeCurrencyName: 'Digital Currency', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Fiat Currency', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Ecoin369 (369)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Bitcoin (BTC)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Ethereum (ETH)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Cardano (ADA)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Binance Coin (BNB)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Tether (USDT)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'XRP (XRP)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Solana (SOL)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Polkadot (DOT)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Dogecoin (DOGE)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Netherlands Antillean Guilder (ANG)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Aruban Florin (AWG)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Australian Dollar (AUD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Barbadian Dollar (BBD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Bulgarian Lev (BGN)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Canadian dollar (CAD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Swiss Franc (CHF)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Danish Krone (DKK)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Eritrean Nakfa (ERN)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Euro (EUR)', currencyExchangeCurrencyIconId: 'euro_symbol' },
  { currencyExchangeCurrencyName: 'British Pound (GBP)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Hong Kong dollar (HKD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Manx Pound (IMP)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Jordanian Dinar (JOD)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Japanese Yen (JPY)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'North Korean Won (KPW)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Lebanese Pound (LBP)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Namibian Dollar (NAD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Nepalese Rupee (NPR)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'New Zealand Dollar (NZD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Saudi Riyal (SAR)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Tuvaluan Dollar (TVD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Ugandan Shilling (UGX)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'United States Dollar (USD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'Central African CFA Franc (XAF)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'Eastern Caribbean Dollar (XCD)', currencyExchangeCurrencyIconId: 'attach_money' },
  { currencyExchangeCurrencyName: 'West African CFA FRANC (XOF)', currencyExchangeCurrencyIconId: 'toll' },
  { currencyExchangeCurrencyName: 'CFP Franc (XPF)', currencyExchangeCurrencyIconId: 'toll' }
]


const DIGITAL_CURRENCY_ALTERNATIVE_FEATURE_LIST = [
  { digitalCurrencyAlternativeFeatureName: 'Complete Transactions', digitalCurrencyAlternativeFeatureIconId: 'swap_horizontal_circle' },
  { digitalCurrencyAlternativeFeatureName: 'Universal Basic Income', digitalCurrencyAlternativeFeatureIconId: 'restore' },
  { digitalCurrencyAlternativeFeatureName: 'New Features', digitalCurrencyAlternativeFeatureIconId: 'offline_bolt' }
]

export {
  TRANSACTION_TYPE_LIST,
  TRANSACTION_PROMOTION_TYPE_LIST,
  PRODUCT_RESOURCE_TYPE_LIST,
  JOB_EMPLOYMENT_POSITION_TYPE_LIST,
  CURRENCY_EXCHANGE_CURRENCY_TYPE_LIST,
  DIGITAL_CURRENCY_ALTERNATIVE_FEATURE_LIST
}

