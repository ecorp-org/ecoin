


declare type IsVisibleVariableTree = {
  [contentId: string]: boolean
}

function toggleShowContentFunction (isVisibleVariableTree: IsVisibleVariableTree, contentNameList: string[], contentName: string, isVisible?: boolean, isIgnoringHistoryTracking?: boolean, exitContentDialog?: any) {
  for (let contentNameTreeId in isVisibleVariableTree) {
    if (!isVisibleVariableTree.hasOwnProperty(contentNameTreeId)) { continue }
    isVisibleVariableTree[contentNameTreeId] = false
  }

  isVisible = isVisible === undefined ? !isVisibleVariableTree[contentName] : isVisible

  isVisibleVariableTree[contentName] = isVisible

  let isNotVisibleContent = true
  for (let contentNameTreeId in isVisibleVariableTree) {
    if (!isVisibleVariableTree.hasOwnProperty(contentNameTreeId)) { continue }
    isNotVisibleContent = isNotVisibleContent && !isVisibleVariableTree[contentNameTreeId]
  }

  // Close the parent dialog if the content visibility is set to false
  if (exitContentDialog && isNotVisibleContent) {
    exitContentDialog()
  }

  if (isVisible && !isIgnoringHistoryTracking) {
    contentNameList.push(contentName)
    // this.latestContentNameSetToIsVisible = contentName
  }
}


const algorithm = {
  function: toggleShowContentFunction
}

export {
  algorithm
}

