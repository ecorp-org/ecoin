
import numeral from 'numeral'

import { numberOfDecimalPlacesAllowed } from '../../../../../../../../../service-for-environment-variables/environment-variables-type-1-general-variables/general-variables-type-1-project-variables/project-variables-4-digital-currency'

const listOfZerosAsString = [...new Array(numberOfDecimalPlacesAllowed)].map(() => '0').reduce((a: string, b: string) => a + b)

function getDigitalCurrencyAmountWithCommaSeparatedValue (numberValue: number): string {
  return (`${numeral(numberValue).format(`0,0.[${listOfZerosAsString}]`)}`)
}

export {
  getDigitalCurrencyAmountWithCommaSeparatedValue
}

