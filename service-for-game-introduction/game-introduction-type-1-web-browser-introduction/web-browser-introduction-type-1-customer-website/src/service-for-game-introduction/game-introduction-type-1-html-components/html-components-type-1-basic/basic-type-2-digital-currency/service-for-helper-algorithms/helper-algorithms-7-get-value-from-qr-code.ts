

import qrCodeReaderModule from 'jsqr'



async function getValueFromQrCode (videoWidth: number, videoHeight: number, resultPlaybackFunction: (videoResult: any, qrCodeTemporaryResult: any) => void, videoStopConditionFunction: () => boolean): Promise<string[]> {

  let qrCodeResultList: any[] = []

  let videoResult = document.createElement('video')
  videoResult.width = videoWidth
  videoResult.height = videoHeight

  let htmlCanvasElement = document.createElement('canvas')
  htmlCanvasElement.width = videoWidth
  htmlCanvasElement.height = videoHeight

  const defaultQRCodeBorderHighlightColor = '#FF3B58'
  const defaultVideoFrameRequestMillisecondDelay = 0

  const videoStreamResult = await navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } })

  videoResult.srcObject = videoStreamResult
  videoResult.setAttribute('playsinline', 'true') // required to tell iOS safari we don't want fullscreen
  videoResult.play().catch((errorMessage) => {
    throw errorMessage
  })

  let isQRCodeVideoSearchStopped = false

  const intervalId = setInterval(function () {
    const qrCodeResult = getVideoQRCodeInformationAndUpdateVideoWithRectangleBorder(htmlCanvasElement, videoResult, defaultQRCodeBorderHighlightColor)

    // Add the qr code result to the list
    if (qrCodeResult) {
      qrCodeResultList.push(qrCodeResult)
    }

    // Respond with a video playback result and temporary qr code
    if (resultPlaybackFunction) {
      resultPlaybackFunction(htmlCanvasElement, qrCodeResult)
    }

    // Stop the video
    const isVideoStopConditionTrue = videoStopConditionFunction()

    if (isVideoStopConditionTrue) {
      clearInterval(intervalId)
      videoResult.pause()
      videoResult.removeAttribute('src') // empty source
      videoResult.load()

      videoStreamResult.getTracks()[0].stop()

      isQRCodeVideoSearchStopped = true
    }
  }, defaultVideoFrameRequestMillisecondDelay)

  // Wait until the video has been stopped by the user
  await startApplyingWaitWithAStopCondition(() => isQRCodeVideoSearchStopped)

  return qrCodeResultList
}

function getVideoQRCodeInformationAndUpdateVideoWithRectangleBorder (htmlCanvasElement: HTMLCanvasElement, htmlVideoElement: any, borderColor: string) {
  // htmlCanvasElement.height = htmlVideoElement.videoHeight
  // htmlCanvasElement.width = htmlVideoElement.videoWidth

  let htmlCanvasElementContext = htmlCanvasElement.getContext('2d')!

  htmlCanvasElementContext.drawImage(htmlVideoElement, 0, 0, htmlCanvasElement.width, htmlCanvasElement.height)
  let imageData = htmlCanvasElementContext.getImageData(0, 0, htmlCanvasElement.width, htmlCanvasElement.height)

  let qrCodeResult = qrCodeReaderModule(imageData.data, imageData.width, imageData.height, {
    inversionAttempts: 'dontInvert'
  })

  if (qrCodeResult) {
    const borderLineList = [{
      startPoint: qrCodeResult.location.topLeftCorner,
      endPoint: qrCodeResult.location.topRightCorner
    }, {
      startPoint: qrCodeResult.location.topRightCorner,
      endPoint: qrCodeResult.location.bottomRightCorner
    }, {
      startPoint: qrCodeResult.location.bottomRightCorner,
      endPoint: qrCodeResult.location.bottomLeftCorner
    }, {
      startPoint: qrCodeResult.location.bottomLeftCorner,
      endPoint: qrCodeResult.location.topLeftCorner
    }]

    updateQRCodeCameraFrameWithRectangleBorder(htmlCanvasElement, borderColor, borderLineList)
  }

  return qrCodeResult ? qrCodeResult.data : ''
}

function updateQRCodeCameraFrameWithRectangleBorder (htmlCanvas: HTMLCanvasElement, borderColor: string, borderLineList: Array<{ startPoint: any, endPoint: any }>) {
  for (let i = 0 ; i < borderLineList.length; i++) {
    const startPoint = borderLineList[i].startPoint
    const endPoint = borderLineList[i].endPoint

    let htmlCanvasElementContext = htmlCanvas.getContext('2d')!
    htmlCanvasElementContext.lineWidth = 4
    htmlCanvasElementContext.strokeStyle = borderColor

    htmlCanvasElementContext.beginPath()
    htmlCanvasElementContext.moveTo(startPoint.x, startPoint.y)
    htmlCanvasElementContext.lineTo(endPoint.x, endPoint.y)
    htmlCanvasElementContext.closePath()
    htmlCanvasElementContext.stroke()
  }
}

//  Miscellaneous Functions

async function startApplyingWaitWithAStopCondition (conditionToStopWaitingFunction: () => boolean, millisecondDelayToCheckForNextStopCondition?: number) {
  let waitingTimerId: any = null
  return new Promise((resolve, reject) => {
    waitingTimerId = setInterval(function () {
      const isStopConditionSatisfied = conditionToStopWaitingFunction()

      if (!isStopConditionSatisfied) {
        return
      }

      clearInterval(waitingTimerId)

      resolve(isStopConditionSatisfied)
    }, millisecondDelayToCheckForNextStopCondition || 100)
  })
}



export {
  getValueFromQrCode
}

