

/* tslint:disable */

// import { THREE } from '../../../../../provider/service/three/constants/three'

// im from '../../../../../provider/service/three/data-structures/stemkoski.github.io/Three'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/Detector'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/Stats'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/OrbitControls'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/THREEx.KeyboardState'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/THREEx.FullScreen'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/THREEx.WindowResize'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/ParticleEngine'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/ParticleEngineExamples'
// import '../../../../../provider/service/three/data-structures/stemkoski.github.io/DAT.GUI'

// console.log(something)


// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/Three.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/Detector.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/Stats.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/OrbitControls.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/THREEx.KeyboardState.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/THREEx.FullScreen.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/THREEx.WindowResize.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/ParticleEngine.js'
// import './service-for-create-digital-currency-fountain-particle-effect/stemkoski.github.io/Three.js/js/DAT.GUI.min.js'



declare const THREE: any
declare const THREEx: any
declare const Detector: any
// declare const Stats: any
declare const ParticleEngine: any
// declare const Examples: any
// declare const dat: any
declare const Type: any
declare const Tween: any
// declare const OrbitControls: any

const self = this as any || window

// const imageUrl = 'http://localhost:8082/precompiled/consumer/web-browser/data-structures/project-service-website/public/img/icons/android-chrome-512x512.png'
// const imageUrl = 'https://live.staticflickr.com/1/26/50149748_431471a532_z.jpg'

// let image = document.createElement('img')
// const texture = new THREE.Texture(image)

// image.onload = function () { texture.needsUpdate = true }
// image.crossOrigin = ''
// image.src = 'http://localhost:8082/precompiled/consumer/web-browser/data-structures/project-service-website/public/img/icons/android-chrome-512x512.png'



// const texture = new THREE.Texture({
//   image : image,
//   wrapS : THREE.RepeatWrapping,
//   wrapT : THREE.RepeatWrapping,
//   repeat : [1,1]
// })


function loadImage (imageSrcUrl: string) {
  let canvas = document.createElement('canvas')
  canvas.style.position = 'absolute'
  canvas.style.top = '0'
  canvas.style.left = '0'

  let texture = new THREE.Texture(canvas)

  let img = new Image()
  img.crossOrigin = ''
  img.onload = function () {
    canvas.width = img.width
    canvas.height = img.height

    let context = canvas.getContext('2d')!
    context.drawImage(img, 0, 0)

    texture.needsUpdate = true
  }
  img.src = imageSrcUrl
  return texture
}

const Examples: any = {
  fountain: {
    positionStyle: Type.CUBE,
    positionBase: new THREE.Vector3(0, 10, 0),
    positionSpread   : new THREE.Vector3(10, 0, 10),

    velocityStyle    : Type.CUBE,
    velocityBase     : new THREE.Vector3(0, 160, 0),
    velocitySpread   : new THREE.Vector3(100, 20, 100),

    accelerationBase : new THREE.Vector3(0, -100, 0),

    particleTexture : undefined, // variableTree.texture, // THREE.ImageUtils.loadTexture( textureUrl ),

    angleBase               : 0,
    angleSpread             : 180,
    angleVelocityBase       : 0,
    angleVelocitySpread     : 36,

    // sizeBase    : 30.0,
    // sizeSpread  : 20.0,

    colorBase   : new THREE.Vector3(1, 1, 1), // H,S,L
    // colorSpread : new THREE.Vector3(0.00, 0.0, 0.2),

    sizeTween    : new Tween([10, 2], [50, 2]), // default value: [0, 1], [1, 20]
    opacityTween : new Tween([2, 3], [1, 0]),
    // colorTween   : new Tween([0.5, 2], [ new THREE.Vector3(0,1,0.5), new THREE.Vector3(0.8, 1, 0.5) ]),

    particlesPerSecond : 50,
    particleDeathAge   : 3.0,
    emitterDeathAge    : Infinity
  },
  firework : {
    positionStyle  : Type.SPHERE,
    positionBase   : new THREE.Vector3(0, 100, 0),
    positionRadius : 10,

    velocityStyle  : Type.SPHERE,
    speedBase      : 90,
    speedSpread    : 10,

    accelerationBase : new THREE.Vector3(0, -80, 0),

    particleTexture : undefined, // THREE.ImageUtils.loadTexture(textureUrl ),

    colorBase   : new THREE.Vector3(1, 1, 1), // H,S,L

    sizeTween    : new Tween([0.5, 0.7, 1.3], [5, 40, 1]),
    opacityTween : new Tween([0.2, 0.7, 2.5], [0.75, 1, 0]),
    // colorTween   : new Tween([0.4, 0.8, 1.0], [ new THREE.Vector3(0,1,1), new THREE.Vector3(0,1,0.6), new THREE.Vector3(0.8, 1, 0.6) ] ),
    blendStyle   : THREE.AdditiveBlending,

    particlesPerSecond : 300,
    particleDeathAge   : 2.5,
    emitterDeathAge    : Infinity
  },
  rain : {
    positionStyle    : Type.CUBE,
    positionBase     : new THREE.Vector3(0, 200, 0),
    positionSpread   : new THREE.Vector3(600, 0, 600),

    velocityStyle    : Type.CUBE,
    velocityBase     : new THREE.Vector3(0, -400, 0),
    velocitySpread   : new THREE.Vector3(10, 50, 10),
    accelerationBase : new THREE.Vector3(0, -10,0),

    particleTexture : undefined, // THREE.ImageUtils.loadTexture( textureUrl ),

    sizeBase    : 200.0,
    sizeSpread  : 4.0,
    colorBase   : new THREE.Vector3(1, 1, 1), // H,S,L
    // colorSpread : new THREE.Vector3(0.00, 0.0, 0.2),
    opacityBase : 0.6,

    particlesPerSecond : 100,
    particleDeathAge   : 1.0,
    emitterDeathAge    : 60
  }
}


// const loader = new THREE.TextureLoader()
// loader.load('http://localhost:8082/precompiled/consumer/web-browser/data-structures/project-service-website/public/img/icons/android-chrome-512x512.png', function (loadedTexture) {
//   console.log('loaded texture', loadedTexture)

//   Examples.fountain.particleTexture = loadImage(imageUrl)

//   self.engine.setValues(Examples.fountain)
//   self.engine.initialize()

//   isEngineInitialized = true
// })

// console.log('texture: ', texture)
// texture.wrapS = THREE.MirroredRepeatWrapping
// texture.wrapT = THREE.MirroredRepeatWrapping



// var path = "http://maps.googleapis.com/maps/api/staticmap?center=39.06029101581083,-94.59737342812502&zoom=15&size=512x512&sensor=false&key=AIzaSyDk3wbT2nVVWhpmPjOb_3DbtrIQBXcsxtk&format=png";

// let geometry = new THREE.PlaneGeometry(200, 200)
// let material = new THREE.MeshLambertMaterial({ map: loadImage(imageSrcUrl) })



function createDigitalCurrencyFountainParticleEffectFunction (container: HTMLElement, imageUrl: string, stopRenderLoopFunction: any) {

  /*
    Three.js "tutorials by example"
    Author: Lee Stemkoski
    Date: July 2013 (three.js v59dev)
  */

  // MAIN


  // standard global variables
  let scene: any // THREE.Scene
  let camera: any // THREE.Camera
  let renderer: any // THREE.Renderer
  let controls: any
  let stats: any
  let keyboard = new THREEx.KeyboardState()
  let clock = new THREE.Clock()
  // custom global variables

  init()
  animate()

  // FUNCTIONS
  function init () {
    // SCENE
    scene = new THREE.Scene()
    // CAMERA
    let SCREEN_WIDTH = container.clientWidth
    let SCREEN_HEIGHT = container.clientHeight
    let VIEW_ANGLE = 45
    let ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT
    let NEAR = 2
    let FAR = 5000
    camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR)
    scene.add(camera)
    camera.position.set(0,200,400)
    camera.lookAt(scene.position)
    // RENDERER
    if (Detector.webgl) {
      renderer = new THREE.WebGLRenderer({ antialias: true })
    } else {
      renderer = new THREE.CanvasRenderer()
    }
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT)
    container.appendChild(renderer.domElement)
    // EVENTS
    THREEx.WindowResize(renderer, camera)
    THREEx.FullScreen.bindKey({ charCode : 'm'.charCodeAt(0) })
    // CONTROLS
    controls = new THREE.OrbitControls(camera, renderer.domElement)
    // STATS
    // stats = new Stats()
    // stats.domElement.style.position = 'absolute'
    // stats.domElement.style.bottom = '0px'
    // stats.domElement.style.zIndex = 100
    // container.appendChild(stats.domElement)
    // LIGHT
    let light = new THREE.PointLight(0xffffff)
    light.position.set(0,250,0)
    scene.add(light)
    // FLOOR
    // let floorTexture = new THREE.ImageUtils.loadTexture(imageUrl)
    // floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping
    // floorTexture.repeat.set(10, 10)
    // let floorMaterial = new THREE.MeshBasicMaterial({ color: 0x444444, map: floorTexture, side: THREE.DoubleSide })
    // let floorGeometry = new THREE.PlaneGeometry(1000, 1000, 10, 10)
    // let floor = new THREE.Mesh(floorGeometry, floorMaterial)
    // floor.position.y = -10.5
    // floor.rotation.x = Math.PI / 2
    // scene.add(floor)
    // SKYBOX/FOG
    // let skyBoxGeometry = new THREE.CubeGeometry(4000, 4000, 4000)
    // let skyBoxMaterial = new THREE.MeshBasicMaterial({ color: 0x000000, side: THREE.BackSide })
    // let skyBox = new THREE.Mesh(skyBoxGeometry, skyBoxMaterial)
    // scene.add(skyBox)

    self.scene = scene
    self.camera = camera
    self.renderer = renderer
    self.controls = controls
    self.stats = stats
    self.keyboard = keyboard
    self.clock = clock

    ////////////
    // CUSTOM //
    ////////////

    const imageTexture = loadImage(imageUrl)

    Examples.fountain.particleTexture = imageTexture
    Examples.firework.particleTexture = imageTexture
    Examples.rain.particleTexture = imageTexture

    self.engine = new ParticleEngine()
    self.engine.setValues(Examples.fountain)
    self.engine.initialize()

    // GUI for experimenting with parameters

    // let gui = new dat.GUI()
    // let parameters = {
      // fountain:   function () { restartEngine(Examples.fountain) } // ,
      // startunnel: function () { restartEngine(Examples.startunnel) },
      // starfield:  function () { restartEngine(Examples.starfield) },
      // fireflies:  function () { restartEngine(Examples.fireflies) },
      // clouds:     function () { restartEngine(Examples.clouds) },
      // smoke:      function () { restartEngine(Examples.smoke) },
      // fireball:   function () { restartEngine(Examples.fireball) },
      // candle:     function () { restartEngine(Examples.candle) },
      // rain:       function () { restartEngine(Examples.rain) },
      // snow:       function () { restartEngine(Examples.snow) },
      // firework:   function () { restartEngine(Examples.firework) }
    // }

    // gui.add(parameters, 'fountain').name('Star Fountain')
    // gui.add(parameters, 'startunnel').name('Star Tunnel')
    // gui.add(parameters, 'starfield').name('Star Field')
    // gui.add(parameters, 'fireflies').name('Fireflies')
    // gui.add(parameters, 'clouds').name('Clouds')
    // gui.add(parameters, 'smoke').name('Smoke')
    // gui.add(parameters, 'fireball').name('Fireball')
    // gui.add(parameters, 'candle').name('Candle')
    // gui.add(parameters, 'rain').name('Rain')
    // gui.add(parameters, 'snow').name('Snow')
    // gui.add(parameters, 'firework').name('Firework')

    // gui.open()
  }

  function animate () {
    if (stopRenderLoopFunction()) {
      return
    }
    requestAnimationFrame(animate)
    render()
    update()
  }

  function restartEngine () {
    resetCamera()
    self.engine.destroy()
    self.engine = new ParticleEngine()
    self.engine.setValues(Examples.fountain)
    self.engine.initialize()
  }

  function resetCamera () {
    // CAMERA
    let SCREEN_WIDTH = container.clientWidth
    let SCREEN_HEIGHT = container.clientHeight
    let VIEW_ANGLE = 45
    let ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT
    let NEAR = 0.1
    let FAR = 20000
    camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR)
    // camera.up = new THREE.Vector3( 0, 0, 1 )
    camera.position.set(0,200,400)
    camera.lookAt(scene.position)
    scene.add(camera)

    controls = new THREE.OrbitControls(camera, renderer.domElement)
    THREEx.WindowResize(renderer, camera)
  }


  function update () {
  /*
    if ( keyboard.pressed("1") )
      restartEngine( fountain );
    if ( keyboard.pressed("2") )
      restartEngine( startunnel );
    if ( keyboard.pressed("3") )
      restartEngine( starfield );
    if ( keyboard.pressed("4") )
      restartEngine( fireflies );

    if ( keyboard.pressed("5") )
      restartEngine( clouds );
    if ( keyboard.pressed("6") )
      restartEngine( smoke );

    if ( keyboard.pressed("7") )
      restartEngine( fireball );
    if ( keyboard.pressed("8") )
      restartEngine( Examples.candle );

    if ( keyboard.pressed("9") )
      restartEngine( rain );
    if ( keyboard.pressed("0") )
      restartEngine( snow );

    if ( keyboard.pressed("q") )
      restartEngine( firework );
    // also: reset camera angle
  */

    // controls.update()
    // stats.update()

    // let dt = clock.getDelta()
    self.engine.update(0.025)
  }

  function render () {
    renderer.render(scene, camera)
  }

  return {
    startRenderLoop: () => {
      // const exampleIdList = Object.keys(Examples)
      // const randomExampleId = exampleIdList[Math.floor(Math.random() * exampleIdList.length)]
      restartEngine()
      animate()
    },
    restartEngine
  }
}

const createDigitalCurrencyFountainParticleEffectFunctionAlgorithm = {
  function: createDigitalCurrencyFountainParticleEffectFunction
}

export {
  createDigitalCurrencyFountainParticleEffectFunctionAlgorithm
}

