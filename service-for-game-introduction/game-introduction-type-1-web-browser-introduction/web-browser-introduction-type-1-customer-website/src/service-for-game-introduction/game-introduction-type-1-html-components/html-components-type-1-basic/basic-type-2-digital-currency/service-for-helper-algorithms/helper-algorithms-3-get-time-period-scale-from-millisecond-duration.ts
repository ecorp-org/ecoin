

import moment from 'moment'

function getTimePeriodScaleFromMillisecondDuration (millisecondDuration: number): string {
  if (millisecondDuration < 1000) {
    return '0 seconds'
  }

  const yearsRemaining = moment.duration(millisecondDuration).years()
  const monthsRemaining = moment.duration(millisecondDuration).months()
  const weeksRemaining = moment.duration(millisecondDuration).weeks()
  const daysRemaining = moment.duration(millisecondDuration).days()
  const hoursRemaining = moment.duration(millisecondDuration).hours()
  const minutesReamining = moment.duration(millisecondDuration).minutes()
  const secondsRamining = moment.duration(millisecondDuration).seconds()
  // const millisecondsRemaining = moment.duration(millisecondDuration).milliseconds()

  let timeRemainingList = [
    { timeRemaining: yearsRemaining, labelSingular: 'year' },
    { timeRemaining: monthsRemaining, labelSingular: 'month' },
    { timeRemaining: weeksRemaining, labelSingular: 'week' },
    { timeRemaining: daysRemaining, labelSingular: 'day' },
    { timeRemaining: hoursRemaining, labelSingular: 'hour' },
    { timeRemaining: minutesReamining, labelSingular: 'minute' },
    { timeRemaining: secondsRamining, labelSingular: 'second' }
  ]

  let timeScale = ''

  for (let i = 0; i < timeRemainingList.length; i++) {
    const timeRemaining = timeRemainingList[i].timeRemaining
    const timeRemainingLabelSingular = timeRemainingList[i].labelSingular

    // Set the first non-zero number as the scale of this millisecond duration
    if (timeRemaining > 0) {
      timeScale = timeRemainingLabelSingular
      break
    }
  }

  return timeScale
}


export {
  getTimePeriodScaleFromMillisecondDuration
}





