

import moment from 'moment'

function getTimeUntilMillisecondDurationReached (millisecondDuration: number): string {

  if (millisecondDuration < 1000) {
    return '0 seconds'
  }

  const yearsRemaining = moment.duration(millisecondDuration).years()
  const monthsRemaining = moment.duration(millisecondDuration).months()
  const weeksRemaining = moment.duration(millisecondDuration).weeks()
  const daysRemaining = moment.duration(millisecondDuration).days()
  const hoursRemaining = moment.duration(millisecondDuration).hours()
  const minutesReamining = moment.duration(millisecondDuration).minutes()
  const secondsRamining = moment.duration(millisecondDuration).seconds()
  // const millisecondsRemaining = moment.duration(millisecondDuration).milliseconds()

  let timeRemainingList = [
    { timeRemaining: yearsRemaining, labelPlural: 'years', labelSingular: 'year' },
    { timeRemaining: monthsRemaining, labelPlural: 'months', labelSingular: 'month' },
    { timeRemaining: weeksRemaining, labelPlural: 'weeks', labelSingular: 'week' },
    { timeRemaining: daysRemaining, labelPlural: 'days', labelSingular: 'day' },
    { timeRemaining: hoursRemaining, labelPlural: 'hours', labelSingular: 'hour' },
    { timeRemaining: minutesReamining, labelPlural: 'minutes', labelSingular: 'minute' },
    { timeRemaining: secondsRamining, labelPlural: 'seconds', labelSingular: 'second' }
  ]

  let timeRemainingResultList = []

  for (let i = 0; i < timeRemainingList.length; i++) {
    const timeRemaining = timeRemainingList[i].timeRemaining
    const timeRemainingLabelPlural = timeRemainingList[i].labelPlural
    const timeRemainingLabelSingular = timeRemainingList[i].labelSingular

    if (timeRemaining !== 0) {
      timeRemainingResultList.push(`${timeRemaining} ${timeRemaining > 1 ? timeRemainingLabelPlural : timeRemainingLabelSingular}`)
    }
  }

  return timeRemainingResultList.join(' ')
}

export {
  getTimeUntilMillisecondDurationReached
}

