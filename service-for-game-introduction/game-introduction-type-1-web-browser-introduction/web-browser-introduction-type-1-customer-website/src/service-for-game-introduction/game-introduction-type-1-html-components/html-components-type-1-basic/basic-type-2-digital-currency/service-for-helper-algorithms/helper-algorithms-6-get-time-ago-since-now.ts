

import moment from 'moment'

function getTimeAgoSinceNow (date: Date): string {
  return moment(date).fromNow()
}

export {
  getTimeAgoSinceNow
}
