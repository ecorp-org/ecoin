


function addDatedElementToList (dateListA: any[], dataListA: any[], dateListB: any[], dataListB: any[]) {
  for (let i = 0; i < dateListB.length; i++) {
    const currentDate = dateListB[i]
    const currentData = dataListB[i]
    const indexOfElementInDateListA = dateListA.indexOf(currentDate)

    // Update with the new data from data list B
    if (indexOfElementInDateListA >= 0) {
      dataListA[indexOfElementInDateListA] = currentData
      continue
    }

    // Push the date to list A if the list A doesn't have that
    // date
    if (currentDate >= dateListA[dateListA.length - 1]) {
      dateListA.push(currentDate)
      dataListA.push(currentData)
      continue
    }

    // Go backward into the list until currentDate is greater
    // than a lower indexed element, then insert into the array
    if (currentDate < dateListA[dateListA.length - 1]) {
      for (let j = dateListA.length - 1; j >= 0; j--) {
        const previousDateInListA = dateListA[j]

        // Insert the current date into array and break
        // out of the loop
        if (previousDateInListA < currentDate) {
          dateListA = [...dateListA.slice(0, j + 1), currentDate, ...dateListA.slice(j + 1, dateListA.length)]
          dataListA = [...dataListA.slice(0, j + 1), currentData, ...dataListA.slice(j + 1, dataListA.length)]
          break
        }
      }
    }
  }

  return {
    dateList: [...dateListA],
    dataList: [...dataListA]
  }
}

// mergeList (listA: any[], listB: any[], startIndex: number) {
//   for (let i = startIndex; i < startIndex + listB.length; i++) {
//     if (listA.length >= i) {
//       listA[i] = listB[i - startIndex]
//     } else {
//       // Add to the list until it's at least as long as
//       // startIndex
//       let listALength = listA.length
//       while (listALength <= startIndex) {
//         listALength = listA.push(null)
//       }
//     }
//   }
//   return listA
// }


export {
  addDatedElementToList
}


