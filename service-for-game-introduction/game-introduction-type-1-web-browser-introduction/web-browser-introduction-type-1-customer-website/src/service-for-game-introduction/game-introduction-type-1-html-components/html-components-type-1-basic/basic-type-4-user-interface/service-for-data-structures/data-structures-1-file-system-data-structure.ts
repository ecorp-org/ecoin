


const itemTypeFolder = 'folder'
const itemTypeAccount = 'account'
const itemTypeTransaction = 'transaction'
const itemTypeDescriptionList = 'description-list'
const itemTypeGraph = 'graph'
const itemTypeProductResource = 'product-resource'
const itemTypeSettings = 'settings'

declare type ItemType = 'folder' | 'account' | 'transaction' | 'description-list' | 'graph' | 'product-resource' | 'settings' | undefined


class DataTableFile {
  itemType?: ItemType = itemTypeFolder
  actionButtonList?: ActionButton[] = []

  itemId: string = ''
  itemName: string = ''
  itemIconImageUrl: string = ''
  dateCreated?: any

  fileVariableTable?: {
    [fileItemId: string]: DataTableFile
  } = {}

  itemStatisticsList?: string[] = []

  itemData?: any
}

class ActionButton {
  iconImageId?: string = ''
  text?: string = ''
  cssClass?: any
  evaluateFunction?: any = () => false
}

class GraphViewDataTableItem extends DataTableFile {
  closingActionButtonIconId?: string
  closingActionButtonText?: string
  openFolderOrFileItem?: boolean
  closingActionButtonFunction?: Function
}

export {
  DataTableFile,
  ActionButton,
  GraphViewDataTableItem,
  ItemType,
  itemTypeFolder,
  itemTypeAccount,
  itemTypeTransaction,
  itemTypeDescriptionList,
  itemTypeGraph,
  itemTypeProductResource,
  itemTypeSettings
}

