
// Example
// Inspired by

// https://stackoverflow.com/questions/65720284/echarts-group-axis-labels-with-the-same-value


// import * as echarts from 'echarts'
import VChart from 'vue-echarts' // refers to components/ECharts.vue in webpack
// import 'echarts/lib/chart/line'

import moment from 'moment'

import { use } from 'echarts/core'

// import ECharts modules manually to reduce bundle size
import {
  CanvasRenderer
} from 'echarts/renderers'
import { BarChart } from 'echarts/charts'
import {
  TitleComponent,
  GridComponent,
  TooltipComponent,
  LegendComponent,
  DataZoomComponent
} from 'echarts/components'

use([
  CanvasRenderer,
  BarChart,
  GridComponent,
  TooltipComponent,
  TitleComponent,
  LegendComponent,
  DataZoomComponent
])


function generateData (start: any, end: any) {
  let dates = []
  let values = []

  let startDate = moment(start)
  let endDate = moment(end)
  let hourDiff = endDate.diff(startDate, 'hours')

  for (let i = 0; i < hourDiff; i++) {
    dates.push(startDate.add(1, 'hours').format('YYYY-MM-DD HH:mm'))
    values.push(parseFloat(Math.random().toFixed(2)) / 2)
  }
  return [dates, values]
}

let [dates, values] = generateData('2020-10-25 00:00', '2020-10-30 00:00')

let option = {
  xAxis: [{
    id: 'date',
    type: 'category',
    data: dates,
    position: 'bottom',
    axisLabel: {
      formatter: (d: any) => moment(d).format('DD-MMM-YYYY HH'),
      interval: 24
    }
  }, {
    name: 'Date',
    nameLocation: 'middle'
  }],
  yAxis: [{
    type: 'value'
  }, {
    name: 'Number of Accounts',
    nameLocation: 'middle'
  }],
  series: [{
    name: 'value',
    type: 'bar',
    data: values,
    color: 'steelBlue'
  }],
  dataZoom: [{
    type: 'inside'
  }, {
    type: 'slider',
    show: true,
    xAxisIndex: [0, 1],
    labelFormatter: (_v: any, vStr: any) => moment(vStr).format('HH') + 'h / ' + moment(vStr).format('DD.MM')
  }],
  title: {
    text: 'Test Chart',
    left: 'center'
  },
  tooltip: {
    trigger: 'axis',
    formatter: function (param: any) {
      return 'Rejected Vials:<br />' + param[0].axisValue.substring(0, param[0].axisValue.length - 1) + '-' +
          (+param[0].axisValue.substring(0, param[0].axisValue.length - 1) + 1) + 'h<br />' + param[0].data + ' %'
    },
    axisPointer: {
      type: 'shadow',
      label: {
        show: false
      }
    }
  }
}




