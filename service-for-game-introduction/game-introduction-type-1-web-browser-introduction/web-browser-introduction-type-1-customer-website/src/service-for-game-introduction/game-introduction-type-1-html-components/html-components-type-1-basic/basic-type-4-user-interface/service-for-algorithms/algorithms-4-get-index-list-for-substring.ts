

function getIndexListForSubstring (stringValue: string, substringValue: string) {

  stringValue = stringValue || ''
  const regex = new RegExp(substringValue,'gi')
  let result = regex.exec(stringValue)
  let indexList = []

  while (result) {
    indexList.push(result.index)
    result = regex.exec(stringValue)
  }

  return indexList
}

export {
  getIndexListForSubstring
}

