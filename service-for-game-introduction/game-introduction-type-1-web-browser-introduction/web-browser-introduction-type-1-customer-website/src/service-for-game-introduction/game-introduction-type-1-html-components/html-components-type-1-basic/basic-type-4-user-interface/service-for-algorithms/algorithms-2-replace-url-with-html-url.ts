

function replaceUrlWithHtmlUrl (stringValue: string) {
  if (typeof (stringValue as any) !== 'string') {
    return ''
  }
  let expression = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig
  return stringValue.replace(expression,`<a href='$1' target="_blank" class="router-link">$1</a>`)
}

export {
  replaceUrlWithHtmlUrl
}
