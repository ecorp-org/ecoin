
import moment from 'moment'

import { getDigitalCurrencyAmountWithCommaSeparatedValue } from '../../../basic-type-2-digital-currency/service-for-helper-algorithms/helper-algorithms-2-get-digital-currency-amount-with-comma-separated-value'

class BarChartOptions {
  xAxisLabel?: string = ''
  yAxisLabel?: string = ''
  dateDataList: any[] = []
  dataList: any[] = []
}


function getBarChartOptions (options: BarChartOptions) {
  const barChartOptions = {
    xAxis: [{
      id: 'date',
      type: 'category',
      data: options.dateDataList,
      position: 'bottom',
      axisLabel: {
        formatter: (d: any) => moment(d).format('DD-MMM-YYYY HH') // ,
        // interval: 24
      }
    }, {
      name: options.xAxisLabel,
      nameLocation: 'middle'
    }],
    yAxis: [{
      type: 'value'
    }, {
      name: options.yAxisLabel,
      nameLocation: 'middle'
    }],
    series: [{
      name: 'value',
      type: 'bar',
      data: options.dataList,
      color: 'steelBlue'
    }],
    dataZoom: [{
      type: 'inside'
    }, {
      type: 'slider',
      show: true,
      xAxisIndex: [0, 1],
      labelFormatter: (_v: any, vStr: any) => moment(vStr).format('HH') + 'h / ' + moment(vStr).format('DD.MM')
    }],
    // title: {
    //   text: 'Test Chart',
    //   left: 'center'
    // },
    tooltip: {
      trigger: 'axis',
      formatter: function (param: any) {
        return `${moment(param[0].axisValue).format('DD-MMM-YYYY')}: ${getDigitalCurrencyAmountWithCommaSeparatedValue(param[0].value)}`
        // return 'Rejected Vials:<br />' + param[0].axisValue.substring(0, param[0].axisValue.length - 1) + '-' +
            // (+param[0].axisValue.substring(0, param[0].axisValue.length - 1) + 1) + 'h<br />' + param[0].data + ' %'
      },
      axisPointer: {
        type: 'shadow',
        label: {
          show: false
        }
      }
    }
  }

  return barChartOptions
}


export {
  getBarChartOptions,
  BarChartOptions
}


