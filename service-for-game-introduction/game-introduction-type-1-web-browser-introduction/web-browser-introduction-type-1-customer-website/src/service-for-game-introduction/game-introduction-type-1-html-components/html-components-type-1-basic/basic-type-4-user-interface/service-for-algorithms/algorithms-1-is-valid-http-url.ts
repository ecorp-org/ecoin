

function isValidHttpUrl (stringValue: string) {
  let url

  try {
    url = new URL(stringValue)
  } catch (_) {
    return false
  }

  return url.protocol === 'http:' || url.protocol === 'https:'
}

export {
  isValidHttpUrl
}

