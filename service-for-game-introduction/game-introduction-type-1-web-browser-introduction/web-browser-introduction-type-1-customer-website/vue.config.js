

// Constants

const pwaSettings = {
  workboxOptions: {
      skipWaiting: true
  }
}

const transpileDependenciesSettings = [
  'vuetify'
]

const devServer = {
  disableHostCheck: true
}

// Algorithms

function chainWebpack (config) {
  config.module.rule('markdown').test(/\.md$/)
    .use('html-loader')
      .loader('html-loader').end()
    .use('markdown-loader')
      .loader('markdown-loader').end()
}

module.exports = {
  pwa: pwaSettings,
  transpileDependencies: transpileDependenciesSettings,
  chainWebpack,
  devServer
}