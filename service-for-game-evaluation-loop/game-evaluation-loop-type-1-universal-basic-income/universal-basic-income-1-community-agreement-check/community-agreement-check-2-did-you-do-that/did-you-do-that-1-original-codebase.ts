
/*

The idea relating
to this page is
to

(1) build an 
interesting
database

(2) build a 
crazy interesting
database

*/

/*

Create
Get
Update
Delete

*/
/*

Database
Event Listener
Permission Rule
Network Protocol
Scale Matrix Amana
Interactive Graph
Computer Program Manager

*/



// Web Server Imports
import express from 'express'
import http from 'http'

import * as WebSocket from 'websocket'
import SimplePeer from 'simple-peer'
import * as wrtc from 'wrtc'

// 3rd Party Service Imports

// Firebase
import firebase from 'firebase/app'
// import 'firebase/app'
import 'firebase/database'
// import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'
firebase.setLogLevel('silent')

import * as firebaseAdmin from 'firebase-admin'
import * as firebaseTesting from '@firebase/testing'


// LocalForage
import * as localForage from 'localforage'


// import {
//   getStringProtocolMessageEncoding,
//   PLACE_ID_TYPE_GENERAL_STRING_ADDRESS_PROTOCOL
// } from '../../universal-basic-income-1-community-agreement-check'

// Temporary Imports
const getStringProtocolMessageEncoding = (..._something: any): any => {}
const PLACE_ID_TYPE_GENERAL_STRING_ADDRESS_PROTOCOL = 'hello-world'


class DatabaseConstructorItem {
  activate = databaseConstructorItemActivateAlgorithm
}

class EventListenerConstructorItem {
  activate = databaseConstructorItemActivateAlgorithm
}

class PermissionRuleConstructorItem {
  activate = databaseConstructorItemActivateAlgorithm
}

class NetworkProtocolConstructorItem {
  activate = databaseConstructorItemActivateAlgorithm
}

class ScaleMatrixAmanaConstructorItem {
  activate = databaseConstructorItemActivateAlgorithm
}

class InteractiveGraphConstructorItem {
  activate = databaseConstructorItemActivateAlgorithm
}

class ComputerProgramManagerConstructorItem {
  activate = databaseConstructorItemActivateAlgorithm
}

// - - - 

class DatabaseConstructorItemActivateFunctionItemDataStructure<OriginalItemDataStructure> {
  originalItem: OriginalItemDataStructure
  copyableItem: OriginalItemDataStructure
  runableItem: Function
  secondRunableItem: Function
  emergencyCatchUpItem: Function
  carryForthTheOperationAndPerformThisListOfTasksAsWell: Function

  constructor (
    originalItem: OriginalItemDataStructure,
    copyableItem: OriginalItemDataStructure,
    runableItem: Function,
    secondRunableItem: Function,
    emergencyCatchUpItem: Function,
    carryForthTheOperationAndPerformThisListOfTasksAsWell: Function
  ) {
    this.originalItem = originalItem
    this.copyableItem = copyableItem
    this.runableItem = runableItem
    this.secondRunableItem = secondRunableItem
    this.emergencyCatchUpItem = emergencyCatchUpItem
    this.carryForthTheOperationAndPerformThisListOfTasksAsWell = carryForthTheOperationAndPerformThisListOfTasksAsWell
  }
}

const itemOfInterestList = [
  DatabaseConstructorItem,
  EventListenerConstructorItem,
  PermissionRuleConstructorItem,
  NetworkProtocolConstructorItem,
  ScaleMatrixAmanaConstructorItem,
  InteractiveGraphConstructorItem,
  ComputerProgramManagerConstructorItem
]


async function createItem (itemId: string, item: DatabaseConstructorItemActivateFunctionItemDataStructure<any>, itemOfInterestCount?: number): Promise<any> {

  const numberOfItemsOfInterestToIncludeInTheResult = itemOfInterestCount || 1
  const itemOfInterestResultList = []

  if (numberOfItemsOfInterestToIncludeInTheResult > itemOfInterestList.length) {
    throw new Error(`Cannot return more items than available. ${itemOfInterestCount} items are requested. ${itemOfInterestList.length} items are available.`)
  }

  for (let i = 0; i < numberOfItemsOfInterestToIncludeInTheResult; i++) {
    const itemOfInterest = new itemOfInterestList[i]()
    const itemOfInterestActivationResult = await itemOfInterest.activate(itemId, item)
    itemOfInterestResultList.push(itemOfInterestActivationResult)
  }

  return itemOfInterestResultList
}

async function updateItem (_itemId: string, _itemRecoveryId?: string, _item?: DatabaseConstructorItemActivateFunctionItemDataStructure<any>): Promise<any> {
  // itemOfInterest.activateOnRepeatMeasure
}

async function getItem (_itemId: string, _itemRecoveryId?: string): Promise<any> {
  // itemOfInterest.getMeasure
}

async function deleteItem (_itemId: string, _itemRecoveryId?: string): Promise<any> {
  // itemOfInterest.remove
}


// - - - - - - - - - - DatabaseConstructorItem Algorithms

// Constants

const DATABASE_INTERCEPTING_MESSENGER_SERVICE_ID_CONSTANT_ID = 'interceptingMessengerServiceId'
const DATABASE_DIRECT_INDIVIDUAL_MESSENGER_SERVICE_ID_CONSTANT_ID = 'directIndividualMessengerServiceId'

const DATABASE_INITIAL_TEMPORARY_PASSWORD = 'password'

const DATABASE_EXPECTING_MANY_UPDATES_CONSTANT_ID = 'expectingManyUpdates'

// Constants - Firebase

const PLACE_ID_TYPE_FIREBASE = 'firebase'

const FIREBASE_VERSION_7_API_KEY_CONSTANT_ID = 'firebaseApiKey'
const FIREBASE_VERSION_7_AUTH_DOMAIN_CONSTANT_ID = 'firebaseAuthDomain'
const FIREBASE_VERSION_7_DATABASE_URL_CONSTANT_ID = 'firebaseDatabaseURL'
const FIREBASE_VERSION_7_PROJECT_ID_CONSTANT_ID = 'firebaseProjectId'
const FIREBASE_VERSION_7_CLOUD_STORAGE_BUCKET_CONSTANT_ID = 'firebaseStorageBucket'
const FIREBASE_VERSION_7_MESSAGING_SENDER_ID_CONSTANT_ID = 'firebaseMessagingSenderId'
const FIREBASE_VERSION_7_APP_ID_CONSTANT_ID = 'firebaseAppId'
const FIREBASE_VERSION_7_MEASUREMENT_ID_CONSTANT_ID = 'firebaseMeasurementId'

const FIREBASE_VERSION_7_REALTIME_DATABASE_SELECTED_CONSTANT_ID = 'selectRealtimeDatabase'
const FIREBASE_VERSION_7_CLOUD_STORAGE_SELECTED_CONSTANT_ID = 'selectStorage'
const FIREBASE_VERSION_7_CLOUD_FIRESTORE_SELECTED_CONSTANT_ID = 'selectCloudFirestore'

// Constants - Firebase Admin

const PLACE_ID_TYPE_FIREBASE_ADMIN = 'firebase-admin'

const FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CERTIFICATE_CONSTANT_ID = 'firebaseAdminCredentialCertificate'
const FIREBASE_ADMIN_VERSION_8_CREDENTIAL_REFRESH_TOKEN_CONSTANT_ID = 'firebaseAdminCredentialRefreshToken'
const FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CONSTANT_ID = 'firebaseAdminCredential'
const FIREBASE_ADMIN_VERSION_8_SERVICE_ACCOUNT_ID_CONSTANT_ID = 'firebaseAdminServiceAccountId'
const FIREBASE_ADMIN_VERSION_8_API_KEY_CONSTANT_ID = 'firebaseAdminAPIKey'
const FIREBASE_ADMIN_VERSION_8_AUTH_DOMAIN_CONSTANT_ID = 'firebaseAdminAuthDomain'
const FIREBASE_ADMIN_VERSION_8_DATABASE_URL_CONSTANT_ID = 'firebaseAdminDatabaseURL'
const FIREBASE_ADMIN_VERSION_8_PROJECT_ID_CONSTANT_ID = 'firebaseAdminProjectId'
const FIREBASE_ADMIN_VERSION_8_STORAGE_BUCKET_CONSTANT_ID = 'firebaseAdminStorageBucket'
const FIREBASE_ADMIN_VERSION_8_MESSAGING_SENDER_ID_CONSTANT_ID = 'firebaseAdminMessagingSenderId'
const FIREBASE_ADMIN_VERSION_8_APP_ID_CONSTANT_ID = 'firebaseAdminAppId'
const FIREBASE_ADMIN_VERSION_8_MEASUREMENT_ID_CONSTANT_ID = 'firebaseAdminMeasurementId'

const FIREBASE_ADMIN_VERSION_8_REALTIME_DATABASE_SELECTED_CONSTANT_ID = 'selectRealtimeDatabase'
const FIREBASE_ADMIN_VERSION_8_STORAGE_SELECTED_CONSTANT_ID = 'selectStorage'

// Constants - Firebase Testing

const PLACE_ID_TYPE_FIREBASE_TESTING = 'firebase-testing'

const FIREBASE_TESTING_VERSION_0_DATABASE_NAME_CONSTANT_ID = 'firebaseTestingDatabaseName'
const FIREBASE_TESTING_VERSION_0_AUTH_UID_CONSTANT_ID = 'firebaseTestingAuthUID'
const FIREBASE_TESTING_VERSION_0_PROJECT_ID_CONSTANT_ID = 'firebaseTestingProjectId'
// const FIREBASE_TESTING_VERSION_0_API_KEY_CONSTANT_ID = 'firebaseTestingAPIKey'
const FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_RULES_CONSTANT_ID = 'firebaseTestingRealtimeDatabaseRules'

const FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_SELECTED_CONSTANT_ID = 'selectFirebaseRealtimeDatabase'
const FIREBASE_TESTING_VERSION_0_FIREBASE_ADMIN_REALTIME_DATABASE_SELECTED_CONSTANT_ID = 'selectFirebaseAdminRealtimeDatabase'

// Constants - LocalForage

const PLACE_ID_TYPE_LOCAL_FORAGE = 'localforage'

const LOCALFORAGE_VERSION_1_DATABASE_NAME_CONSTANT_ID = 'localForageDatabaseName'
const LOCALFORAGE_VERSION_1_DATABASE_STORE_NAME_LIST_CONSTANT_ID = 'localForageDatabaseStoreNameList'
const LOCALFORAGE_VERSION_1_DATABASE_PASSWORD_CONSTANT_ID = 'localForageDatabasePassword'


// Variables

const localMemoryDatabase: { [localMemoryAddressId: string]: any } = {}

/*
const localIntermediateDataLocation = {
  webAddressCreationRequest: {},
  webAddressUpdateRequest: {},
  webAddressDeletionRequest: {},
  webAddressGetRequest: {},
  webAddressCreationResponse: {},
  webAddressUpdateResponse: {},
  webAddressDeletionResponse: {},
  webAddressGetResponse: {},

  arbitraryInformationRequest: {},
  arbitraryInformationResponse: {}
}
*/

// Algorithms


async function databaseConstructorItemActivateAlgorithm (itemId: string, item: DatabaseConstructorItemActivateFunctionItemDataStructure<any>) {

  const generalProtocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_GENERAL_STRING_ADDRESS_PROTOCOL, itemId)
  
  // const generalProtocolPlace = generalProtocolMessage.protocolPlace
  const generalProtocolAddress = generalProtocolMessage.protocolAddress
  // const generalProtocolPathList = generalProtocolMessage.protocolPathList
  const generalProtocolQueryParameterObjectDataStructure = generalProtocolMessage.protocolQueryParameterObjectDataStructure

  const interceptingMessengerServiceId = generalProtocolQueryParameterObjectDataStructure[DATABASE_INTERCEPTING_MESSENGER_SERVICE_ID_CONSTANT_ID]
  const directIndividualMessengerServiceId = generalProtocolQueryParameterObjectDataStructure[DATABASE_DIRECT_INDIVIDUAL_MESSENGER_SERVICE_ID_CONSTANT_ID]
  const expectingManyUpdates = generalProtocolQueryParameterObjectDataStructure[DATABASE_EXPECTING_MANY_UPDATES_CONSTANT_ID]

  // create an item
  let createItemResultList = []

  // (1) intercepting messenger service + direct
  // individual messenger (HTTP + WebSocket / WebRTC + ??)

  if (interceptingMessengerServiceId || directIndividualMessengerServiceId) {
    const createItemResult = performIncerceptingMessagingProtocol(interceptingMessengerServiceId, directIndividualMessengerServiceId, expectingManyUpdates)
    createItemResultList.push(createItemResult)
  }

  // (2.1) firebase
  // get information on firebase information
  // get information on firebase specific implementation
  // send message to create item
  const {
    firebaseApp,
    // firebaseProtocolPlace,
    firebaseProtocolAddress,
    // firebaseProtocolPathList,
    firebaseProtocolQueryParameterObjectDataStructure
  } = getFirebaseInformation(generalProtocolAddress)

  // Firebase Realtime Database
  if (firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_REALTIME_DATABASE_SELECTED_CONSTANT_ID]) {
    const firebaseRealtimeDatabase = firebaseApp.database()
    const createItemResult = await firebaseRealtimeDatabasePublish(firebaseRealtimeDatabase, firebaseProtocolAddress, item)
    createItemResultList.push(createItemResult)
  }
  
  // Firebase Cloud Storage
  if (firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_CLOUD_STORAGE_SELECTED_CONSTANT_ID]) {
    const firebaseCloudStorage = firebaseApp.storage()
    const createItemResult = await firebaseStoragePublish(firebaseCloudStorage, firebaseProtocolAddress, item)
    createItemResultList.push(createItemResult)
  }

  // Firebase Cloud Firestore
  if (firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_CLOUD_FIRESTORE_SELECTED_CONSTANT_ID]) {
    // const firebaseCloudFirestore = firebaseApp.firestore()
  }

  // (2.2) firebase-admin
  // get information on firebase-admin information
  // get information on firebase-admin specific information
  // send message to create item
  const {
    firebaseAdminApp,
    // firebaseProtocolPlace,
    firebaseAdminProtocolAddress,
    // firebaseProtocolPathList,
    firebaseAdminProtocolQueryParameterObjectDataStructure
  } = getFirebaseAdminInformation(generalProtocolAddress)

  // Firebase Admin Realtime Database
  if (firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_REALTIME_DATABASE_SELECTED_CONSTANT_ID]) {
    const firebaseAdminRealtimeDatabase = firebaseAdminApp.database()
    const createItemResult = await firebaseAdminRealtimeDatabasePublish(firebaseAdminRealtimeDatabase, firebaseAdminProtocolAddress, item)
    createItemResultList.push(createItemResult)
  }

  // Firebase Admin Storage
  if (firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_STORAGE_SELECTED_CONSTANT_ID]) {
    const firebaseAdminStorage = firebaseAdminApp.storage()
    const createItemResult = await firebaseAdminStoragePublish(firebaseAdminStorage, firebaseAdminProtocolAddress, item)
    createItemResultList.push(createItemResult)
  }

  // (2.3) firebase-testing
  // get information on firebase-testing information
  // get information on firebase-testing specific information
  // send message to create item

  const {
    firebaseTestingApp,
    firebaseTestingAdminApp,
    // firebaseTestingProtocolPlace,
    // firebaseTestingProtocolAddress,
    // firebaseTestingProtocolPathList,
    firebaseTestingProtocolQueryParameterObjectDataStructure
  } = await getFirebaseTestingInformation(generalProtocolAddress)

  // Firebase Testing - Firebase Realtime Database
  if (firebaseTestingProtocolQueryParameterObjectDataStructure[FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_SELECTED_CONSTANT_ID]) {
    const firebaseTestingRealtimeDatabase = firebaseTestingApp.database()
    const createItemResult = await firebaseRealtimeDatabasePublish(firebaseTestingRealtimeDatabase, firebaseProtocolAddress, item)
    createItemResultList.push(createItemResult)
  }

  // Firebase Testing - Firebase Admin Realtime Database
  if (firebaseTestingProtocolQueryParameterObjectDataStructure[FIREBASE_TESTING_VERSION_0_FIREBASE_ADMIN_REALTIME_DATABASE_SELECTED_CONSTANT_ID]) {
    const firebaseAdminTestingRealtimeDatabase = firebaseTestingAdminApp.database()
    const createItemResult = await firebaseAdminRealtimeDatabasePublish(firebaseAdminTestingRealtimeDatabase, firebaseAdminProtocolAddress, item)
    createItemResultList.push(createItemResult)
  }

  // (3) localforage
  // get information on localforage information
  // get information on localforage specific information
  // send message to create item

  const {
    // localForageDatabaseName,
    localForageDatabaseStoreNameList,
    // localForageDatabasePassword,
    // referenceTable,
    informationTable,
    // localForageProtocolPlace,
    localForageProtocolAddress,
    // localForageProtocolPathList,
    // localForageProtocolQueryParameterObjectDataStructure
  } = await getLocalForageInformation(generalProtocolAddress)

  for (let i = 0; i < localForageDatabaseStoreNameList.length; i++) {
    const databaseStoreName = localForageDatabaseStoreNameList[i]
    const localForageDatabaseStore = informationTable[databaseStoreName]
    const createItemResult = await localForageDatabasePublish(localForageDatabaseStore, localForageProtocolAddress, item)
    createItemResultList.push(createItemResult)
  }

  // (4) ipfs
  // (5) orbit-db

  // (6) webdb
  // (7) dat-archive
  // () nodejs-file-system
  // () google drive
  // () boxdrop

  // (8) browser-fs
  // (9) couch-db
  // (10) mongodb
  // () google cloud storage
  // () google cloud datastore
  // 

  // (5) local-memory
  // get information on localforage information
  // get information on localforage specific information
  // send message to create item

  // (6) heroku
  // get information on heroku information
  // get information on heroku specific information
  // send message to create item

  // (6) glitch
  // (7) google cloud platform
  // (8) microsoft azure
  // (9) amazon web services
  // (10) digital ocean
  // () web-browser-web-worker
  // () browserstack
  // () web-browser-execution-environment
  // 

  // () neocities
  // () gitlab
  // () github
  // () perkeep
  // () web-browser-service-worker
  // () vercel
  // () solid
  // () npm-js

  // () webtorrent

  // () guerillamail
  // () protonmail
  // () slack
  // () mailchimp

  // () youtube
  // () twitter

  // () js-qr
  // 

  // () vuejs-vuetify
  // () 

  // create an item of interest
  // create information log or data entry listing of item
  // create information log event listener

  // update item of interest
  // publish information on updates automatically based on 
    // what the user has requested such as a
    // message on what other items are doing
    // with respect to this item
    // Artificial Intelligence Is Like This.

  // start item of interest
  // start the computer program
}


// - - - - - - - - - - - A Global Anonymous Function

const DEFAULT_HTTP_WEB_ADDRESS_PORT_NUMBER = 9193

createAnonymousFunction()

function createAnonymousFunction (httpWebAddressPortNumber?: number) {
  // const webAddressTable = {}
  // const webAddressTypeToIdentityTable = {}

  // registerForCreatingAWebAddress

  // startWebServer
  const webServerResponse = startWebServer()
  const webServerWebAddressPortNumber = httpWebAddressPortNumber || DEFAULT_HTTP_WEB_ADDRESS_PORT_NUMBER

  webServerResponse.webServerResponse.httpServer.listen(webServerWebAddressPortNumber, () => {
    console.log('Web Server Started At: ', webServerWebAddressPortNumber)
  })
  webServerResponse.webServerResponse.httpServer.get(`/`, (_request, response) => { response.send('Hello World') } )

  // startWebServerDeveloper

  // startVistaAttackResiliance

  // startSelfCodebaseDeveloper
    // for managing the local codebase

  return {
    webServerResponse
  }
}

// - - - - - - - - - - - Computer Program For Self-Codebase Development

function startSelfCodebaseDeveloper () {
  // add event listener to new web address requests
    // respond to web address requests
  // add event listener to new arbitrary information requests
    // respond to arbitrary information requests
  
  // are there any programs we should consider here
  // for example, things where memory leaks or
  // information leaks from this self computer program
  // codebase could start becoming a problem for
  // our customer or even for the code that they run
  // or even for arbitrary codebases that run using this
  // codebase evaluation context?
  // having anonymous functions to encapsulate a lot of
  // our local variables helps ensure that we don't have
  // 'free' information leaks so that some of our
  // available information isn't available on demand
  // at 'global' or 'window' or places where we're not
  // able to run functions using 'eval()' or something
  // like that and not feel confident that our program
  // might leak some weird or strange information
  // that we may not want being revealed.
  //
  // In general, this type of corner case is quit extreme
  // but we could possibly want to think about it for the
  // future of the programs that we write.
  //
  // That future possibly involves things like
  // (1) being able to run arbitrary programs (even
  //    malicious programs) in our own 'so-called' secure
  //    execution environments. Without having to start up
  //    other virtual machines or web workers or things
  //    that could possibly change the 'threaded' context
  //    environment or something like that.
  // (2) -_- some other weird ideas -_-
}

function isTheCurrentComputerDoingPoorly () {
  // 
}


// - - - - - - - - - - - Computer Program For Web Server Services

// web server

// Start Web Server

function startWebServer (_information?: any) {
  // express
  const webServerResponse = createWebServer()
  // web socket
  // const websocketResponse = createWebServerWebSocketAddon({ httpServer: webServerResponse.httpServerByHttp })
  // web socket to webrtc relay
  // const websocketWebRTCRelayComputerProgramResponse = createWebServerWebSocketWebRTCRelayComputerProgram(webServerResponse.httpServer, websocketResponse.websocketServer)
  // // n-ary device relay
  // const something4 = createNAryDeviceRelayComputerProgram()
  // // webpack information
  // const something5 = createWebpackReadyComputerProgram()
  // // web server information
  // const something6 = createWebServerReadyComputerProgram()

  return {
    webServerResponse
  }
}

// Create Web Server

function createWebServer () {
  // initialize expressjs
  const httpServer = express()
  const httpServerByHttp = http.createServer(httpServer)

  // initialize expressjs plugins
  httpServer.use(express.urlencoded({ extended: false }))
  httpServer.use(express.json())

  return {
    httpServer,
    httpServerByHttp
  }
}

// Create Web Server WebSocket Addon

function createWebServerWebSocketAddon (websocketServerConfig?: WebSocket.IServerConfig) {
  // initialize web socket addon ideas
  const websocketServer = new WebSocket.server(websocketServerConfig)

  return {
    websocketServer
  }
}

// Create Web Server WebSocket WebRTC Relay Computer Program

function createWebServerWebSocketWebRTCRelayComputerProgram (_httpServer: express.Express, _websocketServer: WebSocket.server) {
  // create a variable table for (1) requests to connect
    // to a webrtc client
  const webRTCClientRequestTable = {}
  // (2) responses that have been replied to
  const webRTCClientResponseTable = {}
  // (3) requests that didn't work
  const webRTCClientRequestFailedTable = {}
  // (4) responses that didn't work
  const webRTCClientResponseFailedTable = {}
  // (5) information for the administrator to know about
  const administratorInformation = {}

  // create a messaging channel for the http server to
    // receive requests for networking with a webrtc client
  
  // (1) http (postponed)
  // (2) websocket (postponed)
  // (3) webrtc

  

  // create a messaging channel for acknowledging that
    // the webrtc response has been connected to appropriately
  
  // create a messaging channel for acknowledging that
    // the webrtc response has been not connected to appropriately
  
  return {
    webRTCClientRequestTable,
    webRTCClientResponseTable,
    webRTCClientRequestFailedTable,
    webRTCClientResponseFailedTable,
    administratorInformation
  }
}

const WEB_ADDRESS_CONNECTION_TYPE_WEBRTC = 'WEB_RTC_CONNECTION'
const WEB_ADDRESS_CONNECTION_TYPE_WEBSOCKET = 'WEBSOCKET_CONNECTION'
const WEB_ADDRESS_CONNECTION_TYPE_HTTP = 'HTTP_CONNECTION'

function registerForCreatingAWebAddress (isAllowedConnectionTypeTable: { [connectionTypeId: string]: boolean }, isConnectionInitiator?: boolean) {
  const webAddressTable: { [webAddressIdentity: string]: any } = {}
  const webAddressTypeToIdentityMap: { [webAddressType: string]: any } = {}

  // webrtc web address
  if (isAllowedConnectionTypeTable[WEB_ADDRESS_CONNECTION_TYPE_WEBRTC]) {
    const webRTCComputerConnectionIdentity = getComputerIdentity()
    const something = registerForCreatingAWebRTCWebAddress(isConnectionInitiator)

    webAddressTable[webRTCComputerConnectionIdentity] = something

    if (!webAddressTypeToIdentityMap[WEB_ADDRESS_CONNECTION_TYPE_WEBRTC]) {
      webAddressTypeToIdentityMap[WEB_ADDRESS_CONNECTION_TYPE_WEBRTC] = {}
    }
    webAddressTypeToIdentityMap[WEB_ADDRESS_CONNECTION_TYPE_WEBRTC][webRTCComputerConnectionIdentity] = true
  }
  // websocket web address
  if (isAllowedConnectionTypeTable[WEB_ADDRESS_CONNECTION_TYPE_WEBSOCKET]) {
    //
  }
  // http web address
  if (isAllowedConnectionTypeTable[WEB_ADDRESS_CONNECTION_TYPE_HTTP]) {
    //
  }

  return {
    webAddressTable,
    webAddressTypeToIdentityMap
  }
}

function registerForCreatingAWebRTCWebAddress (
  isRequestCreatorOrInitiator?: boolean,
  onConnectMessageCreated?: Function,
  onDataMessageCreated?: Function,
  onStreamMessageCreated?: Function,
  onTrackMessageCreated?: Function,
  onCloseMessageCreated?: Function,
  onErrorMessageCreated?: Function
) {
  const simplePeer = new SimplePeer({
    initiator: isRequestCreatorOrInitiator,
    wrtc: SimplePeer.WEBRTC_SUPPORT ? wrtc : null
  })
  let webRTCSignalMessageAsWebAddress: any = {
    webAddress: null
  }

  simplePeer.on('signal', function (signalData) {
    webRTCSignalMessageAsWebAddress.webAddress = signalData
  })
  simplePeer.on('connect', function (connectionData: any) {
    if (!onConnectMessageCreated) {
      return
    }
    onConnectMessageCreated(connectionData)
  })
  simplePeer.on('data', function (data) {
    if (!onDataMessageCreated) {
      return
    }
    onDataMessageCreated(data)
  })
  simplePeer.on('stream', function (streamData) {
    if (!onStreamMessageCreated) {
      return
    }
    onStreamMessageCreated(streamData)
  })
  simplePeer.on('track', function (trackData) {
    if (!onTrackMessageCreated) {
      return
    }
    onTrackMessageCreated(trackData)
  })
  simplePeer.on('close', function (closeData: any) {
    if (!onCloseMessageCreated) {
      return
    }
    onCloseMessageCreated(closeData)
  })
  simplePeer.on('error', function (errorMessage) {
    if (!onErrorMessageCreated) {
      return
    }
    onErrorMessageCreated(errorMessage)
  })

  return {
    simplePeer,
    webRTCSignalMessageAsWebAddress
  }
}

function createWebSocketWebRTCConnection (_targetWebAddressIdentity: string, _connectionType?: string) {
  // a connection needs an initiator
  
  // a connection needs a respondant
  
  // one respondant is fine

  // then we need to check on how the connection is going

  // did the response work?

}


// Create N-Ary Device Relay Computer Program

function createNAryDeviceRelayComputerProgram () {
  //
}

// Create Webpack Ready Computer Program

function createWebpackReadyComputerProgram () {
  //
}

// Create Web Server Ready Computer Program

function createWebServerReadyComputerProgram () {
  //
}



// developer

function startWebServerDeveloper (_information: any) {
  // n-ary-device acceptance measure
  // administrator acceptance measure
}

// vista attack resiliance

function startVistaAttackResiliance (_information: any) {
  // block IP Addresses
  // block users with certain information
  // block everyone
  // wait for the administrator to respond to a request
  // start things that the administrator says to start
}



// - - - - - - - - - - - Computer Programs For 3rd Party Services

async function performIncerceptingMessagingProtocol (interceptingMessengerServiceId: string, directIndividualMessengerServiceId: string, expectingManyUpdates: boolean) {

  const promise = new Promise(async (resolve, _reject) => {
    let directIndividualNetworkConnection: any

    const informationToAccessDirectIndividual = await getItem(interceptingMessengerServiceId)

    if (localMemoryDatabase[directIndividualMessengerServiceId]) {
      directIndividualNetworkConnection = localMemoryDatabase[directIndividualMessengerServiceId].networkConnection
    }

    // start a network connection
      // use directIndividualMessengerServiceId to prompt
      // the individual to perform the requested service
    directIndividualNetworkConnection = directIndividualNetworkConnection || await startNetworkConnection(informationToAccessDirectIndividual)
    directIndividualNetworkConnection.sendMessage('', directIndividualMessengerServiceId)
    directIndividualNetworkConnection.addMessageEventListener('', function (messageType: string, message: any) {
      if (expectingManyUpdates) {
        resolve(message)
        return
      }
      if (messageType !== 'created-item') {
        resolve(message)
        return
      }
      // if (message !== is the same message) {
      //    resolve(message)
      //    return
      // }
      resolve(message)
      directIndividualNetworkConnection.closeConnection()
    })
  })

  return promise
}

function startNetworkConnection (_information: any) {

}


// - - - - - - - - - - 3rd Party Service Provider Programs


function getFirebaseInformation (generalProtocolAddress: string): any {
  const firebaseProtocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_FIREBASE, generalProtocolAddress)

  const firebaseProtocolPlace = firebaseProtocolMessage.protocolPlace
  const firebaseProtocolAddress = firebaseProtocolMessage.protocolAddress
  const firebaseProtocolPathList = firebaseProtocolMessage.protocolPathList
  const firebaseProtocolQueryParameterObjectDataStructure = firebaseProtocolMessage.protocolQueryParameterObjectDataStructure

  // get information on firebase information
    // Developer Note: Nothing Here Right Now
      // Firebase Version Information is an example of
      // information that can be collected here.

  // get information on firebase specific implementation
  const firebaseConfigurationInformation = {
    apiKey: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_API_KEY_CONSTANT_ID],
    authDomain: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_AUTH_DOMAIN_CONSTANT_ID],
    databaseURL: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_DATABASE_URL_CONSTANT_ID],
    projectId: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_PROJECT_ID_CONSTANT_ID],
    storageBucket: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_CLOUD_STORAGE_BUCKET_CONSTANT_ID],
    messagingSenderId: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_MESSAGING_SENDER_ID_CONSTANT_ID],
    appId: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_APP_ID_CONSTANT_ID],
    measurementId: firebaseProtocolQueryParameterObjectDataStructure[FIREBASE_VERSION_7_MEASUREMENT_ID_CONSTANT_ID]
  }

  // send message to create item
  const firebaseApp = firebase.apps.length > 0 ? firebase.app() : firebase.initializeApp(firebaseConfigurationInformation)

  return {
    firebaseApp,
    firebaseProtocolPlace,
    firebaseProtocolAddress,
    firebaseProtocolPathList,
    firebaseProtocolQueryParameterObjectDataStructure
  }
}

async function firebaseRealtimeDatabasePublish (firebaseRealtimeDatabase: any, firebaseItemAddress: string, firebaseItem: any): Promise<any> {
  let createItemResult = null

  if (typeof firebaseItem === 'object') {
    for (let propertyId in firebaseItem) {
      if (!firebaseItem.hasOwnProperty(propertyId)) { continue }
      try {
        await firebaseRealtimeDatabase.ref(`${firebaseItemAddress}/${propertyId}`).set(firebaseItem[propertyId])
      } catch (errorMessage) {
        console.error(errorMessage)
        // TODO: Something to do with error messages from firebase partial read/write rules .
      }
    }
  // set non-object item .
  } else {
    await firebaseRealtimeDatabase.ref(`${firebaseItemAddress}`).set(firebaseItem)
  }

  // get the created item for non-file document types .
  createItemResult = (await firebaseRealtimeDatabase.ref(`${firebaseItemAddress}`).once('value')).val()

  return createItemResult
}

async function firebaseStoragePublish (firebaseCloudStorage: any, firebaseItemAddress: string, firebaseItem: any): Promise<any> {
  let createItemResult = null
  firebaseCloudStorage.ref(`${firebaseItemAddress}`).put(firebaseItem)
  createItemResult = firebaseCloudStorage.ref(`${firebaseItemAddress}`).getDownloadURL()
  return createItemResult
}

function getFirebaseAdminInformation (generalProtocolAddress: string) {
  const firebaseAdminProtocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_FIREBASE_ADMIN, generalProtocolAddress)

  const firebaseAdminProtocolPlace = firebaseAdminProtocolMessage.protocolPlace
  const firebaseAdminProtocolAddress = firebaseAdminProtocolMessage.protocolAddress
  const firebaseAdminProtocolPathList = firebaseAdminProtocolMessage.protocolPathList
  const firebaseAdminProtocolQueryParameterObjectDataStructure = firebaseAdminProtocolMessage.protocolQueryParameterObjectDataStructure

  // get information on firebase-admin information
  // get information on firebase-admin specific information

  const firebaseCredentialCertificate = firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CERTIFICATE_CONSTANT_ID]
  const firebaseCredentialRefreshToken = firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_CREDENTIAL_REFRESH_TOKEN_CONSTANT_ID]

  const firebaseAdminConfigurationInformation = {
    credential: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CONSTANT_ID] || firebaseAdmin.credential.cert(firebaseCredentialCertificate) || firebaseAdmin.credential.refreshToken(firebaseCredentialRefreshToken),
    serviceAccountId: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_SERVICE_ACCOUNT_ID_CONSTANT_ID],
    apiKey: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_API_KEY_CONSTANT_ID],
    authDomain: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_AUTH_DOMAIN_CONSTANT_ID],
    databaseURL: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_DATABASE_URL_CONSTANT_ID],
    projectId: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_PROJECT_ID_CONSTANT_ID],
    storageBucket: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_STORAGE_BUCKET_CONSTANT_ID],
    messagingSenderId: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_MESSAGING_SENDER_ID_CONSTANT_ID],
    appId: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_APP_ID_CONSTANT_ID],
    measurementId: firebaseAdminProtocolQueryParameterObjectDataStructure[FIREBASE_ADMIN_VERSION_8_MEASUREMENT_ID_CONSTANT_ID]
  }

  // send message to create item
  const firebaseAdminApp = firebaseAdmin.apps.length > 0 ? firebaseAdmin.app() : firebaseAdmin.initializeApp(firebaseAdminConfigurationInformation)

  return {
    firebaseAdminApp,
    firebaseAdminProtocolPlace,
    firebaseAdminProtocolAddress,
    firebaseAdminProtocolPathList,
    firebaseAdminProtocolQueryParameterObjectDataStructure
  }
}

async function firebaseAdminRealtimeDatabasePublish (firebaseAdminRealtimeDatabase: any, firebaseAdminItemAddress: string, firebaseAdminItem: any): Promise<any> {
  let createItemResult = await firebaseAdminRealtimeDatabase.ref(`${firebaseAdminItemAddress}`).set(firebaseAdminItem)
  return createItemResult
}

async function firebaseAdminStoragePublish (firebaseAdminStorage: any, firebaseAdminItemAddress: string, firebaseAdminItem: any): Promise<any> {
  const createItemResult = await firebaseAdminStorage.bucket(`${firebaseAdminItemAddress}`).upload(firebaseAdminItem)
  return createItemResult
}


async function getFirebaseTestingInformation (generalProtocolAddress: string): Promise<any> {
  const firebaseTestingProtocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_FIREBASE_TESTING, generalProtocolAddress)

  const firebaseTestingProtocolPlace = firebaseTestingProtocolMessage.protocolPlace
  const firebaseTestingProtocolAddress = firebaseTestingProtocolMessage.protocolAddress
  const firebaseTestingProtocolPathList = firebaseTestingProtocolMessage.protocolPathList
  const firebaseTestingProtocolQueryParameterObjectDataStructure = firebaseTestingProtocolMessage.protocolQueryParameterObjectDataStructure

  // get information on firebase-testing information
  // get information on firebase-testing specific information

  const firebaseTestingDatabaseName = firebaseTestingProtocolQueryParameterObjectDataStructure[FIREBASE_TESTING_VERSION_0_DATABASE_NAME_CONSTANT_ID]
  const firebaseTestingAuthUID = firebaseTestingProtocolQueryParameterObjectDataStructure[FIREBASE_TESTING_VERSION_0_AUTH_UID_CONSTANT_ID]
  const firebaseTestingRealtimeDatabaseRules = firebaseTestingProtocolQueryParameterObjectDataStructure[FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_RULES_CONSTANT_ID]
  const firebaseTestingProjectId = firebaseTestingProtocolQueryParameterObjectDataStructure[FIREBASE_TESTING_VERSION_0_PROJECT_ID_CONSTANT_ID]
  // const firebaseTestingAPIKey = firebaseTestingProtocolQueryParameterObjectDataStructure[FIREBASE_TESTING_VERSION_0_API_KEY_CONSTANT_ID]

  // send message to create item

  const firebaseTestingApp = firebaseTesting.initializeTestApp({
    databaseName: firebaseTestingDatabaseName,
    auth: { uid: firebaseTestingAuthUID },
    projectId: firebaseTestingProjectId // ,
    // apiKey: firebaseTestingAPIKey
  })
  
  const firebaseTestingAdminApp = firebaseTesting.initializeAdminApp({
    databaseName: firebaseTestingDatabaseName
  })
  
  // load database rules
  await firebaseTesting.loadDatabaseRules({
    databaseName: firebaseTestingDatabaseName,
    rules: firebaseTestingRealtimeDatabaseRules || JSON.stringify({
      rules: {
        '.read': true,
        '.write': true
      }
    })
  })

  return {
    firebaseTestingApp,
    firebaseTestingAdminApp,
    firebaseTestingProtocolPlace,
    firebaseTestingProtocolAddress,
    firebaseTestingProtocolPathList,
    firebaseTestingProtocolQueryParameterObjectDataStructure
  }
}

async function getLocalForageInformation (generalProtocolAddress: string) {

  const localForageProtocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_LOCAL_FORAGE, generalProtocolAddress)

  const localForageProtocolPlace = localForageProtocolMessage.protocolPlace
  const localForageProtocolAddress = localForageProtocolMessage.protocolAddress
  const localForageProtocolPathList = localForageProtocolMessage.protocolPathList
  const localForageProtocolQueryParameterObjectDataStructure = localForageProtocolMessage.protocolQueryParameterObjectDataStructure

  const localForageDatabaseName = localForageProtocolQueryParameterObjectDataStructure[LOCALFORAGE_VERSION_1_DATABASE_NAME_CONSTANT_ID] || localForageProtocolPathList[0]
  const localForageDatabaseStoreNameList = localForageProtocolQueryParameterObjectDataStructure[LOCALFORAGE_VERSION_1_DATABASE_STORE_NAME_LIST_CONSTANT_ID]
  const localForageDatabasePassword = localForageProtocolQueryParameterObjectDataStructure[LOCALFORAGE_VERSION_1_DATABASE_PASSWORD_CONSTANT_ID]

  const currentLocalForagePassword = (await getItem('', '')) || DATABASE_INITIAL_TEMPORARY_PASSWORD

  let referenceTable: LocalForage|null = null
  let informationTable: { [informationTableId: string]: LocalForage } = {}

  // If the password is provided, access the database
  if (currentLocalForagePassword === localForageDatabasePassword) {
    referenceTable = localForage.createInstance({ name: localForageDatabaseName })

    try {
      await referenceTable.iterate(async (_: any, _something: string) => {
        // await referenceTable.setItem(storeId, true)
        // storeTable[storeId] = referenceTable.createInstance({ name: storeId })
      })
    } catch (errorMessage) {
      throw Error('')
    }
  }

  // Return the results
  return {
    localForageDatabaseName,
    localForageDatabaseStoreNameList,
    localForageDatabasePassword,
    referenceTable,
    informationTable,
    localForageProtocolPlace,
    localForageProtocolAddress,
    localForageProtocolPathList,
    localForageProtocolQueryParameterObjectDataStructure
  }
}

async function localForageDatabasePublish (localForageDatabaseStore: any, localForageItemAddress: string, localForageItem: any): Promise<any> {
  const createItemResult = await localForageDatabaseStore.setItem(localForageItemAddress, localForageItem)
  return createItemResult
}

// - - - - - - - - - - - Computer Program For General Tasks

function getComputerIdentity (additiveAmount?: number) {

  let resultNumber = ''
  additiveAmount = additiveAmount || 1

  if (additiveAmount <= 0) {
    additiveAmount = 1
  }

  for (let i = 0; i < additiveAmount; i++) {
    let randomNumber = (Math.random()).toString(16)
    randomNumber = randomNumber.slice('0.'.length, randomNumber.length)
    resultNumber = `${resultNumber}${randomNumber}`
  }

  return resultNumber
}


export {

  // General Data Structures
  DatabaseConstructorItem,
  EventListenerConstructorItem,
  PermissionRuleConstructorItem,
  NetworkProtocolConstructorItem,
  ScaleMatrixAmanaConstructorItem,
  InteractiveGraphConstructorItem,
  ComputerProgramManagerConstructorItem,
  DatabaseConstructorItemActivateFunctionItemDataStructure,

  // General Variables
  itemOfInterestList,

  // General Algorithms
  createItem,
  updateItem,
  getItem,
  deleteItem,

  // General Constants

  DATABASE_INTERCEPTING_MESSENGER_SERVICE_ID_CONSTANT_ID,
  DATABASE_DIRECT_INDIVIDUAL_MESSENGER_SERVICE_ID_CONSTANT_ID,
  DATABASE_INITIAL_TEMPORARY_PASSWORD,
  DATABASE_EXPECTING_MANY_UPDATES_CONSTANT_ID,

  // 3rd Party Service Constants - Firebase
  PLACE_ID_TYPE_FIREBASE,

  FIREBASE_VERSION_7_API_KEY_CONSTANT_ID,
  FIREBASE_VERSION_7_AUTH_DOMAIN_CONSTANT_ID,
  FIREBASE_VERSION_7_DATABASE_URL_CONSTANT_ID,
  FIREBASE_VERSION_7_PROJECT_ID_CONSTANT_ID,
  FIREBASE_VERSION_7_CLOUD_STORAGE_BUCKET_CONSTANT_ID,
  FIREBASE_VERSION_7_MESSAGING_SENDER_ID_CONSTANT_ID,
  FIREBASE_VERSION_7_APP_ID_CONSTANT_ID,
  FIREBASE_VERSION_7_MEASUREMENT_ID_CONSTANT_ID,

  FIREBASE_VERSION_7_REALTIME_DATABASE_SELECTED_CONSTANT_ID,
  FIREBASE_VERSION_7_CLOUD_STORAGE_SELECTED_CONSTANT_ID,
  FIREBASE_VERSION_7_CLOUD_FIRESTORE_SELECTED_CONSTANT_ID,

  // 3rd Party Service Constants - Firebase Admin
  PLACE_ID_TYPE_FIREBASE_ADMIN,

  FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CERTIFICATE_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_CREDENTIAL_REFRESH_TOKEN_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_CREDENTIAL_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_SERVICE_ACCOUNT_ID_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_API_KEY_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_AUTH_DOMAIN_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_DATABASE_URL_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_PROJECT_ID_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_STORAGE_BUCKET_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_MESSAGING_SENDER_ID_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_APP_ID_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_MEASUREMENT_ID_CONSTANT_ID,

  FIREBASE_ADMIN_VERSION_8_REALTIME_DATABASE_SELECTED_CONSTANT_ID,
  FIREBASE_ADMIN_VERSION_8_STORAGE_SELECTED_CONSTANT_ID,

  // 3rd Party Service Constants - Firebase Testing

  PLACE_ID_TYPE_FIREBASE_TESTING,

  FIREBASE_TESTING_VERSION_0_DATABASE_NAME_CONSTANT_ID,
  FIREBASE_TESTING_VERSION_0_AUTH_UID_CONSTANT_ID,
  FIREBASE_TESTING_VERSION_0_PROJECT_ID_CONSTANT_ID,
  // FIREBASE_TESTING_VERSION_0_API_KEY_CONSTANT_ID,
  FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_RULES_CONSTANT_ID,

  FIREBASE_TESTING_VERSION_0_FIREBASE_REALTIME_DATABASE_SELECTED_CONSTANT_ID,
  FIREBASE_TESTING_VERSION_0_FIREBASE_ADMIN_REALTIME_DATABASE_SELECTED_CONSTANT_ID,

  // 3rd Party Service Constants - Local Forage

  PLACE_ID_TYPE_LOCAL_FORAGE,

  LOCALFORAGE_VERSION_1_DATABASE_NAME_CONSTANT_ID,
  LOCALFORAGE_VERSION_1_DATABASE_STORE_NAME_LIST_CONSTANT_ID,
  LOCALFORAGE_VERSION_1_DATABASE_PASSWORD_CONSTANT_ID,
  
  localMemoryDatabase,

  createAnonymousFunction,
  startSelfCodebaseDeveloper,
  isTheCurrentComputerDoingPoorly,

  // Web Server
  startWebServer,
  createWebServer,
  createWebServerWebSocketAddon,
  createWebServerWebSocketWebRTCRelayComputerProgram,

  WEB_ADDRESS_CONNECTION_TYPE_WEBRTC,
  WEB_ADDRESS_CONNECTION_TYPE_WEBSOCKET,
  WEB_ADDRESS_CONNECTION_TYPE_HTTP,

  registerForCreatingAWebAddress,
  registerForCreatingAWebRTCWebAddress,
  createWebSocketWebRTCConnection,
  createNAryDeviceRelayComputerProgram,
  createWebpackReadyComputerProgram,
  createWebServerReadyComputerProgram,
  startWebServerDeveloper,
  startVistaAttackResiliance,

  performIncerceptingMessagingProtocol,
  startNetworkConnection,

  // Database Constructor Algorithms

  databaseConstructorItemActivateAlgorithm,

  // Functions - Firebase
  getFirebaseInformation,
  firebaseRealtimeDatabasePublish,
  firebaseStoragePublish,

  // Functions - Firebase Admin
  getFirebaseAdminInformation,
  firebaseAdminRealtimeDatabasePublish,
  firebaseAdminStoragePublish,

  // Functions - Local Forage
  getFirebaseTestingInformation,
  getLocalForageInformation,
  localForageDatabasePublish,

  // General Functions

  getComputerIdentity,

  firebase
}




