
/*

(1) an interesting
database is 
interesting

Basically,
you need a new
data structure
to support
an interesting
database.

(1) Build a big
cavern to
echolocate all the
things in the
database so they
never disappear
ever again.

(2) Use your 
echolocation ladder
to allocate more
space to other
spaces in the
fields of interest
that are partially
invisible
and partially
non-invisible.

(3) Try
to understand
that your database
is not a master
database
or a 'main'
database and
the term is not
really accurate
since
'multidimensionality'
theory exists
and so that means
your small
database is
small.

*/

/*

All of this
is theoretical.

It is nice to
have a theoretical
foundation.

Theoretical
foundations
build nice models
for how to
echolocate yourself
forward
and backward
in time
and other
dimensions of interest
like social order
sorted
time sampled
events.

Like what if that
person existed.

Or what if that
person didn't exist.

And so new timelines
are arbitrarily
created all the
time at runtime
and without stop.

So this theory
is useful since
already people
possibly have
alternative
cultural views
that 'that person
possibly exists'
even if I don't
know about them.

*/

/*

Thanks for
reading the
theory.

But for now.
I am lazy.

I will work
on smaller things.

*/

/*

Basically,
we'll use
simple functions
for all the
database
ideas.

*/

/*

Create
Get
Update
Delete

*/
/*

Database
Event Listener
Permission Rule
Network Protocol
Scale Matrix Amana
Interactive Graph
Computer Program Manager

*/

/*

Thank You For
Reading.

*/







