
// Community Agreement Check

/*

Pseudo Code:

- add a method for allowing users to 'check' that their
  'universal basic income' is coming from the trusted party.

- a third party that sends a universal basic income
  is also of interest because individuals can send
  regular income to one another besides the default
  administrator service

- communicate that there is a type of agreement
  and that it can be validated to ensure
  that the income is in deed arriving from the
  correct facility and perhaps not a third party
  between the ascribed to facility

*/

/*

Pseudo Code Part II:

- Prevent third parties from tampering with the information
  that is being checked.

*/

/*

Function syntax is weird without first thinking.

- Cryptography is a nice way to possibly allow for
  things like 'distributed denial of service' integrity
  to our codebase.

- A centralized computer program architecture based on
  passwords from a single authority isn't necessarily
  'cryptography' worthy since for example the centralized
  computer program can have keys that aren't 'portable'
  to other environments

- Portable cryptographic keys are useful

*/

/*

An initial first implementation of this function
suggests there are various things to think about:

- Cryptographic keys

- Third Party Authenticators like 'Firebase'

- Password-Archival solutions like Email-Recoverable Passwords
  or possibly renamable to 'Email-Rewinding' since you are
  using a default contact service to 'rewind' your
  'permission access'

*/

/*

Two or 3 different types of data structures could emerge
from the previous note.

Fast forward recovery of each itemc ould be important depending
on the implementation strategy.

One data structure is possibly enough to cover a wide range
of usecases.

*/

/*

A portable 'rewindable' data structure is possibly an
item of interest. However, there may be 'time access'
constraints to implementing a usable one in practice.

class PortableCryptographyAuthenticationKey {
  cryptographicPublicKey: string = ''
  cryptographicPrivateKey: string = ''

  providerServiceId: string = '' // e.g. firebase-google, firebase-facebook, etc.
  providerAuthenticationId: string = ''
  providerAuthenticationInformationList: string[] = []

  cryptographyPublicKeyAlgorithmId: string = ''
  cryptographyPublicKeyCryptographyInformationList: string[] = []

  individualId: string = ''
  individualName: string = ''
  individualInformationList: string[] = []

  individualRecoverableInformationDirectoryList: string[] = []

  // Additional Information
  serviceResourceOriginalLocationUrl: string = ''
}

*/

/*

Once you have an authentication key, possibly that
is a way to determine that you are indeed speaking
with who you have been expecting to speak with.

I don't know if this is a really good method since
it is still interesting with possible caveats
like 'where is the default location' for which
to find this item of interest?

Those superfluous details are possibly superfluous.

I'm not really sure. You could always add it
just in case. I have no real idea if that is 
so called 'idempotent' -_- . . but it doesn't seem
like a bad idea.

. . 

The full path of the URL isn't necessarily a bad idea . . 

The problem with using this method is that

(1) it opens ways to possibly attack a network

(2) it makes portability strange and possibly
not consistent in various ideas like keeping
secrets.

(3) Portability means accessing a resource and
not necessarily tampering with where it was retrieved from.

. . 

If you have information that is in a distributed network
maybe you could possibly want an 'originator' id . . but
that is possibly a clue why programs would not
be very safe.

. . 

*/

/*

function getInformationOfAuthenticatorValidity () {
  // validate that the cryptographic public key is valid
  // validate that the provider authentication id is valid
}

*/

/*

function getInformationOfAuthenticatorValidity (authenticatorKeyReferenceId: string, message: any) {
  // the reference id presents
    // a location for discovering where
    // the authenticator key is
  // get the authenticator key
  // validate that the message was created
    // by the authenticator key
}

*/

/*

function getInformationOfAuthenticatorValidity (authenticatorKeyReferenceId: string, message: any) {
  // the reference id could present several places
    // to want to search for the authenticator key
    // - local memory address
    // - http service
    // - local file system
    // - miscellaneous-unknown-service
  //
  // A reference id needs to look like this:
    // place://place-id/?password='password'
    // example:
      // local-memory://globalVariableId/?password=''
      // - in local memory
      // - located with a globalVariableId
      // - no password
  //
  // place://place-id/?password='password'
    // this syntax collides with the existing http
    // syntax with
    // protocol://address/?queryParameter='parameter'
  //
  // A different unique, equivalent syntax could possibly
    // be useful
  //
  // :// . . is replaced with . . !!!
  // / . . is replaced with . . *%* 
  // ? . . is replaced with . . ^^^
  // = . . is replaced with ===
  // & . . is replaced with *&*
  //
  // Place could be:
    // (1) local-memory
    // (2) http
    // (3) file-system
  //
  // http!!!https://website.com/hello-world/place*%*^^^password==='password'*&*httpMethod='get'
  //
  // local-memory!!!globalVariableTable.placeId.placeId2*%*^^^password==='password'
  //
  // file-system!!!User/person/Desktop/place*%*another-idea*%*^^^password==='password'*&*anotherIdea==='hello'
  
  //
  // Is this item valid? true or false?
  // is there a date for when it will possibly not be valid?
  // how can I find out more about this item's validity

}

*/


import * as elliptic from 'elliptic'
import * as cryptoJsSha256Algorithm from 'crypto-js/sha256'
import firebase from 'firebase/compat/app'
import * as nodeJsFileSystem from 'fs'
import * as nodeJsPathModule from 'path'
import qrCodeReaderModule from 'jsqr'

const EdDSA = elliptic.eddsa

// - - - - - - - - - Variables

const globalVariableTable = {}


// - - - - - - - - - Data Structures

class PortableCryptographyAuthenticationKey {
  cryptographicPublicKey: string = ''
  cryptographicPrivateKey: string = ''

  providerServiceId: string = '' // e.g. firebase-authentication://google, firebase-authentication://facebook, json-web-token://standard, etc.
  providerAuthenticationId: string = ''
  providerAuthenticationInformationTable: any

  cryptographyPublicKeyAlgorithmId: string = ''
  cryptographyPublicKeyCryptographyInformationTable: any

  individualId: string = ''
  individualName: string = ''
  individualInformationTable: any

  individualRecoverableInformationDirectoryTable: any

  // Additional Information
  serviceResourceOriginalLocationUrl: string = ''
}

class PortableMessage {
  messageContent: string = ''

  messageCryptographicSignature: string = ''

  messageOriginalAuthorAuthenticationId: string = ''

  messageReplicatingAuthorAuthenticationIdTable: any

  cryptographicPublicKeyAlgorithmId: string = ''
  cryptographicPublicKeyCryptographyInformationTable: any

  generalArbitraryInformationTable: any
}

// - - - - - - - - - Constants

const ellipticEd25519KeyIdea = new EdDSA('ed25519')

// Protocol Type
const PLACE_ID_TYPE_GENERAL_STRING_ADDRESS_PROTOCOL = 'general-string-address-protocol'
const PLACE_ID_TYPE_LOCAL_MEMORY = 'local-memory'
const PLACE_ID_TYPE_HTTP = 'http'
const PLACE_ID_TYPE_FILE_SYSTEM = 'file-system'

// General Protocol
const PLACE_ID_PROTOCOL_SEPARATOR = '!!!'
const PLACE_ID_PATH_SEPARATOR = '*%*'
const PLACE_ID_QUERY_PARAMETER_SEPARATOR = '^^^'
const PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = '==='
const PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR = '*&*'
const PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR = '[]'

// Local Memory - Protocol
const PLACE_ID_LOCAL_MEMORY_PROTOCOL_SEPARATOR = '???'
const PLACE_ID_LOCAL_MEMORY_PATH_SEPARATOR = '.'
const PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_SEPARATOR = '???'
const PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = '???'
const PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_CONCATENATION_SEPARATOR = '???'
const PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_ARRAY_INDICATOR = '???'

// HTTP - Protocol
const PLACE_ID_HTTP_PROTOCOL_SEPARATOR = '://'
const PLACE_ID_HTTP_PATH_SEPARATOR = '/'
const PLACE_ID_HTTP_QUERY_PARAMETER_SEPARATOR = '?'
const PLACE_ID_HTTP_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = '='
const PLACE_ID_HTTP_QUERY_PARAMETER_CONCATENATION_SEPARATOR = '&'
const PLACE_ID_HTTP_QUERY_PARAMETER_ARRAY_INDICATOR = '[]'

// File System - Protocol
const PLACE_ID_FILE_SYSTEM_PROTOCOL_SEPARATOR = '???'
const PLACE_ID_FILE_SYSTEM_PATH_SEPARATOR = '/'
const PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_SEPARATOR = '???'
const PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = '???'
const PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_CONCATENATION_SEPARATOR = '???'
const PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_ARRAY_INDICATOR = '???'

// Default Values - For Protocol
const DEFAULT_PASSWORD_QUERY_PARAMETER = 'password'
const DEFAULT_HTTP_METHOD_QUERY_PARAMETER = 'httpMethod'
const DEFAULT_HTTP_RESPONSE_TEXT_PARSE_START_TOKEN_QUERY_PARAMETER = 'httpResponseTextParseStartToken'
const DEFAULT_HTTP_RESPONSE_TEXT_PARSE_END_TOKEN_QUERY_PARAMETER = 'httpResponseTextParseEndToken'

const DEFAULT_FILE_TEXT_PARSE_START_TOKEN_QUERY_PARAMETER = 'fileTextParseStartToken'
const DEFAULT_FILE_TEXT_PARSE_END_TOKEN_QUERY_PARAMETER = 'fileTextParseEndToken'
const DEFAULT_FILE_SYSTEM_PERMISSION_IS_NODEJS_FILE_SELECTION_ALLOWED_QUERY_PARAMETER = 'isNodeJsFileSelectionAllowed'
const DEFAULT_FILE_SYSTEM_PERMISSION_IS_WEB_BROWSER_FILE_SELECTION_ALLOWED_QUERY_PARAMETER = 'isWebBrowserFileSelectionAllowed'
const DEFAULT_FILE_SYSTEM_PERMISSION_IS_QR_CODE_FILE_SELECTION_ALLOWED_QUERY_PARAMETER = 'isQRCodeFileSelectionAllowed'

const DEFAULT_FILE_ENCODING_QUERY_PARAMETER = 'fileEncoding'

// Default Values - For Portable Message
const PORTABLE_MESSAGE_SPECIAL_CHARACTER_START_INDICATOR_FOR_THIS_PARTICULAR_PACKAGE = '?!?!?!?!' // Name: Big Question Indicator
const PORTABLE_MESSAGE_SPECIAL_CHARACTER_END_INDICATOR_FOR_THIS_PARTICULAR_PACKAGE = '!!!!!!!!!!' // Name: Queue Indicator

const DEFAULT_UNTAMPERED_WITH_CODEBASE_INDICATOR = 'unTamperedWithCodebaseIndicator'
const DEFAULT_EXPIRATION_DATE_INDICATOR = 'expirationDateIndicator'
const DEFAULT_GLOBAL_VARIABLE_TABLE_ID_INDICATOR = 'globalVariableTableIdIndicator'
const DEFAULT_UNEXPECTED_MESSAGE_CONTACT_INFORMATION_INDICATOR = 'unexpectedMessageContactInformationIndicator'
const DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE = 'mostImportantThingAboutThisMessage'
const DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE_2 = 'mostImportantThingAboutThisMessage2'
const DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE_3 = 'mostImportantThingAboutThisMessage3'
const DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE_4 = 'mostImportantThingAboutThisMessage4'

const VALIDITY_CHECK_ID_USER_CREATED = 'userCreatedMessage'
const VALIDITY_CHECK_ID_NOT_EXPIRED = 'notExpiredMessage'
const VALIDITY_CHECK_ID_NOT_TAMPERED_WITH = 'notTamperedWithMessage'
const VALIDITY_CHECK_ID_NOT_GOING_TO_BE_HARMFUL = 'notGoingToBeHarmfulMessage'
const VALIDITY_CHECK_ID_NOT_INTERESTING = 'notInterestingMessage'
const VALIDITY_CHECK_ID_NOT_GOING_TO_BE_SPONTANEOUS = 'notGoingToBeSpontaneousMessage'
const VALIDITY_CHECK_ID_NOT_GOING_TO_BE_IGNORANT_OF_POLITICS = 'notGoingToBeIgnorantOfPoliticsMessage'
const VALIDITY_CHECK_ID_NOT_GOING_TO_BE_YOUR_FAVORITE = 'notGoingToBeYourFavoriteMessage'
const VALIDITY_CHECK_ID_IS_EASY = 'isEasyMessage'

// - - - - - - - - - - Algorithms

async function createAuthenticatorKey (individualId: string, individualName: string, individualInformationTable: any, isCryptographicAuthenticatorKey: boolean, nameHere: boolean, providerServiceId: string, keepTrackOfThisKey: boolean, authenticatorKeyPermissionRuleTable: any): Promise<PortableCryptographyAuthenticationKey> {
  const authenticatorKey = new PortableCryptographyAuthenticationKey()

  return Promise.resolve(authenticatorKey)
}

async function createPortableMessage (messageContent: any, authenticatorKey: PortableCryptographyAuthenticationKey): Promise<PortableMessage> {
  return Promise.resolve(new PortableMessage())
}

async function getAuthenticatorKeyList (placeOfInterestId: string): Promise<PortableCryptographyAuthenticationKey[]> {
  return []
}

async function updateAuthenticatorKey (serviceResourceOriginalLocationUrl: string): Promise<PortableCryptographyAuthenticationKey> {
  return new PortableCryptographyAuthenticationKey()
}

async function deleteAuthenticatorKey (serviceResourceOriginalLocationUrl: string) {
  //
}

async function getInformationOfAuthenticatorValidity (authenticatorKeyReferenceId: string, message: PortableMessage, globalVariableTableId: string, validityCheckId: string, useBothCryptographyAndOriginalProvider?: boolean): Promise<{
  isValidMessage: boolean,
  dateMessageValidityExpires: Date,
  moreInformationAboutValidity: any
}> {
  
  const authenticatorKey = await getAuthenticatorKeyFromGlobalVariableTable(authenticatorKeyReferenceId, globalVariableTableId, 'asynchronous')

  // return early in case no Authenticator key is available
  if (!authenticatorKey) {
    return {
      isValidMessage: false,
      dateMessageValidityExpires: null,
      moreInformationAboutValidity: authenticatorKey
    }
  }

  // validate cryptographic public key created this message
  const isValidMessageFromCryptographicKey = await getIsValidMessageFromCryptographyProvider(authenticatorKey, message, validityCheckId)
  const moreInformationAboutValidity = authenticatorKey

  // validate that provider authentication id created this message
  const isValidMessageFromProviderAuthenticatorId = await getIsValidMessageFromFirebaseAuthenticationProvider(authenticatorKey, message, validityCheckId)
  const moreInformationAboutValidity2 = authenticatorKey

  // Check if the message is valid
  const isValidMessageForBothCryptographyAndOriginalProvider = (useBothCryptographyAndOriginalProvider && (isValidMessageFromCryptographicKey && isValidMessageFromProviderAuthenticatorId))
  const isValidMessageForEitherCryptographyOrOriginalProvider = isValidMessageFromCryptographicKey || isValidMessageFromProviderAuthenticatorId

  // Create a resultant message
  const resultMessage = {
    isValidMessage: isValidMessageForBothCryptographyAndOriginalProvider || isValidMessageForEitherCryptographyOrOriginalProvider,
    dateMessageValidityExpires: message.generalArbitraryInformationTable[DEFAULT_EXPIRATION_DATE_INDICATOR],
    moreInformationAboutValidity: moreInformationAboutValidity || moreInformationAboutValidity2
  }

  return resultMessage
}

async function getAuthenticatorKeyFromGlobalVariableTable (authenticatorKeyReferenceId: string, globalVariableTableId?: string, synchronousOrAsynchronousFunctionEvaluation?: string): Promise<PortableCryptographyAuthenticationKey> {
  
  const generalProtocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_GENERAL_STRING_ADDRESS_PROTOCOL, authenticatorKeyReferenceId)
  
  const generalProtocolPlace = generalProtocolMessage.protocolPlace
  const generalProtocolAddress = generalProtocolMessage.protocolAddress
  const generalProtocolPathList = generalProtocolMessage.protocolPathList
  const generalProtocolQueryParameterObjectDataStructure = generalProtocolMessage.protocolQueryParameterObjectDataStructure

  // get the authenticator key
  let authenticatorKey = null

  switch (generalProtocolPlace) {
    case PLACE_ID_TYPE_LOCAL_MEMORY:

      authenticatorKey = getAuthenticatorKeyFromLocalMemory(generalProtocolAddress, globalVariableTableId)
      
      break;
    case PLACE_ID_TYPE_HTTP:

      authenticatorKey = await getAuthenticatorKeyFromHTTP(generalProtocolAddress)

      break;
    case PLACE_ID_TYPE_FILE_SYSTEM:

      authenticatorKey = await getAuthenticatorKeyFromFileSystem(generalProtocolAddress, globalVariableTableId, synchronousOrAsynchronousFunctionEvaluation)

      break;
    default:
      break;
  }

  // return the authenticator key
  return authenticatorKey
}

function getAuthenticatorKeyFromLocalMemory (generalProtocolAddress: string, globalVariableTableId: string): PortableCryptographyAuthenticationKey {

  let authenticatorKey = null

  // Get the protocol information
  const protocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_LOCAL_MEMORY, generalProtocolAddress)

  const protocolPlace = protocolMessage.protocolPlace
  const protocolAddress = protocolMessage.protocolAddress
  const protocolPathList = protocolMessage.protocolPathList
  const protocolQueryParameterObjectDataStructure = protocolMessage.protocolQueryParameterObjectDataStructure

  // Search for the authenticator key from the global variable
  const localInstanceOfGlobalVariableTable = globalVariableTable || this[globalVariableTableId] || global[globalVariableTableId] || window[globalVariableTableId] || self[globalVariableTableId]
  
  authenticatorKey = getObjectValueByPropertyKeyNameList(localInstanceOfGlobalVariableTable, protocolPathList)
  authenticatorKey = authenticatorKey ? authenticatorKey : getObjectValueByPropertyKeyNameList(localInstanceOfGlobalVariableTable, protocolPathList.slice(1))

  return authenticatorKey
}

async function getAuthenticatorKeyFromHTTP (generalProtocolAddress: string): Promise<PortableCryptographyAuthenticationKey> {

  let authenticatorKey = null

  // Get the protocol information
  const protocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_HTTP, generalProtocolAddress)

  const protocolPlace = protocolMessage.protocolPlace
  const protocolAddress = protocolMessage.protocolAddress
  const protocolPathList = protocolMessage.protocolPathList
  const protocolQueryParameterObjectDataStructure = protocolMessage.protocolQueryParameterObjectDataStructure

  // Search for the authenticator key from the http address
  const httpResponseTextParseStartToken = protocolQueryParameterObjectDataStructure[DEFAULT_HTTP_RESPONSE_TEXT_PARSE_START_TOKEN_QUERY_PARAMETER]
  const httpResponseTextParseEndToken = protocolQueryParameterObjectDataStructure[DEFAULT_HTTP_RESPONSE_TEXT_PARSE_END_TOKEN_QUERY_PARAMETER]

  authenticatorKey = await fetch(generalProtocolAddress).then(response => response.json()).catch((errorMessage) => {
    return null
  })

  authenticatorKey = authenticatorKey ? authenticatorKey : await fetch(generalProtocolAddress).then(response => response.text()).catch((errorMessage) => {
    return null
  })
  
  // Using a unique start key like '<<<' could possibly help
  // locate an object
  // <data>, </data> could also be unique start and end
  // locators for objects in a string
  if (typeof authenticatorKey === 'string') {
    const authenticatorKeyStartIndex = authenticatorKey.indexOf(httpResponseTextParseStartToken)
    const authenticatorKeyEndIndex = authenticatorKey.indexOf(httpResponseTextParseEndToken)
    const authenticatorKeyObjectString = authenticatorKey.substring(authenticatorKeyStartIndex, authenticatorKeyEndIndex).trim()
    authenticatorKey = JSON.parse(authenticatorKeyObjectString)
  }

  return authenticatorKey
}

async function getAuthenticatorKeyFromFileSystem (generalProtocolAddress: string, globalVariableTableId: string, synchronousOrAsynchronousFunctionEvaluation?: string, qrCodeResultPlayBackCollectionFunction?: any, qrCodeStopConditionFunction?: any): Promise<PortableCryptographyAuthenticationKey> {

  // Get the protocol information
  const protocolMessage = getStringProtocolMessageEncoding(PLACE_ID_TYPE_FILE_SYSTEM, generalProtocolAddress)

  const protocolPlace = protocolMessage.protocolPlace
  const protocolAddress = protocolMessage.protocolAddress
  const protocolPathList = protocolMessage.protocolPathList
  const protocolQueryParameterObjectDataStructure = protocolMessage.protocolQueryParameterObjectDataStructure

  // Identify the type of file system / the type of file selection
  // being allowed

  const isNodeJsFileSelectionAllowed = protocolQueryParameterObjectDataStructure[DEFAULT_FILE_SYSTEM_PERMISSION_IS_NODEJS_FILE_SELECTION_ALLOWED_QUERY_PARAMETER]
  const isWebBrowserFileSelectionAllowed = protocolQueryParameterObjectDataStructure[DEFAULT_FILE_SYSTEM_PERMISSION_IS_WEB_BROWSER_FILE_SELECTION_ALLOWED_QUERY_PARAMETER]
  const isQRCodeFileSelectionAllowed = protocolQueryParameterObjectDataStructure[DEFAULT_FILE_SYSTEM_PERMISSION_IS_QR_CODE_FILE_SELECTION_ALLOWED_QUERY_PARAMETER]

  // File System Settings
  const fileEncoding = protocolQueryParameterObjectDataStructure[DEFAULT_FILE_ENCODING_QUERY_PARAMETER]

  // Search for the authenticator key from a file system file
  let authenticatorKeyObjectString = ''

  // Use the node.js file system

  if (!authenticatorKeyObjectString && isNodeJsFileSelectionAllowed) {
    let fileData = ''

    const directoryListItem1 = nodeJsPathModule.resolve(__dirname, globalVariableTableId)
    const directoryListItem2 = nodeJsPathModule.resolve(globalVariableTableId)

    // Be sure to have a diverse enough set of 'protocol addresses'
    // in case the file system is not able to find the
    // immediately requested directory name and path.
    //
    // For example, in a complex distributed network of computers
    // you may need to have a way to reverse search and index
    // the various other so called 'globalVariableTableIds'
    //
    const protocolAddressList = [
      nodeJsPathModule.resolve(directoryListItem1, protocolAddress),
      nodeJsPathModule.resolve(directoryListItem2, protocolAddress),
      nodeJsPathModule.resolve(globalVariableTableId, protocolAddress),
      protocolAddress
    ]

    // Iterate through each protocol address
    for (let i = 0; i < protocolAddressList.length; i++) {
      fileData = await readNodeJsFile(protocolAddressList[i], fileEncoding, synchronousOrAsynchronousFunctionEvaluation)
      if (fileData) {
        break
      }
    }
    
    authenticatorKeyObjectString = fileData
  }

  // Use the web browser file system

  if (!authenticatorKeyObjectString && isWebBrowserFileSelectionAllowed) {
    // Open the document input file type
    let htmlInputFileElement = document.createElement('input')
    htmlInputFileElement.type = 'file'
    let webBrowserFile = null
    let isWebBrowserFileSelected = false
    htmlInputFileElement.onchange = function (event) {
      webBrowserFile = (event as any).path[0].files[0]
      isWebBrowserFileSelected = true
    }
    htmlInputFileElement.click()

    // Wait until the web browser file is selected
    await startApplyingWaitWithAStopCondition(function () {
      return isWebBrowserFileSelected
    })

    // Read the document input file type
    let fileReader = new FileReader()
    let webBrowserFileContent = null
    let isWebBrowserFileContentRead = false
    fileReader.onload = function (event) {
      webBrowserFileContent = event.target.result
      isWebBrowserFileContentRead = true
    }
    fileReader.readAsText(webBrowserFile)

    // Wait until the file reader has read the file
    await startApplyingWaitWithAStopCondition(function () {
      return isWebBrowserFileContentRead
    })

    // Set the authenticator key object string
    authenticatorKeyObjectString = webBrowserFileContent
  }

  // Use QR Code File System

  if (!authenticatorKeyObjectString && isQRCodeFileSelectionAllowed) {
    let numberOfTemporaryQRCodeResults = 0

    const defaultQRCodeResultPlayBackCollectionFunction = (_videoResult: any, qrCodeTemporaryResult: any) => {
      if (qrCodeTemporaryResult) {
        numberOfTemporaryQRCodeResults += 1
      }
    }

    const defaultQRCodeStopConditionFunction = () => {
      return numberOfTemporaryQRCodeResults > 0
    }

    const qrCodeResultList = await getQRCodeValueFromCamera(
      null,
      qrCodeResultPlayBackCollectionFunction || defaultQRCodeResultPlayBackCollectionFunction,
      qrCodeStopConditionFunction || defaultQRCodeStopConditionFunction
    )

    authenticatorKeyObjectString = qrCodeResultList[0]
  }

  // Parse the file for an object like "{ 'hello': 'yes' }"
  // Specifically, a JSON object.
  //
  // Without start and stop tokens, we can suppose that
  // the entire file is a JSON object. The JSON object is
  // expected to contain the authenticatorKey data.

  const fileTextParseStartToken = protocolQueryParameterObjectDataStructure[DEFAULT_FILE_TEXT_PARSE_START_TOKEN_QUERY_PARAMETER]
  const fileTextParseEndToken = protocolQueryParameterObjectDataStructure[DEFAULT_FILE_TEXT_PARSE_END_TOKEN_QUERY_PARAMETER]

  const authenticatorKeyStartIndex = authenticatorKeyObjectString.indexOf(fileTextParseStartToken) || 0
  const authenticatorKeyEndIndex = authenticatorKeyObjectString.indexOf(fileTextParseEndToken) || authenticatorKeyObjectString.length
  authenticatorKeyObjectString = authenticatorKeyObjectString.substring(authenticatorKeyStartIndex, authenticatorKeyEndIndex).trim()
  const authenticatorKey = JSON.parse(authenticatorKeyObjectString)

  return authenticatorKey
}

async function readNodeJsFile (protocolAddress: string, fileEncoding?: string, synchronousOrAsynchronousFunctionEvaluation?: string): Promise<any> {
  let fileData = ''

  if (!synchronousOrAsynchronousFunctionEvaluation) {
    synchronousOrAsynchronousFunctionEvaluation = 'synchronous'
  }

  if (synchronousOrAsynchronousFunctionEvaluation === 'synchronous') {
    fileData = nodeJsFileSystem.readFileSync(protocolAddress, fileEncoding || 'utf8')
  } else if (synchronousOrAsynchronousFunctionEvaluation === 'asynchronous') {
    fileData = await new Promise((resolve, reject) => {
      nodeJsFileSystem.readFile(protocolAddress, fileEncoding || 'utf8', (errorMessage, data) => {
        if (errorMessage) {
          console.error(errorMessage)
          return
        }
        resolve(data)
      })
    })
  }

  return fileData
}

function getStringProtocolMessageEncoding (
  protocolType: string,
  protocolString: string,
  placeIdProtocolSeparator?: string,
  placeIdPathSeparator?: string,
  placeIdQueryParameterSeparator?: string,
  placeIdQueryParameterAssignmentSeparator?: string,
  placeIdQueryParameterConcatenationSeparator?: string,
  placeIdQueryParameterArrayIndicator?: string
): {
  protocolPlace: string,
  protocolAddress: string,
  protocolPathList: string[],
  protocolQueryParameterObjectDataStructure: Object
} {

  let LOCAL_PLACE_ID_PROTOCOL_SEPARATOR = ''
  let LOCAL_PLACE_ID_PATH_SEPARATOR = ''
  let LOCAL_PLACE_ID_QUERY_PARAMETER_SEPARATOR = ''
  let LOCAL_PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = ''
  let LOCAL_PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR = ''
  let LOCAL_PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR = ''

  switch (protocolType) {
    case PLACE_ID_TYPE_GENERAL_STRING_ADDRESS_PROTOCOL:
      LOCAL_PLACE_ID_PROTOCOL_SEPARATOR = PLACE_ID_PROTOCOL_SEPARATOR
      LOCAL_PLACE_ID_PATH_SEPARATOR = PLACE_ID_PATH_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_SEPARATOR = PLACE_ID_QUERY_PARAMETER_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR = PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR = PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR
      break;
    case PLACE_ID_TYPE_LOCAL_MEMORY:
      LOCAL_PLACE_ID_PROTOCOL_SEPARATOR = PLACE_ID_LOCAL_MEMORY_PROTOCOL_SEPARATOR
      LOCAL_PLACE_ID_PATH_SEPARATOR = PLACE_ID_LOCAL_MEMORY_PATH_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_SEPARATOR = PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR = PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_CONCATENATION_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR = PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_ARRAY_INDICATOR
      break;
    case PLACE_ID_TYPE_HTTP:
      LOCAL_PLACE_ID_PROTOCOL_SEPARATOR = PLACE_ID_HTTP_PROTOCOL_SEPARATOR
      LOCAL_PLACE_ID_PATH_SEPARATOR = PLACE_ID_HTTP_PATH_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_SEPARATOR = PLACE_ID_HTTP_QUERY_PARAMETER_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = PLACE_ID_HTTP_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR = PLACE_ID_HTTP_QUERY_PARAMETER_CONCATENATION_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR = PLACE_ID_HTTP_QUERY_PARAMETER_ARRAY_INDICATOR
      break;
    case PLACE_ID_TYPE_FILE_SYSTEM:
      LOCAL_PLACE_ID_PROTOCOL_SEPARATOR = PLACE_ID_FILE_SYSTEM_PROTOCOL_SEPARATOR
      LOCAL_PLACE_ID_PATH_SEPARATOR = PLACE_ID_FILE_SYSTEM_PATH_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_SEPARATOR = PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR = PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_CONCATENATION_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR = PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_ARRAY_INDICATOR
      break;
    default:
      LOCAL_PLACE_ID_PROTOCOL_SEPARATOR = placeIdProtocolSeparator || PLACE_ID_HTTP_PROTOCOL_SEPARATOR
      LOCAL_PLACE_ID_PATH_SEPARATOR = placeIdPathSeparator || PLACE_ID_HTTP_PATH_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_SEPARATOR = placeIdQueryParameterSeparator || PLACE_ID_HTTP_QUERY_PARAMETER_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR = placeIdQueryParameterAssignmentSeparator || PLACE_ID_HTTP_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR = placeIdQueryParameterConcatenationSeparator || PLACE_ID_HTTP_QUERY_PARAMETER_CONCATENATION_SEPARATOR
      LOCAL_PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR = placeIdQueryParameterArrayIndicator || PLACE_ID_HTTP_QUERY_PARAMETER_ARRAY_INDICATOR
      break;
  }

  // determine which place to look
  const protocolSeparatorList = protocolString.split(PLACE_ID_PROTOCOL_SEPARATOR)
  const protocolPathSeparatorList = protocolSeparatorList[1].split(PLACE_ID_PATH_SEPARATOR)
  const protocolQueryParameterSeparatorList = protocolPathSeparatorList[1].split(PLACE_ID_QUERY_PARAMETER_SEPARATOR)
  const protocolQueryParameterConcatenationSeparatorList = protocolQueryParameterSeparatorList[1].split(PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR)
  const protocolQueryParameterItemList: Array<{ itemId: string, itemValue: string }> = []
  const protocolQueryParameterObjectDataStructure = {}

  // prepare query parameter object data structure (part 1):

  for (let i = 0; i < protocolQueryParameterConcatenationSeparatorList.length; i++) {
    const protocolQueryParameterAssignmentSeparatorList = protocolQueryParameterConcatenationSeparatorList[i].split(PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR)
    const protocolQueryParameterLocalVariableItemId = protocolQueryParameterAssignmentSeparatorList[0]
    const protocolQueryParameterLocalVariableItemValue = protocolQueryParameterAssignmentSeparatorList[1]
    protocolQueryParameterItemList.push({
      itemId: protocolQueryParameterLocalVariableItemId,
      itemValue: protocolQueryParameterLocalVariableItemValue
    })
  }

  // prepare query parameter object data structure (part 2):

  for (let i = 0; i < protocolQueryParameterItemList.length; i++) {
    const queryParameterItem = protocolQueryParameterItemList[i]

    // check if the array indicator (ie. '[]') is at the end of the itemId
    const isQueryParameterArrayType = queryParameterItem.itemId.indexOf(PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR) === (queryParameterItem.itemId.length - PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR.length)

    // have a default item id and item value
    let itemId = queryParameterItem.itemId
    let itemValue: any|any[] = queryParameterItem.itemValue
  
    if (isQueryParameterArrayType) {
      itemId = queryParameterItem.itemId.slice(0, queryParameterItem.itemId.length - PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR.length)
      itemValue = protocolQueryParameterObjectDataStructure[itemId] || []

      if (Array.isArray(itemValue)) {
        itemValue.push(queryParameterItem.itemValue)
      } else {
        itemValue = [itemValue]
      }
    }

    protocolQueryParameterObjectDataStructure[itemId] = itemValue
  }

  // protocol defintions

  const protocolPlace = protocolSeparatorList[0]
  const protocolAddress = protocolPathSeparatorList[0]
  const protocolPathList = protocolPathSeparatorList.slice(1)

  return {
    protocolPlace,
    protocolAddress,
    protocolPathList,
    protocolQueryParameterObjectDataStructure
  }
}

function createProtocolEncoding (
  protocolNameId: string,
  protocolAddress: string,
  protocolPathList: string[],
  protocolQueryParameterObjectDataStructure: any,
  defaultProtocolType?: string,
  protocolPlaceIdSeparatorToken?: string,
  protocolPathSeparatorToken?: string,
  protocolQueryParameterSeparatorToken?: string,
  protocolQueryParameterAssignmentSeparatorToken?: string,
  protocolQueryParameterConcatenationSeparatorToken?: string,
  protocolQueryParameterArrayIndicatorToken?: string
): string {

  const defaultProtocolPlaceIdSeparatorToken = protocolPlaceIdSeparatorToken || PLACE_ID_PROTOCOL_SEPARATOR
  const defaultProtocolPathSeparatorToken = protocolPathSeparatorToken || PLACE_ID_PATH_SEPARATOR
  const defaultProtocolQueryParameterSeparatorToken = protocolQueryParameterSeparatorToken || PLACE_ID_QUERY_PARAMETER_SEPARATOR
  const defaultProtocolQueryParameterAssignmentSeparatorToken = protocolQueryParameterAssignmentSeparatorToken || PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR
  const defaultProtocolQueryParameterConcatenationSeparatorToken = protocolQueryParameterConcatenationSeparatorToken || PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR
  const defaultProtocolQueryParameterArrayIndicator = protocolQueryParameterArrayIndicatorToken || PLACE_ID_QUERY_PARAMETER_ARRAY_INDICATOR

  let queryParameterString = ''
  let numberOfQueryParameters = Object.keys(protocolQueryParameterObjectDataStructure).length

  let currentQueryParameterLoopIndex = 0
  for (let queryParameterId in protocolQueryParameterObjectDataStructure) {
    if (!protocolQueryParameterObjectDataStructure.hasOwnProperty(queryParameterId)) {
      continue
    }
    let queryParameterValue = protocolQueryParameterObjectDataStructure[queryParameterId]
    const isArray = Array.isArray(queryParameterValue)

    if (isArray) {
      for (let i = 0; i < queryParameterValue.length; i++) {
        queryParameterString += `${queryParameterId}${defaultProtocolQueryParameterArrayIndicator}${defaultProtocolQueryParameterAssignmentSeparatorToken}${queryParameterValue[i]}`
        queryParameterString += i < queryParameterValue.length ? defaultProtocolQueryParameterConcatenationSeparatorToken : ''
      }
    } else if (typeof queryParameterValue === 'object') {
      queryParameterValue = JSON.stringify(queryParameterValue)
    }

    queryParameterString += `${queryParameterId}${defaultProtocolQueryParameterAssignmentSeparatorToken}${queryParameterValue}`

    queryParameterString += currentQueryParameterLoopIndex < numberOfQueryParameters ? defaultProtocolQueryParameterConcatenationSeparatorToken : ''
    currentQueryParameterLoopIndex += 1
  }

  const resultString = `
  ${protocolNameId}
  ${defaultProtocolPlaceIdSeparatorToken}
  ${protocolAddress}
  ${protocolPathList.join(defaultProtocolPathSeparatorToken)}
  ${defaultProtocolPathSeparatorToken}
  ${protocolQueryParameterSeparatorToken}
  ${queryParameterString}
  `.replace(/\n/g, '')

  return resultString
}

// Cryptography Provider (Elliptic)

function getIsValidMessageFromCryptographyProvider (authenticatorKey: PortableCryptographyAuthenticationKey, message: PortableMessage, validityCheckId: string): boolean {

  let isValidMessage = false

  if (validityCheckId === VALIDITY_CHECK_ID_USER_CREATED) {

    switch (authenticatorKey.cryptographyPublicKeyAlgorithmId) {
      case 'ed25519': // https://www.npmjs.com/package/elliptic/v/6.5.4
        const messageContentOriginalString = JSON.stringify(message.messageContent)
        const ellipticKeyResource = ellipticEd25519KeyIdea.keyFromPublic(authenticatorKey.cryptographicPublicKey)

        // Is the original message string
        // signed by this public key signature?
        isValidMessage = ellipticKeyResource.verify(messageContentOriginalString, message.messageCryptographicSignature)
        break
      default:
        break
    }

  } else if (validityCheckId === VALIDITY_CHECK_ID_NOT_EXPIRED) {
    const messageExpirationDate = getUTCDate(message.generalArbitraryInformationTable[DEFAULT_EXPIRATION_DATE_INDICATOR])
    const millisecondExpirationDate = messageExpirationDate.getTime()
    const currentDate = getUTCDate()
    const millisecondCurrentDate = currentDate.getTime()

    // Has the expiration date been elapsed yet?
    isValidMessage = millisecondExpirationDate < millisecondCurrentDate
  } else if (validityCheckId === VALIDITY_CHECK_ID_NOT_TAMPERED_WITH) {
    const messageContinuationHash = message.generalArbitraryInformationTable[DEFAULT_UNTAMPERED_WITH_CODEBASE_INDICATOR]
    const messageContentOriginalString = JSON.stringify(message.messageContent)
    const messageRecreatedHash = cryptoJsSha256Algorithm(messageContentOriginalString)

    // Is the original continuation hash equal to 
    // the newly recorvered continuation hash?
    isValidMessage = messageContinuationHash === messageRecreatedHash
  }

  return isValidMessage
}

// Firebase Authentication Provider

function getIsValidMessageFromFirebaseAuthenticationProvider (authenticatorKey: PortableCryptographyAuthenticationKey, message: PortableMessage, validityCheckId: string): boolean {
  // Check that the original author id is in fact
  // being validated by the provider id for
  // that particular individual id

  authenticatorKey.individualId
  authenticatorKey.providerServiceId
  message.messageOriginalAuthorAuthenticationId

  // access a resource like http
  // access a resource like a local database
  // access a resource like a teleprompter

  // validityCheckId: expiration date

  message.generalArbitraryInformationTable

  return true
}

// Miscellaneous Object Functions

function getObjectValueByPropertyKeyNameList (object: any, propertyKeyNameList: (number | string)[]): any {
  if (!object) {
    return undefined
  }
  if (propertyKeyNameList.length === 0) {
    return object
  }
  if (!object[propertyKeyNameList[0]]) {
    return undefined
  }
  if (propertyKeyNameList.length === 1) {
    return object[propertyKeyNameList[0]]
  }
  return getObjectValueByPropertyKeyNameList(object[propertyKeyNameList[0]], propertyKeyNameList.slice(1))
}

// Miscellaneous Time Function

async function startApplyingWaitWithAStopCondition (conditionToStopWaitingFunction: () => boolean, millisecondDelayToCheckForNextStopCondition?: number) {
  let waitingTimerId = null
  return new Promise((resolve, reject) => {
    waitingTimerId = setInterval(function () {
      const isStopConditionSatisfied = conditionToStopWaitingFunction()

      if (!isStopConditionSatisfied) {
        return
      }

      clearInterval(waitingTimerId)

      resolve(isStopConditionSatisfied)
    }, millisecondDelayToCheckForNextStopCondition || 100)
  })
}

async function startApplyingWaitForMillisecondDelay (millisecondDelay: number): Promise<any> {

  const promise = new Promise((resolve) => {
    setTimeout(resolve, millisecondDelay)
  })

  return promise
}

async function asynchronousFunctionEvaluation (functionValue: any, millisecondDelay?: number): Promise<any> {
  if (window && window.requestAnimationFrame) {
    if (millisecondDelay) {
      await startApplyingWaitForMillisecondDelay(millisecondDelay)
    }
    return requestAnimationFrame(functionValue)
  } else if (!window.requestAnimationFrame) {
    return (async () => {
      let result = null
      setTimeout(async () => {
        result = await functionValue()
      }, 0)
      return result
    })()
  }
}

function getUTCDate (calendarDate?: Date) {
  let establishedDate = calendarDate || new Date()
  establishedDate = new Date(establishedDate.getUTCFullYear(), establishedDate.getUTCMonth(), establishedDate.getUTCDate(), establishedDate.getUTCHours(), establishedDate.getUTCMinutes(), establishedDate.getUTCSeconds())
  return establishedDate
}

// Miscellaneous QR Code Reader Function

async function getQRCodeValueFromCamera (_cameraAddress: string, resultPlaybackFunction: (videoResult: any, qrCodeTemporaryResult: any) => void, videoStopConditionFunction: () => boolean): Promise<string[]> {

  let qrCodeResultList = []
  let videoResult = document.createElement('video')
  let htmlCanvasElement = document.getElementById('canvas') as HTMLCanvasElement
  let htmlCanvasElementContext = htmlCanvasElement.getContext('2d')
  const defaultQRCodeBorderHighlightColor = 'FF3B58'
  const defaultVideoFrameRequestMillisecondDelay = 0

  const videoStreamResult = await navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } })

  videoResult.srcObject = videoStreamResult
  videoResult.setAttribute('playsinline', 'true') // required to tell iOS safari we don't want fullscreen
  videoResult.play()

  let isQRCodeVideoSearchStopped = false

  const intervalId = setInterval(function () {
    const qrCodeResult = getVideoQRCodeInformationAndUpdateVideoWithRectangleBorder(htmlCanvasElement, videoResult, defaultQRCodeBorderHighlightColor)

    // Add the qr code result to the list
    if (qrCodeResult) {
      qrCodeResultList.push(qrCodeResult)
    }

    // Respond with a video playback result and temporary qr code
    if (resultPlaybackFunction) {
      resultPlaybackFunction(videoResult, qrCodeResult)
    }

    // Stop the video
    const isVideoStopConditionTrue = videoStopConditionFunction()

    if (isVideoStopConditionTrue) {
      clearInterval(intervalId)
      videoResult.pause()
      videoResult.removeAttribute('src') // empty source
      videoResult.load()

      isQRCodeVideoSearchStopped = true
    }
  }, defaultVideoFrameRequestMillisecondDelay)

  // Wait until the video has been stopped by the user
  await startApplyingWaitWithAStopCondition(() => isQRCodeVideoSearchStopped)

  return qrCodeResultList
}

function getVideoQRCodeInformationAndUpdateVideoWithRectangleBorder (htmlCanvasElement: HTMLCanvasElement, htmlVideoElement: any, borderColor: string) {
  htmlCanvasElement.height = htmlVideoElement.videoHeight
  htmlCanvasElement.width = htmlVideoElement.videoWidth

  let htmlCanvasElementContext = htmlCanvasElement.getContext('2d')

  htmlCanvasElementContext.drawImage(htmlVideoElement, 0, 0, htmlCanvasElement.width, htmlCanvasElement.height)
  let imageData = htmlCanvasElementContext.getImageData(0, 0, htmlCanvasElement.width, htmlCanvasElement.height)

  let qrCodeResult = qrCodeReaderModule(imageData.data, imageData.width, imageData.height, {
    inversionAttempts: 'dontInvert',
  })

  if (qrCodeResult) {
    const borderLineList = [{
      startPoint: qrCodeResult.location.topLeftCorner,
      endPoint: qrCodeResult.location.topRightCorner
    }, {
      startPoint: qrCodeResult.location.topRightCorner,
      endPoint: qrCodeResult.location.bottomRightCorner
    }, {
      startPoint: qrCodeResult.location.bottomRightCorner,
      endPoint: qrCodeResult.location.bottomLeftCorner
    }, {
      startPoint: qrCodeResult.location.bottomLeftCorner,
      endPoint: qrCodeResult.location.topLeftCorner
    }]

    updateQRCodeCameraFrameWithRectangleBorder(htmlCanvasElement, borderColor, borderLineList)
  }

  return qrCodeResult.data
}

function updateQRCodeCameraFrameWithRectangleBorder (htmlCanvas: HTMLCanvasElement, borderColor: string, borderLineList: Array<{ startPoint: any, endPoint: any }>) {
  for (let i = 0 ; i < borderLineList.length; i++) {
    const startPoint = borderLineList[i].startPoint
    const endPoint = borderLineList[i].endPoint

    let htmlCanvasElementContext = htmlCanvas.getContext('2d')
  
    htmlCanvasElementContext.beginPath()
    htmlCanvasElementContext.moveTo(startPoint.x, startPoint.y)
    htmlCanvasElementContext.lineTo(endPoint.x, endPoint.y)
    htmlCanvasElementContext.lineWidth = 4
    htmlCanvasElementContext.strokeStyle = borderColor
    htmlCanvasElementContext.stroke()
  }
}


export {

  // - - - - - - - - - - Variables

  globalVariableTable,

  // - - - - - - - - - - Data Structures

  PortableCryptographyAuthenticationKey,
  PortableMessage,

  // - - - - - - - - - - Constants

  PLACE_ID_TYPE_GENERAL_STRING_ADDRESS_PROTOCOL,
  PLACE_ID_TYPE_LOCAL_MEMORY,
  PLACE_ID_TYPE_HTTP,
  PLACE_ID_TYPE_FILE_SYSTEM,

  // General Protocol
  PLACE_ID_PROTOCOL_SEPARATOR,
  PLACE_ID_PATH_SEPARATOR,
  PLACE_ID_QUERY_PARAMETER_SEPARATOR,
  PLACE_ID_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR,
  PLACE_ID_QUERY_PARAMETER_CONCATENATION_SEPARATOR,

  // Local Memory - Protocol
  PLACE_ID_LOCAL_MEMORY_PROTOCOL_SEPARATOR,
  PLACE_ID_LOCAL_MEMORY_PATH_SEPARATOR,
  PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_SEPARATOR,
  PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR,
  PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_CONCATENATION_SEPARATOR,
  PLACE_ID_LOCAL_MEMORY_QUERY_PARAMETER_ARRAY_INDICATOR,

  // HTTP - Protocol
  PLACE_ID_HTTP_PROTOCOL_SEPARATOR,
  PLACE_ID_HTTP_PATH_SEPARATOR,
  PLACE_ID_HTTP_QUERY_PARAMETER_SEPARATOR,
  PLACE_ID_HTTP_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR,
  PLACE_ID_HTTP_QUERY_PARAMETER_CONCATENATION_SEPARATOR,
  PLACE_ID_HTTP_QUERY_PARAMETER_ARRAY_INDICATOR,

  // File System - Protocol
  PLACE_ID_FILE_SYSTEM_PROTOCOL_SEPARATOR,
  PLACE_ID_FILE_SYSTEM_PATH_SEPARATOR,
  PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_SEPARATOR,
  PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_ASSIGNMENT_SEPARATOR,
  PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_CONCATENATION_SEPARATOR,
  PLACE_ID_FILE_SYSTEM_QUERY_PARAMETER_ARRAY_INDICATOR,

  // Default Values - For Protocol
  DEFAULT_PASSWORD_QUERY_PARAMETER,
  DEFAULT_HTTP_METHOD_QUERY_PARAMETER,
  DEFAULT_HTTP_RESPONSE_TEXT_PARSE_START_TOKEN_QUERY_PARAMETER,
  DEFAULT_HTTP_RESPONSE_TEXT_PARSE_END_TOKEN_QUERY_PARAMETER,
  DEFAULT_FILE_TEXT_PARSE_START_TOKEN_QUERY_PARAMETER,
  DEFAULT_FILE_TEXT_PARSE_END_TOKEN_QUERY_PARAMETER,
  DEFAULT_FILE_SYSTEM_PERMISSION_IS_NODEJS_FILE_SELECTION_ALLOWED_QUERY_PARAMETER,
  DEFAULT_FILE_SYSTEM_PERMISSION_IS_WEB_BROWSER_FILE_SELECTION_ALLOWED_QUERY_PARAMETER,
  DEFAULT_FILE_SYSTEM_PERMISSION_IS_QR_CODE_FILE_SELECTION_ALLOWED_QUERY_PARAMETER,
  DEFAULT_FILE_ENCODING_QUERY_PARAMETER,

  // Default Values - Portable Message
  PORTABLE_MESSAGE_SPECIAL_CHARACTER_START_INDICATOR_FOR_THIS_PARTICULAR_PACKAGE,
  PORTABLE_MESSAGE_SPECIAL_CHARACTER_END_INDICATOR_FOR_THIS_PARTICULAR_PACKAGE,
  DEFAULT_UNTAMPERED_WITH_CODEBASE_INDICATOR,
  DEFAULT_EXPIRATION_DATE_INDICATOR,
  DEFAULT_GLOBAL_VARIABLE_TABLE_ID_INDICATOR,
  DEFAULT_UNEXPECTED_MESSAGE_CONTACT_INFORMATION_INDICATOR,
  DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE,
  DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE_2,
  DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE_3,
  DEFAULT_MOST_IMPORTANT_THING_ABOUT_THIS_MESSAGE_4,

  VALIDITY_CHECK_ID_USER_CREATED,
  VALIDITY_CHECK_ID_NOT_EXPIRED,
  VALIDITY_CHECK_ID_NOT_TAMPERED_WITH,
  VALIDITY_CHECK_ID_NOT_GOING_TO_BE_HARMFUL,
  VALIDITY_CHECK_ID_NOT_INTERESTING,
  VALIDITY_CHECK_ID_NOT_GOING_TO_BE_SPONTANEOUS,
  VALIDITY_CHECK_ID_NOT_GOING_TO_BE_IGNORANT_OF_POLITICS,
  VALIDITY_CHECK_ID_NOT_GOING_TO_BE_YOUR_FAVORITE,
  VALIDITY_CHECK_ID_IS_EASY,

  // - - - - - - - - - - Algorithms

  getInformationOfAuthenticatorValidity,
  getAuthenticatorKeyFromGlobalVariableTable,

  getAuthenticatorKeyFromLocalMemory,
  getAuthenticatorKeyFromHTTP,
  getAuthenticatorKeyFromFileSystem,

  readNodeJsFile,
  getStringProtocolMessageEncoding,
  createProtocolEncoding,

  // Cryptography Authentication Provider (Elliptic)
  getIsValidMessageFromCryptographyProvider,
  // getValidityExpirationDateFromCryptographyProvider,

  // Firebase Authentication Provider
  getIsValidMessageFromFirebaseAuthenticationProvider,
  // getValidityExpirationDateFromFirebaseAuthenticationProvider,

  getObjectValueByPropertyKeyNameList,
  startApplyingWaitWithAStopCondition,
  getUTCDate
}



