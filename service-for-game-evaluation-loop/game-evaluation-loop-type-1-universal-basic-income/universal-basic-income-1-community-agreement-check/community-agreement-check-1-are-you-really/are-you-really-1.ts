
/*

The idea with this
page of the
documentation is to

(1) ensure

(2) facilitate

*/

/*

Ensure there is
an authenticator

Facilitate the
creation of an
authenticator

*/

// - - - - - - - - - Data Structures

class PortableCryptographyAuthenticationKey {
  cryptographicPublicKey: string = ''
  cryptographicPrivateKey: string = ''

  providerServiceId: string = '' // e.g. firebase-authentication://google, firebase-authentication://facebook, json-web-token://standard, etc.
  providerAuthenticationId: string = ''
  providerAuthenticationInformationTable: any

  cryptographyPublicKeyAlgorithmId: string = ''
  cryptographyPublicKeyCryptographyInformationTable: any

  individualId: string = ''
  individualName: string = ''
  individualInformationTable: any

  individualRecoverableInformationDirectoryTable: any

  // Additional Information
  serviceResourceOriginalLocationUrl: string = ''
}


// - - - - - - - - - - Algorithms

async function createAuthenticatorKey (inputOptions: {
  individualId: string,
  individualName: string,
  individualInformationTable: any,
  isCryptographicAuthenticatorKey: boolean,
  nameHere: boolean,
  providerServiceId: string,
  keepTrackOfThisKey: boolean,
  authenticatorKeyPermissionRuleTable: any
}): Promise<PortableCryptographyAuthenticationKey> {

  const authenticatorKey = new PortableCryptographyAuthenticationKey()

  authenticatorKey.individualId = inputOptions.individualId
  authenticatorKey.individualName = inputOptions.individualName
  authenticatorKey.individualInformationTable = inputOptions.individualInformationTable

  authenticatorKey.providerServiceId = inputOptions.providerServiceId


  return Promise.resolve(authenticatorKey)
}


export {
  PortableCryptographyAuthenticationKey,
  createAuthenticatorKey
}
